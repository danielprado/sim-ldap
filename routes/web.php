<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\CsrfCookieController;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/home', function () {
    return view('welcome');
})->name('home');

Route::get('/verification/verify', function () {
    return view('verification');
});

Route::get('/csfr-cookie', [ CsrfCookieController::class, 'show' ])
            ->middleware('web')
            ->name('csfr.cookie');
