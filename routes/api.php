<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\NotificationController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Citizen\EpsController;
use App\Http\Controllers\Citizen\FileController;
use App\Http\Controllers\Citizen\FileTypeController;
use App\Http\Controllers\Citizen\ModalityController;
use App\Http\Controllers\Citizen\ObservationController;
use App\Http\Controllers\Citizen\OrganizationController;
use App\Http\Controllers\Citizen\ParticipantController;
use App\Http\Controllers\Citizen\PrivacyController;
use App\Http\Controllers\Citizen\ProcedureController;
use App\Http\Controllers\Citizen\ProfileController;
use App\Http\Controllers\Citizen\ProfileScheduleController;
use App\Http\Controllers\Citizen\ProgramController as ProgramControllerAlias;
use App\Http\Controllers\Citizen\ScheduleController;
use App\Http\Controllers\Citizen\StageController;
use App\Http\Controllers\Citizen\StatusProfileController;
use App\Http\Controllers\Citizen\TeamController;
use App\Http\Controllers\Orfeo\OrfeoController;
use App\Http\Controllers\Parks\LocationController;
use App\Http\Controllers\Parks\ParkController;
use App\Http\Controllers\Parks\ParkControllerAppInfo;
use App\Http\Controllers\Parks\ParkEndowmentController;
use App\Http\Controllers\Parks\ScaleController;
use App\Http\Controllers\Payments\PaymentAppInfoController;
use App\Http\Controllers\Payments\PaymentController;
use App\Http\Controllers\Recreation\CalendarController;
use App\Http\Controllers\SitemapController;
use App\Http\Controllers\SocialManagement\ProgramController;
use App\Http\Controllers\Parks\SwimmingPoolController;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('oauth/token', [AccessTokenController::class, 'issueToken']);

Route::get('sitemap.xml', [SitemapController::class, 'index']);

Route::prefix('api')->group( function () {
    Route::prefix('orfeo')->group( function () {
        Route::post("attachments", [OrfeoController::class, "getAttachment"]);
        Route::post("history", [OrfeoController::class, "getHistory"]);
        Route::get("sports", [OrfeoController::class, "sports"]);
        Route::get("autocomplete", [OrfeoController::class, "show"]);
        // Route::get("autocomplete-school", [OrfeoController::class, "showSchool"]);
    });
    Route::get("terms-and-conditions", [PrivacyController::class, "terms"]);
    Route::prefix('auth')->group( function () {
        Route::get('social', function () {
            return Socialite::driver('google')->stateless()->redirect();
        });
        Route::get('social/google', function () {
            $base = config('app.env') != 'production'
                ? env('APP_FRONTEND_URL_DEV')
                : env('APP_FRONTEND_URL_PROD');
            $token = Socialite::driver('google')->stateless()->user()->token;
            return redirect()->away(
                "$base/usuarios/iniciar-sesion?token=$token"
            );
        });
    });
    Route::post('login', [ LoginController::class, 'login' ])->name('passport.login');
    Route::post('register', [ RegisterController::class, 'register' ])->name('passport.register');
    Route::post('register-entity', [ RegisterController::class, 'registerEntity' ])->name('passport.registerEntity');
    Route::post('pre-register', [VerificationController::class, 'preRegister'])->name("verification.pre-register");
    Route::post('pre-register-entity', [VerificationController::class, 'preRegisterEntity'])->name("verification.pre-register-entity");
    Route::post('pre-register-entity-init-validation', [VerificationController::class, 'preRegisterInitValidation'])->name("init_validation.pre-register-entity");
    Route::get('register-status', [StatusProfileController::class, 'index']);
    Route::get('instructive', function () {
        return response()->json([
            "data" => collect(\Illuminate\Support\Facades\Storage::disk("public")->files("instructive", true))
                ->map(function ($file) {
                    $fname = explode("/", $file);
                    return [
                        "icon" => "mdi-file",
                        "target" => "_blank",
                        "href"   => config("app.url")."/storage/".$file,
                        "text"  => end($fname)
                    ];
                })
        ]);
    });
    Route::post('orfeo-clubes', [OrfeoController::class, 'index']);
    Route::post('orfeo-schools', [OrfeoController::class, 'storeSchool']);
    Route::prefix('password')->group( function () {
        Route::post('forgot', [ ForgotPasswordController::class, 'sendResetLinkEmail' ])->name('password.forgot');
        Route::post('reset', [ ResetPasswordController::class, 'reset' ])->name('password.reset');
        Route::post('change', [ ResetPasswordController::class, 'changePassword' ])
            ->middleware('auth:api')
            ->name('password.change');
    });

    Route::prefix('email')->group( function () {
        Route::post('verify/{id}', [VerificationController::class, 'verify'])->name('verification.verify');
        Route::post('resend', [VerificationController::class, 'resend'])->name('verification.resend');
        Route::get('manual/{user}', [VerificationController::class, 'manualVerificationCode'])->name('verification.manual');
        Route::post('verify', [VerificationController::class, 'show'])->name('verification.notice');
        Route::post('change', [VerificationController::class, 'changeEmail'])
            ->middleware('auth:api')
            ->name('verification.change');
        Route::post('confirm-change', [VerificationController::class, 'confirmEmailChange'])
            ->middleware('auth:api')
            ->name('verification.confirm-change');
        Route::post('resend-confirm-change', [VerificationController::class, 'resendNewEmailConfirmation'])
            ->middleware('auth:api')
            ->name('verification.resend-confirm-change');
        Route::post('pre-register-verification', [VerificationController::class, 'preRegisterValidation'])
            ->name("verification.pre-register-verification");
        Route::post('pre-register-entity-verification', [VerificationController::class, 'preRegisterEntityValidation'])
            ->name("verification.pre-register-entity-verification");
    });

    // Parks
    Route::resource('parks.endowments', ParkEndowmentController::class, [
        'only' =>  ["index", "show"],
        'parameters' =>  ['parks' => 'park', "endowments" => "endowment"],
    ]);
    Route::prefix('parks')->group( function () {
        Route::get('/', [ParkController::class, 'finder']);
        Route::get('{park}/details', [ParkController::class, 'show']);
        Route::get('synthetic-fields', [ParkController::class, 'synthetic']);
        Route::get('swimming-pools', [ParkController::class, 'swimmingPools']);
        Route::get('grills', [ParkController::class, 'grill']);
        Route::get('gyms', [ParkController::class, 'gym']);
        Route::get('tennis-courts', [ParkController::class, 'tennisCourt']);
        Route::get('natural-soccer-fields', [ParkController::class, 'naturalSoccerField']);


        Route::get('schedules/{endowment}', [ParkController::class, 'schedule']);
        Route::get('diagrams', [ParkController::class, 'diagrams']);

        Route::get('{park}/economic-use', [ParkController::class, 'economic']);
        Route::get('{park}/sectors', [ParkController::class, 'sectors']);
        Route::get('{park}/equipment/{equipment}', [ParkController::class, 'fields']);

        Route::get('swimming-pool-lanes', [SwimmingPoolController::class, 'getLanesByPool']);

        //routes APP Vive IDRD
        Route::get('app-info/synthetic-fields', [ParkControllerAppInfo::class, 'synthetic']);
        Route::get('app-info/swimming-pools', [ParkControllerAppInfo::class, 'swimmingPools']);
        Route::get('app-info/grills', [ParkControllerAppInfo::class, 'grill']);
        Route::get('app-info/gyms', [ParkControllerAppInfo::class, 'gym']);
        Route::get('app-info/tennis-courts', [ParkControllerAppInfo::class, 'tennisCourt']);
        Route::get('app-info/natural-soccer-fields', [ParkControllerAppInfo::class, 'naturalSoccerField']);
    });
    Route::get('localities', [LocationController::class, 'index']);
    Route::get('scales', [ScaleController::class, 'index']);

    // Recreation
    Route::prefix('recreation')->group( function () {
        Route::get('{park}/calendar', [CalendarController::class, 'index']);
        Route::get('others', [CalendarController::class, 'others']);
    });
    // Social Management
    Route::prefix('social-management')->group( function () {
        Route::get('{park}/history', [ProgramController::class, 'index']);
    });
    // Citizen Portal
    Route::prefix('citizen-portal')->group( function () {
        Route::get('programs', [ProgramControllerAlias::class, 'index']);
        Route::get('stages', [StageController::class, 'index']);
        Route::get('public-schedules', [ScheduleController::class, 'index']);
        Route::get('public-schedules/{schedule}', [ScheduleController::class, 'show']);
        Route::get('schedules/categories', [ScheduleController::class, 'chips']);

        //routes APP Vive IDRD
        Route::post('app-info/{endowment}/payment', [PaymentAppInfoController::class, 'payment']);
        Route::post('app-info/validateFreePayment', [PaymentAppInfoController::class, 'validateFreePayment']);
    });

    // Register Data
    Route::get('eps', [EpsController::class, 'index']);

    // Organizations
    Route::resource('organizations', OrganizationController::class, [
        'only'     =>  ['index', 'show', 'store', 'update'],
        'parameters' =>  ['organizations' => 'organization']
    ]);

    Route::get('organization', [OrganizationController::class, 'getOrganization']);

    Route::get("profiles/search/{search?}", [ProfileController::class, "search"]);
});

Route::middleware('auth:api')->prefix('api')->group( function () {
    Route::middleware('verified')->group(function () {
        Route::get("informed-consent", [PrivacyController::class, "consents"]);
        Route::get("profiles/list", [ProfileController::class, "allProfiles"]);
        Route::get("profiles/verified", [ProfileController::class, "allProfilesVerified"]);
        Route::get("profiles/subscriptions", [ProfileScheduleController::class, "activities"]);
        Route::get("profiles/subscriptions/{schedule}", [ProfileScheduleController::class, "show"]);
        Route::match(["post", "delete"],"profiles/subscriptions/{schedule}", [ProfileScheduleController::class, "unsubscribe"]);
        Route::resource('profiles', ProfileController::class, [
            'only'     =>  ['index', 'show', 'store', 'update', 'destroy'],
            'parameters' =>  ['profiles' => 'profile']
        ]);
        Route::post('profiles/{profile}/observations', [ObservationController::class, 'markAllAsRead']);
        Route::resource('profiles.observations', ObservationController::class, [
            'only'     =>  ['index', 'show'],
            'parameters' =>  ['profiles' => 'profile', 'observations' => 'observation']
        ]);
        Route::get('modalities', [ModalityController::class, 'index']);
        Route::resource('teams', TeamController::class, [
            'only'     =>  ['index', 'show', 'store', 'update', 'destroy'],
            'parameters' =>  ['teams' => 'team']
        ]);
        Route::get('participants-pending', [TeamController::class, 'pending']);
        Route::post('team/{team}/schedule/{schedule}', [TeamController::class, 'schedule']);
        Route::get('participants-roles', [TeamController::class, 'roles']);
        Route::get('search-participants', [TeamController::class, 'search']);
        Route::post('team-register', [TeamController::class, 'register']);
        Route::resource('teams.participants', ParticipantController::class, [
            'only'     =>  ['index', 'store', 'update', 'destroy'],
            'parameters' =>  ['teams' => 'team', 'participants' => 'participant']
        ]);
        Route::resource('profiles.files', FileController::class, [
            'only'     =>  ['index', 'show', 'store'],
            'parameters' =>  ['profiles' => 'profile', 'files' => 'file']
        ]);
        Route::get('files', [FileTypeController::class, 'index']);
        Route::resource('procedures', ProcedureController::class, [
            'only'     =>  ['index', 'show', 'store'],
            'parameters' =>  ['procedures' => 'procedure']
        ]);

        // Procedures
        Route::post('orfeo-clubes', [OrfeoController::class, 'index']);
        Route::post('orfeo-schools', [OrfeoController::class, 'storeSchool']);

        // Citizen Portal
        Route::prefix('citizen-portal')->group( function () {
            Route::get('payments', [PaymentController::class, 'index']);
            Route::put('payments/activities/{schedule}/{reference}', [PaymentController::class, 'activityPayment']);
            Route::get('payments/autocomplete', [PaymentController::class, 'pseData']);
            Route::get('payments/export', [PaymentController::class, 'export']);
            Route::get('bookings', [PaymentController::class, 'bookings']);
            Route::get('pending-to-pay', [PaymentController::class, 'pendingToPay']);
        });
        /*
        Route::resource('profiles.schedules', ProfileScheduleController::class, [
            'only'     =>  ['index', 'update'],
            'parameters' =>  ['schedules' => 'schedule', 'profiles' => 'profile']
        ]);
        */
        Route::get('profiles/{profile}/schedules', [ProfileScheduleController::class, 'index']);
        Route::get('payments/status/{payment}/schedule', [ProfileScheduleController::class, 'approveStatusInPayment']);
        Route::match(["post", "put", "patch"],'profiles/{profile}/schedules/{schedule}', [ProfileScheduleController::class, 'update']);
        Route::post('parks/schedules/{endowment}/payment', [PaymentController::class, 'payment']);
        Route::put('parks/schedules/freepayment/{id}', [PaymentController::class, 'freePayment']);
        Route::put('parks/schedules/validateFreePayment/{id}', [PaymentController::class, 'validateFreePayment']);

    });

    Route::get('notifications', [NotificationController::class, 'index']);
    Route::post('notifications', [NotificationController::class, 'markAllAsRead']);
    Route::get('notifications/{id}', [NotificationController::class, 'markAsRead']);
    Route::delete('notifications/{id}', [NotificationController::class, 'destroy']);
    Route::post('logout', [LoginController::class, 'logout']);
    Route::post('logout-all-devices', [LoginController::class, 'logoutAllDevices']);
    Route::get('/user', [LoginController::class, 'user']);
});
