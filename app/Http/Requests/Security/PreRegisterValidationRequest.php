<?php

namespace App\Http\Requests\Security;

use Illuminate\Foundation\Http\FormRequest;

class PreRegisterValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users', 'exists:pre_register'],
            'code' => ['required', 'numeric', 'digits:6'],
        ];
    }
}
