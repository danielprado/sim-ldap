<?php

namespace App\Http\Requests\Security;

use App\Entities\CitizenPortal\Profile;
use Illuminate\Foundation\Http\FormRequest;

class PreRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $profile = new Profile();
        return [
            'document_type_id'  => "required|numeric|exists:mysql_sim.tipo_documento,Id_TipoDocumento",
            'document'  => "required|numeric|unique:{$profile->getConnectionName()}.{$profile->getTable()},document|digits_between:3,20|unique:pre_register",
            'name'  => "required|string|between:2,90",
            's_name'    => "nullable|string|between:2,90",
            'surname'   => "required|string|between:2,90",
            's_surname' => "nullable|string|between:2,90",
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users', 'unique:pre_register'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'document_type_id'  => "tipo de documento",
            'document'  => "número de documento",
            'name'  => "primer nombre",
            's_name'    => "segundo nombre",
            'surname'   => "primer apellido",
            's_surname' => "segundo apellido",
            'email' => 'correo electrónico',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'document.unique'  => "El número de documento ya se encuentra registrado o está en proceso de verificación",
            'email.unique' => 'El correo electrónico ya se encuentra registrado o está en proceso de verificación',
        ];
    }
}
