<?php

namespace App\Http\Requests\Security;

use App\Entities\CitizenPortal\Profile;
use Illuminate\Foundation\Http\FormRequest;

class PreRegisterEntityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $profile = new Profile();
        return [
            'organization_type_id'  => "required",
            'organization_subtype_id'  => "",
            'organization_name'  => "required|string|between:2,90",
            'has_legal_person'  => "boolean",
            'has_sporting_aval'  => "boolean",
            'sporting_aval_number'  => "string",
            'person_type_id'  => "",
            'document_type_id'  => "numeric|exists:mysql_sim.tipo_documento,Id_TipoDocumento",
            'document'  => "numeric|unique:{$profile->getConnectionName()}.{$profile->getTable()},document|digits_between:3,20|unique:pre_register",
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users', 'unique:pre_register', 'unique:pre_register_entity'],
            'legal_rep_document' => ['required', 'string'],
            'legal_rep_name' => ['required', 'string']
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'document_type_id'  => "tipo de documento",
            'document'  => "número de documento",
            'organization_name'  => "nombre organización",
            'email' => 'correo electrónico',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'document.unique'  => "El número de documento ya se encuentra registrado o está en proceso de verificación",
            'email.unique' => 'El correo electrónico ya se encuentra registrado o está en proceso de verificación',
        ];
    }
}
