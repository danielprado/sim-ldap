<?php

namespace App\Http\Requests\Parks;

use App\Entities\Parks\Enclosure;
use App\Entities\Parks\Location;
use App\Entities\Parks\Neighborhood;
use App\Entities\Parks\Scale;
use App\Entities\Parks\Upz;
use App\Rules\Parks\ParkFinderRule;
use Illuminate\Foundation\Http\FormRequest;

class ParkFinderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query'        => [ new ParkFinderRule() ],
            'locality_id'   => [
                'nullable',
                function($attribute, $value, $fail) {
                    $values = is_array($value) ? $value : explode(',', $value);
                    if (!count($values) > 0 && !Location::query()->whereKey($value)->count() > 0) {
                        $fail(__('validation.exists', ['attribute' => $attribute]));
                    }
                },
            ],
            'upz_id'        => [
                'nullable',
                function($attribute, $value, $fail) {
                    $values = is_array($value) ? $value : explode(',', $value);
                    if (!count($values) > 0 && !Upz::query()->whereKey($value)->count() > 0) {
                        $fail(__('validation.exists', ['attribute' => $attribute]));
                    }
                },
            ],
            'neighborhood_id'  => [
                'nullable',
                function($attribute, $value, $fail) {
                    $values = is_array($value) ? $value : explode(',', $value);
                    if (!count($values) > 0 && !Neighborhood::query()->whereKey($value)->count() > 0) {
                        $fail(__('validation.exists', ['attribute' => $attribute]));
                    }
                },
            ],
            'vigilance'     => [
                'nullable',
                'string',
                function($attribute, $value, $fail) {
                    $items = ['sin vigilancia', 'con vigilancia'];
                    if (!in_array(toLower($value), $items)) {
                        $fail("El campo $attribute debe contener alguno de los siguiente valores: Sin vigilancia, Con vigilancia");
                    }
                },
            ],
            'enclosure'     => [
                'nullable',
                function($attribute, $value, $fail) {
                    $values = is_array($value) ? $value : explode(',', $value);
                    if (!count($values) > 0 && !Enclosure::query()->whereIn('Cerramiento', $value)->count() > 0) {
                        $fail(__('validation.exists', ['attribute' => $attribute]));
                    }
                },
            ],
            'type_id'       => [
                'nullable',
                function($attribute, $value, $fail) {
                    $values = is_array($value) ? $value : explode(',', $value);
                    if (!count($values) > 0 && !Scale::query()->whereKey($value)->count() > 0) {
                        $fail(__('validation.exists', ['attribute' => $attribute]));
                    }
                },
            ]
        ];
    }
}
