<?php

namespace App\Http\Requests\Orfeo;

use Illuminate\Foundation\Http\FormRequest;

class OrfeoSchoolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school'            =>  'required|string|max:191',
            'email'             =>  'required|string|email|max:125',
            'director'          =>  'required|string|max:191',
            'phone'             =>  'required|numeric|digits_between:7,10',
            'request_type'      =>  'required|numeric|between:1,10',
            'school_type'       =>  'required|string|in:ESCUELA DE FORMACIÓN',
            'accept_media'      =>  'required|boolean',

            "file_1_1"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_2"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_3"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_4"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_5"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_5_1"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_5_2"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_5_3"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_5_4"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_5_5"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_5_6"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_5_7"        =>  'nullable|file|mimes:pdf',
            "file_1_5_8"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6_1"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6_2"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6_3"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6_4"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6_5"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6_6"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6_7"        =>  'nullable|file|mimes:pdf',
            "file_1_6_8"        =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6_9"        =>  'nullable|file|mimes:pdf',
            "file_1_7"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_8"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_9"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_10"         =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_11"         =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_12"         =>  'required_if:request_type,1|file|mimes:pdf',

            "file_2_1"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_2"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_3"          =>  'nullable|file|mimes:pdf',
            "file_2_4"          =>  'nullable|file|mimes:pdf',
            "file_2_5"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_5_1"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_5_2"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_5_3"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_5_4"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_5_5"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_5_6"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_5_7"        =>  'nullable|file|mimes:pdf',
            "file_2_5_8"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6_1"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6_2"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6_3"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6_4"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6_5"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6_6"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6_7"        =>  'nullable|file|mimes:pdf',
            "file_2_6_8"        =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6_9"        =>  'nullable|file|mimes:pdf',
            "file_2_7"          =>  'required_if:request_type,2|file|mimes:pdf',

            "file_3_1"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_2"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_3"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_4"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_4_1"        =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_4_2"        =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_4_3"        =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_4_4"        =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_4_5"        =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_4_6"        =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_4_7"        =>  'nullable|file|mimes:pdf',
            "file_3_4_8"        =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_4_9"        =>  'nullable|file|mimes:pdf',
            "file_3_5"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_6"          =>  'required_if:request_type,3|file|mimes:pdf',

            "file_4_1"          =>  'required_if:request_type,4|file|mimes:pdf',
            "file_4_2"          =>  'nullable|file|mimes:pdf',

            "file_5_1"          =>  'required_if:request_type,5|file|mimes:pdf',
            "file_5_2"          =>  'nullable|file|mimes:pdf',

            "file_6_1"          =>  'required_if:request_type,6|file|mimes:pdf',
            "file_6_2"          =>  'nullable|file|mimes:pdf',

            "file_7_1"          =>  'required_if:request_type,7|file|mimes:pdf',
            "file_7_2"          =>  'nullable|file|mimes:pdf',

            "file_8_1"          =>  'required_if:request_type,8|file|mimes:pdf',
            "file_8_2"          =>  'nullable|file|mimes:pdf',

            "file_9_1"          =>  'required_if:request_type,9|file|mimes:pdf',
            "file_9_2"          =>  'nullable|file|mimes:pdf',

            "file_10_1"          =>  'nullable|file|mimes:pdf',
            "file_10_2"          =>  'nullable|file|mimes:pdf',

            "sports"            => 'required|array',
            "sports.*.id"          => 'required|numeric',
            "sports.*.name"          => 'required|string',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'school'            =>  'nombre de la escuela de formación',
            'email'             =>  'correo electrónico de la escuela de formación',
            'director'          =>  'nombre del director',
            'phone'             =>  'teléfono',
            'request_type'      =>  'tipo de solicitud',
            'school_type'       =>  'tipo de entidad',
            'accept_media'      =>  'medio de comunicación',

            'sport'             =>  'deporte',
            'sport.*.id'        =>  'deporte',
            'sport.*.name'      =>  'deporte',
            "file_1_1"          =>  'solicitud de Aval para Escuelas de formación deportiva dirigida a la Subdirección Técnica de Recreción y Deportes del IDRD',
            "file_1_2"          =>  'ficha de inscripción de la Escuela de Formación Deportiva',
            "file_1_3"          =>  'fotocopias de las cédulas de ciudadanía de cada uno de los firmantes relacionados en la ficha de inscripción de la Escuela de Formación Deportiva',
            "file_1_4"          =>  'acta de creación',
            "file_1_5"          =>  'hoja de vida del director (a) de la Escuela de Formación Deportiva con los soportes de formación académica y experiencia laboral',
            "file_1_5_1"        =>  'certificado del curso de primer respondiente actualizado',
            "file_1_5_2"        =>  'antecedentes judiciales vigente',
            "file_1_5_3"        =>  'antecedentes Contraloría vigente',
            "file_1_5_4"        =>  'antecedentes Personería vigente',
            "file_1_5_5"        =>  'antecedentes Procuraduría vigente',
            "file_1_5_6"        =>  'certificado de inhabilidades Ley 1918 de 2018',
            "file_1_5_7"        =>  'curso de salvamento acuático vigente',
            "file_1_5_8"        =>  'curso de fundamentos en administración deportiva no superior a dos años',
            "file_1_6"          =>  'hoja de vida de (los - las) instructores (as) de la Escuela de formación deportiva con los soportes de formación académica y experiencia laboral',
            "file_1_6_1"        =>  'certificado del curso de primer respondiente actualizado',
            "file_1_6_2"        =>  'antecedentes judiciales vigente',
            "file_1_6_3"        =>  'antecedentes Contraloría vigente',
            "file_1_6_4"        =>  'antecedentes Personería vigente',
            "file_1_6_5"        =>  'antecedentes Procuraduría vigente',
            "file_1_6_6"        =>  'certificado de inhabilidades Ley 1918 de 2018',
            "file_1_6_7"        =>  'curso de salvamento acuático vigente',
            "file_1_6_8"        =>  'curso de fundamentos en administración deportiva no superior a dos años',
            "file_1_6_9"        =>  'permiso de trabajo para el país (En el caso de los extranjeros)',
            "file_1_7"          =>  'manual de normas de funcionamiento de la Escuela de Formación Deportiva',
            "file_1_8"          =>  'plan pedagógico y metodológico',
            "file_1_9"          =>  'planilla de inscritos vigente del IDRD',
            "file_1_10"         =>  'certificación vigente de afiliación a la EPS o SISBEN de cada uno de los inscritos de la Escuela de Formación Deportiva',
            "file_1_11"         =>  'certificado de veracidad de la documentación emitida por el Director (a)',
            "file_1_12"         =>  'póliza de accidentes deportivos para el total de inscritos de la Escuela de Formación Deportiva',

            "file_2_1"          =>  'carta de solicitud de renovación del Aval Deportivo, radicada en el Instituto Distrital de Recreación y Deporte -IDRD- con cuarenta y cinco (45) días hábiles previos al vencimiento de este',
            "file_2_2"          =>  'ficha de inscripción de la Escuela Deportiva',
            "file_2_3"          =>  'fotocopia de las cédulas de ciudadanía de cada uno de los firmantes relacionados en la ficha de inscripción de la Escuela Deportiva',
            "file_2_4"          =>  'acta de reunión en la que conste la intención de renovar el Aval deportivo de la Escuela de Formación Deportiva',
            "file_2_5"          =>  'hoja de vida del Director (a) de la Escuela de Formación Deportiva con los soportes de formación académica y experiencia laboral',
            "file_2_5_1"        =>  'certificado del curso de primer respondiente actualizado',
            "file_2_5_2"        =>  'antecedentes judiciales vigente',
            "file_2_5_3"        =>  'antecedentes Contraloría vigente',
            "file_2_5_4"        =>  'antecedentes Personería vigente',
            "file_2_5_5"        =>  'antecedentes Procuraduría vigente',
            "file_2_5_6"        =>  'certificado de inhabilidades Ley 1918 de 2018',
            "file_2_5_7"        =>  'curso de salvamento acuático vigente',
            "file_2_5_8"        =>  'curso de fundamentos en administración deportiva no superior a dos años',
            "file_2_6"          =>  'hoja(s) de vida de (los - las) instructores (as) de la Escuela de formación deportiva con los soportes de formación académica y experiencia laboral',
            "file_2_6_1"        =>  'certificado del curso de primer respondiente actualizado',
            "file_2_6_2"        =>  'antecedentes judiciales vigente',
            "file_2_6_3"        =>  'antecedentes Contraloría vigente',
            "file_2_6_4"        =>  'antecedentes Personería vigente',
            "file_2_6_5"        =>  'antecedentes Procuraduría vigente',
            "file_2_6_6"        =>  'certificado de inhabilidades Ley 1918 de 2018',
            "file_2_6_7"        =>  'curso de salvamento acuático vigente',
            "file_2_6_8"        =>  'curso de fundamentos en administración deportiva no superior a dos años',
            "file_2_6_9"        =>  'permiso de trabajo para el país (En el caso de los extranjeros)',
            "file_2_7"          =>  'listado de alumnos (as) inscritos (as): actualizada a la fecha de la solicitud con los requisitos establecidos en el numeral decimo (10°) del artículo once (11°) de la resolución 639 del 2021 del IDRD',

            "file_3_1"          =>  'carta de solicitud de inclusión de nuevos deportes',
            "file_3_2"          =>  'ficha de inscripción de la Escuela de Formación Deportiva',
            "file_3_3"          =>  'fotocopia de las cédulas de ciudadanía de cada uno de los firmantes relacionados en la ficha de inscripción de la Escuela de Formación Deportiva',
            "file_3_4"          =>  'hoja(s) de vida de (los - las) instructores (as) de las nuevas disciplinas deportivas de la Escuela de formación deportiva con los soportes de formación académica y experiencia laboral',
            "file_3_4_1"        =>  'certificado del curso de primer respondiente actualizado',
            "file_3_4_2"        =>  'antecedentes judiciales vigente',
            "file_3_4_3"        =>  'antecedentes Contraloría vigente',
            "file_3_4_4"        =>  'antecedentes Personería vigente',
            "file_3_4_5"        =>  'antecedentes Procuraduría vigente',
            "file_3_4_6"        =>  'certificado de inhabilidades Ley 1918 de 2018',
            "file_3_4_7"        =>  'curso de salvamento acuático vigente',
            "file_3_4_8"        =>  'curso de fundamentos en administración deportiva no superior a dos años',
            "file_3_4_9"        =>  'permiso de trabajo para el país (En el caso de los extranjeros)',
            "file_3_5"          =>  'listado de alumnos (as) inscritos (as) de las nuevas disciplinas deportivas: actualizada a la fecha de solicitud con los requisitos establecidos en el numeral decimo (10°) del artículo once (11°) de la resolución 639 del 2021 del IDRD',
            "file_3_6"          =>  'plan pedagógico y metodológico de las nuevas disciplinas deportivas',

            "file_4_1"          =>    "solicitud de actualización de expediente de la Escuela de Formación",
            "file_4_2"          =>    "anexos *",

            "file_5_1"          =>    "solicitud con la cual se subsanan las inconsistencias reportadas por el IDRD",
            "file_5_2"          =>    "anexos *",

            "file_6_1"          =>    "petición",
            "file_6_2"          =>    "anexos *",

            "file_7_1"          =>    "petición",
            "file_7_2"          =>    "anexos *",

            "file_8_1"          =>    "petición",
            "file_8_2"          =>    "anexos *",

            "file_9_1"          =>    "petición",
            "file_9_2"          =>    "anexos *",

            "file_10_1"         =>    "recurso contra acto administrativo que otorga/renueva/actualiza el aval deportivo. *",
            "file_10_2"         =>    "recurso contra acto administrativo que niega el otorgamiento/renovación/actualización del aval deportivo. *",
        ];
    }
}
