<?php

namespace App\Http\Requests\Orfeo;

use Illuminate\Foundation\Http\FormRequest;

class OrfeoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'club'      =>  'required|string|max:191',
            'email'     =>  'required|string|email|max:125',
            'president' =>  'required|string|max:191',
            'phone'     =>  'required|numeric|digits_between:7,10',
            'profile_id'            =>  'required|numeric',
            'entity_profile_id'     =>  'required|numeric',
            'request_type'  =>  'required|numeric|between:1,18',
            'club_type'     =>  'required|string|in:ENTIDAD DEPORTIVA,ENTIDAD NO DEPORTIVA',
            'accept_media'  =>  'required|boolean',
            "file_1_1"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_2"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_3"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_4"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_5"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_6"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_7"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_8"          =>  'required_if:request_type,1|file|mimes:pdf',
            "file_1_9"          =>  'required_if:request_type,1|file|mimes:pdf',

            "file_2_1"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_2"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_3"          =>  'nullable|file|mimes:pdf',
            "file_2_4"          =>  'nullable|file|mimes:pdf',
            "file_2_5"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_6"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_7"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_8"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_9"          =>  'required_if:request_type,2|file|mimes:pdf',
            "file_2_10"          =>  'nullable|file|mimes:pdf',
            "file_2_11"          =>  'required_if:request_type,2|file|mimes:pdf',

            "file_3_1"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_2"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_3"          =>  'nullable|file|mimes:pdf',
            "file_3_4"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_5"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_6"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_7"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_8"          =>  'required_if:request_type,3|file|mimes:pdf',
            "file_3_9"          =>  'nullable|file|mimes:pdf',

            "file_4_1"          =>  'required_if:request_type,4|file|mimes:pdf',
            "file_4_2"          =>  'required_if:request_type,4|file|mimes:pdf',
            "file_4_3"          =>  'nullable|file|mimes:pdf',
            "file_4_4"          =>  'nullable|file|mimes:pdf',
            "file_4_5"          =>  'required_if:request_type,4|file|mimes:pdf',
            "file_4_6"          =>  'required_if:request_type,4|file|mimes:pdf',
            "file_4_7"          =>  'nullable|file|mimes:pdf',

            "file_5_1_1"          =>  'required_if:request_type,5|file|mimes:pdf',
            "file_5_1_2"          =>  'required_if:request_type,5|file|mimes:pdf',
            "file_5_1_3"          =>  'nullable|file|mimes:pdf',
            "file_5_1_4"          =>  'required_if:request_type,5|file|mimes:pdf',
            "file_5_1_5"          =>  'required_if:request_type,5|file|mimes:pdf',
            "file_5_1_6"          =>  'required_if:request_type,5|file|mimes:pdf',
            "file_5_1_7"          =>  'required_if:request_type,5|file|mimes:pdf',
            "file_5_1_8"          =>  'nullable|file|mimes:pdf',

            "file_5_2_1"          =>  'required_if:request_type,6|file|mimes:pdf',
            "file_5_2_2"          =>  'required_if:request_type,6|file|mimes:pdf',
            "file_5_2_3"          =>  'nullable|file|mimes:pdf',
            "file_5_2_4"          =>  'required_if:request_type,6|file|mimes:pdf',
            "file_5_2_5"          =>  'required_if:request_type,6|file|mimes:pdf',

            "file_5_3_1"          =>  'required_if:request_type,7|file|mimes:pdf',
            "file_5_3_2"          =>  'required_if:request_type,7|file|mimes:pdf',
            "file_5_3_3"          =>  'nullable|file|mimes:pdf',
            "file_5_3_4"          =>  'required_if:request_type,7|file|mimes:pdf',
            "file_5_3_5"          =>  'required_if:request_type,7|file|mimes:pdf',
            "file_5_3_6"          =>  'required_if:request_type,7|file|mimes:pdf',

            "file_6_1_1"          =>  'required_if:request_type,8|file|mimes:pdf',
            "file_6_1_2"          =>  'required_if:request_type,8|file|mimes:pdf',
            "file_6_1_3"          =>  'required_if:request_type,8|file|mimes:pdf',
            "file_6_1_4"          =>  'nullable|file|mimes:pdf',
            "file_6_1_5"          =>  'nullable|file|mimes:pdf',

            "file_6_2_1"          =>  'required_if:request_type,9|file|mimes:pdf',
            "file_6_2_2"          =>  'required_if:request_type,9|file|mimes:pdf',
            "file_6_2_3"          =>  'required_if:request_type,9|file|mimes:pdf',

            "file_6_3_1"          =>  'required_if:request_type,10|file|mimes:pdf',
            "file_6_3_2"          =>  'required_if:request_type,10|file|mimes:pdf',
            "file_6_3_3"          =>  'required_if:request_type,10|file|mimes:pdf',
            "file_6_3_4"          =>  'required_if:request_type,10|file|mimes:pdf',

            "file_7_1"          =>  'nullable|file|mimes:pdf',
            "file_7_2"          =>  'nullable|file|mimes:pdf',
            "file_7_3"          =>  'nullable|file|mimes:pdf',
            "file_7_4"          =>  'nullable|file|mimes:pdf',
            "file_7_5"          =>  'nullable|file|mimes:pdf',
            "file_7_6"          =>  'nullable|file|mimes:pdf',

            "file_8_1"          =>  'required_if:request_type,12|file|mimes:pdf',
            "file_8_2"          =>  'nullable|file|mimes:pdf',

            "file_9_1"          =>  'required_if:request_type,13|file|mimes:pdf',
            "file_9_2"          =>  'nullable|file|mimes:pdf',

            "file_10_1"          =>  'required_if:request_type,14|file|mimes:pdf',
            "file_10_2"          =>  'nullable|file|mimes:pdf',

            "file_11_1"          =>  'required_if:request_type,15|file|mimes:pdf',
            "file_11_2"          =>  'nullable|file|mimes:pdf',

            "file_12_1"          =>  'required_if:request_type,16|file|mimes:pdf',
            "file_12_2"          =>  'nullable|file|mimes:pdf',

            "file_13_1"          =>  'required_if:request_type,17|file|mimes:pdf',
            "file_13_2"          =>  'nullable|file|mimes:pdf',

            "file_14_1"          =>  'nullable|file|mimes:pdf',
            "file_14_2"          =>  'nullable|file|mimes:pdf',
            "sports"            => 'required|array',
            "sports.*.id"          => 'required|numeric',
            "sports.*.name"          => 'required|string',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'club'      =>  'nombre del club',
            'email'     =>  'correo electrónico del club',
            'president' =>  'nombre del presidente',
            'phone'     =>  'teléfono',
            'request_type'  =>  'tipo de solicitud',
            'club_type'     =>  'tipo de entidad',
            'accept_media'  =>  'medio de comunicación',
            'sport'  =>  'deporte',
            'sport.*.id'  =>  'deporte',
            'sport.*.name'  =>  'deporte',
            "file_1_1"    =>    "solicitud de otorgamiento del reconocimiento deportivo.",
            "file_1_2"    =>    "formato único de reconocimiento deportivo (FURD).",
            "file_1_3"    =>    "acta de asamblea de constitución del club deportivo.",
            "file_1_4"    =>    "acta de reunión del órgano de administración del club deportivo.",
            "file_1_5"    =>    "acreditación por parte de los miembros de órgano de administración.",
            "file_1_6"    =>    "fotocopia legible del documento de identidad del presidente del club deportivo.",
            "file_1_7"    =>    "copia de las tarjetas profesionales de los revisores fiscales y certificado de vigencia de la matricula profesional.",
            "file_1_8"    =>    "plan de desarrollo deportivo del club.",
            "file_1_9"    =>    "estatuto aprobado por la asamblea.",

            "file_2_1"    =>    "solicitud de otorgamiento del reconocimiento deportivo.",
            "file_2_2"    =>    "formato único de reconocimiento deportivo (FURD).",
            "file_2_3"    =>    "certificado de existencia y representación legal. *",
            "file_2_4"    =>    "reconocimiento de carácter oficial. *",
            "file_2_5"    =>    "fotocopia legible del documento de identidad del representante legal del club.",
            "file_2_6"    =>    "resolución de creación y reglamentación del club deportivo.",
            "file_2_7"    =>    "constancia de elección de dos miembros de la comisión disciplinaria por parte de la asamblea de afiliados.",
            "file_2_8"    =>    "constancia de elección del tercer miembro de la comisión disciplinaria por parte del órgano de administración.",
            "file_2_9"    =>    "reglamento en el cual se establezca el comité deportivo.",
            "file_2_10"    =>    "acta de designación del comisionado por cada deporte. *",
            "file_2_11"    =>    "plan de desarrollo deportivo.",

            "file_3_1"    =>    "solicitud de renovación del reconocimiento deportivo.",
            "file_3_2"    =>    "formato único de reconocimiento deportivo (FURD).",
            "file_3_3"    =>    "resolución de convocatoria a asamblea electiva. *",
            "file_3_4"    =>    "acta de asamblea electiva.",
            "file_3_5"    =>    "acta de reunión del órgano de administración.",
            "file_3_6"    =>    "fotocopia legible del documento de identidad del presidente del club deportivo.",
            "file_3_7"    =>    "copia de las tarjetas profesionales de los revisores fiscales y certificado de vigencia de la matricula profesional.",
            "file_3_8"    =>    "acreditación por parte de los miembros de órgano de administración.",
            "file_3_9"    =>    "plan de desarrollo deportivo. *",

            "file_4_1"    =>    "solicitud de renovación del reconocimiento deportivo.",
            "file_4_2"    =>    "formato único de reconocimiento deportivo (FURD).",
            "file_4_3"    =>    "certificado de existencia y representación legal de la entidad. *",
            "file_4_4"    =>    "reconocimiento de carácter oficial. *",
            "file_4_5"    =>    "constancia de elección de dos miembros de la comisión disciplinaria por parte de la asamblea de afiliados.",
            "file_4_6"    =>    "constancia de elección del tercer miembro de la comisión disciplinaria por parte del órgano de administración.",
            "file_4_7"    =>    "constancia de nombramiento del rector o director de la institución. *",

            "file_5_1_1"    =>    "solicitud de actualización del reconocimiento deportivo.",
            "file_5_1_2"    =>    "formato único de reconocimiento deportivo (FURD).",
            "file_5_1_3"    =>    "resolución de convocatoria a asamblea electiva. *",
            "file_5_1_4"    =>    "acta de asamblea electiva, en la cual se suple el cargo de presidente del organismo deportivo.",
            "file_5_1_5"    =>    "acta de reunión del órgano de administración del club deportivo (nueva designación del cargo).",
            "file_5_1_6"    =>    "fotocopia de la cédula de ciudadanía del nuevo presidente.",
            "file_5_1_7"    =>    "acreditación del nuevo presidente.",
            "file_5_1_8"    =>    "plan de desarrollo deportivo. *",

            "file_5_2_1"    =>    "solicitud de actualización del reconocimiento.",
            "file_5_2_2"    =>    "formato único de reconocimiento deportivo (FURD)",
            "file_5_2_3"    =>    "resolución de convocatoria a asamblea electiva. *",
            "file_5_2_4"    =>    "acta de la asamblea.",
            "file_5_2_5"    =>    "estatutos reformados.",

            "file_5_3_1"    =>    "solicitud de actualización del reconocimiento deportivo.",
            "file_5_3_2"    =>    "formato único de reconocimiento deportivo (furd).",
            "file_5_3_3"    =>    "resolución de convocatoria a asamblea electiva. *",
            "file_5_3_4"    =>    "acta de la asamblea.",
            "file_5_3_5"    =>    "estatutos reformados.",
            "file_5_3_6"    =>    "plan de desarrollo deportivo conforme al nuevo deporte.",

            "file_6_1_1"    =>    "solicitud de actualización del reconocimiento deportivo.",
            "file_6_1_2"    =>    "formato único de reconocimiento deportivo (FURD).",
            "file_6_1_3"    =>    "fotocopia de la cédula de ciudadanía del nuevo representante legal.",
            "file_6_1_4"    =>    "certificado de existencia y representación legal. *",
            "file_6_1_5"    =>    "reconocimiento de carácter oficial. *",

            "file_6_2_1"    =>    "solicitud de actualización del reconocimiento deportivo.",
            "file_6_2_2"    =>    "formato único de reconocimiento deportivo (FURD).",
            "file_6_2_3"    =>    "reglamento del club deportivo donde se evidencie el nuevo nombre.",

            "file_6_3_1"    =>    "solicitud de actualización del reconocimiento deportivo.",
            "file_6_3_2"    =>    "formato único de reconocimiento deportivo (FURD).",
            "file_6_3_3"    =>    "acta de designación del comisionado para cada deporte en caso de cambio o adición de disciplinas deportivas.",
            "file_6_3_4"    =>    "plan de desarrollo deportivo, donde se incluya el nuevo deporte y/o disciplinas deportivas.",

            "file_7_1"    =>    "formato único de reconocimiento deportivo (FURD).  *",
            "file_7_2"    =>    "plan de desarrollo deportivo. *",
            "file_7_3"    =>    "copia acta de asamblea. *",
            "file_7_4"    =>    "acta de comité ejecutivo. *",
            "file_7_5"    =>    "resolución de inscripción de nuevos deportistas. *",
            "file_7_6"    =>    "otros. (dejar espacio para que el usuario digite el nombre del documento que va a adjuntar.) *",

            "file_8_1"    =>    "solicitud con la cual se subsana el reconocimiento deportivo (campo donde se solicite el número de radicado al que le esta dando respuesta)",
            "file_8_2"    =>    "anexos *",

            "file_9_1"    =>    "petición",
            "file_9_2"    =>    "anexos *",

            "file_10_1"    =>    "petición",
            "file_10_2"    =>    "anexos *",

            "file_11_1"    =>    "petición",
            "file_11_2"    =>    "anexos *",

            "file_12_1"    =>    "petición",
            "file_12_2"    =>    "anexos *",

            "file_13_1"    =>    "petición",
            "file_13_2"    =>    "anexos *",

            "file_14_1"    =>    "recurso contra acto administrativo que otorga/renueva/actualiza el reconocimiento. *",
            "file_14_2"    =>    "recurso contra acto administrativo que niega el otorgamiento/renovación/actualización del reconocimiento. *",
        ];
    }
}
