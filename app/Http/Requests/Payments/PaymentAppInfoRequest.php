<?php

namespace App\Http\Requests\Payments;

use App\Entities\Parks\Endowment;
use App\Entities\Parks\Synthetic;
use App\Entities\Payments\Payment;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class PaymentAppInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = ['date', 'start_hour', 'final_hour', 'document'];
        $tipo_material = $this->route('endowment')->MaterialPiso ?? 0;
        // Si el tipo == 6 es cancha natural

        $servicio_id = map_endowment_to_service($this->route('endowment')->Id_Dotacion, $tipo_material);
        $date = $this->has('date')
            ? Carbon::parse($this->get('date'))
            : null;
        $start_date = $this->has($inputs)
            ? Carbon::parse($this->get('date') . ' ' . $this->get('start_hour'))
            : null;
        $final_date  = $this->has($inputs)
            ? Carbon::parse($this->get('date') . ' ' . $this->get('final_hour'))
            : null;
        $diffInHours = 0;
        if (!is_null($start_date) && !is_null($final_date)) {
            $diffInHours = $start_date->diffInHours($final_date);
        }

        $validationDate = clone $date;
        $timeLimit = $validationDate ? $validationDate->setTime(18, 0, 0) : null;

        $field = $this->route('endowment')->Id_Dotacion != Endowment::GRILL;


        $min_hours = $field
            ? 1
            : 3;

        $max_hours = $field
            ? 2
            : 10;

        $has_invalid_range = $diffInHours > $max_hours || $diffInHours <= 0;

        $has_invalid_min_range = $diffInHours < $min_hours;

        $has_invalid_hours = false;


         /** AHORA VALIDAR QUE EL USUARIO NO PUEDA RESERVAR A LA MISMA FECHA; A LA MISMA HORA UN SERVICIO */
        $document = $this->get('document'); // Obtén el documento directamente del request



        $payments = Payment::query()
            ->where('identificacion', $document)
            ->where('created_at', '>=', now()->subDays(7))
            ->whereIn('estado_id', [1, 2])
            ->whereNotNull('id_reserva')
            ->with('booking') // Carga la relación de la reserva
            ->get();



        $booking_with_already_schedule = false;

         if ($payments->isNotEmpty()) {
                foreach ($payments as $payment) {
                    if ( isset($payment->booking) && $payment->booking->hora_inicio == $start_date->format('H:i:s') &&  $payment->booking->fecha->format('Y-m-d') == $date->format('Y-m-d') ) {
                        $booking_with_already_schedule = true;
                }
            }
        }

        if ($diffInHours > 0 && !is_null($timeLimit)) {
            $has_invalid_hours = $final_date->isAfter($timeLimit) && $start_date->isBefore($timeLimit);
        }

        $currentDateTime = now();
        $min_offset_booking = false;
         // Valida si la reserva está dentro de las próximas 24 horas
        if ($date && $this->has('start_hour')) {
            $selectedDateTime = Carbon::parse($date->format('Y-m-d') . ' ' . $this->get('start_hour'));
            if ($selectedDateTime->lt($currentDateTime->copy()->addDay())) {
                $min_offset_booking = true;
            }
        }






        $max_count_per_service = Payment::query()
            ->where('identificacion', $document)
            ->where('created_at', '>=', $date->subDays(7))
            ->where('servicio_id', $servicio_id)
            ->whereIn('estado_id', [1, 2])
            ->whereNotNull('id_reserva')
            ->selectRaw('servicio_id, COUNT(*) as count_per_service')
            ->groupBy(['servicio_id'])
            ->orderByDesc('count_per_service')
            ->first();






        if ($max_count_per_service) {
            $max_count = $max_count_per_service->count_per_service; // Aquí está el conteo máximo
        } else {
            $max_count = 0;
        }

        return [
            'date'  =>  [
                'required',
                "date",
                "date_format:Y-m-d",
                "after_or_equal:now",
                 function ($attribute, $value, $fail) use ($max_count, $field, $min_offset_booking) {
                    try {
                        if (Carbon::parse($value)->isMonday()) {
                            // $fail("Los lunes no hay servicio");
                        }
                        $text = $field ? 'este servicio' : 'este asador';
                        if ($max_count >= 2) {
                            //$fail("Ya tienes dos o más reservas registradas para $text.");
                        }
                        if ($min_offset_booking) {
                            $fail("No puedes reservar con menos de 24 horas de anticipación este espacio");
                        }
                    } catch (\Exception $exception) {
                        $fail("La fecha no es válida");
                    }
                },
            ],
            'start_hour' => [
                "required",
                "date_format:g:i A",
                "before:final_hour",
                function ($attribute, $value, $fail) use ($has_invalid_range, $has_invalid_min_range, $has_invalid_hours, $max_count, $field, $min_hours) {

                    $text = $field ? 'este servicio' : 'este asador';
                    if ($max_count >= 2) {
                        //$fail("Ya tienes dos o más reservas registradas para $text.");
                    }
                    if ($has_invalid_range) {
                        //$fail("No se puede reservar por más de dos horas");
                    }
                    if ($has_invalid_hours) {
                        //$fail("Solo se puede reservar en una franja horaria diurna o nocturna");
                    }
                    if ($has_invalid_min_range) {
                        //$fail("No puedes reservar este servicio por menos de $min_hours hora(s)");
                    }
                },
            ],
            'final_hour' => [
                "required",
                "date_format:g:i A",
                "after:start_hour",
                function ($attribute, $value, $fail) use ($has_invalid_range, $has_invalid_hours, $max_count, $field, $has_invalid_min_range, $min_hours) {
                    $text = $field ? 'este servicio' : 'este asador';
                    if ($max_count >= 2) {
                        //$fail("Ya tienes dos o más reservas registradas para $text.");
                    }
                    if ($has_invalid_range) {
                        $fail("No se puede reservar por más de dos horas");
                    }
                    if ($has_invalid_hours) {
                        $fail("Solo se puede reservar en una franja horaria diurna o nocturna");
                    }
                    if ($has_invalid_min_range) {
                        $fail("No puedes reservar este servicio por menos de $min_hours hora(s)");
                    }
                },
            ],
            'document' => [
                'required',
                'numeric',
                function ($attribute, $value, $fail) use ($max_count, $field, $booking_with_already_schedule) {
                    $text = $field ? 'este servicio' : 'este asador';
                    if ($max_count >= 2) {
                        $fail("Ya tienes dos o más reservas registradas para $text.");
                    }
                    if ($booking_with_already_schedule) {
                        $fail("Ya tienes una reserva para esta fecha y hora");
                    }
                },
            ],
            'lane' => "nullable"
        ];
    }

    public function attributes()
    {
        return [
            'date'  =>  'El campo fecha',
            'start_hour' => 'El campo hora inicial',
            'final_hour' => 'El campo hora final',
            'document' => 'Documento del usuario',
            'lane' => 'El campo id carril'
        ];
    }
}
