<?php

namespace App\Http\Requests\Payments;

use App\Entities\Parks\Endowment;
use App\Entities\CitizenPortal\Profile;
use App\Entities\Payments\Payment;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

        /**
     * Obtiene los límites de reservas según el tipo de dotación.
     */
    protected function getReservationLimits($endowmentId)
    {
        switch ($endowmentId) {
            case Endowment::FIELD_TENNIS:
            case Endowment::FIELD_NATURAL:
            case Endowment::FIELD_TENNIS_MINI:
            case Endowment::FIELD_SYNTHETIC:
                return [
                    'max_reservations' => 4,
                    'max_hours' => 4,
                    'max_daily_hours' => 2,
                    'min_daily_hours' => 1,
                ];
            case Endowment::GYM:
            case Endowment::SWIMMING_POOL:
                return [
                    'max_reservations' => 4,
                    'max_hours' => 4,
                    'max_daily_hours' => 1,
                    'min_daily_hours' => 1,
                ];
            case Endowment::GRILL:
                return [
                    'max_reservations' => 2,
                    'max_hours' => 20,
                    'max_daily_hours' => 10,
                    'min_daily_hours' => 3,
                ];
            default:
                return [
                    'max_reservations' => 2,
                    'max_hours' => 2,
                    'max_daily_hours' => 2,
                    'min_daily_hours' => 1,
                ];
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = ['date', 'start_hour', 'final_hour'];
        $tipo_material = $this->route('endowment')->MaterialPiso ?? 0;
        $id_dotacion = $this->route('endowment')->Id_Dotacion;
        // Si el tipo == 6 es cancha natural

        $servicio_id = map_endowment_to_service($id_dotacion, $tipo_material);
        $date = $this->has('date')
            ? Carbon::parse($this->get('date'))
            : null;
        $start_date = $this->has($inputs)
            ? Carbon::parse($this->get('date') . ' ' . $this->get('start_hour'))
            : null;
        $final_date = $this->has($inputs)
            ? Carbon::parse($this->get('date') . ' ' . $this->get('final_hour'))
            : null;
        // Si el usuario no envía el documento, se toma el documento del perfil
        $document = $this->has('document')
            ? $this->get('document')
            : Profile::query()
                ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                ->where('user_id', auth('api')->user()->id)
                ->first()->document;

        $user_documents = auth('api')->user()->profiles->pluck('document')->toArray();
        $is_pool_or_gym = check_if_endowment_is_pool_or_gym($id_dotacion);
        $document_forbidden = false;


        // Si es piscina o gimnasio, revisar si se envia el documento del usuario; si no se envía, se toma el documento del perfil principal
        if ( $is_pool_or_gym ) {
            if (is_null($document)) {
                $document = Profile::query()
                    ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                    ->where('user_id', auth('api')->user()->id)
                    ->first()->document;
            }
        }


        // Verificar que el documento del beneficiario o propio pertenezca al usuario
        // Así no se le permite al usuario hacer reservas con documentos que no le pertenecen
        if (
            !is_null($document) &&
            !in_array($document, $user_documents)) {
            $document_forbidden = true;
        }


        $diffInHours = 0;
        if (!is_null($start_date) && !is_null($final_date)) {
            $diffInHours = $start_date->diffInHours($final_date);
        }

        $validationDate = clone $date;
        $timeLimit = $validationDate ? $validationDate->setTime(18, 0, 0) : null;
        $endowmentId = $this->route('endowment')->Id_Dotacion;
        $limits = $this->getReservationLimits($endowmentId);

        $max_reservations = $limits['max_reservations'];
        $max_hours = $limits['max_hours'];
        $min_daily_hours = $limits['min_daily_hours'];
        $max_daily_hours = $limits['max_daily_hours'];

         // Validación de reservas semanales
         $weekly_reservations = Payment::query()
         ->where('identificacion', $document)
         ->where('created_at', '>=', now()->subDays(7))
         ->where('servicio_id', $servicio_id)
         ->whereIn('estado_id', [1, 2])
         ->count();

         // Validación de horas diarias sumando manualmente
         $daily_hours = Payment::query()
         ->where(
             'identificacion',
             $document
         )
         ->whereHas('booking', function ($query) use ($date) {
             $query->whereDate('fecha', '=', $date->format('Y-m-d'));
         })
         ->where('servicio_id', $servicio_id)
         ->whereNotNull('id_reserva')
         ->with('booking')
         ->whereIn('estado_id', [1, 2])
         ->get()
         ->sum(function ($payment) {
             $start = Carbon::parse($payment->booking->hora_inicio);
             $end = Carbon::parse($payment->booking->hora_fin);
             return $start->diffInHours($end);
         });



        $field = $this->route('endowment')->Id_Dotacion != Endowment::GRILL;


        $min_hours = $field
            ? 1
            : 3;

        $max_hours_service = $field
            ? 2
            : 10;

        $has_invalid_range = $diffInHours > $max_hours_service || $diffInHours <= 0;

        $has_invalid_min_range = $diffInHours < $min_hours;

        $has_invalid_hours = false;


        /** AHORA VALIDAR QUE EL USUARIO NO PUEDA RESERVAR A LA MISMA FECHA; A LA MISMA HORA UN SERVICIO */

        $payments = Payment::query()
            ->where(
                'identificacion',
                $document
            )
            ->where('created_at', '>=', now()->subDays(7))
            ->whereIn('estado_id', [1, 2])
            ->whereNotNull('id_reserva')
            ->with('booking') // Carga la relación de la reserva
            ->get();



            $booking_with_already_schedule = false;

            if ($payments->isNotEmpty()) {
                foreach ($payments as $payment) {
                    if ( isset($payment->booking) && $payment->booking->hora_inicio == $start_date->format('H:i:s') &&  $payment->booking->fecha->format('Y-m-d') == $date->format('Y-m-d') ) {
                        $booking_with_already_schedule = true;
                }
            }
        }

        if ($diffInHours > 0 && !is_null($timeLimit)) {
            $has_invalid_hours = $final_date->isAfter($timeLimit) && $start_date->isBefore($timeLimit);
        }

        $currentDateTime = now();
        $min_offset_booking = false;
        // Valida si la reserva está dentro de las próximas 24 horas
        if ($date && $this->has('start_hour')) {
            $selectedDateTime = Carbon::parse($date->format('Y-m-d') . ' ' . $this->get('start_hour'));
            if ($selectedDateTime->lt($currentDateTime->copy()->addDay())) {
                $min_offset_booking = true;
            }
        }





        $max_count_per_service = Payment::query()
            ->where(
                'identificacion',
                $document
            )
            ->where('created_at', '>=', now()->subDays(7))
            ->where('servicio_id', $servicio_id)
            ->whereIn('estado_id', [1, 2])
            ->whereNotNull('id_reserva')
            ->selectRaw('servicio_id, COUNT(*) as count_per_service')
            ->groupBy(['servicio_id'])
            ->orderByDesc('count_per_service') // Ordena para obtener el máximo fácilmente si es necesario
            ->first();



        if ($max_count_per_service) {
            $max_count = $max_count_per_service->count_per_service; // Aquí está el conteo máximo
        } else {
            $max_count = 0;
        }

        $weekly_hours = Payment::query()
            ->where('identificacion', $document)
            ->where('created_at', '>=', now()->subDays(7)) // Últimos 7 días
            ->where('servicio_id', $servicio_id)
            ->whereIn('estado_id', [1, 2])
            ->whereNotNull('id_reserva')
            ->with('booking')
            ->get()
            ->sum(function ($payment) {
                $start = Carbon::parse($payment->booking->hora_inicio);
                $end = Carbon::parse($payment->booking->hora_fin);
                return $start->diffInHours($end);
            });


        $total_hours_after_booking = $weekly_hours + $diffInHours;
        $pass_max_hours_per_week = false;

        if ($total_hours_after_booking > $max_hours) {
            $pass_max_hours_per_week = true;
        }


        return [
            'date' => [
                'required',
                "date",
                "date_format:Y-m-d",
                "after_or_equal:now",
                function ($attribute, $value, $fail) use (
                                                            $max_count,
                                                            $field,
                                                            $min_offset_booking,
                                                            $booking_with_already_schedule,
                                                            $weekly_reservations,
                                                            $max_reservations,
                                                            $document_forbidden,
                                                            $pass_max_hours_per_week,
                                                            $max_hours
                                                            ) {
                    try {
                        if (Carbon::parse($value)->isMonday()) {
                            // $fail("Los lunes no hay servicio");
                        }
                        if ($weekly_reservations >= $max_reservations) {
                            $fail("No puedes hacer más de $max_reservations reservas a la semana para este servicio.");
                        }
                        $text = $field ? 'este servicio' : 'este asador';
                        if ($min_offset_booking) {
                            $fail("No puedes reservar con menos de 24 horas de anticipación este espacio");
                        }
                        if ($booking_with_already_schedule) {
                            $fail("Ya tienes una reserva para esta fecha y hora");
                        }
                        if ($document_forbidden) {
                            $fail("El documento no está asociado a tu perfil");
                        }
                        if ($pass_max_hours_per_week) {
                            $fail("El límite de horas semanales es $max_hours para este servicio.");
                        }
                    } catch (\Exception $exception) {
                        $fail("La fecha no es válida");
                    }
                },
            ],
            'start_hour' => [
                "required",
                "date_format:g:i A",
                "before:final_hour",
                function ($attribute, $value, $fail) use ($daily_hours, $max_daily_hours, $diffInHours, $min_daily_hours, $has_invalid_range, $has_invalid_min_range, $has_invalid_hours, $max_count, $field, $min_hours) {

                    $text = $field ? 'este servicio' : 'este asador';
                    if ($has_invalid_range) {
                        $fail("No se puede reservar por más de dos horas");
                    }
                    if ($has_invalid_hours) {
                        $fail("Solo se puede reservar en una franja horaria diurna o nocturna");
                    }
                    if ($daily_hours + $diffInHours > $max_daily_hours) {
                        $fail("No puedes reservar más de $max_daily_hours horas diarias para este servicio.");
                    }
                    if ($diffInHours < $min_daily_hours) {
                        $fail("Debes reservar al menos $min_daily_hours hora.");
                    }
                    if ($has_invalid_min_range) {
                        $fail("No puedes reservar este servicio por menos de $min_hours hora(s)");
                    }
                },
            ],
            'final_hour' => [
                "required",
                "date_format:g:i A",
                "after:start_hour",
                function ($attribute, $value, $fail) use ($has_invalid_range, $has_invalid_hours, $max_count, $field, $has_invalid_min_range, $min_hours) {
                    $text = $field ? 'este servicio' : 'este asador';
                    if ($has_invalid_range) {
                        $fail("No se puede reservar por más de dos horas");
                    }
                    if ($has_invalid_hours) {
                        $fail("Solo se puede reservar en una franja horaria diurna o nocturna");
                    }
                    if ($has_invalid_min_range) {
                        $fail("No puedes reservar este servicio por menos de $min_hours hora(s)");
                    }
                },
            ],
            'document' => "nullable",
            'lane' => "nullable"
        ];
    }

    public function attributes()
    {
        return [
            'date' => 'El campo fecha',
            'start_hour' => 'El campo hora inicial',
            'final_hour' => 'El campo hora final',
            'document' => 'El campo documento',
            'lane' => 'El campo id carril'
        ];
    }
}
