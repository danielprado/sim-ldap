<?php

namespace App\Http\Requests\Payments;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'nullable|date|date_format:Y-m-d|before:final_hour',
            'final_date' => 'required_with:start_date|date|date_format:Y-m-d|after:start_hour',
        ];
    }

    public function attributes()
    {
        return [
            'start_date' => 'el campo hora inicial',
            'final_date' => 'el campo hora final',
        ];
    }
}
