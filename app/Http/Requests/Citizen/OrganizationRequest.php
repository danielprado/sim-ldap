<?php

namespace App\Http\Requests\Citizen;

use App\Entities\CitizenPortal\CitizenSchedule;
use App\Entities\CitizenPortal\FileType;
use App\Entities\CitizenPortal\Modality;
use App\Entities\CitizenPortal\Schedule;
use App\Entities\Payments\PsePark;
use App\Entities\Payments\ServiceOffered;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'organization_type_id' => 'required|numeric|exists:mysql_sim.tipo_organizacion,id',
            'organization_subtype_id' => 'nullable|numeric|exists:mysql_sim.organismo_deportivo,id',
            'legal_rep_document' => 'required|numeric|exists:profiles,document|digits_between:3,20',
            'legal_rep_name' => 'required|max:100|min:3',
            'document_type_id' => 'nullable|numeric|exists:mysql_sim.tipo_documento,Id_TipoDocumento',
            'document' => 'nullable|numeric|digits_between:3,20',
            'dv' => 'nullable|numeric',
            'organization_name' => 'required|string|max:100',
            'address' => 'required|string|between:3,191',
            'email' => 'required|string|min:8|confirmed',
            'password' => 'required|string|min:8|confirmed',
            'phone' => 'required|numeric|digits_between:7,10',
            'person_type_id' => 'nullable|numeric',
            'has_legal_person' => 'nullable|numeric',
            'has_sporting_aval' => 'nullable|numeric',
            'has_sporting_recognition' => 'nullable|numeric',
            'sporting_aval_number' => 'nullable|required_if:has_sporting_aval,1|required_if:has_sporting_recognition,1|numeric',
            'file_legal_representation'  => 'nullable|file|mimes:pdf',
            'file_sport_recognition'  => 'nullable|file|mimes:pdf',
            // 'test' => 'required'
        ];
    }
}
