<?php

namespace App\Http\Requests\Citizen;

use App\Entities\CitizenPortal\CitizenSchedule;
use App\Entities\CitizenPortal\FileType;
use App\Entities\CitizenPortal\Modality;
use App\Entities\CitizenPortal\Profile;
use App\Entities\Payments\PsePark;
use App\Entities\Payments\ServiceOffered;
use Illuminate\Foundation\Http\FormRequest;

class TeamSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $profile = new Profile();
        return [
            'document' => "required|numeric|exists:{$profile->getConnectionName()}.{$profile->getTable()},document"
        ];
    }
}
