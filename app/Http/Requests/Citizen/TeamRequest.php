<?php

namespace App\Http\Requests\Citizen;

use App\Entities\CitizenPortal\CitizenSchedule;
use App\Entities\CitizenPortal\FileType;
use App\Entities\CitizenPortal\Modality;
use App\Entities\CitizenPortal\Schedule;
use App\Entities\Payments\PsePark;
use App\Entities\Payments\ServiceOffered;
use Illuminate\Foundation\Http\FormRequest;

class TeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $modality = new Modality();
        $activity = new Schedule();
        return [
            'name' => 'required|max:191|min:3',
            'confirmed'  => "required|boolean",
            'activity_id' => "required|numeric|exists:{$activity->getConnectionName()}.{$activity->getTable()},{$activity->getKeyName()}"
        ];
    }
}
