<?php

namespace App\Http\Requests\Citizen;

use App\Entities\CitizenPortal\CitizenSchedule;
use App\Entities\CitizenPortal\FileType;
use App\Entities\Payments\PsePark;
use App\Entities\Payments\ServiceOffered;
use Illuminate\Foundation\Http\FormRequest;

class FileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $file = new FileType();
        $schedule = new CitizenSchedule();
        $park = new PsePark();
        $service = new ServiceOffered();
        return [
            "file" => "required|file|mimes:pdf",
            "file_type_id" => "required|numeric|exists:{$file->getConnectionName()}.{$file->getTable()},{$file->getKeyName()}",
            "citizen_schedule_id" => "nullable|required_if:file_type_id,".FileType::PAYMENT."|numeric|exists:{$schedule->getConnectionName()}.{$schedule->getTable()},{$schedule->getKeyName()}",
            // PSE INFO
            'parkSelected'  => "nullable|required_if:file_type_id,".FileType::PAYMENT."|numeric|exists:{$park->getConnectionName()}.{$park->getTable()},{$park->getKeyName()},deleted_at,NULL",
            'serviceParkSelected'   => "nullable|required_if:file_type_id,".FileType::PAYMENT."|numeric|exists:{$service->getConnectionName()}.{$service->getTable()},{$service->getKeyName()},deleted_at,NULL",
            'document'  => "nullable|required_if:file_type_id,".FileType::PAYMENT."|numeric",
            'documentTypeSelected'  => "nullable|required_if:file_type_id,".FileType::PAYMENT."|string",
            'number_bank'   => "nullable|required_if:file_type_id,".FileType::PAYMENT."|numeric",
            'email' => "nullable|required_if:file_type_id,".FileType::PAYMENT."|email",
            'name'  => "nullable|required_if:file_type_id,".FileType::PAYMENT."|string",
            'lastName'  => "nullable|required_if:file_type_id,".FileType::PAYMENT."|string",
            'phone' => "nullable|required_if:file_type_id,".FileType::PAYMENT."|numeric",
            'concept'   => "nullable|required_if:file_type_id,".FileType::PAYMENT."|string|max:100",
            'totalPay'  => "nullable|required_if:file_type_id,".FileType::PAYMENT."|numeric",
            'emailPayer'    => "nullable|required_if:file_type_id,".FileType::PAYMENT."|email",
            'namePayer' => "nullable|required_if:file_type_id,".FileType::PAYMENT."|string",
            'lastNamePayer' => "nullable|required_if:file_type_id,".FileType::PAYMENT."|string",
            'phonePayer'    => "nullable|required_if:file_type_id,".FileType::PAYMENT."|numeric",
            'documentPayer' => "nullable|required_if:file_type_id,".FileType::PAYMENT."|numeric",
            'documentTypeSelectedPayer' => "nullable|required_if:file_type_id,".FileType::PAYMENT."|string",
        ];
    }
}
