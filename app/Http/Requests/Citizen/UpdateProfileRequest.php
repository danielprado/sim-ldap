<?php

namespace App\Http\Requests\Citizen;

use App\Entities\CitizenPortal\File;
use App\Entities\CitizenPortal\Profile;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->setRules();
    }

    /**
     * @return array
     */
    public function setRules()
    {
        $minAge = now()->subYears(120)->format('Y-m-d');
        $maxAge = now()->format('Y-m-d');
        $age = $this->has('birthdate')
            ? Carbon::parse($this->get('birthdate'))->age
            : 18;
        $profile = new Profile();
        $id = $profile->getKeyName();

        $default = [
            // 'informed_consent'  => "required",
            'relationship'  => "nullable|string|in:HIJO/A,TUTELADO",
            'file'  => [
                "nullable",
                Rule::requiredIf(function () {
                    return $this->route('profile')->files()->where('file_type_id', File::DOCUMENT)->count() === 0;
                }),
                "file",
                "mimes:pdf",
            ],
            'decoded'  => "required|boolean",
            'country_residence_id'  => "required|numeric|exists:mysql_users.countries,id",
            'state_residence_id'    => "required|numeric|exists:mysql_users.states,id",
            'city_residence_id' => "required|numeric|exists:mysql_users.cities,id",
            'locality_id'   => "required|numeric|exists:mysql_parks.localidad,Id_Localidad",
            'upz_id'    => "required|numeric|exists:mysql_parks.upz,cod_upz",
            'neighborhood_id'   => "required|numeric|exists:mysql_parks.Barrios,IdBarrio",
            'other_neighborhood_name'   => "nullable|required_if:neighborhood_id,-1|string|between:3,191",
            'address'   => "required|string|between:3,191",
            'stratum'   => "required|numeric|between:0,6",
            'mobile_phone'  => "required|numeric|digits_between:7,10",
            'country_birth_id'  => "required|numeric|exists:mysql_users.countries,id",
            'state_birth_id'    => "required|numeric|exists:mysql_users.states,id",
            'city_birth_id' => "required|numeric|exists:mysql_users.cities,id",
            'sex'   => "required|numeric|exists:mysql_sim.genero,Id_Genero",
            'gender_id' => [
                "nullable",
                Rule::requiredIf(function () use ($age) {
                    return $age > 17;
                }),
                "numeric",
                "exists:mysql_sim.identidad_de_genero,id"
            ],
            'sexual_orientation_id' => [
                "nullable",
                Rule::requiredIf(function () use ($age) {
                    return $age > 17;
                }),
                "numeric",
                "exists:mysql_sim.lgbti,id"
            ],
            'population_group_id'   => "required|numeric|exists:mysql_sim.social_poblacional,id",
            'blood_type'    => "required|numeric|exists:mysql_sim.grupo_sanguineo,Id_GrupoSanguineo",
            'ethnic_group_id'   => "required|numeric|exists:mysql_sim.etnia,Id_Etnia",
            'eps_id'    => [
                "required",
                "numeric",
                Rule::exists("mysql_sim.eps", "Id_Eps")->where('estado', true),
            ],
            'has_disability'    => "required|string|in:SI,NO",
            'disability_id' => "nullable|required_if:has_disability,SI|numeric|exists:mysql_sim.discapacidad,id",
            'contact_name'  => "required|string|between:3,191",
            'contact_phone' => "required|numeric|digits_between:7,10",
            // 'contact_relationship'  => "required|numeric|in:3,7",
        ];

        if (!$this->route('profile')->birthdate || !$this->route('profile')->verified_at) {
            $default = Arr::add($default, 'birthdate',"required|date|date_format:Y-m-d|before:$maxAge|after:$minAge");
        }
        if (!$this->route('profile')->document_type_id) {
            $default = Arr::add($default, 'document_type_id',"required|numeric|exists:mysql_sim.tipo_documento,Id_TipoDocumento");
        }
        if (!$this->route('profile')->document || !$this->route('profile')->verified_at) {
            $default = Arr::add($default, 'document',"required|numeric|digits_between:3,20|unique:{$profile->getConnectionName()}.{$profile->getTable()},document".",{$this->route('profile')->{$id}},$id");
        }
        if (!$this->route('profile')->name || !$this->route('profile')->verified_at) {
            $default = Arr::add($default, 'name',"required|string|between:3,90");
        }
        if (!$this->route('profile')->s_name || !$this->route('profile')->verified_at) {
            $default = Arr::add($default, 's_name',"nullable|string|between:3,90");
        }
        if (!$this->route('profile')->surname || !$this->route('profile')->verified_at) {
            $default = Arr::add($default, 'surname',"required|string|between:3,90");
        }
        if (!$this->route('profile')->s_surname || !$this->route('profile')->verified_at) {
            $default = Arr::add($default, 's_surname',"nullable|string|between:3,90");
        }
        return $default;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'relationship' => "parentesco",
            'birthdate' => "fecha de nacimiento",
            'document_type_id'  => "tipo de documento",
            'document'  => "número de documento",
            'file'  => "archivo de documento de identidad",
            'decoded'   => "decodificación",
            'name'  => "primer nombre",
            's_name'    => "segundo nombre",
            'surname'   => "primer apellido",
            's_surname' => "segundo apellido",
            'country_residence_id'  => "país de residencia",
            'state_residence_id'    => "departamento de residencia",
            'city_residence_id' => "ciudad de residencia",
            'locality_id'   => "localidad",
            'upz_id'    => "upz",
            'neighborhood_id'   => "barrio",
            'other_neighborhood_name'   => "otro nombre del barrio",
            'address'   => "dirección",
            'stratum'   => "estrato",
            'mobile_phone'  => "teléfono",
            'country_birth_id'  => "país de nacimiento",
            'state_birth_id'    => "departamento de nacimiento",
            'city_birth_id' => "ciudad de nacimiento",
            'sex'   => "sexo",
            'gender_id' => "género",
            'sexual_orientation_id' => "orientación sexual",
            'population_group_id'   => "grupo poblacional",
            'blood_type'    => "tipo de sangre",
            'ethnic_group_id'   => "grupo étnico",
            'eps_id'    => "eps",
            'has_disability'    => "tiene discapacidad",
            'disability_id' => "tipo de discapacidad",
            'contact_name'  => "nombre contacto de emergencia",
            'contact_phone' => "teléfono contacto de emergencia",
        ];
    }
}
