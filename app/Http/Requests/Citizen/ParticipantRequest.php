<?php

namespace App\Http\Requests\Citizen;

use App\Entities\CitizenPortal\CitizenSchedule;
use App\Entities\CitizenPortal\FileType;
use App\Entities\CitizenPortal\Modality;
use App\Entities\CitizenPortal\ParticipantRole;
use App\Entities\CitizenPortal\Profile;
use App\Entities\Payments\PsePark;
use App\Entities\Payments\ServiceOffered;
use Illuminate\Foundation\Http\FormRequest;

class ParticipantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $profile = new Profile();
        $roles = new ParticipantRole();
        return [
            'profile_id' => "required|numeric|exists:{$profile->getConnectionName()}.{$profile->getTable()},{$profile->getKeyName()}",
            'role_id'   =>  "required|numeric|exists:{$roles->getConnectionName()}.{$roles->getTable()},{$roles->getKeyName()}",
        ];
    }
}
