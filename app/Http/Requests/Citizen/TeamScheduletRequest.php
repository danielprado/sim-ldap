<?php

namespace App\Http\Requests\Citizen;

use App\Entities\CitizenPortal\Schedule;
use Illuminate\Foundation\Http\FormRequest;

class TeamScheduletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $schedule = new Schedule();
        return [
            'team' => "required|numeric",
            'activity_id'   =>  "required|numeric|exists:{$schedule->getConnectionName()}.{$schedule->getTable()},{$schedule->getKeyName()}",
        ];
    }
}
