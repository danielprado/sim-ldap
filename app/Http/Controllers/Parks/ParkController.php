<?php

namespace App\Http\Controllers\Parks;

use Carbon\Carbon;
use App\Entities\Parks\Gym;
use App\Entities\Parks\Park;
use Illuminate\Http\Request;
use App\Entities\Parks\Grill;
use Illuminate\Http\Response;
use App\Entities\Parks\Endowment;
use App\Entities\Parks\Synthetic;
use App\Entities\Payments\Booking;
use App\Entities\SwimmingPool\Lane;
use App\Entities\Parks\TennisCourt;
use App\Entities\Payments\Schedule;
use App\Entities\Parks\SwimmingPool;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Entities\Parks\ParkEndowment;
use App\Entities\CitizenPortal\Profile;
use App\Entities\Parks\EconomicUsePark;
use App\Entities\Parks\NaturalSoccerField;
use App\Http\Resources\Parks\ParkResource;
use App\Http\Requests\Parks\BookingRequest;
use App\Http\Requests\Parks\ParkFinderRequest;
use App\Http\Resources\Parks\EndowmentResource;
use App\Http\Resources\Parks\ParkFinderResource;
use App\Http\Resources\Parks\ParkEndowmentResource;
use App\Http\Resources\Parks\SyntheticParkResource;
use App\Http\Resources\Parks\EconomicUseParkResource;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ParkController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function finder(ParkFinderRequest $request)
    {
        $parksId = [];
        if (isset($this->query)) {
            $parksId = Park::query()->search($this->query)
                ->orWhere('Id_IDRD', 'like', "%{$this->query}%")
                ->get(['Id'])->pluck('Id')->toArray();
        }
        $parks = $this->setQuery(Park::query(), (new Park)->getSortableColumn($this->column))
            ->select( ['Id', 'Id_IDRD', 'Nombre', 'Direccion', 'Upz', 'Id_Localidad', 'Id_Tipo'] )
            ->when($this->query, function ($query) use($parksId) {
                return $query->whereIn('Id', $parksId);
            })
            ->when(request()->has('locality_id'), function ($query) use ($request) {
                $localities = $request->get('locality_id');
                return is_array($localities)
                    ? $query->whereIn('Id_Localidad', $localities)
                    : $query->where('Id_Localidad', $localities);
            })
            ->when(request()->has('upz_id'), function ($query) use ($request) {
                $upz = $request->get('upz_id');
                return is_array($upz)
                    ? $query->whereIn('Upz', $upz)
                    : $query->where('Upz', $upz);
            })
            ->when(request()->has('neighborhood_id'), function ($query) use ($request) {
                $neighborhood = $request->get('neighborhood_id');
                return is_array($neighborhood)
                    ? $query->whereIn('Id_Barrio', $neighborhood)
                    : $query->where('Id_Barrio', $neighborhood);
            })
            ->when(request()->has('type_id'), function ($query) use ($request) {
                $types = $request->get('type_id');
                return is_array($types)
                    ? $query->whereIn('Id_Tipo', $types)
                    : $query->where('Id_Tipo', $types);
            })
            ->when(request()->has('vigilance'), function ($query) use ($request) {
                return $query->where('Vigilancia', $request->get('vigilance'));
            })
            ->when($request->has('stratum'), function ($query) use ($request) {
                $stratum = $request->get('stratum');
                if (is_array($stratum) && count($stratum) > 0)
                    return $query->whereIn('Estrato', $stratum);

                return $query;
            })
            ->when($request->has('endowment_id'), function ($query) use ($request) {
                $endowment = $request->get('endowment_id');
                return $query->whereHas('park_endowment', function ($query) use ($endowment) {
                    return $query->where('Id_Dotacion', $endowment);
                });
            })
            ->when(request()->has('enclosure'), function ($query) use ($request) {
                $types = $request->get('enclosure');
                return is_array($types)
                    ? $query->whereIn('Cerramiento', $types)
                    : $query->where('Cerramiento', $types);
            })
            ->orderBy((new Park)->getSortableColumn($this->column), $this->order)
            ->paginate($this->per_page);
        return $this->success_response( ParkFinderResource::collection( $parks ) );
    }

    public function show(Park $park)
    {
        return $this->success_response( new ParkResource( $park ) );
        // return $this->error_response(__('validation.handler.park_does_not_exist', ['code' => $park]));
    }

    public function synthetic()
    {
        /*
        $parks = Synthetic::query()
                        ->when( !is_null($this->query), function ($query) {
                            return $query->where('park_endowment_id', 'LIKE', "%$this->query%")
                                        ->orWhere('park_name', 'LIKE', "%$this->query%")
                                        ->orWhere('park_address', 'LIKE', "%$this->query%")
                                        ->orWhere('park_code', 'LIKE', "%$this->query%")
                                        ->orWhere('endowment_description', 'LIKE', "%$this->query%")
                                        ->orWhere('locality', 'LIKE', "%$this->query%")
                                        ->orWhere('upz_code', 'LIKE', "%$this->query%");
                        })
                        ->where('park_endowment_id', '!=', 116)
                        ->paginate($this->per_page);

        return $this->success_response(
            SyntheticParkResource::collection( $parks ),
            ResponseAlias::HTTP_OK,
            SyntheticParkResource::headers()
        );
        */

        $parks = Synthetic::search($this->query)
                            ->where('tag', "fields")
                            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? []),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    public function grill()
    {
        /*
        $parks = Grill::query()
                        ->when( !is_null($this->query), function ($query) {
                            return $query->where('park_endowment_id', 'LIKE', "%$this->query%")
                                        ->orWhere('park_name', 'LIKE', "%$this->query%")
                                        ->orWhere('park_address', 'LIKE', "%$this->query%")
                                        ->orWhere('park_code', 'LIKE', "%$this->query%")
                                        ->orWhere('endowment_description', 'LIKE', "%$this->query%")
                                        ->orWhere('locality', 'LIKE', "%$this->query%")
                                        ->orWhere('upz_code', 'LIKE', "%$this->query%");
                        })
                        // where Provisional debido a que están mal categorizados
                        ->whereNotIn('park_endowment_id', [15159, 16006])
                        // fin del where Provisional
                        ->paginate($this->per_page);
        return $this->success_response(
            SyntheticParkResource::collection( $parks ),
            ResponseAlias::HTTP_OK,
            SyntheticParkResource::headers()
        );
        */
        $parks = Grill::search($this->query)
            ->where('tag', "grills")
            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? []),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    public function gym()
    {

        $parks = Gym::search($this->query)
            ->where('tag', "gyms")
            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? []),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }


    public function naturalSoccerField()
    {
        $parks = NaturalSoccerField::search($this->query)
            ->where('tag', "naturalSoccerFields")
            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? []),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    /**
     *
     */
    public function swimmingPools()
    {
        $parks = SwimmingPool::search($this->query)
            ->where('tag', "swimmingPools")
            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? []),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    public function tennisCourt()
    {
        $parks = TennisCourt::search($this->query)
            ->where('tag', "tennisCourts")
            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? []),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    public function schedule(ParkEndowment $endowment, BookingRequest $request)
    {
        $time = strtotime(now()->startOfWeek()->addDay()->format('Y-m-d'));
        $newformat = date('N',$time);
        $fecha = date('Y-m-d',$time);
        $add_seven = Carbon::parse($fecha)->addDays(7);
        $fechas = [];
        $is_swimming_pool = SwimmingPool::where('park_endowment_id', $endowment->Id)->exists();
        if ($is_swimming_pool){
            return $this->scheduleSwimPools($endowment, $request, $fecha, $add_seven);
        } else {
            $is_gym = Gym::query()->where('park_endowment_id', $endowment->Id)->exists();
            if ($is_gym){
                return $this->scheduleGym($endowment, $request, $fecha, $add_seven);
            } else {
                $actuales =  Booking::query()->where('id_dotacion',$endowment->Id)
                    ->where('fecha', '>=', $fecha)
                    ->where('fecha', '<=', $add_seven)
                    ->whereHas('payment',function($query){
                        return $query->whereIn('estado_id', [1, 2]);
                    })
                    ->with('payment')
                    ->get();
                $actualesSinInicioPago =  Booking::where('id_dotacion',$endowment->Id)
                    ->where('created_at', '>', now()->subMinutes(5))
                    ->where('fecha', '>=', $fecha)
                    ->where('fecha', '<=', $add_seven)
                    ->doesntHave('payment')
                    ->get();

                $f = Carbon::parse($fecha)->timezone("America/Bogota");
                $days = [
                    $f->format('Y-m-d'),
                    $f->addDay()->format('Y-m-d'),
                    $f->addDay()->format('Y-m-d'),
                    $f->addDay()->format('Y-m-d'),
                    $f->addDay()->format('Y-m-d'),
                    $f->addDay()->format('Y-m-d'),
                    $f->addDay()->format('Y-m-d'),
                ];
                foreach ($days as $day) {
                    $rango = Schedule::query()->where('id_dotacion',$endowment->Id)
                        ->where('dia', intval(date('N',strtotime($day))))
                        ->get();

                    foreach ($rango as $value) {
                        $horai = date('g:i A', strtotime($value->horai));
                        $horaf = date('g:i A', strtotime($value->horaf));
                        $fechas[] =    array(
                            'title' =>'Espacio Reservable',
                            'color' => 'primary',
                            'can'   => true,
                            'start' =>"{$day}T$value->horai",
                            'end'   =>"{$day}T$value->horaf",
                            'description'=>'Hora de uso '.$horai.' a '.$horaf ,
                            'category' => 'Disponible',
                        );
                    }
                }

                foreach ($actuales as $value) {
                    $horai = date('g:i A', strtotime($value->hora_inicio));
                    $horaf = date('g:i A', strtotime($value->hora_fin));
                    $fechas[] =    array(
                        'title' => $value->payment->estado_id == 2 ? 'Espacio Reservado' : 'Procesando reserva',
                        'color' => $value->payment->estado_id == 2 ? 'error' : 'warning',
                        'can'   => false,
                        'start' =>"{$value->fecha->format('Y-m-d')}T$value->hora_inicio",
                        'end'   =>"{$value->fecha->format('Y-m-d')}T$value->hora_fin",
                        'description'=>'Hora de uso '.$horai.' a '.$horaf ,
                        'category' => 'Reservado',
                    );
                }
                foreach ($actualesSinInicioPago as $value) {
                    $horai = date('g:i A', strtotime($value->hora_inicio));
                    $horaf = date('g:i A', strtotime($value->hora_fin));
                    $fechas[] =    array(
                        'title' => 'Procesando reserva',
                        'color'   => "warning",
                        'can'   => false,
                        'start' =>"{$value->fecha->format('Y-m-d')}T$value->hora_inicio",
                        'end'   =>"{$value->fecha->format('Y-m-d')}T$value->hora_fin",
                        'description'=>'Hora de uso '.$horai.' a '.$horaf ,
                        'category' => 'Reservado',
                    );
                }
            }

        }

        return $this->success_message($fechas, 200, 200, new ParkEndowmentResource($endowment));
    }

    public function scheduleSwimPools(ParkEndowment $endowment, BookingRequest $request, $init_date, $final_date)
    {
        $dates = [];
        $lane_id = $request->get('lane');
        $document = $request->get('document');
        $lane = Lane::find($lane_id);
        if (!isset($lane)) {
            return $this->success_message($dates, 200, 200, new ParkEndowmentResource($endowment));
        }

        $commonConditions = function ($query) use ($endowment, $lane_id, $init_date, $final_date) {
            $query->where('id_dotacion', $endowment->Id)
                  ->where('id_carril', $lane_id)
                  ->whereBetween('fecha', [$init_date, $final_date]);
        };

        $current_bookings_with_payment = Booking::query()
            ->where($commonConditions)
            ->whereHas('payment', function ($query) {
                $query->whereIn('estado_id', [1, 2]);
            })
            ->with('payment')
            ->get();

        $current_bookings_without_payment = Booking::query()
            ->where($commonConditions)
            ->where('created_at', '>', now()->subMinutes(5))
            ->doesntHave('payment')
            ->get();

        $quota = $lane->cupo;

        $start = Carbon::parse($init_date)->timezone("America/Bogota");
        $f = Carbon::parse($start)->timezone("America/Bogota");

        $days = [
            $f->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
        ];
        $now = now(); // Hora actual
        $startAfter = $now->addDay(); // 24 horas después del momento actual

        foreach ($days as $day) {
            $range = Schedule::query()->where('id_dotacion', $endowment->Id)
                        ->where('id_carril', $lane_id)
                        ->where('dia', intval(date('N', strtotime($day))))
                        ->get();

            foreach ($range as $value) {
                $init_hour = date('g:i A', strtotime($value->horai));
                $final_hour = date('g:i A', strtotime($value->horaf));
                $status = '';
                $color = '';
                $can = true;
                $is_after_24_hours = true;

                 // Calcula el inicio de la reserva
                $dateTimeStart = Carbon::parse("{$day} {$value->horai}", 'America/Bogota');

                // Si la reserva está dentro de las próximas 24 horas
                if ($dateTimeStart->lessThan($startAfter)) {
                    // Siguiente iteracion para no guardar la franja horaria
                    $is_after_24_hours = false;
                }


                $current_bookings_with_payment_by_hour = $current_bookings_with_payment->filter(function ($booking) use ($value,$day) {
                    return $booking->fecha->format('Y-m-d') == $day && $booking->hora_inicio == $value->horai;
                });
                $current_bookings_without_payment_by_hour = $current_bookings_without_payment->filter(function ($booking) use ($value,$day) {
                    return $booking->fecha->format('Y-m-d') == $day && $booking->hora_inicio == $value->horai;
                });

                $total_bookings_for_hour = $current_bookings_with_payment_by_hour->count() + $current_bookings_without_payment_by_hour->count();

                if ($total_bookings_for_hour >= $quota) {
                    if (
                        $current_bookings_with_payment_by_hour->contains(function ($booking) use ($document) {
                            return $booking->payment->estado_id === "2" && $booking->documento === $document;
                        })
                    ) {
                        $status = 'Cupo Reservado';
                        $color = 'error';
                        $can = false;
                    } elseif (
                        $current_bookings_with_payment_by_hour->contains(function ($booking) use ($document) {
                            return $booking->payment->estado_id === "1" && $booking->documento === $document;
                        }) ||
                        $current_bookings_without_payment_by_hour->contains(function ($booking) use ($document) {
                            return $booking->documento === $document;
                        })
                    ) {
                        $status = 'Procesando Cupo';
                        $color = 'warning';
                        $can = false;
                    } else {
                        if (
                            $current_bookings_with_payment_by_hour->contains(function ($booking) {
                                return $booking->payment->estado_id === "1";
                            }) ||
                            $current_bookings_without_payment_by_hour->count() > 0
                        ) {
                            $status = 'Procesando Reservas';
                            $color = 'warning';
                        } else {
                            $status = 'Espacio Reservado';
                            $color = 'error';
                        }
                        $can = false;
                    }
                } else {
                    if (
                        $current_bookings_with_payment_by_hour->contains(function ($booking) use ($document) {

                            return $booking->payment->estado_id === "2" && $booking->documento === $document;
                        })
                    ) {
                        $status = 'Cupo Reservado';
                        $color = 'error';
                        $can = false;
                    } elseif (
                        $current_bookings_with_payment_by_hour->contains(function ($booking) use ($document) {
                            return $booking->payment->estado_id === "1" && $booking->documento === $document;
                        }) ||
                        $current_bookings_without_payment_by_hour->contains(function ($booking) use ($document) {
                            return $booking->documento === $document;
                        })
                    ) {
                        $status = 'Procesando Cupo';
                        $color = 'warning';
                        $can = false;
                    } else {
                        $status = 'Espacio Reservable';
                        $color = 'primary';
                        $can = true;
                    }
                }

                if (!$is_after_24_hours) {
                    $can = false;
                    $status = 'Espacio No Disponible';
                }

                $dates[] = [
                    'quota' => $quota,
                    'used_quota' => $total_bookings_for_hour,
                    'title' => $status,
                    'color' => $color,
                    'can'   => $can,
                    'start' => "{$day}T{$value->horai}",
                    'end'   => "{$day}T{$value->horaf}",
                    'description' => "Hora de uso $init_hour a $final_hour",
                    'category' => $status === 'Espacio Reservado' ? 'Reservado' : 'Disponible',
                ];
            }
        }

        return $this->success_message($dates, 200, 200, new ParkEndowmentResource($endowment));
    }

    public function scheduleGym(ParkEndowment $endowment, BookingRequest $request, $init_date, $final_date)
    {
        $dates = [];
        $document = $request->get('document');

        $commonConditions = function ($query) use ($endowment, $init_date, $final_date) {
            $query->where('id_dotacion', $endowment->Id)
                  ->whereBetween('fecha', [$init_date, $final_date]);
        };

        $current_bookings_with_payment = Booking::query()
            ->where($commonConditions)
            ->whereHas('payment', function ($query) {
                $query->whereIn('estado_id', [1, 2]);
            })
            ->with('payment')
            ->get();

        $current_bookings_without_payment = Booking::query()
            ->where($commonConditions)
            ->where('created_at', '>', now()->subMinutes(5))
            ->doesntHave('payment')
            ->get();

        $quota = 15;

        $start = Carbon::parse($init_date)->timezone("America/Bogota");
        $days = [];
        $f = Carbon::parse($start)->timezone("America/Bogota");

        $days = [
            $f->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
            $f->addDay()->format('Y-m-d'),
        ];

        foreach ($days as $day) {
            $range = Schedule::query()->where('id_dotacion', $endowment->Id)
                        ->where('dia', intval(date('N', strtotime($day))))
                        ->get();
            foreach ($range as $value) {
                $time_intervals = $this->generateTimeIntervals($value->horai, $value->horaf);
                foreach($time_intervals as $time){
                    $init_hour = date('g:i A', strtotime($time['startTime']));
                    $final_hour = date('g:i A', strtotime($time['endTime']));
                    $status = '';
                    $color = '';
                    $can = true;
                    $current_bookings_with_payment_by_hour = $current_bookings_with_payment->filter(function ($booking) use ($time, $day) {
                        $booking_time_intervals = $this->generateTimeIntervals($booking->hora_inicio, $booking->hora_fin);
                        $matchingInterval = collect($booking_time_intervals)->contains(function ($interval) use ($time) {
                            return $interval['startTime'] === $time['startTime'] && $interval['endTime'] === $time['endTime'];
                        });
                        return $booking->fecha->format('Y-m-d') === $day && $matchingInterval;
                    });
                    $current_bookings_without_payment_by_hour = $current_bookings_without_payment->filter(function ($booking) use ($time, $day) {
                        $booking_time_intervals = $this->generateTimeIntervals($booking->hora_inicio, $booking->hora_fin);
                        $matchingInterval = collect($booking_time_intervals)->contains(function ($interval) use ($time) {
                            return $interval['startTime'] === $time['startTime'] && $interval['endTime'] === $time['endTime'];
                        });
                        return $booking->fecha->format('Y-m-d') === $day && $matchingInterval;
                    });

                    $total_bookings_for_hour = $current_bookings_with_payment_by_hour->count() + $current_bookings_without_payment_by_hour->count();

                    if ($total_bookings_for_hour >= $quota) {
                        if (
                            $current_bookings_with_payment_by_hour->contains(function ($booking) use ($document) {
                                return $booking->payment->estado_id === "2" && $booking->documento === $document;
                            })
                        ) {
                            $status = 'Cupo Reservado';
                            $color = 'error';
                            $can = false;
                        } elseif (
                            $current_bookings_with_payment_by_hour->contains(function ($booking) use ($document) {
                                return $booking->payment->estado_id === "1" && $booking->documento === $document;
                            }) ||
                            $current_bookings_without_payment_by_hour->contains(function ($booking) use ($document) {
                                return $booking->documento === $document;
                            })
                        ) {
                            $status = 'Procesando Cupo';
                            $color = 'warning';
                            $can = false;
                        } else {
                            if (
                                $current_bookings_with_payment_by_hour->contains(function ($booking) {
                                    return $booking->payment->estado_id === "1";
                                }) ||
                                $current_bookings_without_payment_by_hour->count() > 0
                            ) {
                                $status = 'Procesando Reservas';
                                $color = 'warning';
                            } else {
                                $status = 'Espacio Reservado';
                                $color = 'error';
                            }
                            $can = false;
                        }
                    } else {
                        if (
                            $current_bookings_with_payment_by_hour->contains(function ($booking) use ($document) {

                                return $booking->payment->estado_id === "2" && $booking->documento === $document;
                            })
                        ) {
                            $status = 'Cupo Reservado';
                            $color = 'error';
                            $can = false;
                        } elseif (
                            $current_bookings_with_payment_by_hour->contains(function ($booking) use ($document) {
                                return $booking->payment->estado_id === "1" && $booking->documento === $document;
                            }) ||
                            $current_bookings_without_payment_by_hour->contains(function ($booking) use ($document) {
                                return $booking->documento === $document;
                            })
                        ) {
                            $status = 'Procesando Cupo';
                            $color = 'warning';
                            $can = false;
                        } else {
                            $status = 'Espacio Reservable';
                            $color = 'primary';
                            $can = true;
                        }
                    }

                    $dates[] = [
                        'quota' => $quota,
                        'used_quota' => $total_bookings_for_hour,
                        'title' => $status,
                        'color' => $color,
                        'can'   => $can,
                        'start' => "{$day}T{$time['startTime']}",
                        'end'   => "{$day}T{$time['endTime']}",
                        'description' => "Hora de uso $init_hour a $final_hour",
                        'category' => $status === 'Espacio Reservado' ? 'Reservado' : 'Disponible',
                    ];
                }
            }
        }

        return $this->success_message($dates, 200, 200, new ParkEndowmentResource($endowment));
    }

    public function generateTimeIntervals($startTime, $endTime)
    {
        $currentTime = new \DateTime($startTime);
        $finalTime = new \DateTime($endTime);

        $timeIntervals = [];

        while ($currentTime < $finalTime) {
            $nextTime = clone $currentTime;
            $nextTime->modify('+1 hour');

            $timeIntervals[] = [
                'startTime' => $currentTime->format('H:i:s'),
                'endTime' => $nextTime->format('H:i:s'),
            ];

            $currentTime = $nextTime;
        }

        return $timeIntervals;
    }

    public function diagrams()
    {
        $parks = Park::query()
            ->select( ['Id', 'Id_IDRD', 'Nombre', 'Direccion', 'Upz', 'Id_Localidad', 'Id_Tipo'] )
            ->where('Estado', 1)
            ->where('Id', '!=', 1)->paginate($this->per_page);
        return $this->success_response( ParkFinderResource::collection( $parks ) );
    }

    public function economic($park)
    {
        $data = EconomicUsePark::with('economic_use')->whereHas('economic_use')->where('IdParque', $park)->get();
        if ( $data ) {
            return $this->success_response( EconomicUseParkResource::collection( $data ) );
        }

        return $this->error_response(__('validation.handler.park_does_not_exist', ['code' => $park]));
    }

    public function sectors($park)
    {
        $park = Park::with('sectors.endowments')
            ->select( ['Id', 'Id_IDRD', 'Nombre', 'Direccion', 'Upz', 'Id_Localidad', 'Id_Tipo', 'Estado'] )
            ->where('Id_IDRD', $park)
            ->where('Estado', true)
            ->first();
        if ( $park ) {
            $type = $park->sectors->where('tipo', 1)->count();
            return $this->success_message([
                'park'  =>  new ParkFinderResource($park),
                'type'  =>  $type,
            ]);
        }
        return $this->error_response(__('validation.handler.resource_not_found_url'), Response::HTTP_NOT_FOUND);
    }

    public function fields($park, $equipment)
    {
        $parks = ParkEndowment::whereHas('endowment', function ($query) use ($equipment) {
                return $query->where('Id_Equipamento', $equipment);
            })
            ->where('Id_Parque', $park)
            ->paginate($this->per_page);
        return $this->success_response( EndowmentResource::collection( $parks ) );
    }
}
