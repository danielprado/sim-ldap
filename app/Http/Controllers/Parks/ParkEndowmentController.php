<?php

namespace App\Http\Controllers\Parks;

use App\Entities\Parks\Endowment;
use App\Entities\Parks\Park;
use App\Entities\Parks\ParkEndowment;
use App\Http\Resources\Parks\EndowmentResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParkEndowmentController extends Controller
{
    public function index(Park $park)
    {
        return $this->success_response(
            EndowmentResource::collection($park->park_endowment()->paginate($this->per_page))
        );
    }

    public function show(Park $park, ParkEndowment $endowment)
    {
        return $this->success_response(
            new EndowmentResource($endowment)
        );
    }
}
