<?php

namespace App\Http\Controllers\Parks;

use App\Entities\Parks\Location;
use App\Http\Resources\Parks\LocationResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    public function index()
    {
        return $this->success_response( LocationResource::collection( Location::all() ) );
    }
}
