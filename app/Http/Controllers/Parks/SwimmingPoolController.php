<?php

namespace App\Http\Controllers\Parks;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\SwimmingPool\LaneResource;
use App\Entities\SwimmingPool\Lane;
use App\Entities\SwimmingPool\SwimPool;

class SwimmingPoolController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function getLanesByPool(Request $request)
    {
        $pool = SwimPool::where('id_dotacion', $request->get('id_dotacion'))->first();
        $lanes = Lane::with('level', 'direction', 'schedules')
        ->where('id_piscina', $pool->id_piscina)
        ->get()
        ->filter(function ($lane) {
            return $lane->schedules->isNotEmpty();
        });
        return $this->success_response(
            LaneResource::collection($lanes)
        );
    }
}
