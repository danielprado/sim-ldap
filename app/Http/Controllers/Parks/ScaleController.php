<?php

namespace App\Http\Controllers\Parks;

use App\Entities\Parks\Scale;
use App\Http\Resources\Parks\ScaleResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScaleController extends Controller
{
    public function index()
    {
        return $this->success_response( ScaleResource::collection( Scale::all() ) );
    }
}
