<?php

namespace App\Http\Controllers\Parks;

use App\Entities\Parks\Gym;
use App\Entities\Parks\Grill;
use Illuminate\Http\Response;
use App\Entities\Parks\Synthetic;
use App\Entities\Parks\TennisCourt;
use App\Entities\Parks\SwimmingPool;
use App\Http\Controllers\Controller;
use App\Entities\Parks\NaturalSoccerField;
use App\Http\Resources\Parks\SyntheticParkResource;


class ParkControllerAppInfo extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function synthetic()
    {
        $parks = Synthetic::search($this->query)
                            ->where('tag', "fields")
                            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? [])->filter(function ($item) {
                return $item['for_booking'] > 0;
            })->values()->toArray(),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    public function grill()
    {
        $parks = Grill::search($this->query)
            ->where('tag', "grills")
            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? [])->filter(function ($item) {
                return $item['for_booking'] > 0;
            })->values()->toArray(),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    public function gym()
    {

        $parks = Gym::search($this->query)
            ->where('tag', "gyms")
            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? [])->filter(function ($item) {
                return $item['for_booking'] > 0;
            })->values()->toArray(),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    public function naturalSoccerField()
    {
        $parks = NaturalSoccerField::search($this->query)
            ->where('tag', "naturalSoccerFields")
            ->paginateRaw($this->per_page);

        return $this->success_message(
             SyntheticParkResource::transformMeili($parks->items()['hits'] ?? [])->filter(function ($item) {
                return $item['for_booking'] > 0;
            })->values()->toArray(),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    public function swimmingPools()
    {
        $parks = SwimmingPool::search($this->query)
            ->where('tag', "swimmingPools")
            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? [])->filter(function ($item) {
                return $item['for_booking'] > 0;
            })->values()->toArray(),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }

    public function tennisCourt()
    {
        $parks = TennisCourt::search($this->query)
            ->where('tag', "tennisCourts")
            ->paginateRaw($this->per_page);

        return $this->success_message(
            SyntheticParkResource::transformMeili($parks->items()['hits'] ?? [])->filter(function ($item) {
                return $item['for_booking'] > 0;
            })->values()->toArray(),
            Response::HTTP_OK,
            Response::HTTP_OK,
            SyntheticParkResource::headers(),
            SyntheticParkResource::paginate($parks)
        );
    }
}
