<?php

namespace App\Http\Controllers\Orfeo;

use App\Entities\CitizenPortal\Procedure;
use App\Entities\CitizenPortal\ProfileEntity;
use App\Entities\Clubes\Club;
use App\Entities\Clubes\Sport;
use App\Entities\Clubes\SportClub;
use App\Entities\Schools\School;
use App\Entities\Schools\SportSchool;
use App\Http\Requests\Orfeo\OrfeoRequest;
use App\Http\Requests\Orfeo\OrfeoSchoolRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use SendGrid\Mail\Attachment;
use SendGrid\Mail\Mail;

class OrfeoController extends Controller
{
    const CLUB = 1;
    const SCHOOL = 2;

    public function index2(OrfeoRequest $request)
    {
        try {
            $subject = $this->subjects()->where('id', $request->get('request_type'))->first();
            $mailer = new Mail();
            $mailer->setFrom(
                "portal.ciudadano@idrd.gov.co",
                "Portal Ciudadano"
            );
            $mailer->addTo(
                'brayan.aldana@idrd.gov.co',
                "Correspondencia IDRD"
            );
            $mailer->addCc(
                $request->get('email'),
                $request->get('president')
            );
            foreach ($this->fileKeys() as $file) {
                if ($request->hasFile($file)) {
                    $mailer->addAttachment(
                        base64_encode($request->file($file)->getRealPath()),
                        $request->file($file)->getMimeType(),
                        $request->file($file)->getClientOriginalName(),
                        "attachment"
                    );
                }
            }
            /*
            $mailer->setTemplateId("d-a1669232a461435dac420fa15bd47aa5");
            $mailer->addDynamicTemplateData([
                'club'      =>  $request->get('club'),
                'email'     =>  $request->get('email'),
                'president' =>  $request->get('president'),
                'phone'     =>  $request->get('phone'),
                'request_type'  =>  $subject['name'] ?? "",
                'request_type_description'  =>  $subject['description'] ?? null,
                'club_type'     =>  $request->get('club_type'),
            ]);
            */
            $request_type_description = $subject['description'] ?? "Sin descripción";
            $request_type = $subject['name'] ?? "";

            $mailer->setSubject("Trámite Clubes: {$request->get('club')}");
            $accept = (bool) $request->get('accept_media', false);
            $text = $accept ? "Acepto": "No acepto";

            $template = (new MailMessage())
                ->greeting("¡Hola {$request->get('president')}!")
                ->line("Hemos registrado tu solicitud con éxito. Recibirás un correo electrónico de respuesta con el número de radicado y código web para realizar la consulta y seguimiento al trámite.")
                ->line("# Datos registrados")
                ->line("Nombre del club: **{$request->get('club')}**")
                ->line("Presidente: **{$request->get('president')}**")
                ->line("Teléfono: **{$request->get('phone')}**")
                ->line("Email: **{$request->get('email')}**")
                ->line("Tipo de club: **{$request->get('club_type')}**")
                ->line("Tipo de solicitud: **$request_type**")
                ->line("Medio de notificación: **{$text} que el medio de comunicación sea al correo electrónico registrado**")
                ->line("Descripción tipo de solicitud: **{$request_type_description}**")
                ->action('Hacer Seguimiento', "http://localhost:8080/portal-ciudadano-dev/parques/radicados")
                ->line("Si no realizaste ningún registro no se require ninguna acción")
                ->salutation(Lang::getFromJson('Citizen Portal - IDRD'))
                ->render();

            $mailer->addContent(
                "text/html",
                (string) $template
            );
            $sendgrid = new \SendGrid(config("mail.sendgrid.api"));
            $response = $sendgrid->send($mailer);
            return $this->success_message(
                "Hemos registrado tu solicitud con éxito.",
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                "Se enviará un correo electrónico con el número de radicado y código web con el que podrá realizar seguimiento a su proceso."
            );
        } catch (\Exception $exception) {
            return $this->error_response(
              "No pudimos registrar tu solicitud, por favor intenta más tarde.",
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }

    public function index(OrfeoRequest $request)
    {
        try {
            $club = Club::query()
                ->where('email', $request->get('email'))
                ->where('request_type', $request->get('request_type'))
                ->exists();

            if ($club) {
                return $this->error_response(
                    "Ya se realizó un trámite con el tipo de solicitud seleccionado, realice el seguimiento con el código web y número de radicado."
                );
            }

            $form = new Club();
            $form->fill(
                $request->only([
                    'club',
                    'email',
                    'president',
                    'phone',
                    'request_type',
                    'club_type',
                    'accept_media',
                ])
            );
            $subject = $this->subjects()->where('id', $request->get('request_type'))->first();
            $mailer = new Mail();
            $mailer->setFrom(
                "portal.ciudadano@idrd.gov.co",
                "Portal Ciudadano"
            );
            $mailer->addTo(
                config("mail.clubes.from"),
                config("mail.clubes.name")
            );
            $mailer->addCc(
                $request->get('email'),
                $request->get('president')
            );
            foreach ($this->fileKeys() as $file) {
                if ($request->hasFile($file)) {
                    $file_name = random_img_name()."_".now()->format("YmdHis").".".$request->file($file)->getClientOriginalExtension();
                    $request->file($file)->storeAs("clubes", $file_name, [
                        "disk" => "local"
                    ]);
                    $form->{$file} = $file_name;
                    $mailer->addAttachment(
                        base64_encode(Storage::disk('local')->get("/clubes/$file_name")),
                        $request->file($file)->getMimeType(),
                        $request->file($file)->getClientOriginalName(),
                        "attachment"
                    );
                }
            }
            $accept = (bool) $request->get('accept_media', false);
            $text = $accept ? "Acepto": "No acepto";
            $mailer->setTemplateId("d-a1669232a461435dac420fa15bd47aa5");
            $sports = collect($request->get("sports", []))
                    ->implode("name", ", ");
            $mailer->addDynamicTemplateDatas([
                'club'      =>  $request->get('club'),
                'email'     =>  $request->get('email'),
                'president' =>  $request->get('president'),
                'phone'     =>  $request->get('phone'),
                'request_type'  =>  $subject['name'] ?? "",
                'request_type_description'  =>  $subject['description'] ?? null,
                'club_type'     =>  $request->get('club_type'),
                'sports' => $sports,
                'accept_media'     =>  $text,
            ]);
            $mailer->setSubject("Trámite Clubes: {$request->get('club')}");

            $sendgrid = new \SendGrid(config("mail.sendgrid.api"));
            $response = $sendgrid->send($mailer);

            $form->status = $response->statusCode();
            $form->response = $response->body();
            $form->save();

            $procedure = new Procedure([
                'procedure_type_id' => Procedure::RECONOCIMIENTO_DEPORTIVO_A_CLUBES,
                'profile_id' => $request->get('profile_id'),
                'entity_profile_id' => $request->get('entity_profile_id'),
                'club_id' => $form->id,
                'state' => 0
            ]);
            $procedure->save();

            $organization = ProfileEntity::query()
                ->where('email', $request->get('email'))
                ->first();

            $organization->club_id = $form->id;
            $organization->save();

            DB::commit();
            foreach ($request->get("sports", []) as $sport) {
                if (isset($sport['id'])) {
                    $sports = new SportClub();
                    $sports->club_id = $form->id;
                    $sports->sport_id = $sport['id'];
                    $sports->save();
                }
            }
            return $this->success_message(
                "Hemos registrado tu solicitud con éxito.",
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                "Se enviará un correo electrónico con el número de radicado y código web con el que podrá realizar seguimiento a su proceso."
            );
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->error_response(
                "No pudimos registrar tu solicitud, por favor intenta más tarde.",
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception
            );
        }
    }

    public function show(Request $request)
    {
        if ($request->has('email') && $request->has('request_type')) {
            if ($request->get('request_type') == self::CLUB) {
                $query = Club::query()
                    ->where("email", $request->get("email"))
                    ->latest()
                    ->first([
                        'id',
                        'club',
                        'president',
                        'phone',
                        'request_type',
                        'club_type',
                        'accept_media',
                    ]);
                $sports = [];
                if (isset($query->id)) {
                    $sports = SportClub::query()
                        ->where("club_id", $query->id)
                        ->latest()
                        ->get()
                        ->pluck("sport_id")
                        ->toArray();
                    if (count($sports) > 0) {
                        $sports = Sport::query()->whereKey($sports)->get()->map(function($sport) {
                            return [
                                "id"    => (int) $sport->PK_I_ID_DEPORTE,
                                "name"  => toUpper($sport->V_NOMBRE_DEPORTE)
                            ];
                        })->toArray();
                    }
                }
                $query = isset($query->id) ? $query->toArray() : [];
                $response = [];
                return $this->success_message(array_merge($response, $query, ["sports" => $sports]));
            }
            if ($request->get('request_type') == self::SCHOOL) {
                $query = School::query()
                    ->where("email", $request->get("email"))
                    ->latest()
                    ->first([
                        'id',
                        'school',
                        'director',
                        'phone',
                        'request_type',
                        'school_type',
                        'accept_media',
                    ]);

                $sports = [];
                if (isset($query->id)) {
                    $sports = SportSchool::query()
                        ->where("school_id", $query->id)
                        ->latest()
                        ->get()
                        ->pluck("sport_id")
                        ->toArray();
                    if (count($sports) > 0) {
                        $sports = Sport::query()->whereKey($sports)->get()->map(function($sport) {
                            return [
                                "id"    => (int) $sport->PK_I_ID_DEPORTE,
                                "name"  => toUpper($sport->V_NOMBRE_DEPORTE)
                            ];
                        })->toArray();
                    }
                }
                $query = isset($query->id) ? $query->toArray() : [];
                $response = [];
                return $this->success_message(array_merge($response, $query, ["sports" => $sports]));
            }
        }

        return $this->success_message([]);
    }

    public function storeSchool(OrfeoSchoolRequest $request): JsonResponse
    {
        try {
            $school = School::query()
                ->where('email', $request->get('email'))
                ->where('request_type', $request->get('request_type'))
                ->exists();

            if ($school) {
                return $this->error_response(
                    "Ya se realizó un trámite con el tipo de solicitud seleccionado, realice el seguimiento con el código web y número de radicado."
                );
            }

            $form = new School();
            $form->fill(
                $request->only([
                    'school',
                    'email',
                    'director',
                    'phone',
                    'request_type',
                    'school_type',
                    'accept_media',
                ])
            );
            $subject = $this->subjectsSchool()->where('id', $request->get('request_type'))->first();
            $mailer = new Mail();
            $mailer->setFrom(
                "portal.ciudadano@idrd.gov.co",
                "Portal Ciudadano"
            );
            $mailer->addTo(
                config("mail.clubes.from"),
                config("mail.clubes.name")
            );
            $mailer->addCc(
                $request->get('email'),
                $request->get('director')
            );
            foreach ($this->fileKeysSchool() as $file) {
                if ($request->hasFile($file)) {
                    $file_name = random_img_name()."_".now()->format("YmdHis").".".$request->file($file)->getClientOriginalExtension();
                    $request->file($file)->storeAs("schools", $file_name, [
                        "disk" => "local"
                    ]);
                    $form->{$file} = $file_name;
                    $mailer->addAttachment(
                        base64_encode(Storage::disk('local')->get("schools/$file_name")),
                        $request->file($file)->getMimeType(),
                        $request->file($file)->getClientOriginalName(),
                        "attachment"
                    );
                }
            }
            $accept = (bool) $request->get('accept_media', false);
            $text = $accept ? "Acepto": "No acepto";
            $mailer->setTemplateId("d-9a081fa01e6d41f09f23c2618263d380");
            $sports = collect($request->get("sports", []))
                ->implode("name", ", ");
            $mailer->addDynamicTemplateDatas([
                'school'    =>  $request->get('school'),
                'email'     =>  $request->get('email'),
                'director'  =>  $request->get('director'),
                'phone'     =>  $request->get('phone'),
                'request_type'  =>  $subject['name'] ?? "",
                'request_type_description'  =>  $subject['description'] ?? null,
                'school_type'   =>  $request->get('school_type'),
                'sports' => $sports,
                'accept_media'     =>  $text,
            ]);
            $mailer->setSubject("Trámite Escuelas de Formación: {$request->get('school')}");

            $sendgrid = new \SendGrid(config("mail.sendgrid.api"));
            $response = $sendgrid->send($mailer);

            $form->status = $response->statusCode();
            $form->response = $response->body();
            $form->save();

            foreach ($request->get("sports", []) as $sport) {
                if (isset($sport['id'])) {
                    $sports = new SportSchool();
                    $sports->school_id = $form->id;
                    $sports->sport_id = $sport['id'];
                    $sports->save();
                }
            }

            return $this->success_message(
                "Hemos registrado tu solicitud con éxito.",
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                "Se enviará un correo electrónico con el número de radicado y código web con el que podrá realizar seguimiento a su proceso."
            );
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->error_response(
                "No pudimos registrar tu solicitud, por favor intenta más tarde.",
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception
            );
        }
    }


    public function subjects()
    {
        return collect([
            0 => [
                'id' => 1,
                'name' => 'REQUISITOS PARA EL OTORGAMIENTO DEL RECONOCIMIENTO DEPORTIVO',
                'to' => [
                    0 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            1 => [
                'id' => 2,
                'name' => 'REQUISITOS PARA EL OTORGAMIENTO DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                ],
            ],
            2 => [
                'id' => 3,
                'name' => 'REQUISITOS PARA LA RENOVACIÒN DEL RECONOCIMIENTO DEPORTIVO',
                'to' => [
                    0 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            3 => [
                'id' => 4,
                'name' => 'REQUISITOS PARA LA RENOVACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                ],
            ],
            4 => [
                'id' => 5,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO',
                'description' => 'CUANDO SE PRODUZCA UNA NUEVA DESIGNACIÓN DEL PRESIDENTE DEL CLUB',
                'to' => [
                    0 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            5 => [
                'id' => 6,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO',
                'description' => 'CUANDO SE PRODUZCA EL CAMBIO DE NOMBRE DEL CLUB',
                'to' => [
                    0 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            6 => [
                'id' => 7,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO',
                'description' => 'CUANDO SE PRODUZCA EL CAMBIO DEL DEPORTE O LA ADICIÓN DE DISCIPLINAS DEPORTIVAS A UN CLUB',
                'to' => [
                    0 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            7 => [
                'id' => 8,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
                'description' => 'CUANDO SE PRODUZCA UNA NUEVA DESIGNACIÓN DEL PRESIDENTE DEL CLUB',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                ],
            ],
            8 => [
                'id' => 9,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
                'description' => 'CUANDO SE PRODUZCA EL CAMBIO DE NOMBRE DEL CLUB',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                ],
            ],
            9 => [
                'id' => 10,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
                'description' => 'CUANDO SE PRODUZCA EL CAMBIO DEL DEPORTE O LA ADICIÓN DE DISCIPLINAS DEPORTIVAS',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                ],
            ],
            10 => [
                'id' => 11,
                'name' => 'REQUISITOS PARA ACTUALIZACIÓN EXPEDIENTE',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                    1 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            11 => [
                'id' => 12,
                'name' => 'SUBSANE',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                    1 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            12 => [
                'id' => 13,
                'name' => 'DERECHO DE PETICIÓN',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                    1 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            13 => [
                'id' => 14,
                'name' => 'SOLICITUD DE INFORMACIÓN',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                    1 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            14 => [
                'id' => 15,
                'name' => 'EXPEDICIÓN DE COPIAS O SCANNER',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                    1 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            15 => [
                'id' => 16,
                'name' => 'CERTIFICACIONES',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                    1 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            16 => [
                'id' => 17,
                'name' => 'DESISTIMIENTO',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                    1 => 'ENTIDAD DEPORTIVA',
                ],
            ],
            17 => [
                'id' => 18,
                'name' => 'RECURSO DE REPOSICIÓN',
                'to' => [
                    0 => 'ENTIDAD NO DEPORTIVA',
                    1 => 'ENTIDAD DEPORTIVA',
                ],
            ],
        ]);
    }

    public function subjectsSchool(): Collection
    {
        return collect([
            0 => [
                'id' => 1,
                'name' => 'REQUISITOS PARA EL OTORGAMIENTO DE AVAL PARA LAS ESCUELAS DE FORMACIÓN DEPORTIVA',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
            1 => [
                'id' => 2,
                'name' => 'REQUISITOS PARA LA RENOVACIÓN DE AVAL PARA LAS ESCUELAS DE FORMACIÓN DEPORTIVA',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
            2 => [
                'id' => 3,
                'name' => 'REQUISITOS PARA LA INCLUSIÓN DE NUEVOS DEPORTES EN LAS ESCUELAS DE FORMACIÓN',
                'description' => 'CUANDO SE PRODUZCA EL CAMBIO DEL DEPORTE O LA ADICIÓN DE DISCIPLINAS DEPORTIVAS',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
            3 => [
                'id' => 4,
                'name' => 'REQUISITOS PARA ACTUALIZACIÓN EXPEDIENTE',
                'description' => 'LA ESCUELA DE FORMACIÓN PODRÁ RADICAR CUALQUIERA DE LOS SIGUIENTES DOCUMENTOS CON EL FIN DE ACTUALIZAR EL EXPEDIENTE',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
            4 => [
                'id' => 5,
                'name' => 'SUBSANE',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
            5 => [
                'id' => 6,
                'name' => 'DERECHO DE PETICIÓN',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
            6 => [
                'id' => 7,
                'name' => 'SOLICITUD DE INFORMACIÓN',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
            7 => [
                'id' => 8,
                'name' => 'EXPEDICIÓN DE COPIAS O SCANNER',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
            8 => [
                'id' => 9,
                'name' => 'DESISTIMIENTO',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
            9 => [
                'id' => 10,
                'name' => 'RECURSO DE REPOSICIÓN',
                'to' => [
                    0 => 'ESCUELA DE FORMACIÓN',
                ],
            ],
        ]);
    }

    public function fileKeys()
    {
        return [
            "file_1_1",
            "file_1_2",
            "file_1_3",
            "file_1_4",
            "file_1_5",
            "file_1_6",
            "file_1_7",
            "file_1_8",
            "file_1_9",
            "file_2_1",
            "file_2_2",
            "file_2_3",
            "file_2_4",
            "file_2_5",
            "file_2_6",
            "file_2_7",
            "file_2_8",
            "file_2_9",
            "file_2_10",
            "file_2_11",
            "file_3_1",
            "file_3_2",
            "file_3_3",
            "file_3_4",
            "file_3_5",
            "file_3_6",
            "file_3_7",
            "file_3_8",
            "file_3_9",
            "file_4_1",
            "file_4_2",
            "file_4_3",
            "file_4_4",
            "file_4_5",
            "file_4_6",
            "file_4_7",
            "file_5_1_1",
            "file_5_1_2",
            "file_5_1_3",
            "file_5_1_4",
            "file_5_1_5",
            "file_5_1_6",
            "file_5_1_7",
            "file_5_1_8",
            "file_5_2_1",
            "file_5_2_2",
            "file_5_2_3",
            "file_5_2_4",
            "file_5_2_5",
            "file_5_3_1",
            "file_5_3_2",
            "file_5_3_3",
            "file_5_3_4",
            "file_5_3_5",
            "file_5_3_6",
            "file_6_1_1",
            "file_6_1_2",
            "file_6_1_3",
            "file_6_1_4",
            "file_6_1_5",
            "file_6_2_1",
            "file_6_2_2",
            "file_6_2_3",
            "file_6_3_1",
            "file_6_3_2",
            "file_6_3_3",
            "file_6_3_4",
            "file_7_1",
            "file_7_2",
            "file_7_3",
            "file_7_4",
            "file_7_5",
            "file_7_6",
            "file_8_1",
            "file_8_2",
            "file_9_1",
            "file_9_2",
            "file_10_1",
            "file_10_2",
            "file_11_1",
            "file_11_2",
            "file_12_1",
            "file_12_2",
            "file_13_1",
            "file_13_2",
            "file_14_1",
            "file_14_2",
        ];
    }

    public function fileKeysSchool(): array
    {
        return [
            'file_1_1',
            'file_1_2',
            'file_1_3',
            'file_1_4',
            'file_1_5',
            'file_1_5_1',
            'file_1_5_2',
            'file_1_5_3',
            'file_1_5_4',
            'file_1_5_5',
            'file_1_5_6',
            'file_1_5_7',
            'file_1_5_8',
            'file_1_6',
            'file_1_6_1',
            'file_1_6_2',
            'file_1_6_3',
            'file_1_6_4',
            'file_1_6_5',
            'file_1_6_6',
            'file_1_6_7',
            'file_1_6_8',
            'file_1_6_9',
            'file_1_7',
            'file_1_8',
            'file_1_9',
            'file_1_10',
            'file_1_11',
            'file_1_12',
            'file_2_1',
            'file_2_2',
            'file_2_3',
            'file_2_4',
            'file_2_5',
            'file_2_5_1',
            'file_2_5_2',
            'file_2_5_3',
            'file_2_5_4',
            'file_2_5_5',
            'file_2_5_6',
            'file_2_5_7',
            'file_2_5_8',
            'file_2_6',
            'file_2_6_1',
            'file_2_6_2',
            'file_2_6_3',
            'file_2_6_4',
            'file_2_6_5',
            'file_2_6_6',
            'file_2_6_7',
            'file_2_6_8',
            'file_2_6_9',
            'file_2_7',
            'file_3_1',
            'file_3_2',
            'file_3_3',
            'file_3_4',
            'file_3_4_1',
            'file_3_4_2',
            'file_3_4_3',
            'file_3_4_4',
            'file_3_4_5',
            'file_3_4_6',
            'file_3_4_7',
            'file_3_4_8',
            'file_3_4_9',
            'file_3_5',
            'file_3_6',
            'file_4_1',
            'file_4_2',
            'file_5_1',
            'file_5_2',
            'file_6_1',
            'file_6_2',
            'file_7_1',
            'file_7_2',
            'file_8_1',
            'file_8_2',
            'file_9_1',
            'file_9_2',
            'file_10_1',
            'file_10_2',
        ];
    }

    public function orfeoApi(array $headers)
    {
        try {
            $http = new \GuzzleHttp\Client();
            $response = $http->get(config("app.orfeo.endpoint"), [
                "auth" => [config("app.orfeo.user"), config("app.orfeo.password")],
                'headers' => $headers,
                'decode_content' => false,
            ]);
            $data = json_decode($response->getBody()->getContents(), true);
            if (!is_array($data) && str_starts_with($data, "No posee")) {
                return $this->error_response($data);
            } else {
                return $this->success_message($data);
            }
        } catch (\Exception $exception) {
            return $this->error_response($exception->getMessage());
        }
    }

    public function getAttachment(Request $request)
    {
        return $this->orfeoApi([
            "action" => "getAnexo",
            'codigo' => "{$request->get("orfeo")}00001",
            'radicado'  => $request->get("orfeo"),
        ]);
    }

    public function getHistory(Request $request)
    {
        return $this->orfeoApi([
            "action" => "consultaHistorico",
            'tipo' => 1,
            'radicado'  => $request->get("orfeo"),
        ]);
    }

    public function sports()
    {
        return $this->success_message(
            Sport::query()->orderBy('V_NOMBRE_DEPORTE')->get()->map(function($sport) {
                return [
                    "id"    => (int) $sport->PK_I_ID_DEPORTE,
                    "name"  => toUpper($sport->V_NOMBRE_DEPORTE)
                ];
            })
        );
    }
}
