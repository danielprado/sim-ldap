<?php

namespace App\Http\Controllers\Recreation;

use App\Entities\Parks\Park;
use App\Entities\Recreation\Calendar;
use App\Http\Resources\Recreation\CalendarResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function Clue\StreamFilter\fun;

class CalendarController extends Controller
{
    public function index(Request $request, $park)
    {
        $start_date = $request->has('start_date')
                    ? $request->get('start_date')
                    : now()->startOfMonth()->format('Y-m-d');
        $final_date = $request->has('final_date')
                    ? $request->get('final_date')
                    : now()->endOfMonth()->format('Y-m-d');
        $data = Calendar::where('park_code', $park)
                        ->whereDate('date', '>=', $start_date)
                        ->whereDate('date', '<=', $final_date)
                        ->get();
        return $this->success_response(CalendarResource::collection( $data ));
    }

    /**
     * @throws \Exception
     */
    public function others(Request $request)
    {
        $start_date = $request->has('start_date')
            ? $request->get('start_date')
            : now()->startOfMonth()->format('Y-m-d');
        $final_date = $request->has('final_date')
            ? $request->get('final_date')
            : now()->endOfMonth()->format('Y-m-d');
        $parks = cache()->remember('idrd_codes', now()->addHour(), function() {
            return Park::query()->select(['Id_IDRD'])->get()->pluck('Id_IDRD')->toArray();
        });
        $data = Calendar::whereNotIn('park_code', $parks)
            ->whereDate('date', '>=', $start_date)
            ->whereDate('date', '<=', $final_date)
            ->get();
        return $this->success_response(CalendarResource::collection( $data ));
    }
}
