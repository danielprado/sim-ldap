<?php

namespace App\Http\Controllers;

use App\Entities\CitizenPortal\ScheduleView;
use App\Entities\Parks\Park;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SitemapController extends Controller
{
    public function index()
    {
        $url = "https://sim.idrd.gov.co/portal-ciudadano";
        $links = [
            [
                'loc' => "$url/",
                'lastmod' => now()->startOfDay()->tz('UTC')->toAtomString(),
                "changefreq" => "daily",
                "priority" => 1.0,
            ],
            [
                'loc' => "$url/usuarios/iniciar-sesion",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.9,
            ],
            [
                'loc' => "$url/usuarios/olvide-mi-contrasena",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.9,
            ],
            [
                'loc' => "$url/usuarios/registrarme",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.9,
            ],
            [
                'loc' => "$url/parques/buscar",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/canchas-sinteticas",
                'lastmod' =>now()->startOf('week')->tz('UTC')->toAtomString(),
                "changefreq" => "weekly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/asadores",
                'lastmod' => now()->startOf('week')->tz('UTC')->toAtomString(),
                "changefreq" => "weekly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/agenda-idrd",
                'lastmod' => now()->startOfDay()->tz('UTC')->toAtomString(),
                "changefreq" => "daily",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/actividades",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/actividades/recreacion",
                'lastmod' => now()->startOfDay()->tz('UTC')->toAtomString(),
                "changefreq" => "daily",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/actividades/deportes",
                'lastmod' => now()->startOfDay()->tz('UTC')->toAtomString(),
                "changefreq" => "daily",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/localidades",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/diagramas-de-parques",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/mapa",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/escalas-de-parques/1-regional",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/escalas-de-parques/2-metropolitano",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/escalas-de-parques/3-zonal",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/escalas-de-parques/4-vecinal",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/escalas-de-parques/5-bolsillo",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/escalas-de-parques/6-gran-escenario",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/escalas-de-parques/7-metropolitano-propuesto",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
            [
                'loc' => "$url/parques/escalas-de-parques/8-zonal-propuesto",
                'lastmod' => now()->startOfMonth()->tz('UTC')->toAtomString(),
                "changefreq" => "monthly",
                "priority" => 0.8,
            ],
        ];
        Park::chunk(1000, function ($parks) use (&$links, $url) {
            foreach ($parks as $park) {
                $code = $park->Id_IDRD ?? $park->Id;
                $links[] = [
                    'loc' => "$url/parques/{$code}/detalles",
                    'lastmod' => $park->created_at
                        ? $park->created_at->tz('UTC')->toAtomString()
                        : now()->startOfMonth()->tz('UTC')->toAtomString(),
                    "changefreq" => "monthly",
                    "priority" => 0.7,
                ];
                if ($park->Estado == 1 && $park->Id != 1) {
                    $links[] = [
                        'loc' => "$url/parques/{$code}/detalles/diagrama",
                        'lastmod' => $park->created_at
                            ? $park->created_at->tz('UTC')->toAtomString()
                            : now()->startOfMonth()->tz('UTC')->toAtomString(),
                        "changefreq" => "monthly",
                        "priority" => 0.7,
                    ];
                }
                foreach ($park->park_endowment as $endowment) {
                    $links[] = [
                        'loc' => "$url/parques/{$code}/detalles/{$endowment->Id}/dotacion",
                        'lastmod' => $park->created_at
                            ? $park->created_at->tz('UTC')->toAtomString()
                            : now()->startOfMonth()->tz('UTC')->toAtomString(),
                        "changefreq" => "monthly",
                        "priority" => 0.7,
                    ];
                }
                $links[] = [
                    'loc' => "$url/parques/{$code}/detalles/gestion-social",
                    'lastmod' => $park->created_at
                        ? $park->created_at->tz('UTC')->toAtomString()
                        : now()->startOfMonth()->tz('UTC')->toAtomString(),
                    "changefreq" => "monthly",
                    "priority" => 0.7,
                ];
                $links[] = [
                    'loc' => "$url/parques/{$code}/detalles/actividades-de-recreacion",
                    'lastmod' => $park->created_at
                        ? $park->created_at->tz('UTC')->toAtomString()
                        : now()->startOfDay()->tz('UTC')->toAtomString(),
                    "changefreq" => "daily",
                    "priority" => 0.7,
                ];
            }
        });
        ScheduleView::query()
            ->where('quota', '>', 0)
            ->where('is_activated', true)
            ->where('is_initiate', true)
            ->where('final_date', '>=', now()->startOfDay())
            ->whereColumn('taken', '<', 'quota')
            ->chunk(1000, function ($schedules) use (&$links, $url) {
                foreach ($schedules as $schedule) {
                    $slug = toLower(Str::slug($schedule->activity_name));
                    $links[] = [
                        'loc' => "$url/parques/actividades/deportes/{$schedule->id}-$slug",
                        'lastmod' => $schedule->created_at
                            ? $schedule->created_at->tz('UTC')->toAtomString()
                            : now()->startOfDay()->tz('UTC')->toAtomString(),
                        "changefreq" => "daily",
                        "priority" => 0.7,
                    ];
                }
            });
        return response()->view('sitemap', [
            'links' => $links
        ])->header('Content-Type', 'text/xml');
    }
}
