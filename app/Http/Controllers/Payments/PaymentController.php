<?php

namespace App\Http\Controllers\Payments;

use App\Entities\CitizenPortal\CitizenSchedule;
use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\ProfileView;
use App\Entities\Parks\ParkEndowment;
use App\Entities\Parks\Synthetic;
use App\Entities\Parks\NaturalSoccerField;
use App\Entities\Parks\Grill;
use App\Entities\Parks\Gym;
use App\Entities\Parks\TennisCourt;
use App\Entities\SwimmingPool\Lane;
use App\Entities\Payments\Booking;
use App\Entities\Payments\BookingView;
use App\Entities\Payments\Payment;
use App\Entities\Payments\Price;
use App\Entities\Payments\PsePark;
use App\Entities\Payments\Schedule;
use App\Entities\Payments\Status;
use App\Entities\Payments\SwimmingPool;
use App\Entities\SwimmingPool\SwimPool;
use App\Exports\PaymentExport;
use App\Helpers\Excel;
use App\Http\Resources\Citizen\CitizenScheduleResource;
use App\Http\Resources\Payments\BookingViewResource;
use App\Http\Resources\UserResource;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Excel as Xlsx;
use App\Http\Requests\Payments\BookingRequest;
use App\Http\Requests\Payments\PaymentRequest;
use App\Http\Resources\Payments\BookingResource;
use App\Http\Resources\Payments\PaymentResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $payments = Payment::query()
            ->when($this->query, function ($query) {
                return $query->search($this->query);
            })
            ->whereIn(
                'identificacion',
                auth('api')->user()->profiles->pluck('document')->toArray()
            )
            ->orderByDesc('fecha_pago')
            ->paginate($this->per_page);
        return $this->success_response(
            PaymentResource::collection($payments),
            Response::HTTP_OK,
            PaymentResource::headers()
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function export(Request $request)
    {
        $file = Excel::raw(new PaymentExport($request), Xlsx::XLSX);
        $name = random_img_name();
        $response =  array(
            'name' => "PORTAL-CIUDADANO-$name.xlsx",
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($file),
        );
        return $this->success_message($response);
    }

    public function payment(ParkEndowment $endowment, PaymentRequest $request)
    {
        /****Procesar pago para canchas sinteticas y canchas naturales ****/
        $is_field_syntetic = Synthetic::query()
            ->where('park_endowment_id', $endowment->Id)
            ->count();

        $is_field_natural = NaturalSoccerField::query()
            ->where('park_endowment_id', $endowment->Id)
            ->count();

        if ($is_field_natural > 0 || $is_field_syntetic > 0) {
            return $this->processField($endowment, $request);
        }
        else{
            /****Procesar pago para canchas de tenis ****/
            $is_tennis_court = TennisCourt::query()
                ->where('park_endowment_id', $endowment->Id)
                ->count();
            if ($is_tennis_court > 0) {
                return $this->processTennisCourt($endowment, $request);
            }
            /****Procesar pago para gimnasios ****/
            $is_gym = Gym::query()
                ->where('park_endowment_id', $endowment->Id)
                ->count();
            
            if ($is_gym > 0) {
                    return $this->processGym($endowment, $request);
            }
            else{
                /****Procesar pago para asadores ****/
                $is_grill= Grill::query()
                    ->where('park_endowment_id', $endowment->Id)
                    ->count();

                if ($is_grill > 0){
                    return $this->processGrills($endowment, $request);
                } else {
                    // $is_swimming_pool = SwimmingPool::query()
                    //     ->where('park_endowment_id', $endowment->Id)
                    //     ->count();
                    return $this->processSwimPools($endowment, $request);
                }
            }
        }
    }

    public function processField(ParkEndowment $endowment, PaymentRequest $request)
    {
        try {
            $date = Carbon::parse($request->get('date'));
            $start_date = Carbon::parse($request->get('date').' '.$request->get('start_hour'));
            $final_date = Carbon::parse($request->get('date').' '.$request->get('final_hour'));
            $hours = $start_date->diffInHours($final_date, false);

            $pse_code = park_code_to_pse_code($endowment->park->Id_IDRD ?? "");

            $auxParquePse = PsePark::where('codigo_parque', $pse_code)->firstOrFail();

            // Almacena los errores que se generan en las validaciones.
            $errors = collect([]);

            $dias=[1=>'Lunes',2=>'Martes',3=>'Miercoles',4=>'Jueves',5=>'Viernes',6=>'Sabados',7=>'Domingos'];

            if($hours > 2 || $hours <= 0){
                $errors->push('No se puede reservar por mas de 2 horas');
            }

            if ( strtotime($final_date->format("H:i:s")) > strtotime('18:00:00') && strtotime($request->get('start_hour')) < strtotime('18:00:00')){
                $errors->push('Solo se puede reservar en una franja horaria dia o noche');
            }
            
            $user = Profile::query()
                    ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                    ->where('user_id', auth('api')->user()->id)
                    ->first();
            
            $totalHorasDia = Booking::where('id_dotacion', $endowment->getKey())
                            ->where('fecha', $request->get('date'))->where('documento', $user->document)
                            ->selectRaw('SUM(TIMESTAMPDIFF(HOUR, hora_inicio, hora_fin)) as total_horas')
                            ->value('total_horas');
            
            if($totalHorasDia >= 2){
                $errors->push('No se puede reservar más de 2 horas por día por usuario.');
            }

            // dayOfWeekIso returns a number between 1 (monday) and 7 (sunday)
            $valido_inicial = Schedule::where('id_dotacion', $endowment->getKey())
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<=',$start_date->format('H:i:s'))
                ->where('horaf','>', $start_date->format('H:i:s'))
                ->count();
            $valido_final = Schedule::where('id_dotacion', $endowment->getKey())
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<', $final_date->format('H:i:s'))
                ->where('horaf','>=',$final_date->format('H:i:s'))
                ->count();

            $aux_sin_horario = false;

            if($valido_inicial && $valido_final) {

            } else {
                $rango = Schedule::where('id_dotacion', $endowment->getKey())
                    ->where('dia',$date->dayOfWeekIso)
                    ->get();
                foreach ($rango as $value) {
                    $horai = date('g:i A', strtotime($value->horai));
                    $horaf = date('g:i A', strtotime($value->horaf));
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' solo hay servicio de  '.$horai.' - '.$horaf);
                    $aux_sin_horario = true;
                }
            }
            if($valido_inicial == 0 && $valido_final == 0){
                if(! $aux_sin_horario){
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' no hay horarios disponibles');
                }
            }

            $actuales =  Booking::whereDate('fecha', $date)
                                ->where('id_dotacion',$endowment->getKey())
                                ->wherehas('payment',function($query){
                                    return $query->whereIn('estado_id', [1, 2])
                                        ->orWhereNull('estado_id');
                                });

            if ( $actuales->where('hora_inicio',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2])
                        ->orWhereNull('estado_id');
                });
            if ( $actuales->where('hora_fin',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2])
                        ->orWhereNull('estado_id');
                });
            if ( $actuales->where('hora_inicio','<',$start_date->format('H:i:s'))->where('hora_fin','>',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actuales =  Booking::whereDate('fecha', $date)
                                ->where('id_dotacion',$endowment->getKey())
                                ->wherehas('payment',function($query){
                                    return $query->whereIn('estado_id', [1, 2])
                                        ->orWhereNull('estado_id');
                                });
            if ( $actuales->where('hora_inicio','<',$final_date->format('H:i:s'))->where('hora_fin','>',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }

            // Toma las reservas realizadas con 5 minutos de anterioridad con respecto a la dotación
            // que se va a reservar.
            // $dateParalela = now()->addMinutes(-5);
            $dateParalela = now()->subMinutes(5);
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                                        ->where('id_dotacion', $endowment->getKey())
                                        // ->doesntHave('payment')
                                        ->where('created_at', '>=', $dateParalela);

            if ( $actualesParalelas->where('hora_inicio',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                // ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);
            if ( $actualesParalelas->where('hora_fin',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva en proceso');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                // ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);

            if ( $actualesParalelas->where('hora_inicio','<',$start_date->format('H:i:s'))->where('hora_fin','>',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva en proceso');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                                        ->where('id_dotacion', $endowment->getKey())
                                        // ->doesntHave('payment')
                                        ->where('created_at', '>=', $dateParalela);
            if ( $actualesParalelas->where('hora_inicio','<',$final_date->format('H:i:s'))->where('hora_fin','>',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva en proceso');
            }

            if ($errors->count() > 0) {
                return $this->error_response(
                    'Por favor verifique los datos e intente nuevamente',
                    422,
                    $errors->toArray()
                );
            }

            $service = $endowment->MaterialPiso == 6 ? Payment::NATURAL_SERVICE : Payment::SYNTHETIC_SERVICE;
            $id_parque = $auxParquePse->id_parque;


             /***** INICIO CALCULAR TARIFA A APLICAR ACTUALIZACIÓN PROTOCOLO DE APROVECHAMIENTO ECONÓMICO******/
             $tarifaIdAux = null;
             $parkEndowmentCategory = 'A';
             $sizeField = 1;
             $isDay = true;
             $total = 0;
             $valor = 0;

            /********* INICIO ESTABLECER SI ES HORARIO DIURNO O NOCTURNO **********/
            $day = clone $start_date;
            $night = clone $start_date;

            $horai = $start_date->format('H:i:s');
            $horaf = $final_date->format('H:i:s');

            $hours = $start_date->diffInHours($final_date);

            $details = [
                "hours" => $hours,
                "unit"  => 0,
                "type" => "Diurno",
                "stratum" => $endowment->park->Estrato ?? null,
                "park_endowment_category" => $parkEndowmentCategory,
            ];

            if ( $start_date->isBefore($day->setTime(18, 0, 0, 0)))
                $isDay = true;
            if ($start_date->greaterThanOrEqualTo($night->setTime(18, 0, 0, 0)))
                $isDay = false;

            $details["type"] = $isDay ? "Diurno" : "Nocturno";

            /********* FIN ESTABLECER SI ES HORARIO DIURNO O NOCTURNO **********/

            /********* INICIO ESTABLECER TIPO CANCHA **********/
            if(str_contains(toLower($endowment->Descripcion), 'bol 5'))
                $sizeField = 5;
            if(str_contains(toLower($endowment->Descripcion), 'bol 7') || str_contains(toLower($endowment->Descripcion), 'bol 8') || str_contains(toLower($endowment->Descripcion), 'bol 9'))
                $sizeField = 8;
            if(str_contains(toLower($endowment->Descripcion), 'bol 11'))
                $sizeField = 11;
            /********* FIN ESTABLECER TIPO CANCHA **********/


            /********* ESTABLECER GATEGORÍA DEL PARQUE **********/
            $auxField = Synthetic::query()
                        ->where('park_endowment_id', $endowment->Id)
                        ->first();
            // si no es cancha sintetica, buscar en vista de canchas grama natural
            if(!$auxField){
                $auxField = NaturalSoccerField::query()
                        ->where('park_endowment_id', $endowment->Id)
                        ->first();
            }
            $parkEndowmentCategory = $auxField ? $auxField->park_endowment_category : 'SIN DEFINIR';
            $details["park_endowment_category"] = $parkEndowmentCategory;
            /********* ESTABLECER TARIFA A APLICAR **********/
            switch ($parkEndowmentCategory) {
                case 'A':
                    if($sizeField === 5)
                        $tarifaIdAux = $isDay ?  Price::PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_5_DAY :  Price::PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_5_NIGHT;
                    if($sizeField === 8)
                        $tarifaIdAux = $isDay ?  Price::PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_8_DAY :  Price::PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_8_NIGHT;
                    if($sizeField === 11)
                        $tarifaIdAux = $isDay ?  Price::PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_11_DAY :  Price::PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_11_NIGHT;
                break;
                case 'B':
                    if($sizeField === 5)
                        $tarifaIdAux = $isDay ?  Price::PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_5_DAY :  Price::PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_5_NIGHT;
                    if($sizeField === 8)
                        $tarifaIdAux = $isDay ?  Price::PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_8_DAY :  Price::PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_8_NIGHT;
                    if($sizeField === 11)
                        $tarifaIdAux = $isDay ?  Price::PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_11_DAY :  Price::PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_11_NIGHT;
                break;
                case 'C':
                    if($sizeField === 5)
                        $tarifaIdAux = $isDay ?  Price::PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_5_DAY :  Price::PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_5_NIGHT;
                    if($sizeField === 8)
                        $tarifaIdAux = $isDay ?  Price::PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_8_DAY :  Price::PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_8_NIGHT;
                    if($sizeField === 11)
                        $tarifaIdAux = $isDay ?  Price::PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_11_DAY :  Price::PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_11_NIGHT;
                break;
                default:
                    # code...
                    break;
            }
            if($tarifaIdAux === null)
                $tarifaIdAux = Price::PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_5_DAY;

            $valor = Price::find($tarifaIdAux);
            $total = $hours * intval($valor->D_TOTAL_REDONDEADO);
            $details["unit"] = intval($valor->D_TOTAL_REDONDEADO);

            /***** FIN CALCULAR TARIFA A APLICAR ACTUALIZACIÓN PROTOCOLO DE APROVECHAMIENTO ECONÓMICO ******/

            $reserva = new Booking();
            $reserva->hora_inicio = $horai;
            $reserva->hora_fin    = $horaf;
            $reserva->fecha       = $date;
            $reserva->id_dotacion = $endowment->getKey();
            $reserva->valor       = $total;
            $reserva->documento   = $user->document;
            $reserva->save();

            //$auxConcepto = "Reserva Cancha ". $endowment->getKey() . ' ' . $fecha . ' ' . $horai . ' a ' . $horaf;

            $auxConcepto = "Reserva Cancha #{$endowment->getKey()}. Parque: {$endowment->park->Id_IDRD} / {$date->format('Y-m-d')} $horai a $horaf";

            $user = Profile::query()
                ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                ->where('user_id', auth('api')->user()->id)
                ->first();

            return $this->success_message([
                'name'          => toUpper("{$user->name} {$user->s_name}"),
                'surname'       => toUpper("{$user->surname} {$user->s_surname}"),
                'document_type_id' => pse_document_type($user->document_type->name ?? null),
                'document'       => $user->document ?? null,
                'address'       => $user->address ?? null,
                'email'         => $user->user->email ?? null,
                'phone'         => $user->mobile_phone ?? null,
                'booking_id'    => $reserva->id,
                'amount'        => $total,
                'park_id'       => $id_parque,
                'service_id'    => $service,
                'person_type_id'=> "N",
                'bank_id'       => null,
                'ip_address'    => $request->getClientIp(),
                'concept'       => Str::substr(toUpper($auxConcepto), 0, 100),
                'details'       => $details,
                'created_at'    => isset($reserva->created_at) ? $reserva->created_at->addMinutes(5)->format('Y-m-d H:i:s') : now()->addMinutes(5)->format('Y-m-d H:i:s'),
            ]);

        } catch (\Exception $exception) {
            if ($exception instanceof ModelNotFoundException) {
                return $this->error_response(
                    'Parque no registrado en el sistema de reservas',
                    422,
                    park_code_to_pse_code($endowment->park->Id_IDRD ?? "")
                );
            }

            return $this->error_response(
                'Algo pasó, por favor intenta nuevamente',
                500,
                $exception->getMessage()
            );
        }
    }

    public function processGym(ParkEndowment $endowment, PaymentRequest $request)
    {
        try {
            $date = Carbon::parse($request->get('date'));
            $start_date = Carbon::parse($request->get('date').' '.$request->get('start_hour'));
            $final_date = Carbon::parse($request->get('date').' '.$request->get('final_hour'));
            $hours = $start_date->diffInHours($final_date, false);

            // If date is before today, return error
            if ($date->isBefore(now()->startOfDay())) {
                return $this->error_response(
                    'No se puede reservar para fechas anteriores a hoy, fecha seleccionada: '.$date->format('Y-m-d'),
                    422
                );
            }

            $pse_code = park_code_to_pse_code($endowment->park->Id_IDRD ?? "");

            $auxParquePse = PsePark::where('codigo_parque', $pse_code)->firstOrFail();

            // Almacena los errores que se generan en las validaciones.
            $errors = collect([]);

            $dias=[1=>'Lunes',2=>'Martes',3=>'Miercoles',4=>'Jueves',5=>'Viernes',6=>'Sabados',7=>'Domingos'];

            // 
            $user = Profile::query()
                    ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                    ->where('user_id', auth('api')->user()->id)
                    ->first();
            
            
            
            // Extraemos los datos necesarios
            $birthdate = $user->birthdate;
            $disability = $user->has_disability;
            $gender = $user->gender_id;
            $document = $user->document;

            // Si el documento es nulo, retornar error
            if (is_null($document)) {
                return $this->error_response(
                    'No se encontró el documento del usuario',
                    422
                );
            }



            if($hours > 1 || $hours <= 0){
                $errors->push('No se puede reservar por mas de 1 hora');
            }

            if ( strtotime($final_date->format("H:i:s")) > strtotime('18:00:00') && strtotime($request->get('start_hour')) < strtotime('18:00:00')){
                $errors->push('Solo se puede reservar en una franja horaria dia o noche');
            }

            // dayOfWeekIso returns a number between 1 (monday) and 7 (sunday)
            $valido_inicial = Schedule::where('id_dotacion', $endowment->getKey())
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<=',$start_date->format('H:i:s'))
                ->where('horaf','>', $start_date->format('H:i:s'))
                ->count();
            $valido_final = Schedule::where('id_dotacion', $endowment->getKey())
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<', $final_date->format('H:i:s'))
                ->where('horaf','>=',$final_date->format('H:i:s'))
                ->count();

            $aux_sin_horario = false;

            if($valido_inicial && $valido_final) {

            } else {
                $rango = Schedule::where('id_dotacion', $endowment->getKey())
                    ->where('dia',$date->dayOfWeekIso)
                    ->get();
                foreach ($rango as $value) {
                    $horai = date('g:i A', strtotime($value->horai));
                    $horaf = date('g:i A', strtotime($value->horaf));
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' solo hay servicio de  '.$horai.' - '.$horaf);
                    $aux_sin_horario = true;
                }
            }
            
            if($valido_inicial == 0 && $valido_final == 0){
                if(! $aux_sin_horario){
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' no hay horarios disponibles');
                }
            }
            
            $actuales =  Booking::whereDate('fecha', $date)
                                ->where('id_dotacion',$endowment->getKey())
                                ->wherehas('payment',function($query){
                                    return $query->whereIn('estado_id', [1, 2])
                                        ->orWhereNull('estado_id');
                                });

            $actualesUsuario = Booking::whereHas('payment', function($query) use ($document){
                return $query
                    ->where('identificacion', $document)    
                    ->whereIn('estado_id', [1, 2])
                    ->orWhereNull('estado_id');
            })    
            ->where('fecha', $date)
                ->where('hora_inicio', $start_date->format('H:i:s'))
                ->where('hora_fin', $final_date->format('H:i:s'));

                if ( $actualesUsuario->count() > 0){
                    $errors->push('Hay un cruce con una reserva tuya actual');
                }


            if ( $actuales->where('hora_inicio',$start_date->format('H:i:s'))->count() > 14){
                $errors->push('El cupo está lleno');
            }


            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2])
                        ->orWhereNull('estado_id');
                });
                                
            if ( $actuales->where('hora_fin',$final_date->format('H:i:s'))->count() > 14){
                $errors->push('El cupo está lleno');
            }


            

            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2])
                        ->orWhereNull('estado_id');
                });

            if ( $actuales->where('hora_inicio','<',$start_date->format('H:i:s'))->where('hora_fin','>',$start_date->format('H:i:s'))->count() > 14){
                $errors->push('El cupo está lleno');
            }
                  
                        
            $actuales =  Booking::whereDate('fecha', $date)
                                ->where('id_dotacion',$endowment->getKey())
                                ->wherehas('payment',function($query){
                                    return $query->whereIn('estado_id', [1, 2])
                                        ->orWhereNull('estado_id');
                                });
            if ( $actuales->where('hora_inicio','<',$final_date->format('H:i:s'))->where('hora_fin','>',$final_date->format('H:i:s'))->count() > 14 && $actuales->where('fecha', $date)->count() > 0){
                $errors->push('El cupo está lleno');
            }

              

            // Toma las reservas realizadas con 5 minutos de anterioridad con respecto a la dotación
            // que se va a reservar.
            // $dateParalela = now()->addMinutes(-5);
            $dateParalela = now()->subMinutes(5);
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                                        ->where('id_dotacion', $endowment->getKey())
                                        // ->doesntHave('payment')
                                        ->where('created_at', '>=', $dateParalela);

            
            
            if ( $actualesParalelas->where('hora_inicio',$start_date->format('H:i:s'))->count() > 14){
                $errors->push('El cupo está lleno');
            }



            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                // ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);
            if ( $actualesParalelas->where('hora_fin',$final_date->format('H:i:s'))->count() > 14){
                $errors->push('Hay un cruce con una reserva en proceso');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                // ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);

                
            if ( $actualesParalelas->where('hora_inicio','<',$start_date->format('H:i:s'))->where('hora_fin','>',$start_date->format('H:i:s'))->count() > 14){
                $errors->push('Hay un cruce con una reserva en proceso');
            }

            $actualesParalelasUsuario = $actualesParalelas->where('documento', $document);
            if ( $actualesParalelasUsuario->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }

            $actualesParalelas =  Booking::whereDate('fecha',$date)
                                        ->where('id_dotacion', $endowment->getKey())
                                        // ->doesntHave('payment')
                                        ->where('created_at', '>=', $dateParalela);
            if ( $actualesParalelas->where('hora_inicio','<',$final_date->format('H:i:s'))->where('hora_fin','>',$final_date->format('H:i:s'))->count() > 14){
                $errors->push('Hay un cruce con una reserva en proceso');
            }

            $actualesParalelasUsuario = $actualesParalelas->where('documento', $document);
            if ( $actualesParalelasUsuario->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            

            if ($errors->count() > 0) {
                return $this->error_response(
                    'Por favor verifique los datos e intente nuevamente',
                    422,
                    $errors->toArray()
                );
            }

             /***** VERIFICAR SI SE LE VA A COBRAR AL USUARIO ******/

            /**
             * 1. Si el usuario tiene discapacidad, no se le cobra.
             * 2. Si el usuario es adulto mayor, no se le cobra.
             * 3. Tarifa depende del género del usuario.
             * 
             */

            //  fecha de nacimiento del usuario: birthdate
            //  género del usuario: gender_id: masculino
            //  has_disability: NO

           

    

            $currentDate = Carbon::now();
            $age = $currentDate->diffInYears($birthdate);                    
            // Determinar si es de la tercera edad
            $isSeniorCitizen = $age >= 60;


            // Si el genero es masculino, $idTarifaDiaAux = Price::GYM_MALE_PRICE ELSE $idTarifaDiaAux = Price::GYM_FEAMLE_PRICE

            $idTarifaDiaAux = $gender == 1 ? Price::GYM_MALE_PRICE : Price::GYM_FEMALE_PRICE;
            // If Disability == "SI" or isSeniorCitizen == true, $idTarifaDiaAux = 0
            if($disability == 'SI'){
                $idTarifaDiaAux = 0;
            }

            if ($isSeniorCitizen) {
                $idTarifaDiaAux = 0;
            }



            // Calculamos la edad del usuario con la fecha de nacimiento con Carbon            
            $service = Payment::GYM_SERVICE; 
            $id_parque = $auxParquePse->id_parque;



             /***** INICIO CALCULAR TARIFA A APLICAR ACTUALIZACIÓN PROTOCOLO DE APROVECHAMIENTO ECONÓMICO******/
             $isDay = true;
             $total = 0;
             $valor = 0;

            /********* INICIO ESTABLECER SI ES HORARIO DIURNO O NOCTURNO **********/
            $day = clone $start_date;
            $night = clone $start_date;

            $horai = $start_date->format('H:i:s');
            $horaf = $final_date->format('H:i:s');

            $hours = $start_date->diffInHours($final_date);

            $details = [
                "hours" => $hours,
                "unit"  => 0,
                "type" => "Diurno",
            ];



            if ( $start_date->isBefore($day->setTime(18, 0, 0, 0)))
                $isDay = true;
            if ($start_date->greaterThanOrEqualTo($night->setTime(18, 0, 0, 0)))
                $isDay = false;

            $details["type"] = $isDay ? "Diurno" : "Nocturno";

            $valor = 
            $idTarifaDiaAux == 0 ? 0 : Price::find($idTarifaDiaAux);
            $total = 
            $idTarifaDiaAux == 0 ? 0 : $hours * intval($valor->D_TOTAL_REDONDEADO);
            $details["unit"] = 
            $idTarifaDiaAux == 0 ? 0 :
            intval($valor->D_TOTAL_REDONDEADO);
            
            /***** FIN CALCULAR TARIFA A APLICAR ACTUALIZACIÓN PROTOCOLO DE APROVECHAMIENTO ECONÓMICO ******/

            $reserva = new Booking();
            $reserva->hora_inicio = $horai;
            $reserva->hora_fin    = $horaf;
            $reserva->fecha       = $date;
            $reserva->id_dotacion = $endowment->getKey();
            $reserva->valor       = $total;
            // Adñdir el documento
            $reserva->documento = $document;
            $reserva->is_successful = 0;
            $reserva->save();

            //$auxConcepto = "Reserva Cancha ". $endowment->getKey() . ' ' . $fecha . ' ' . $horai . ' a ' . $horaf;

            $auxConcepto = "Reserva Gimnasio #{$endowment->getKey()}. Parque: {$endowment->park->Id_IDRD} / {$date->format('Y-m-d')} $horai a $horaf";

            $user = Profile::query()
                ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                ->where('user_id', auth('api')->user()->id)
                ->first();

            return $this->success_message([
                'name'          => toUpper("{$user->name} {$user->s_name}"),
                'surname'       => toUpper("{$user->surname} {$user->s_surname}"),
                'document_type_id' => pse_document_type($user->document_type->name ?? null),
                'document'       => $user->document ?? null,
                'address'       => $user->address ?? null,
                'email'         => $user->user->email ?? null,
                'phone'         => $user->mobile_phone ?? null,
                'booking_id'    => $reserva->id,
                'is_successful' => $reserva->is_successful,
                'amount'        => $total,
                'park_id'       => $id_parque,
                'service_id'    => $service,
                'person_type_id'=> "N",
                'bank_id'       => null,
                'ip_address'    => $request->getClientIp(),
                'concept'       => Str::substr(toUpper($auxConcepto), 0, 100),
                'details'       => $details,
                'created_at'    => isset($reserva->created_at) ? $reserva->created_at->addMinutes(5)->format('Y-m-d H:i:s') : now()->addMinutes(5)->format('Y-m-d H:i:s'),
            ]);

        } catch (\Exception $exception) {
            if ($exception instanceof ModelNotFoundException) {
                return $this->error_response(
                    'Parque no registrado en el sistema de reservas',
                    422,
                    park_code_to_pse_code($endowment->park->Id_IDRD ?? "")
                );
            }

            return $this->error_response(
                'Algo pasó, por favor intenta nuevamente',
                500,
                $exception->getMessage()
            );
        }
    }


    public function processTennisCourt(ParkEndowment $endowment, PaymentRequest $request)
    {
        try {
            $date = Carbon::parse($request->get('date'));
            $start_date = Carbon::parse($request->get('date').' '.$request->get('start_hour'));
            $final_date = Carbon::parse($request->get('date').' '.$request->get('final_hour'));
            $hours = $start_date->diffInHours($final_date, false);
            // $pse_code = park_code_to_pse_code($code);
            $pse_code = park_code_to_pse_code($endowment->park->Id_IDRD ?? "");

            $auxParquePse = PsePark::where('codigo_parque', $pse_code)->firstOrFail();

            // Almacena los errores que se generan en las validaciones.
            $errors = collect([]);

            $dias=[1=>'Lunes',2=>'Martes',3=>'Miercoles',4=>'Jueves',5=>'Viernes',6=>'Sabados',7=>'Domingos'];

            if($hours > 2 || $hours <= 0){
                $errors->push('No se puede reservar por mas de 2 horas');
            }

            if ( strtotime($final_date->format("H:i:s")) > strtotime('18:00:00') && strtotime($request->get('start_hour')) < strtotime('18:00:00')){
                $errors->push('Solo se puede reservar en una franja horaria dia o noche');
            }

            // dayOfWeekIso returns a number between 1 (monday) and 7 (sunday)
            $valido_inicial = Schedule::where('id_dotacion', $endowment->getKey())
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<=',$start_date->format('H:i:s'))
                ->where('horaf','>', $start_date->format('H:i:s'))
                ->count();
            $valido_final = Schedule::where('id_dotacion', $endowment->getKey())
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<', $final_date->format('H:i:s'))
                ->where('horaf','>=',$final_date->format('H:i:s'))
                ->count();

            $aux_sin_horario = false;

            if($valido_inicial && $valido_final) {

            } else {
                $rango = Schedule::where('id_dotacion', $endowment->getKey())
                    ->where('dia',$date->dayOfWeekIso)
                    ->get();
                foreach ($rango as $value) {
                    $horai = date('g:i A', strtotime($value->horai));
                    $horaf = date('g:i A', strtotime($value->horaf));
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' solo hay servicio de  '.$horai.' - '.$horaf);
                    $aux_sin_horario = true;
                }
            }
            if($valido_inicial == 0 && $valido_final == 0){
                if(! $aux_sin_horario){
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' no hay horarios disponibles');
                }
            }

            $actuales =  Booking::whereDate('fecha', $date)
                                ->where('id_dotacion',$endowment->getKey())
                                ->wherehas('payment',function($query){
                                    return $query->whereIn('estado_id', [1, 2])
                                        ->orWhereNull('estado_id');
                                });

            if ( $actuales->where('hora_inicio',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2])
                        ->orWhereNull('estado_id');
                });
            if ( $actuales->where('hora_fin',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2])
                        ->orWhereNull('estado_id');
                });
            if ( $actuales->where('hora_inicio','<',$start_date->format('H:i:s'))->where('hora_fin','>',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2])
                            ->orWhereNull('estado_id');
                });
            if ( $actuales->where('hora_inicio','<',$final_date->format('H:i:s'))->where('hora_fin','>',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }

            // Toma las reservas realizadas con 5 minutos de anterioridad con respecto a la dotación
            // que se va a reservar.
            // $dateParalela = now()->addMinutes(-5);
            $dateParalela = now()->subMinutes(5);
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);

            if ( $actualesParalelas->where('hora_inicio',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                // ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);
            if ( $actualesParalelas->where('hora_fin',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva en proceso');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                // ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);

            if ( $actualesParalelas->where('hora_inicio','<',$start_date->format('H:i:s'))->where('hora_fin','>',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva en proceso');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                                        ->where('id_dotacion', $endowment->getKey())
                                        // ->doesntHave('payment')
                                        ->where('created_at', '>=', $dateParalela);
            if ( $actualesParalelas->where('hora_inicio','<',$final_date->format('H:i:s'))->where('hora_fin','>',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva en proceso');
            }

            if ($errors->count() > 0) {
                return $this->error_response(
                    'Por favor verifique los datos e intente nuevamente',
                    422,
                    $errors->toArray()
                );
            }

            $service = Payment::TENNIS_COURT_SERVICE;
            $id_parque = $auxParquePse->id_parque;

            $highStratum = $endowment->park->Estrato > 3;

            $total = 0;
            $idTarifaDiaAux   = $highStratum ? Price::TENNIS_COURT_4_5_6_DAY_NIGHT : Price::TENNIS_COURT_1_2_3_DAY_NIGHT;

            $day = clone $start_date;
            $night = clone $start_date;

            $horai = $start_date->format('H:i:s');
            $horaf = $final_date->format('H:i:s');

            $hours = $start_date->diffInHours($final_date);

            $details = [
                "hours" => $hours,
                "unit"  => 0,
                "type"  => "Diurno",
                "stratum" => $endowment->park->Estrato ?? null,
            ];

            $valor = Price::find($idTarifaDiaAux);
            $total = $hours * intval($valor->D_TOTAL_REDONDEADO);
            $details['unit'] = intval($valor->D_TOTAL_REDONDEADO);
            if ($start_date->greaterThanOrEqualTo($night->setTime(18, 0, 0, 0))){
                $details['type'] = "Nocturno";
            }

            $reserva = new Booking();
            $reserva->hora_inicio = $horai;
            $reserva->hora_fin    = $horaf;
            $reserva->fecha       = $date;
            $reserva->id_dotacion = $endowment->getKey();
            $reserva->valor       = $total;
            $reserva->save();

            //$auxConcepto = "Reserva Cancha ". $endowment->getKey() . ' ' . $fecha . ' ' . $horai . ' a ' . $horaf;

            $auxConcepto = "Reserva Cancha Tenis #{$endowment->getKey()}. Parque: {$endowment->park->Id_IDRD} / {$date->format('Y-m-d')} $horai a $horaf";

            $user = Profile::query()
                ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                ->where('user_id', auth('api')->user()->id)
                ->first();

            return $this->success_message([
                'name'          => toUpper("{$user->name} {$user->s_name}"),
                'surname'       => toUpper("{$user->surname} {$user->s_surname}"),
                'document_type_id' => pse_document_type($user->document_type->name ?? null),
                'document'       => $user->document ?? null,
                'email'         => $user->user->email ?? null,
                'address'       => $user->address ?? null,
                'phone'         => $user->mobile_phone ?? null,
                'booking_id'    => $reserva->id,
                'amount'        => $total,
                'park_id'       => $id_parque,
                'service_id'    => $service,
                'person_type_id'=> "N",
                'bank_id'       => null,
                'ip_address'    => $request->getClientIp(),
                'concept'       => Str::substr(toUpper($auxConcepto), 0, 100),
                'details'       => $details,
                'created_at'    => isset($reserva->created_at) ? $reserva->created_at->addMinutes(5)->format('Y-m-d H:i:s') : now()->addMinutes(5)->format('Y-m-d H:i:s'),
            ]);

        } catch (\Exception $exception) {
            if ($exception instanceof ModelNotFoundException) {
                return $this->error_response(
                    'Parque no registrado en el sistema de reservas',
                    422,
                    park_code_to_pse_code($endowment->park->Id_IDRD ?? "")
                );
            }

            return $this->error_response(
                'Algo pasó, por favor intenta nuevamente',
                500,
                $exception
            );
        }
    }

    public function processGrills(ParkEndowment $endowment, PaymentRequest $request)
    {
        try {
            $date = Carbon::parse($request->get('date'));
            $start_date = Carbon::parse($request->get('date').' '.$request->get('start_hour'));
            $final_date = Carbon::parse($request->get('date').' '.$request->get('final_hour'));

            $code = $endowment->park->Id_IDRD ?? "";

            $pse_code = park_code_to_pse_code($code);

            // El parque la Florida es el unico parque se reserva por todo el día.
            $isFlorida = $code == Payment::LA_FLORIDA && $endowment->all_day;

            if ($isFlorida) {
                $schedule = Schedule::where('id_dotacion', $endowment->getKey())
                    ->where('dia', $date->dayOfWeekIso)
                    ->first();
                $start_hour = $schedule->horai ?? '00:00:00';
                $final_hour = $schedule->horaf ?? '23:59:59';
                $start_date = Carbon::parse($request->get('date')." $start_hour");
                $final_date = Carbon::parse($request->get('date')." $final_hour");
            }

            $auxParquePse = PsePark::where('codigo_parque', $pse_code)->firstOrFail();

            // Almacena los errores que se generan en las validaciones.
            $errors = collect([]);

            $dias=[1=>'Lunes',2=>'Martes',3=>'Miercoles',4=>'Jueves',5=>'Viernes',6=>'Sabados',7=>'Domingos'];

            // dayOfWeekIso returns a number between 1 (monday) and 7 (sunday)
            $valido_inicial = Schedule::where('id_dotacion', $endowment->getKey())
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<=',$start_date->format('H:i:s'))
                ->where('horaf','>', $start_date->format('H:i:s'))
                ->count();
            $valido_final = Schedule::where('id_dotacion', $endowment->getKey())
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<', $final_date->format('H:i:s'))
                ->where('horaf','>=',$final_date->format('H:i:s'))
                ->count();

            $aux_sin_horario = false;

            if($valido_inicial && $valido_final) {

            } else {
                $rango = Schedule::where('id_dotacion', $endowment->getKey())
                    ->where('dia',$date->dayOfWeekIso)
                    ->get();
                foreach ($rango as $value) {
                    $horai = date('g:i A', strtotime($value->horai));
                    $horaf = date('g:i A', strtotime($value->horaf));
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' solo hay servicio de  '.$horai.' - '.$horaf);
                    $aux_sin_horario = true;
                }
            }
            if($valido_inicial == 0 && $valido_final == 0){
                if(! $aux_sin_horario){
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' no hay horarios disponibles');
                }
            }

            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2]);
                });

            if ( $actuales->where('hora_inicio',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2]);
                });
            if ( $actuales->where('hora_fin',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2]);
                });
            if ( $actuales->where('hora_inicio','<',$start_date->format('H:i:s'))->where('hora_fin','>',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actuales =  Booking::whereDate('fecha', $date)
                ->where('id_dotacion',$endowment->getKey())
                ->wherehas('payment',function($query){
                    return $query->whereIn('estado_id', [1, 2]);
                });
            if ( $actuales->where('hora_inicio','<',$final_date->format('H:i:s'))->where('hora_fin','>',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }

            // Toma las reservas realizadas con 5 minutos de anterioridad con respecto a la dotación
            // que se va a reservar.
            // $dateParalela = now()->addMinutes(-5);
            $dateParalela = now()->subMinutes(5);
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);

            if ( $actualesParalelas->where('hora_inicio',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva actual');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);
            if ( $actualesParalelas->where('hora_fin',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva en proceso');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);

            if ( $actualesParalelas->where('hora_inicio','<',$start_date->format('H:i:s'))->where('hora_fin','>',$start_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva en proceso');
            }
            $actualesParalelas =  Booking::whereDate('fecha',$date)
                ->where('id_dotacion', $endowment->getKey())
                ->doesntHave('payment')
                ->where('created_at', '>=', $dateParalela);
            if ( $actualesParalelas->where('hora_inicio','<',$final_date->format('H:i:s'))->where('hora_fin','>',$final_date->format('H:i:s'))->count() > 0){
                $errors->push('Hay un cruce con una reserva en proceso');
            }

            if ($errors->count() > 0) {
                return $this->error_response(
                    'Por favor verifique los datos e intente nuevamente',
                    422,
                    $errors->toArray()
                );
            }

            $service = Payment::GRILLS;
            $id_parque = $auxParquePse->id_parque;

            $highStratum = $endowment->park->Estrato > 3;

            $total = 0;
            $idTarifaAux = $highStratum ? Price::GRILL_4_5_6_DAY_NIGHT : Price::GRILL_1_2_3_DAY_NIGHT;

            $night = clone $start_date;
            $horai = $start_date->format('H:i:s');
            $horaf = $final_date->format('H:i:s');
            $hours = $start_date->diffInHours($final_date);

            $details = [
                "hours" => $hours,
                "unit"  => 0,
                "type"  => "Diurno",
                "stratum" => $endowment->park->Estrato ?? null,
            ];

            $valor = Price::find($idTarifaAux);
            $total = $hours * intval($valor->D_TOTAL_REDONDEADO);
            $details['unit'] = intval($valor->D_TOTAL_REDONDEADO);

            if ($start_date->greaterThanOrEqualTo($night->setTime(18, 0, 0, 0))){
                $details['type'] = "Nocturno";
            }

            $reserva = new Booking();
            $reserva->hora_inicio = $horai;
            $reserva->hora_fin    = $horaf;
            $reserva->fecha       = $date;
            $reserva->id_dotacion = $endowment->getKey();
            $reserva->valor       = $total;
            $reserva->save();

            //$auxConcepto = "Reserva Cancha ". $endowment->getKey() . ' ' . $fecha . ' ' . $horai . ' a ' . $horaf;

            $auxConcepto = "Reserva Asador #{$endowment->getKey()}. Parque: {$endowment->park->Id_IDRD} / {$date->format('Y-m-d')} $horai a $horaf";

            $user = Profile::query()
                ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                ->where('user_id', auth('api')->user()->id)
                ->first();
            if($code == Payment::LA_FLORIDA && $endowment->all_day){
                $details['unit'] = Payment::LA_FLORIDA_BOOKING_PAYMENT / 9;
                $details['hours'] = 9;
            }

            return $this->success_message([
                'name'          => toUpper("{$user->name} {$user->s_name}"),
                'surname'       => toUpper("{$user->surname} {$user->s_surname}"),
                'document_type_id' => pse_document_type($user->document_type->name ?? null),
                'document'       => $user->document ?? null,
                'email'         => $user->user->email ?? null,
                'address'       => $user->address ?? null,
                'phone'         => $user->mobile_phone ?? null,
                'booking_id'    => $reserva->id,
                'amount'        => $code == Payment::LA_FLORIDA && $endowment->all_day ? Payment::LA_FLORIDA_BOOKING_PAYMENT : $total,
                'park_id'       => $id_parque,
                'service_id'    => $service,
                'person_type_id'=> "N",
                'bank_id'       => null,
                'ip_address'    => $request->getClientIp(),
                'concept'       => Str::substr(toUpper($auxConcepto), 0, 100),
                "details"       => $details,
                'created_at'    => isset($reserva->created_at) ? $reserva->created_at->addMinutes(5)->format('Y-m-d H:i:s') : now()->addMinutes(5)->format('Y-m-d H:i:s'),
            ]);

        } catch (\Exception $exception) {
            if ($exception instanceof ModelNotFoundException) {
                return $this->error_response(
                    'Parque no registrado en el sistema de reservas',
                    422,
                    park_code_to_pse_code($endowment->park->Id_IDRD ?? "")
                );
            }

            return $this->error_response(
                'Algo pasó, por favor intenta nuevamente',
                500,
                $exception
            );
        }
    }


   public function freePayment(Request $request, $id)
    {
        try {
            log::info($request);
            // Aquí, $id es el valor que se pasa directamente en la URL.
            $reserva = Booking::find($id);
            
            if (!$reserva) {
                return $this->error_response(
                    'Reserva no encontrada',
                    404
                );
            }

            $reserva->is_successful = 1; 
            $reserva->save();

            return $this->success_message('Reserva gratis registrada exitosamente', 200, "");
        } catch (\Exception $exception) {
            log::info($exception);
            return $this->error_response(
                'Algo pasó, por favor intenta nuevamente',
                500,
                $exception
            );
        }
    }



    public function processSwimPools(ParkEndowment $endowment, PaymentRequest $request)
    {
        try {
            $date = Carbon::parse($request->get('date'));
            $start_date = Carbon::parse($request->get('date').' '.$request->get('start_hour'));
            $final_date = Carbon::parse($request->get('date').' '.$request->get('final_hour'));
            $hours = $start_date->diffInHours($final_date, false);
            $code = $endowment->park->Id_IDRD ?? "";
            $pse_code = park_code_to_pse_code($code);
            $auxParquePse = PsePark::where('codigo_parque', $pse_code)->firstOrFail();
            $lane_id = $request->get('lane');
            $lane = Lane::with('direction')->find($lane_id);
            $user = Profile::query()
                    ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                    ->where('user_id', auth('api')->user()->id)
                    ->first();

            $errors = collect([]);

            $time = strtotime(now()->startOfWeek()->addDay()->format('Y-m-d'));
            $start_week = date('Y-m-d',$time);
            $final_week = Carbon::parse($start_week)->addDays(7);

            $commonConditionsWeek = function ($query) use ($endowment, $start_week, $final_week, $user) {
                $query->where('id_dotacion', $endowment->getKey())
                            ->whereBetween('fecha', [$start_week, $final_week])
                            ->where('documento', $user->document);
            };
    
            $total_bookings_week_with_payment = Booking::query()
                ->where($commonConditionsWeek)
                ->whereHas('payment', function ($query) {
                    $query->whereIn('estado_id', [1, 2]);
                })
                ->with('payment')
                ->count();
    
            $total_bookings_week_without_payment = Booking::query()
                ->where($commonConditionsWeek)
                ->where('created_at', '>', now()->subMinutes(5))
                ->doesntHave('payment')
                ->count();
            
            $total_bookings_week = $total_bookings_week_with_payment + $total_bookings_week_without_payment;

            if ($total_bookings_week >= 2){
                $errors->push('No se pueden reservar más de 2 horas a la semana.');
                return $this->error_response(
                    'Por favor verifique los datos e intente nuevamente',
                    422,
                    $errors->toArray()
                );
            }

            $dias=[1=>'Lunes',2=>'Martes',3=>'Miercoles',4=>'Jueves',5=>'Viernes',6=>'Sabados',7=>'Domingos'];
            $valido_inicial = Schedule::where('id_dotacion', $endowment->getKey())->where('id_carril', $lane_id)
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<=',$start_date->format('H:i:s'))
                ->where('horaf','>', $start_date->format('H:i:s'))
                ->count();
            $valido_final = Schedule::where('id_dotacion', $endowment->getKey())->where('id_carril', $lane_id)
                ->where('dia', $date->dayOfWeekIso)
                ->where('horai','<', $final_date->format('H:i:s'))
                ->where('horaf','>=',$final_date->format('H:i:s'))
                ->count();

            if($hours > 1 || $hours <= 0){
                $errors->push('No se puede reservar por mas de 1 hora');
            }

            $aux_sin_horario = false;

            if($valido_inicial && $valido_final) {

            } else {
                $rango = Schedule::where('id_dotacion', $endowment->getKey())->where('id_carril', $lane_id)
                    ->where('dia',$date->dayOfWeekIso)
                    ->get();
                foreach ($rango as $value) {
                    $horai = date('g:i A', strtotime($value->horai));
                    $horaf = date('g:i A', strtotime($value->horaf));
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' solo hay servicio de  '.$horai.' - '.$horaf);
                    $aux_sin_horario = true;
                }
            }
            if($valido_inicial == 0 && $valido_final == 0){
                if(! $aux_sin_horario){
                    $errors->push('Los '.$dias[$date->dayOfWeekIso].' no hay horarios disponibles');
                }
            }

            $commonConditions = function ($query) use ($endowment, $lane_id, $date, $start_date, $final_date) {
                $query->where('id_dotacion', $endowment->Id)
                    ->where('id_carril', $lane_id)
                    ->whereDate('fecha', $date)
                    ->where('hora_inicio', $start_date->format('H:i:s'))
                    ->where('hora_fin', $final_date->format('H:i:s'));
            };
    
            $current_bookings_with_payment = Booking::query()
                ->where($commonConditions)
                ->whereHas('payment', function ($query) {
                    $query->whereIn('estado_id', [1, 2]);
                })
                ->with('payment');
    
            $current_bookings_without_payment = Booking::query()
                ->where($commonConditions)
                ->where('created_at', '>', now()->subMinutes(5))
                ->doesntHave('payment');

            $quota = $lane->cupo;
            $used_quota = $current_bookings_with_payment->count() + $current_bookings_without_payment->count();
            
            if ($used_quota >= $quota){
                $errors->push('El total de cupos ('.$quota.') ha sido alcanzado.');
                return $this->error_response(
                    'Por favor verifique los datos e intente nuevamente',
                    422,
                    $errors->toArray()
                );
            }

            if ($current_bookings_with_payment->where('documento',$user->document)->exists()){
                $errors->push('Hay un cruce con una reserva tuya actual');
            }

            if ($current_bookings_without_payment->where('documento',$user->document)->exists()){
                $errors->push('Hay un cruce con una reserva tuya en proceso');
            }

            if ($errors->count() > 0) {
                return $this->error_response(
                    'Por favor verifique los datos e intente nuevamente',
                    422,
                    $errors->toArray()
                );
            }

            $service = Payment::POOLS;
            $id_parque = $auxParquePse->id_parque;
            $highStratum = $endowment->park->Estrato > 3;
            $total = 0;
            $idTarifaAux = Price::SWIMMING_POOL_CEFE;

            $night = clone $start_date;
            $horai = $start_date->format('H:i:s');
            $horaf = $final_date->format('H:i:s');
            $hours = $start_date->diffInHours($final_date);

            $details = [
                "hours" => $hours,
                "unit"  => 0,
                "type"  => "Diurno",
                "stratum" => $endowment->park->Estrato ?? null,
            ];

            $valor = Price::find($idTarifaAux);
            $total = $hours * intval($valor->D_TOTAL_REDONDEADO);
            $details['unit'] = intval($valor->D_TOTAL_REDONDEADO);

            $swimmingPool = SwimPool::where('id_dotacion', $endowment->getKey())->first();
            if ($swimmingPool) {
                if ($swimmingPool->id_clase_piscina === "2") {
                    $total = $details['unit'] = 0;
                } else {
                    $age = Carbon::now()->diffInYears($user->birthdate);
                    $isEligibleForDiscount = $user->has_disability === 'SI' || $age >= 60;
                    if ($isEligibleForDiscount) {
                        $total = $details['unit'] = 0;
                    }
                }
            }                        

            if ($start_date->greaterThanOrEqualTo($night->setTime(18, 0, 0, 0))){
                $details['type'] = "Nocturno";
            }

            $reserva = new Booking();
            $reserva->hora_inicio = $horai;
            $reserva->hora_fin    = $horaf;
            $reserva->fecha       = $date;
            $reserva->id_dotacion = $endowment->getKey();
            $reserva->id_carril   = $lane_id;
            $reserva->is_successful = 0; 
            $reserva->valor       = $total;
            $reserva->documento   = $user->document;
            $reserva->save();

            $auxConcepto = "Reserva Piscina #{$endowment->getKey()} CARRIL {$lane->carril} - {$lane->direction->sentido_carril}. Parque: {$endowment->park->Id_IDRD} / {$date->format('Y-m-d')} $horai a $horaf";

            return $this->success_message([
                'name'          => toUpper("{$user->name} {$user->s_name}"),
                'surname'       => toUpper("{$user->surname} {$user->s_surname}"),
                'document_type_id' => pse_document_type($user->document_type->name ?? null),
                'document'       => $user->document ?? null,
                'email'         => $user->user->email ?? null,
                'address'       => $user->address ?? null,
                'phone'         => $user->mobile_phone ?? null,
                'booking_id'    => $reserva->id,
                'amount'        => $total,
                'park_id'       => $id_parque,
                'service_id'    => $service,
                'person_type_id'=> "N",
                'bank_id'       => null,
                'ip_address'    => $request->getClientIp(),
                'concept'       => Str::substr(toUpper($auxConcepto), 0, 100),
                "details"       => $details,
                'created_at'    => isset($reserva->created_at) ? $reserva->created_at->addMinutes(5)->format('Y-m-d H:i:s') : now()->addMinutes(5)->format('Y-m-d H:i:s'),
            ]);

        } catch (\Exception $exception) {
            if ($exception instanceof ModelNotFoundException) {
                return $this->error_response(
                    'Parque no registrado en el sistema de reservas',
                    422,
                    park_code_to_pse_code($endowment->park->Id_IDRD ?? "")
                );
            }
            
            return $this->error_response(
                'Algo pasó, por favor intenta nuevamente',
                500,
                $exception
            );
        }
    }

    public function pseData(Request $request)
    {
        $user = Profile::query()
            ->where('profile_type_id', Profile::PROFILE_PERSONAL)
            ->where('user_id', auth('api')->user()->id)
            ->first();

        $name = $user->name ?? null;
        $s_name = $user->s_name ?? null;
        $surname = $user->surname ?? null;
        $s_surname = $user->s_surname ?? null;

        return $this->success_message([
            'name'          => toUpper("{$name} {$s_name}"),
            'surname'       => toUpper("{$surname} {$s_surname}"),
            'document_type_id' => pse_document_type($user->document_type->name ?? null),
            'document'      => $user->document ?? null,
            'address'       => $user->address ?? null,
            'email'         => $user->user->email ?? null,
            'phone'         => $user->mobile_phone ?? null,
            'booking_id'    => null,
            'amount'        => null,
            'park_id'       => null,
            'service_id'    => null,
            'person_type_id'=> "N",
            'bank_id'       => null,
            'ip_address'    => $request->getClientIp(),
            'concept'       => null,
            "details"       => [],
            'created_at'    => now()->format('Y-m-d H:i:s'),
        ]);
    }

    public function bookings(BookingRequest $request)
    {
        $bookings = BookingView::query()
            ->whereIn('document', auth('api')->user()->profiles->pluck('document')->toArray())
            ->where('status_id', Status::OK)
            ->latest();

        if ($request->has(['start_date', 'final_date'])) {
            $bookings = $bookings
                ->whereBetween('date', [ $request->get('start_date'), $request->get('final_date') ])
                ->get();
        } else {
            $bookings = $bookings->paginate($this->per_page);
        }

        return $this->success_response(
            BookingViewResource::collection($bookings),
            Response::HTTP_OK,
            BookingViewResource::headers()
        );
    }

    public function bookingsBkp(BookingRequest $request)
    {

        $bookings = Booking::with('endowment:Id,Descripcion,Id_Parque')
                    ->whereHas('payment', function ($query) {
                        return $query->whereIn(
                            'identificacion',
                            auth('api')->user()->profiles->pluck('document')->toArray()
                        )->where('estado_id', Status::OK);
                    })
                    ->latest();

        if ($request->has(['start_date', 'final_date'])) {
            $bookings = $bookings
                ->whereBetween('fecha', [ $request->get('start_date'), $request->get('final_date') ])
                ->get();
        } else {
            $bookings = $bookings->paginate($this->per_page);
        }

        return $this->success_response(
          BookingResource::collection($bookings),
            Response::HTTP_OK,
            BookingResource::headers()
        );
    }

    public function activityPayment(CitizenSchedule $schedule, $reference)
    {
        try {
            $profiles = auth('api')->user()->profiles->pluck('id')->toArray();
            abort_unless(
                in_array($schedule->profile_id, $profiles),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                "No se encuentra la actividad con el perfil asociado."
            );
            $payment = Payment::query()
                ->where('codigo_pago', $reference)
                ->firstOrFail();
            $schedule->reference_pse = $reference;
            $schedule->save();
            return $this->success_message(
                'Un momento por favor, será redirigido a su banco para completar el pago.',
                Response::HTTP_OK,
                Response::HTTP_OK,
                [
                    'payment'  => new PaymentResource($payment),
                    'activity' => new CitizenScheduleResource($schedule),
                ]
            );
        } catch (\Exception $exception) {
            if ($exception instanceof ModelNotFoundException) {
                return $this->error_response(
                    "No se encuentra la referencia de pago $reference en el sistema",
                    422
                );
            }
            return $this->error_response(
                'Algo pasó, por favor intenta nuevamente',
                500,
                $exception
            );
        }
    }

    public function pendingToPay()
    {
        $profiles = auth('api')->user()->profiles->pluck('id')->toArray();
        $pendings = CitizenSchedule::query()
            ->whereIn("profile_id", $profiles)
            ->where("status_id", "!=", \App\Entities\CitizenPortal\Status::SUBSCRIBED)
            ->where("payment_at", ">=", now())
            ->get();
        return $this->success_message(
            $pendings->map(function ($model) {
                return [
                    "id" => $model->id ?? null,
                    "activity" => $model->schedule->activities->name ?? null,
                    "payment_at" => isset($model->payment_at) ? $model->payment_at->format("Y-m-d H:i:s") : null,
                ];
            }),
            Response::HTTP_OK,
            Response::HTTP_OK,
            $pendings
        );
    }

    public function validateFreePayment(Request $request, $id)
{
    try {
        $isFree = false;

        // Obtener el perfil del usuario autenticado
        $user = Profile::query()
            ->where('profile_type_id', Profile::PROFILE_PERSONAL)
            ->where('user_id', auth('api')->user()->id)
            ->first();

        // Buscar la piscina por ID
        $swimmingPool = SwimPool::find($id);

        // Validar si cumple las condiciones para ser gratuito
        if ($swimmingPool) {
            if ($swimmingPool->id_clase_piscina === "2" || $this->validateAgeDiscapacity($user) === true) {
                $isFree = true;
            }
        }
        // Retornar el mensaje de éxito con el valor de $isFree
        return $this->success_message([
            'message' => 'Reserva gratuita',
            'isFree' => $isFree
        ], 200, true);

    } catch (\Exception $exception) {
        return $this->error_response(
            'Algo pasó, por favor intenta nuevamente',
            500,
            $exception
        );
    }
}


   public function validateAgeDiscapacity($user)
    {
        $age = Carbon::now()->diffInYears($user->birthdate);
        return $user->has_disability === 'SI' || $age >= 60;
    }


    
}
