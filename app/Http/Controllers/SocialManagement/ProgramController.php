<?php

namespace App\Http\Controllers\SocialManagement;

use App\Entities\SocialManagement\Program;
use App\Entities\SocialManagement\ProgramActivity;
use App\Http\Resources\SocialManagement\ProgramResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ProgramController extends Controller
{
    public function index($park)
    {
        /*
        $types = [
            16, // Mesas de trabajo
            17, // Acuerdos ciudadanos
            18, // Pactos ciudadanos
            31, // Firmas de acuerdos ciudadanos
        ];
        $programmings = ProgramActivity::whereIn('activity_id', $types)
                                        ->get(['programming_id'])
                                        ->toArray();
        */

        $data = Program::with(['programming_files', 'programming_images', 'programming_activities'])
                        //->whereIn('id', $programmings)
                        ->where('park_code', $park)
                        ->orderBy('execution_date', 'desc')
                        ->paginate( $this->per_page );


        return $this->success_response(
            ProgramResource::collection( $data )
        );
    }
}
