<?php

namespace App\Http\Controllers\Citizen;


use App\Entities\CitizenPortal\Observation;
use App\Entities\CitizenPortal\Profile;
use App\Http\Controllers\Controller;
use App\Http\Resources\Citizen\ObservationResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ObservationController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Profile $profile)
    {
        return $this->success_response(
            ObservationResource::collection(
                $profile->observations()->latest()->paginate($this->per_page)
            ),
            Response::HTTP_OK,
            [
                'title' => trans_choice(
                    'citizen.observation.title',
                    $profile->observations()->whereNull('read_at')->count(),
                    [
                        'pending' => $profile->observations()->whereNull('read_at')->count(),
                        'total' => $profile->observations()->count(),
                    ]
                )
            ]
        );
    }

    /**
     * @param Profile $profile
     * @param Observation $observation
     * @return JsonResponse
     */
    public function show(Profile $profile, Observation $observation)
    {
        $observation->read_at = now();
        $observation->save();
        return $this->success_message(
            __('validation.handler.success')
        );
    }

    public function markAllAsRead(Profile $profile)
    {
        $profile
            ->observations()
            ->update([
                'read_at' => now()
            ]);
        return $this->success_message(
            __('validation.handler.success')
        );
    }
}
