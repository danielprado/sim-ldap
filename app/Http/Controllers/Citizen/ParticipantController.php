<?php

namespace App\Http\Controllers\Citizen;


use App\Entities\CitizenPortal\Participant;
use App\Entities\CitizenPortal\ParticipantRole;
use App\Entities\CitizenPortal\TeanSchedule;
use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\Team;
use App\Http\Controllers\Controller;
use App\Http\Requests\Citizen\ParticipantRequest;
use App\Http\Requests\Citizen\TeamRequest;
use App\Http\Resources\Citizen\ParticipantResource;
use App\Http\Resources\Citizen\TeamResource;
use App\Notifications\SendgridTemplates;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParticipantController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Team $team)
    {
        return $this->success_response(
            ParticipantResource::collection( $team->participants )
        );
    }


    public function store(Team $team, ParticipantRequest $request)
    {
        if (!isset($team->activity)){
            return $this->error_response(
                "El equipo no tiene una actividad vinculada."
            );            
        }
        if ($team->activity->quota < $team->activity->taken) {
            return $this->error_response(
                "Todos los cupos han sido tomados."
            );
        }
        $validate_participants = Participant::query()
            ->where('team_id', $team->id)
            ->where('profile_id', $request->get('profile_id'))
            ->count();
        if ($validate_participants > 0) {
            return $this->validation_errors([
                "profile_id" => ["Este usuario ya pertenece a este equipo"]
            ]);
        }
        $validate_other_participants = Participant::query()
            ->whereHas("team", function ($query) use ($team) {
                return $query->where("activity_id", $team->activity_id);
            })
            ->where('team_id', "!=", $team->id)
            ->where('profile_id', $request->get('profile_id'))
            ->count();
        if ($validate_other_participants > 0) {
            return $this->validation_errors([
                "profile_id" => ["Este usuario ya está participando en otro equipo de esta actividad"]
            ]);
        }
        $activity_max = isset($team->activity->max_team_participants_quota) ? (int) $team->activity->max_team_participants_quota : 0;
        $participants_count = $team->participants()->where('role_id', ParticipantRole::PARTICIPANT)->count();
        if ($participants_count > 0 && $activity_max == $participants_count) {
            return $this->validation_errors([
                "profile_id" => ["Ya superaste el límite de participantes permitidos."]
            ]);
        }
        $profile = Profile::with(['sex_name', 'sexual_orientation', 'gender', 'population_group', 'ethnic_group', 'disability'])
                            ->where("id", $request->get('profile_id'))
                            ->first();
        $age = $profile->birthdate->age ?? null;
        if (is_null($age)) {
            return $this->validation_errors([
                "profile_id" => ["Por favor actualiza la información del beneficiario o del perfil para continuar."]
            ]);
        }
        
        if ($request->get('role_id') == ParticipantRole::DELEGATE) {
            if ($age < 18){
                return $this->error_response(
                    "El delegado debe ser mayor de edad."
                );
            }
            $staff_participants_max = isset($team->activity->staff_participants_num) ? (int)$team->activity->staff_participants_num : 0;
            if ($staff_participants_max > 0){
                $staff_participants_count = $team->participants()->where('role_id', '!=', ParticipantRole::PARTICIPANT)->count();
                if ($staff_participants_count >= $staff_participants_max){
                    return $this->error_response(
                        "El límite de miembros del staff ha sido alcanzado."
                    );
                }
            }
        }

        if ($request->get('role_id') == ParticipantRole::PARTICIPANT) {
            if ($age < $team->activity->min_age || $age > $team->activity->max_age) {
                return $this->validation_errors([
                    "profile_id" => ["No cumples con los requisitos de edad para esta actividad."]
                ]);
            }
            $sex_id = $this->getArray($team->activity->sex_id);
            $orientation_id = $this->getArray($team->activity->orientation_id);
            $gender_id = $this->getArray($team->activity->gender_id);
            $ethnic_group_id = $this->getArray($team->activity->ethnic_group_id);
            $population_group_id = $this->getArray($team->activity->population_group_id);
            $disability_id = $this->getArray($team->activity->disability_id);
    
            if (isset($sex_id) && is_array($sex_id)) {
                if (!isset($profile->sex_name) || !in_array($profile->sex_name->id, $sex_id)){
                    return $this->error_response(
                        "No cumples con los requisitos de sexo para esta actividad."
                    );
                }
            }
            if (isset($orientation_id) && is_array($orientation_id)) {
                if (!isset($profile->sexual_orientation) || !in_array($profile->sexual_orientation->id, $orientation_id)){
                    return $this->error_response(
                        "No cumples con los requisitos de orientación sexual para esta actividad."
                    );
                }
            }
            if (isset($gender_id) && is_array($gender_id)) {
                if (!isset($profile->gender) || !in_array($profile->gender->id, $gender_id)){
                    return $this->error_response(
                        "No cumples con los requisitos de género para esta actividad."
                    );
                }
            }
            if (isset($population_group_id) && is_array($population_group_id)) {
                if (!isset($profile->population_group) || !in_array($profile->population_group->id, $population_group_id)){
                    return $this->error_response(
                        "No cumples con los requisitos de grupo social para esta actividad."
                    );
                }
            }
            if (isset($ethnic_group_id) && is_array($ethnic_group_id)) {
                if (!isset($profile->ethnic_group) || !in_array($profile->ethnic_group->id, $ethnic_group_id)){
                    return $this->error_response(
                        "No cumples con los requisitos de grupo étnico para esta actividad."
                    );
                }
            }
            if (isset($disability_id) && is_array($disability_id)) {
                if (!isset($profile->disability) || !in_array($profile->disability->id, $disability_id)){
                    return $this->error_response(
                        "No cumples con los requisitos de discapacidad para esta actividad."
                    );
                }
            }
        }

        $captain = ParticipantRole::query()->where("name", "CAPITÁN")->first();
        if ($captain){
            $count_captain = Participant::query()
                ->where('team_id', $team->id)
                ->where('role_id', $captain->id)
                ->count();
            if ($count_captain > 0 && $request->get("role_id") == $captain->id) {
                return $this->validation_errors([
                    "role_id" => ["Ya hay un capitán asignado a este equipo."]
                ]);
            }
        }

        try {
            if (isset($profile->user->email)) {
                $notify = new SendgridTemplates();
                $name = auth("api")->user()->user_profile->email_full_name;
                $day = isset($team->activity->weekday_name) ? (string) $team->activity->weekday_name : "";
                $hour = isset($team->activity->daily_name) ? (string) $team->activity->daily_name : "";
                $notify->sendTeamInvitation(
                     $profile->user->email,
                    [
                        "name" => $profile->email_full_name,
                        "line_1" => "Le informamos que {$name} lo agregó al equipo {$team->name} para participar en la",
                        // "team" => $team->name,
                        "sport" => isset($team->activity->activity_name) ? (string) $team->activity->activity_name : "",
                        "stage" => isset($team->activity->stage_name) ? (string) $team->activity->stage_name : "",
                        "hour" => toUpper($day." ".$hour),
                        "observation" => "Debe ingresar al Portal Ciudadano y aceptar la solicitud para poder participar.",
                        "info" => "Le invitamos a ingresar al portal ciudadano para conocer más servicios que el IDRD tiene para usted."
                    ]
                );
            }
            DB::beginTransaction();
            $team->participants()->create($request->validated());
            if (($participants_count+1) >= $team->activity->min_team_participants_quota){
                $team_schedule = TeanSchedule::where('team_id', $team->id)->where('schedule_id', $team->activity->id)->first();
                if ($team_schedule){
                    $team_schedule->update(['status_id' => TeanSchedule::SUBSCRIBED]);
                }
            }
            DB::commit();
            return $this->success_message('Participantes añadidos satisfactoriamente.');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error_response(
                'No se pudo asociar al participante, intenta más tarde.',
                422,
                $e->getMessage()
            );
        }
    }

    public function update(Team $team, Participant $participant)
    {
        $participant->accepted_at = now();
        $participant->save();
        return $this->success_message('Tu invitación ha sido marcada como aceptada.');
    }

    public function destroy(Team $team, Participant $participant)
    {
        $participant_count = $team->participants()->count(); 
        $participant->delete();

        if ($team->activity){
            $team_schedule = TeanSchedule::where('team_id', $team->id)->where('schedule_id', $team->activity->id)->first();
            if (($participant_count-1) < $team->activity->min_team_participants_quota) {
                $team_schedule->update(['status_id' => TeanSchedule::PENDING_SUBSCRIBE]);
            }
        }
        
        if ($participant->role_id == ParticipantRole::DELEGATE){
            $team->delete();   
        }
        return $this->success_message('Participantes eliminado satisfactoriamente.');
    }

    public function getArray($val) {
        return isset($val) ? array_map('intval', explode(',', $val)) : null;
    }
}
