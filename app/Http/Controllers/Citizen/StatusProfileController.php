<?php

namespace App\Http\Controllers\Citizen;

use App\Entities\CitizenPortal\File;
use App\Entities\CitizenPortal\Profile;
use App\Entities\Security\PreRegister;
use App\Entities\Security\User;
use App\Http\Requests\Citizen\StatusProfileRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatusProfileController extends Controller
{
    public function index(StatusProfileRequest $request)
    {
        $pre_register = PreRegister::query()
            ->where("document", $request->get('document'))
            ->where("email", $request->get('email'))
            ->first();
        $response = [];
        if (isset($pre_register->code)) {
            $response[] = [
                "status" => "Tu estado actual es pre registro",
                "details" => "Si no recibiste el código, por favor copia el siguiente **{$pre_register->code}** y da clic en el botón **CONTINUAR MI REGISTRO**. Asegúrate que el correo esté bien escrito ya que recibirás notificaciones por parte del Instituto al correo registrado, si este está mal por favor realiza el cambio de correo una vez inicies sesión.",
                "created_at" => isset($pre_register->created_at) ? $pre_register->created_at->format("Y-m-d H:i:s") : null,
                "action" => [
                    "to" => [
                        "name" => "Register",
                        "params" => [
                            "email" => $request->get('email'),
                            "code"  => $pre_register->code
                        ]
                    ],
                    "text"   => "Continuar mi registro"
                ],
            ];
            return $this->success_message($response);
        }
        $profile = User::query()
            ->where('email', $request->get('email'))
            ->whereHas("profiles", function ($query) use($request) {
                return $query->where("document", $request->get("document"));
            })
            ->with([
                "profiles" => function($query) use($request) {
                    return $query->where("document", $request->get("document"))->first();
                }
            ])
            ->first();
        if (isset($profile->email)) {
            $verified_at = $profile->profiles[0]->verified_at ?? null;
            $status_id = $profile->profiles[0]->status_id ?? null;
            $email = $profile->email_verified_at ?? null;
            $files = collect($profile->profiles[0]->files ?? [])
                ->where("file_type_id", File::DOCUMENT)
                ->first();
            $result = $status_id == Profile::VERIFIED
                ? [
                    "status" => "Tu perfil está verificado",
                    "details" => "Puedes inscribirte a las actividades y programas ofertados por el IDRD",
                    "created_at" => $verified_at ? $verified_at->format("Y-m-d H:i:s") : null
                ]
                : [
                    "status" => "Tu perfil aún no está verificado",
                    "details" => "Debes tener en cuenta las observaciones recibidas por el IDRD, actualiza tus datos y documento de identidad para que la verificación sea satisfactoria y puedas acceder a los programas y servicios que ofrece el instituto",
                    "created_at" => isset($profile->created_at) ? $profile->created_at->format("Y-m-d H:i:s") : null
                ];
            $response = [
                [
                    "status" => "Te encuentras registrado",
                    "details" => "Tu proceso de registro al portal fue satisfactorio.",
                    "created_at" => isset($profile->created_at) ? $profile->created_at->format("Y-m-d H:i:s") : null
                ],
                [
                    "status" => $email ? "Tu correo electrónico está verificado":"Tu correo electrónico está sin verificar",
                    "details" => $email
                        ? "Con tu correo verificado puedes iniciar sesión en el portal, actualizar tu información o registrar beneficiarios."
                        : "Debes iniciar sesión en el portal y realizar el envío del código de verificación a tu correo, si tu correo es incorrecto por favor solicita el cambio del mismo escribiendo a atncliente@idrd.gov.co indicando el nuevo correo y número de documento con el que te registraste. Recuerda revisar tu bandeja de Spam o no deseados",
                    "created_at" => $email ? $email->format("Y-m-d H:i:s") : null,
                ],
                $result,
                [
                    "status" => $files ? "Documento de identidad adjunto" : "Debes adjuntar tu documento de identidad",
                    "details" => $files
                        ? $files->verified_at ? "Tu documento se encuentra verificado" : "Tu documento aún se encuentra en revisión"
                        : "El documento de identidad es importante para realizar la verificación de tu perfil",
                    "created_at" => isset($files->verified_at) ? $files->verified_at->format("Y-m-d H:i:s") : null
                ]
            ];
            return $this->success_message($response);
        }
        $response[] = [
          "status"  => "Tu búsqueda no arrojó ningún resultado",
          "details" => "Por favor verifica los datos con los que realizaste la inscripción.",
          "created_at" => now()->format("Y-m-d H:i:s")
        ];
        return $this->success_message($response);
    }
}
