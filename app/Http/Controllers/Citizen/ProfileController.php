<?php

namespace App\Http\Controllers\Citizen;

use App\Entities\CitizenPortal\File;
use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\ProfileView;
use App\Http\Requests\Citizen\FileRequest;
use App\Http\Requests\Citizen\ProfileRequest;
use App\Http\Requests\Citizen\UpdateProfileRequest;
use App\Http\Resources\Citizen\ProfileResource;
use App\Http\Resources\Citizen\ProfileViewResource;
use App\Notifications\SendgridTemplates;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    private $relations = [];

    public function __construct()
    {
        parent::__construct();
        $this->relations = [
            'observations',
            'observations as observations_read_at_count' => function($query) {
                return $query->whereNull('read_at');
            },
            'files',
            'files as pending_files_count' => function($query) {
                return $query->where('status_id', "!=", Profile::VERIFIED);
            },
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        return $this->success_response(
          ProfileViewResource::collection(
              auth('api')
                  ->user()
                  ->profiles()
                  ->withCount($this->relations)
                  ->where('profile_type_id', Profile::PROFILE_BENEFICIARY)
                  //->orderBy((new Profile)->getSortableColumn($this->column), $this->order)
                  ->paginate($this->per_page)
          ),
            Response::HTTP_OK,
            ProfileResource::headers()
        );
    }

    public function allProfiles(Request $request)
    {
        $profiles = auth('api')->user()->profiles()->get()->map(function ($model) use ($request) {
            $name = $model->name ?? null;
            $s_name = $model->s_name ?? null;
            $surname = $model->surname ?? null;
            $s_surname = $model->s_surname ?? null;

            return [
                "id" => $model->id ?? null,
                "name" => $model->full_name,
                "age" => $model->birthdate->age ?? null,
                "verified" => $model->verified_at,
                "disability" => $model->has_disability ?? "NO",
                'profile_type_id' => $model->profile_type_id,
                "to_payment" => [
                    "principal" =>  $model->profile_type_id == Profile::PROFILE_PERSONAL,
                    "verified" => $model->verified_at,
                    'name'          => toUpper("{$name} {$s_name}"),
                    'surname'       => toUpper("{$surname} {$s_surname}"),
                    "document_type_id" => pse_document_type($model->document_type->name ?? null),
                    "document" => $model->document,
                    "email" => $model->user->email ?? null,
                    "phone" => $model->mobile_phone,
                ]
            ];
        });

        return $this->success_message($profiles);
    }

    public function allProfilesVerified(Request $request)
    {
        $profiles = auth('api')->user()->profiles()
            ->where('status_id', 3) // Filtra por status_id = 3
            ->get()
            ->map(function ($model) use ($request) {
                $name = $model->name ?? null;
                $s_name = $model->s_name ?? null;
                $surname = $model->surname ?? null;
                $s_surname = $model->s_surname ?? null;

                return [
                    "id" => $model->id ?? null,
                    "name" => $model->full_name,
                    "age" => $model->birthdate->age ?? null,
                    "verified" => $model->verified_at,
                    "disability" => $model->has_disability ?? "NO",
                    'profile_type_id' => $model->profile_type_id,
                    'document' => $model->document,
                    "to_payment" => [
                        "principal" =>  $model->profile_type_id == Profile::PROFILE_PERSONAL,
                        "verified" => $model->verified_at,
                        'name'          => toUpper("{$name} {$s_name}"),
                        'surname'       => toUpper("{$surname} {$s_surname}"),
                        "document_type_id" => pse_document_type($model->document_type->name ?? null),
                        "document" => $model->document,
                        "email" => $model->user->email ?? null,
                        "phone" => $model->mobile_phone,
                    ]
                ];
            });

        return $this->success_message($profiles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProfileRequest $request
     * @return JsonResponse
     */
    public function store(ProfileRequest $request)
    {
        try {
            DB::beginTransaction();
            $profile = auth('api')
                ->user()
                ->profiles()
                ->save(new Profile(
                    array_merge(
                        [
                            'profile_type_id' => Profile::PROFILE_BENEFICIARY,
                        ],
                        $request->validated()
                    )
                ));
            $request->request->add([
                "file_type_id" => File::DOCUMENT,
            ]);
            $profile->verified_at = $request->get("decoded") == true ? now() : null;
            $profile->status_id = $request->get("decoded") == true ? Profile::VERIFIED : Profile::PENDING;
            $profile->save();
            $this->saveFile($request, $profile);
            DB::commit();
            $name = toTitle($request->get("name"));
            $surname = toTitle($request->get("surname"));
            $user = auth("api")->user()->user_profile->email_full_name ?? "Ciudadano";
            $notify = new SendgridTemplates();
            $notify->sendWarningMessage(
                auth("api")->user()->email,
                [
                    "name"      => $user,
                    "sub_user"  => "$name $surname",
                    "status"    =>  "En Validación",
                    "observation" => "Los datos de identificación registrados serán validados frente al documento cargado.",
                    "info"  =>  "Este proceso puede tardar entre 3 a 5 días hábiles."
                ],
                "Portal Ciudadano - Registro de Beneficiario"
            );
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED
            );
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param ProfileView $profile
     * @return JsonResponse
     */
    public function show(Profile $profile)
    {
        return $this->success_response(
            new ProfileViewResource($profile->loadCount($this->relations)),
            Response::HTTP_OK,
            ProfileResource::headers()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProfileRequest $request
     * @param Profile $profile
     * @return JsonResponse
     */
    public function update(UpdateProfileRequest $request, Profile $profile)
    {
        if ($request->hasFile("file")) {
            $request->request->add([
                "file_type_id" => File::DOCUMENT,
            ]);
            $this->saveFile($request, $profile, true);
            /*
            if (!is_null($profile->document)) {
                $request->request->add([
                    "file_type_id" => File::DOCUMENT,
                ]);
                if (!$profile->verified_at && $request->get("decoded") == true) {
                    $profile->verified_at =  now();
                    $profile->status_id = Profile::VERIFIED;
                }
                $this->saveFile($request, $profile, true);
            } else if (is_null($profile->document)) {
                $request->request->add([
                    "file_type_id" => File::DOCUMENT,
                ]);
                if (!$profile->verified_at && $request->get("decoded") == true) {
                    $profile->verified_at =  now();
                    $profile->status_id = Profile::VERIFIED;
                }
                $this->saveFile($request, $profile, true);
            } else {
                return $this->error_response(
                  "El número de documento registrado no coincide con el recibido."
                );
            }
            */
        }
        $profile->fill($request->validated());
        $profile->save();
        $profile->disability_id = $request->get('disability_id');
        $profile->save();
        return $this->success_message(
            __('validation.handler.updated')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Profile $profile
     * @return JsonResponse
     */
    public function destroy(Profile $profile)
    {
        $has_observation = $profile->observations()->exists();
        $has_files = $profile->files()->exists();
        $has_activities = $profile->user_schedules()->exists();
        if ($has_observation || $has_files || $has_activities) {
            return $this->error_response(
                "El beneficiario que intentas eliminar ya se encuentra inscrito a una o varias actividades o tiene información relacionada como archivos u observaciones por funcionarios."
            );
        }
        $profile->delete();
        return $this->success_message(
            __('validation.handler.deleted'),
            Response::HTTP_OK,
            Response::HTTP_NO_CONTENT
        );
    }

    public function saveFile(Request $request, Profile $profile, $update = false)
    {
        $filename = app(FileController::class)->saveFile($request, $profile);
        if ($update) {
            $file = $profile->files()->where("file_type_id", File::DOCUMENT)->first();
            if (isset($file->file)) {
                if (Storage::disk("local")->exists("portal/{$file->file}")) {
                    Storage::disk("local")->delete("portal/{$file->file}");
                }
                $file->delete();
            }
        }
        $file = $profile->files()->save(new File([
            'file'          =>  "$filename",
            "file_type_id"  => File::DOCUMENT,
            'status_id'    => $request->get("decoded") == true ? File::VERIFIED : File::PENDING,
        ]));
        $file->verified_at = $request->get("decoded") == true ? now() : null;
        return $file->save();
    }

    public function saveFileComplete(Request $request, Profile $profile, $requestName, $file_type_id, $update = false)
    {
        $filename = app(FileController::class)->saveFileComplete($request, $profile, $requestName);
        if ($update) {
            $file = $profile->files()->where("file_type_id", $file_type_id)->first();
            if (isset($file->file)) {
                if (Storage::disk("local")->exists("portal/{$file->file}")) {
                    Storage::disk("local")->delete("portal/{$file->file}");
                }
                $file->delete();
            }
        }
        $file = $profile->files()->save(new File([
            'file'          =>  "$filename",
            "file_type_id"  => $file_type_id,
            'status_id'    => $request->get("decoded") == true ? File::VERIFIED : File::PENDING,
        ]));
        $file->verified_at = $request->get("decoded") == true ? now() : null;
        return $file->save();
    }

    public function search(Request $request)
    {
        $request->validate([ 'search' => 'nullable|string' ]);
        $query = Profile::query();
        $searchTerm = $request->route('search');
        if ($searchTerm === null || $searchTerm === '') {
            return $this->success_message([]);
        }
        $query->where('document', 'like', '%' . $searchTerm . '%')
              ->orWhere('name', 'like', '%' . $searchTerm . '%')
              ->orWhere('surname', 'like', '%' . $searchTerm . '%')
              ->limit(100);
        $results = $query->get(['id', 'document', 'name', 'surname', 'disability_id']);
        return $this->success_message($results);
    }
}
