<?php

namespace App\Http\Controllers\Citizen;

use App\Entities\CitizenPortal\ScheduleView;
use App\Http\Requests\Citizen\FilterSchedulePublicRequest;
use App\Http\Resources\Citizen\SchedulePublicResource;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ScheduleController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function index(FilterSchedulePublicRequest $request)
    {
        $query = $this->setQuery(ScheduleView::query(), (new ScheduleView)->getSortableColumn($this->column))
            ->withCount('users_schedules')
            ->withCount('teams_schedules')
            ->when(isset($this->query), function ($query) {
                return $query->search($this->query);
            })
            ->when($request->has('weekday_id'), function ($query) use ($request) {
                return $query->whereIn('weekday_id', $request->get('weekday_id'));
            })
            ->when($request->has('daily_id'), function ($query) use ($request) {
                return $query->whereIn('daily_id', $request->get('daily_id'));
            })
            ->when($request->has('age'), function ($query) use ($request) {
                return $query
                    ->where('min_age', '<=', $request->get('age'))
                    ->where('max_age', '>=', $request->get('age'));
            })
            ->when($request->has('activity_id'), function ($query) use ($request) {
                return $query->whereIn('activity_id', $request->get('activity_id'));
            })
            ->when($request->has('stage_id'), function ($query) use ($request) {
                return $query->whereIn('stage_id', $request->get('stage_id'));
            })
            ->when($request->has('is_paid'), function ($query) use ($request) {
                return $query->where('is_paid', $request->get('is_paid'));
            })
            ->when($request->has('program_id'), function ($query) use ($request) {
                return $query->whereIn('program_id', $request->get('program_id'));
            })
            ->where('quota', '>', 0)
            ->where('is_activated', true)
            ->where('is_initiate', true)
            ->where('start_date', '<=', now())
            ->where('final_date', '>=', now()->startOfDay())
            ->whereColumn('taken', '<', 'quota')
            ->orderBy((new ScheduleView)->getSortableColumn($this->column), $this->order)
            ->paginate($this->per_page);

        return $this->success_response(
            SchedulePublicResource::collection($query),
            ResponseAlias::HTTP_OK,
            SchedulePublicResource::headers()
        );
    }

    /**
     * @param ScheduleView $schedule
     * @return JsonResponse
     */
    public function show(ScheduleView $schedule)
    {
        return $this->success_response(
            new SchedulePublicResource($schedule->loadCount(['users_schedules', 'teams_schedules'])),
            ResponseAlias::HTTP_OK,
            SchedulePublicResource::headers()
        );
    }

    /**
     * @return JsonResponse
     */
    public function chips()
    {
        $data = ScheduleView::query()
            ->select("activity_name")
            ->selectRaw(DB::raw("activity_name, SUBSTRING_INDEX( activity_name , ' ', 1 ) AS activity_name_contract"))
            ->groupBy(["activity_name"])
            ->orderBy('activity_name','ASC')
            ->where('quota', '>', 0)
            ->where('is_activated', true)
            ->where('is_initiate', true)
            ->where('final_date', '>=', now()->startOfDay())
            ->whereColumn('taken', '<', 'quota')
            ->get()
            ->map(function ($model) {
                return [
                    'name' => $model->activity_name ?? null,
                    'value' => $model->activity_name_contract ?? null,
                    'icon' => ScheduleView::icons($model->activity_name ?? "other"),
                ];
            })
            ->unique("value")
            ->values();
        return $this->success_message(
            $data,
            200,
            200,
            count($data)
        );
    }
}
