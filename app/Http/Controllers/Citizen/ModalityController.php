<?php

namespace App\Http\Controllers\Citizen;


use App\Entities\CitizenPortal\Modality;
use App\Entities\CitizenPortal\Team;
use App\Http\Controllers\Controller;
use App\Http\Resources\Citizen\ModalityResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ModalityController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $data = $this->setQuery(Modality::query(), (new Team())->getKeyName())->get();
        return $this->success_response(
            ModalityResource::collection( $data )
        );
    }
}
