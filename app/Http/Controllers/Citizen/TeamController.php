<?php

namespace App\Http\Controllers\Citizen;


use App\Entities\CitizenPortal\Participant;
use App\Entities\CitizenPortal\ParticipantRole;
use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\Team;
use App\Entities\CitizenPortal\TeanSchedule;
use App\Http\Controllers\Controller;
use App\Http\Requests\Citizen\TeamRequest;
use App\Http\Requests\Citizen\TeamScheduletRequest;
use App\Http\Requests\Citizen\TeamSearchRequest;
use App\Http\Resources\Citizen\ParticipantResource;
use App\Http\Resources\Citizen\ParticipantRoleResource;
use App\Http\Resources\Citizen\TeamResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TeamController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $participant_role = ParticipantRole::PARTICIPANT;
        $data = $this->setQuery(Team::query(), (new Team())->getKeyName())
            ->withCount(['participants' => function ($query) use ($participant_role) {
                $query->where("role_id", $participant_role);
            }])
            ->with('participants')
            ->where('profile_id', auth('api')->user()->user_profile->id)
            ->when($request->has("activity"), function ($query) use ($request) {
                return $query->where("activity_id", $request->get("activity"));
            })
            ->paginate($this->per_page);
        return $this->success_response(
            TeamResource::collection( $data ),
            200,
            TeamResource::headers()
        );
    }

    public function search(TeamSearchRequest $request)
    {
        $profile = Profile::query()
            ->where('document', $request->get('document'))
            ->first();
        if ($profile->status_id != Profile::VERIFIED) {
            return $this->validation_errors([
                'document' => ['Este usuario no se encuentra verificado para participar en actividades.']
            ]);
        }
        return $this->success_message([[
            'id'    => (int) $profile->id,
            'name'  => $profile->name.' '.$profile->surname
        ]]);
    }

    public function show(Team $team)
    {
        return $this->success_response(
            new TeamResource( $team )
        );
    }

    public function store(TeamRequest $request)
    {
        $count = Team::query()
            ->where("activity_id", $request->get("activity_id"))
            ->where('profile_id', auth('api')->user()->user_profile->id)
            ->count();
        if ($count > 0) {
            return $this->error_response("Ya tienes un equipo registrado en esta actividad.");
        }
        $form = new Team();
        $form->fill($request->validated());
        $form->modality_id = 0;
        $form->profile_id = auth('api')->user()->user_profile->id;
        $form->save();
        return $this->success_message(
            "Equipo creado satisfactoriamente.",
            Response::HTTP_CREATED,
            null,
            $form
        );
    }

    public function update(Team $team, TeamRequest $request)
    {
        $team->fill($request->validated());
        $team->modality_id = 0;
        $team->save();
        return $this->success_message("Equipo actualizado satisfactoriamente.");
    }

    public function destroy(Team $team)
    {
        $team->delete();
        return $this->success_message("Equipo eliminado satisfactoriamente.");
    }

    public function register(TeamScheduletRequest $request)
    {
        $count = TeanSchedule::query()->where("team_id", $request->get("team"))->count();
        if ($count > 0) {
            return $this->error_response("Este equipo ya está registrado en esta actividad");
        }
        $form = new TeanSchedule();
        $form->schedule_id = $request->get("activity_id");
        $form->team_id = $request->get("team");
        $form->status_id = TeanSchedule::PENDING_SUBSCRIBE;
        $form->save();
        return $this->success_message("Has inscrito tu equipo de forma satisfactoria.");
    }

    public function roles()
    {
        return $this->success_response(
            ParticipantRoleResource::collection(ParticipantRole::all())
        );
    }

    public function pending()
    {
        $pendings = Participant::query()
            ->where("profile_id", auth("api")->user()->user_profile->id)
            ->whereNull("accepted_at")
            ->get();
        return $this->success_response(
            ParticipantResource::collection($pendings)
        );
    }

    public function schedule(Team $team, $schedule)
    {

    }
}
