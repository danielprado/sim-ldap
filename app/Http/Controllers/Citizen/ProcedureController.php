<?php

namespace App\Http\Controllers\Citizen;


use App\Entities\CitizenPortal\Procedure;
use App\Entities\CitizenPortal\ProfileEntity;
use App\Http\Controllers\Controller;
use App\Http\Resources\Citizen\ProcedureResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProcedureController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        if (auth('api')->user()->user_profile->is_organization) {
            $organization = ProfileEntity::query()
                ->where('profile_id', auth('api')->user()->user_profile->id)
                ->first();

            $procedures = Procedure::query()
                ->where('entity_profile_id', $organization->id)
                ->paginate($this->per_page);

        } else {
            $procedures = Procedure::query()
                ->where('profile_id', auth('api')->user()->user_profile->id)
                ->paginate($this->per_page);
        }

        return $this->success_response(
            ProcedureResource::collection( $procedures ),
            200,
            ProcedureResource::headers()
        );
    }

    public function show()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }
}
