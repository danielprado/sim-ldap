<?php

namespace App\Http\Controllers\Citizen;


use App\Entities\CitizenPortal\File;
use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\ProfileEntity;
use App\Entities\Security\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Citizen\OrganizationRequest;
use App\Http\Resources\Citizen\OrganizationResource;
use App\Notifications\SendgridTemplates;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class OrganizationController extends Controller
{
    const EMPRESA = 1;
    const CLUB = 1;
    const SCHOOL = 2;

    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth:api')->except(['store']);
    }

    public function index(Request $request): JsonResponse
    {
        $organizations = (ProfileEntity::query()
            ->where('legal_profile_id', auth('api')->user()->user_profile->id))
            ->paginate($this->per_page);

        return $this->success_response(
            OrganizationResource::collection( $organizations ),
            200,
            OrganizationResource::headers()
        );
    }

    public function show(ProfileEntity $entity)
    {
        return $this->success_response(
            new OrganizationResource( $entity )
        );
    }

    /**
     * @throws \Exception
     */
    public function store(OrganizationRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            if ($request->has('document_type_id') && $request->has('document')) {
                $exists = Profile::query()->where('document', $request->get('document'))->exists();
                if ($exists) {
                    return $this->error_response(
                        "El número de documento ya se encuentra registrado o está en proceso de verificación"
                    );
                }
            }

            $exits = User::query()->where('email', $request->get('email'))->exists();
            if ($exits) {
                return $this->error_response(
                    "El correo electrónico ya se encuentra registrado o está en proceso de verificación"
                );
            }

            $authUser = auth('api')->user();

            $organization = ProfileEntity::query()
                ->where('legal_profile_id', empty($authUser) ? $request->get('legal_rep_document') : $authUser->user_profile->id)
                ->where('organization_type_id', '!=', self::EMPRESA)
                ->where('organization_subtype_id', $request->get('organization_subtype_id'))
                ->count();

            if ($organization > 0) {
                return $this->error_response("Ya tienes un organismo deportivo registrado.");
            }

            $user = $this->createUser($request);
            $this->registeredOrganizationResponse($request);
            $profile = $user->user_profile()->latest()->first();
            $legal_profile = Profile::query()->where('document', $request->get('legal_rep_document'))->first();

            $form = new ProfileEntity();
            $form->fill($request->validated());
            if (!$request->has('document_type_id') && !$request->has('document')) {
                $form->document_type_id = empty($authUser) ? $profile->document_type_id : $authUser->user_profile->document_type_id;
                $form->document = empty($authUser) ? $profile->document : $authUser->user_profile->document;
            }
            $form->legal_profile()->associate(empty($authUser) ? $legal_profile : $authUser->user_profile);
            $form->profile()->associate($profile);
            $form->save();
            DB::commit();

            return $this->success_message(
                "Organización y perfil registrado satisfactoriamente.",
                Response::HTTP_CREATED,
                null,
                $form
            );

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function update(ProfileEntity $entity, OrganizationRequest $request): JsonResponse
    {
        $entity->fill($request->validated());
        $entity->save();
        return $this->success_message("La organización se ha actualizado satisfactoriamente.");
    }

    public function getOrganization(Request $request): JsonResponse
    {
        $organization = null;
        if ($request->has('profile_id') && $request->has('organization_subtype_id')) {
            $profile = Profile::query()
                ->where('id', $request->get('profile_id'))
                ->first();

            if ($profile->profile_type_id == Profile::PROFILE_PERSONAL) {
                $organization = ProfileEntity::query()
                    ->where('legal_profile_id', $request->get('profile_id'))
                    ->where('organization_subtype_id', $request->get('organization_subtype_id'))
                    ->when($request->has('organization_subtype_id') == self::CLUB, function ($query) {
                        return $query->with('club');
                    })
                    ->when($request->has('organization_subtype_id') == self::SCHOOL, function ($query) {
                        return $query->with('school');
                    })
                    ->first();
            }

            if ($profile->profile_type_id == Profile::PROFILE_ORGANIZATION) {
                $organization = ProfileEntity::query()
                    ->where('profile_id', $request->get('profile_id'))
                    ->where('organization_subtype_id', $request->get('organization_subtype_id'))
                    ->when($request->has('organization_subtype_id') == self::CLUB, function ($query) {
                        return $query->with('club');
                    })
                    ->when($request->has('organization_subtype_id') == self::SCHOOL, function ($query) {
                        return $query->with('school');
                    })
                    ->first();
            }

            if (!$organization) {
                $message = $profile->profile_type_id == Profile::PROFILE_PERSONAL
                    ? 'No se encontró un organismo deportivo que cumpla los requisitos para realizar está solicitud.'
                    : 'Este perfil no es un organismo deportivo que cumpla los requisitos para realizar está solicitud.';
                return $this->error_response(
                    $message
                );
            }
        }

        return $this->success_response(
            new OrganizationResource( $organization )
        );
    }

    /**
     * @throws \Exception
     */
    public function createUser(Request $request): User
    {
        try {
            DB::beginTransaction();
            $user = new User();
            $full_name = $request->get('organization_name');
            $user->name = toUpper(preg_replace('!\s+!', ' ', $full_name));
            $user->email = $request->get('email');
            $user->password = Hash::make($request->get('password'));
            $user->save();

            $document_type_id = $request->get('document_type_id') ?? auth('api')->user()->user_profile->document_type_id;
            $document = $request->get('document') ?? auth('api')->user()->user_profile->document;
            $profile = $user->profiles()->save(
                new Profile([
                    'profile_type_id' => Profile::PROFILE_ORGANIZATION,
                    'document_type_id' => $document_type_id,
                    'document' => $document,
                    'name' => toUpper($request->get('organization_name')),
                    'address' => toUpper($request->get('address')),
                    'mobile_phone' => $request->get('phone'),
                    'contact_name' => toUpper($request->get('legal_rep_name')),
                    'contact_phone' => $request->get('phone'),
                    'is_organization' => 1,
                ])
            );

            $profile->status_id = Profile::PENDING;
            $profile->save();

            if ($request->hasFile('file_legal_representation')) {
                $request->request->add([
                    "file_type_id" => File::LEGAL_REPRESENTATION,
                ]);
                app(ProfileController::class)->saveFileComplete($request, $profile, 'file_legal_representation', File::LEGAL_REPRESENTATION);
            }
            if ($request->hasFile('file_sport_recognition')) {
                $request->request->add([
                    "file_type_id" => File::SPORT_RECOGNITION,
                ]);
                app(ProfileController::class)->saveFileComplete($request, $profile, 'file_sport_recognition', File::SPORT_RECOGNITION);
            }
            //$user->markEmailAsVerified();
            DB::commit();
            return $user->load('user_profile');

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @throws \Exception
     */
    public function registeredOrganizationResponse(Request $request)
    {
        $email = config('app.env') == 'local'
            ? 'brayan.aldana@idrd.gov.co'
            : $request->get('email');

        $name = toTitle($request->get('organization_name'));
        $notify = new SendgridTemplates();
        $notify->sendWarningMessage(
            $email,
            [
                "name" => $name,
                "status"    =>  "Pendiente de Verificación",
                "observation" => "Los datos de identificación registrados serán validados frente al documento cargado.",
                "info"  =>  "Este proceso puede tardar entre 3 a 5 días hábiles."
            ],
            "Portal Ciudadano - Bienvenido"
        );
    }
}
