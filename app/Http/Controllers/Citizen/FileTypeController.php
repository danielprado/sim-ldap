<?php

namespace App\Http\Controllers\Citizen;

use App\Entities\CitizenPortal\FileType;
use App\Http\Resources\Citizen\FileTypeResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class FileTypeController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $file_types = FileType::query()
            // ->where("id", "!=", FileType::PAYMENT)
            ->whereIn("id", [FileType::ID_DOCUMENT])
            ->get();
        return $this->success_response(
            FileTypeResource::collection($file_types),
            Response::HTTP_OK,
            FileTypeResource::headers()
        );
    }
}
