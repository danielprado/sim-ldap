<?php

namespace App\Http\Controllers\Citizen;

use App\Entities\CitizenPortal\Stage;
use App\Http\Resources\Citizen\StageResource;
use App\Http\Controllers\Controller;

class StageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return $this->success_response(
            StageResource::collection(Stage::has("schedules_view", ">", 0)->withCount("schedules_view")->get())
        );
    }
}
