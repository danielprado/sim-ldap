<?php

namespace App\Http\Controllers\Citizen;


use App\Entities\CitizenPortal\File;
use App\Entities\CitizenPortal\FileType;
use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\Status;
use App\Entities\Payments\Payment;
use App\Http\Controllers\Controller;
use App\Http\Requests\Citizen\FileRequest;
use App\Http\Resources\Citizen\FileResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class FileController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function index(Profile $profile)
    {
        return $this->success_response(
            FileResource::collection($profile->files()->latest()->paginate($this->per_page))
        );
    }

    /**
     * @param Profile $profile
     * @param File $file
     * @return JsonResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function show(Profile $profile, File $file)
    {
        try {
            if ( Storage::disk('local')->exists("portal/$file->file") ) {
                $selected = Storage::disk('local')->get("portal/$file->file");
                $mime = Storage::disk('local')->mimeType("portal/$file->file");
                return $this->success_message([
                    'name'  => $file->file,
                    'mime'  => $mime,
                    'file'   => 'data:'.$mime.';base64,'.base64_encode($selected)
                ]);
            } elseif (Storage::disk('public')->exists('NOT_FOUND.pdf')) {
                $selected = Storage::disk('public')->get('NOT_FOUND.pdf');
                $mime = Storage::disk('public')->mimeType('NOT_FOUND.pdf');
                return $this->success_message([
                    'name'  => $file->file,
                    'mime'  => $mime,
                    'file'   => 'data:'.$mime.';base64,'.base64_encode($selected)
                ]);
            } else {
                return $this->error_response(
                    __('validation.handler.resource_not_found'),
                    Response::HTTP_NOT_FOUND
                );
            }
        } catch (\Exception $exception) {
            if (Storage::disk('local')->exists('templates/NOT_FOUND.pdf')) {
                $selected = Storage::disk('local')->get('templates/NOT_FOUND.pdf');
                $mime = Storage::disk('local')->mimeType('templates/NOT_FOUND.pdf');
                return $this->success_message([
                    'name'  => $file->file,
                    'mime'  => $mime,
                    'file'   => 'data:'.$mime.';base64,'.base64_encode($selected)
                ]);
            } else {
                return $this->error_response(
                    __('validation.handler.resource_not_found'),
                    Response::HTTP_NOT_FOUND
                );
            }
        }
    }

    /**
     * @param FileRequest $request
     * @param Profile $profile
     * @return JsonResponse
     */
    public function store(FileRequest $request, Profile $profile)
    {
        try {
            DB::beginTransaction();
            if ($request->get("file_type_id") == File::DOCUMENT) {
                $file = $profile->files()
                    ->where('file_type_id', File::DOCUMENT)
                    ->first();
                if (isset($file->file) && !in_array($file->status_id, [Status::FILE_VERIFIED, Status::VERIFIED])) {
                    if ($filename = $this->saveFile($request, $profile)) {
                        if (Storage::disk("local")->exists("portal/{$file->file}")) {
                            Storage::disk("local")->delete("portal/{$file->file}");
                        }
                        $file->file = $filename;
                        $file->save();
                        DB::commit();
                        return $this->success_message(
                            __('validation.handler.success'),
                            Response::HTTP_CREATED
                        );
                    } else {
                        DB::rollBack();
                        return $this->error_response(__('validation.handler.unexpected_failure'));
                    }
                } else if (isset($file->status_id) && in_array($file->status_id, [Status::FILE_VERIFIED, Status::VERIFIED])) {
                    DB::rollBack();
                    return $this->error_response('El documento de identidad ya se encuentra cargado y verificado.');
                }
            }
            if ($filename = $this->saveFile($request, $profile)) {
                $reference = Uuid::uuid1();
                if ($request->get("file_type_id") == FileType::PAYMENT) {
                    $payment = new Payment();
                    $payment->fill([
                        'parque_id' => $request->get('parkSelected'),
                        'servicio_id'   => $request->get('serviceParkSelected'),
                        'identificacion'    => $request->get('document'),
                        'tipo_identificacion'   => $payment->getTypeDocument($request->get('documentTypeSelected')),
                        'codigo_pago'   => $reference,
                        'id_transaccion_pse'    => $request->get('number_bank'),
                        'email' => $request->get('email'),
                        'nombre'    => $request->get('name'),
                        'apellido'  => $request->get('lastName'),
                        'telefono'  => $request->get('phone'),
                        'estado_id' => Payment::PAYMENT_OK,
                        'estado_banco'  => "OK",
                        'concepto'  => $request->get('concept'),
                        'moneda'    => "COP",
                        'total' => $request->get('totalPay'),
                        'iva'   => 0,
                        'permiso'   => 999,
                        'tipo_permiso' => "PO",
                        'fecha_pago'    => $request->get('payment_at'),
                        'user_id_pse'   => "CONSIGNACIÓN".$request->get('documentPayer'),
                        'medio_id'  => Payment::PAYMENT_BANK_FILE_TYPE,
                        'email_pagador' => $request->get('emailPayer'),
                        'nombre_pagador'    => $request->get('namePayer'),
                        'apellido_pagador'  => $request->get('lastNamePayer'),
                        'telefono_pagador'  => $request->get('phonePayer'),
                        'identificacion_pagador'    => $request->get('documentPayer'),
                        'tipo_identificacion_pagador'   => $payment->getTypeDocument($request->get('documentTypeSelectedPayer')),
                    ]);
                    $payment->save();
                }
                $profile->files()->save(new File([
                    'file'          =>  "$filename",
                    'file_type_id'  => $request->get('file_type_id'),
                    'status_id'     => Status::FILE_PENDING,
                    'citizen_schedule_id' => $request->has('citizen_schedule_id') && FileType::PAYMENT == (int) $request->get('file_type_id')
                        ? $request->get('citizen_schedule_id')
                        : null,
                    'pse' => FileType::PAYMENT == (int) $request->get('file_type_id')
                        ? $reference
                        : null,
                ]));
                DB::commit();
                return $this->success_message(
                    __('validation.handler.success'),
                    Response::HTTP_CREATED
                );
            }
            DB::rollBack();
            return $this->error_response(__('validation.handler.unexpected_failure'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->error_response(__('validation.handler.unexpected_failure'));
        }
    }

    public function saveFile(Request $request, Profile $profile)
    {
        $ext = $request->file('file')->getClientOriginalExtension();
        $document = $profile->document ?? '00000';
        $type = FileType::find( $request->get('file_type_id') );
        $type = isset( $type->name ) ? toUpper(str_replace(' ', '_', $type->name)) : 'SYSTEM';
        $now = now()->format('YmdHis');
        $filename = "{$document}_{$type}_{$now}.$ext";
        if ($request->file('file')->storeAs('portal', $filename, [ 'disk' => 'local' ])) {
            return $filename;
        }
        return null;
    }

    public function saveFileComplete(Request $request, Profile $profile, $requestName)
    {
        $ext = $request->file($requestName)->getClientOriginalExtension();
        $document = $profile->document ?? '00000';
        $type = FileType::find( $request->get('file_type_id') );
        $type = isset( $type->name ) ? toUpper(str_replace(' ', '_', $type->name)) : 'SYSTEM';
        $now = now()->format('YmdHis');
        $filename = "{$document}_{$type}_{$now}.$ext";
        if ($request->file($requestName)->storeAs('portal', $filename, [ 'disk' => 'local' ])) {
            return $filename;
        }
        return null;
    }
}
