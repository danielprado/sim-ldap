<?php

namespace App\Http\Controllers\Citizen;

use App\Entities\CitizenPortal\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrivacyController extends Controller
{
    public function consents()
    {
        return $this->success_message([
            "title" => __("citizen.privacy.consent.adult.title"),
            "text" => __("citizen.privacy.consent.adult.text", [
                'user'      => auth('api')->user()->user_profile->full_name ?? "",
                "document"  => auth('api')->user()->user_profile->document ?? "",
            ]),
        ]);
    }

    public function terms()
    {
        return $this->success_message([
            "title" => __("citizen.privacy.terms.title"),
            "text" => __("citizen.privacy.terms.text"),
        ]);
    }
}
