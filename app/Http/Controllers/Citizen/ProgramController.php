<?php

namespace App\Http\Controllers\Citizen;

use App\Entities\CitizenPortal\Program;
use App\Http\Resources\Citizen\ProgramResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgramController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return $this->success_response(
            ProgramResource::collection(Program::withCount('schedules')->get())
        );
    }
}
