<?php

namespace App\Http\Controllers\Citizen;


use App\Entities\Security\Eps;
use App\Http\Controllers\Controller;
use App\Http\Resources\Citizen\EpsResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EpsController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $data = $this->setQuery(Eps::query(), (new Eps())->getKeyName())
                ->active()
                ->get();
        return $this->success_response(
            EpsResource::collection( $data )
        );
    }
}
