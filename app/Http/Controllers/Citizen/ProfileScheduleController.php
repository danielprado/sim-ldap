<?php

namespace App\Http\Controllers\Citizen;


use App\Entities\CitizenPortal\CitizenSchedule;
use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\ScheduleView;
use App\Entities\CitizenPortal\Status;
use App\Entities\Payments\Pay;
use App\Entities\Payments\Payment;
use App\Http\Controllers\Controller;
use App\Http\Resources\Citizen\CitizenActivitiesResource;
use App\Http\Resources\Citizen\CitizenScheduleResource;
use App\Notifications\SendgridTemplates;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProfileScheduleController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Profile $profile)
    {
        $data = CitizenSchedule::query()
                    ->where('profile_id', $profile->getKey())
                    ->with('schedule_view')
                    ->paginate($this->per_page);
        return $this->success_response(
            CitizenActivitiesResource::collection( $data )
        );
    }

    public function activities()
    {
        $data = CitizenSchedule::query()
            ->whereIn(
                'profile_id',
                auth('api')->user()->profiles->pluck('id')->toArray()
            )
            ->with('schedule_view')
            ->paginate($this->per_page);
        return $this->success_response(
            CitizenActivitiesResource::collection( $data ),
            Response::HTTP_OK,
            CitizenActivitiesResource::headers()
        );
    }

    public function show(CitizenSchedule $schedule)
    {
        $profiles = auth('api')->user()->profiles->pluck('id')->toArray();

        abort_if(
            !in_array($schedule->profile_id, $profiles),
            Response::HTTP_NOT_FOUND,
            "No se encontró una suscripción $schedule->profile_id válida".implode(",", $profiles)
        );
        return $this->success_response(
            new CitizenActivitiesResource($schedule->load('schedule_view')),
            Response::HTTP_OK,
            CitizenActivitiesResource::headers()
        );
    }

    public function approveStatusInPayment($payment)
    {
        $profiles = auth('api')->user()->profiles->pluck('id')->toArray();
        $schedule = CitizenSchedule::query()
            ->where("status_id", "!=", Status::SUBSCRIBED)
            ->where("reference_pse", $payment)
            ->whereIn("profile_id", $profiles)->first();
        if (isset($schedule->id)) {
            $value_paid = isset($schedule->payment->total) ? (int) $schedule->payment->total : 0;
            $value_to_pay = isset($schedule->schedule->rate_value) ? (int) $schedule->schedule->rate_value : 0;
            $status = isset($schedule->payment->estado_id) ? (int) $schedule->payment->estado_id : 0;
            $medio = isset($schedule->payment->medio_id) ? (int) $schedule->payment->medio_id : 0;
            if (($value_paid >= $value_to_pay) && ($status == Payment::PAYMENT_OK) && $medio == Payment::MEDIO_PSE) {
                $schedule->status_id = Status::SUBSCRIBED;
                $schedule->save();
            }
            return $this->success_message(
                "Inscripción aprobada",
                Response::HTTP_OK,
                [
                    "to_pay" => $value_to_pay,
                    "paid"  => $value_paid,
                    "status" => $status
                ]
            );
        }
        return $this->success_message(
            "No changes",
            Response::HTTP_OK,
            [$schedule, $profiles]
        );
    }

    public function unsubscribe(CitizenSchedule $schedule)
    {
        $profiles = auth('api')->user()->profiles->pluck('id')->toArray();

        abort_if(
            !in_array($schedule->profile_id, $profiles),
            Response::HTTP_NOT_FOUND,
            "No se encontró una suscripción $schedule->profile_id válida".implode(",", $profiles)
        );

        if ($schedule->hasPaymentRegistered()) {
            return $this->error_response(
                "No puedes desinscribirte de esta actividad porque tiene un pago realizado."
            );
        }

        if ($schedule->files()->whereNotNull("pse")->count() > 0) {
            return $this->error_response(
                "No puedes desinscribirte de esta actividad porque tiene una consignación asociada."
            );
        }

        if ($schedule->isAvailableToUnsubscribe()) {
            // $schedule->delete();
            return $this->success_message(
                "Te has desinscrito de esta actividad satisfactoriamente"
            );
        }
        return $this->error_response(
            "No puedes desinscribirte de esta actividad"
        );
    }

    /**
     * @param Profile $profile
     * @param ScheduleView $schedule
     * @return JsonResponse
     */
    public function update(Profile $profile, ScheduleView $schedule)
    {
        $profile->load(['sex_name', 'sexual_orientation', 'gender', 'population_group', 'ethnic_group', 'disability']);
        if ($schedule->quota < $schedule->taken) {
            return $this->error_response(
                "Todos los cupos han sido tomados."
            );
        }
        $age = $profile->birthdate->age ?? null;
        if (is_null($age)) {
            return $this->error_response(
                "Por favor actualiza la información del beneficiario o de tu perfil para continuar."
            );
        }
        if ($age < $schedule->min_age || $age > $schedule->max_age) {
            return $this->error_response(
                "No cumples con los requisitos de edad para esta actividad."
            );
        }
        if (!isset($profile->verified_at)) {
            return $this->error_response(
                "Tu perfil no está verificado, por favor actualiza tu información para verificar tus datos.."
            );
        }
        $sex_id = $this->getArray($schedule->sex_id);
        $orientation_id = $this->getArray($schedule->orientation_id);
        $gender_id = $this->getArray($schedule->gender_id);
        $ethnic_group_id = $this->getArray($schedule->ethnic_group_id);
        $population_group_id = $this->getArray($schedule->population_group_id);
        $disability_id = $this->getArray($schedule->disability_id);

        if (isset($sex_id) && is_array($sex_id)) {
            if (!isset($profile->sex_name) || !in_array($profile->sex_name->id, $sex_id)){
                return $this->error_response(
                    "No cumples con los requisitos de sexo para esta actividad."
                );
            }
        }
        if (isset($orientation_id) && is_array($orientation_id)) {
            if (!isset($profile->sexual_orientation) || !in_array($profile->sexual_orientation->id, $orientation_id)){
                return $this->error_response(
                    "No cumples con los requisitos de orientación sexual para esta actividad."
                );
            }
        }
        if (isset($gender_id) && is_array($gender_id)) {
            if (!isset($profile->gender) || !in_array($profile->gender->id, $gender_id)){
                return $this->error_response(
                    "No cumples con los requisitos de género para esta actividad."
                );
            }
        }
        if (isset($population_group_id) && is_array($population_group_id)) {
            if (!isset($profile->population_group) || !in_array($profile->population_group->id, $population_group_id)){
                return $this->error_response(
                    "No cumples con los requisitos de grupo social para esta actividad."
                );
            }
        }
        if (isset($ethnic_group_id) && is_array($ethnic_group_id)) {
            if (!isset($profile->ethnic_group) || !in_array($profile->ethnic_group->id, $ethnic_group_id)){
                return $this->error_response(
                    "No cumples con los requisitos de grupo étnico para esta actividad."
                );
            }
        }
        if (isset($disability_id) && is_array($disability_id)) {
            if (!isset($profile->disability) || !in_array($profile->disability->id, $disability_id)){
                return $this->error_response(
                    "No cumples con los requisitos de discapacidad para esta actividad."
                );
            }
        }

        $activity = CitizenSchedule::query()
            ->where('schedule_id', $schedule->id)
            ->where('profile_id', $profile->id)
            ->count();

        if ($activity > 0) {
            return $this->error_response(
                "Ya estás participando en esta actividad."
            );
        }

        $sign_in = new CitizenSchedule();
        $sign_in->schedule_id = $schedule->id;
        $sign_in->profile_id = $profile->id;
        // $sign_in->status_id = CitizenSchedule::PENDING_SUBSCRIBE;
        $sign_in->status_id = $schedule->is_paid ? Status::AVAILABLE_TO_PAY : Status::SUBSCRIBED;
        if ($schedule->is_paid) {
            $sign_in->payment_at = now()->addDays(4)->endOfDay();
        }
        $sign_in->save();

        $notify = new SendgridTemplates(true);
        if ($schedule->is_paid) {
            $notify->sendInfoMessage(
                auth("api")->user()->email,
                [
                    "name"      => auth("api")->user()->user_profile->email_full_name ?? "Ciudadano",
                    "sub_user"  => $profile->profile_type_id != Profile::PROFILE_PERSONAL
                        ? toTitle($profile->name ?? "")." ".toTitle($profile->surname ?? "")
                        : null,
                    "status"    =>  "Pendiente de Pago",
                    "sport"     => $sign_in->getKey()." - ".toFirstUpper($schedule->activity_name ?? ""),
                    "stage"     => toFirstUpper($schedule->stage_name ?? ""),
                    "hour"     => toFirstUpper($schedule->weekday_name ?? "")." ".toUpper($schedule->daily_name ?? ""),
                    "observation" => "Para finalizar el proceso por favor ingrese al Portal Ciudadano para realizar su pago por PSE o cargar el comprobante de consignación antes de {$sign_in->payment_at}",
                    "info"  =>  "Le invitamos a ingresar al Portal Ciudadano para conocer más servicios que el IDRD tiene para usted."
                ],
                "Portal Ciudadano - Inscripción {$sign_in->getKey()} - ".toTitle($schedule->activity_name ?? "")
            );
        } else {
            $notify->sendSuccessMessage(
                auth("api")->user()->email,
                [
                    "name"      => auth("api")->user()->user_profile->email_full_name ?? "Ciudadano",
                    "sub_user"  => $profile->profile_type_id != Profile::PROFILE_PERSONAL
                        ? toTitle($profile->name ?? "")." ".toTitle($profile->surname ?? "")
                        : null,
                    "status"    =>  "Aprobado",
                    "sport"     => $sign_in->getKey()." - ".toTitle($schedule->activity_name ?? ""),
                    "stage"     => toTitle($schedule->stage_name ?? ""),
                    "hour"     => toTitle($schedule->weekday_name ?? "")." ".toTitle($schedule->daily_name ?? ""),
                    "observation" => "Su inscripción ha sido exitosa, por favor acercarse al escenario en el horarios y dia inscrito.",
                    "info"  =>  "Le invitamos a ingresar al Portal Ciudadano para conocer más servicios que el IDRD tiene para usted."
                ],
                "Portal Ciudadano - Inscripción {$sign_in->getKey()} - ".toTitle($schedule->activity_name ?? "")
            );
        }
        return $this->success_message(
            "Hemos realizado la preinscripción de {$profile->full_name} a la actividad {$schedule->activity_name}.",
            Response::HTTP_OK,
            Response::HTTP_OK,
            $sign_in->getKey()
        );
    }

    public function getArray($val) {
        return isset($val) ? array_map('intval', explode(',', $val)) : null;
    }
}
