<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CsrfCookieController extends Controller
{
    public function show()
    {
        return response()->noContent();
    }
}
