<?php

namespace App\Http\Controllers\Auth;

use App\Entities\CitizenPortal\Profile;
use App\Entities\Security\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * The maximum number of attempts to allow.
     *
     * @var  int
     */
    protected $maxAttempts = 3;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout', 'user');
    }


    /**
     * Handle a login request to the application.
     *
     * @param Request $request
     * @return RedirectResponse|Response|JsonResponse
     *
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $user = User::where($this->username(), $request->get( $this->username() ))->first();
        return isset($user->password) && Hash::check($request->get('password'), $user->password);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param Request $request
     * @return Response
     */
    protected function sendLoginResponse(Request $request)
    {
        // $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user());
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return $this->getToken($request);
    }

    /**
     * Return token if user is successful logged in.
     *
     * @param Request $request
     * @return JsonResponse
     *
     */
    protected function getToken(Request $request)
    {
        $request->request->add([
            'client_id'     =>  config("passport.client_id"),
            'client_secret' =>  config("passport.client_secret"),
            'grant_type'    =>  config("passport.grant_type"),
            'username'  => $request->get($this->username())
        ]);
        $data = (new DiactorosFactory)->createRequest( $request );
        return app( AccessTokenController::class )->issueToken($data);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Get the failed login response instance.
     *
     * @param Request $request
     * @return JsonResponse
     *
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return $this->error_response(
            __('auth.failed'),
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    /**
     * Redirect the user after determining they are locked out.
     *
     * @param Request $request
     * @return JsonResponse
     *
     */
    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return $this->error_response(
            __('auth.throttle', ['seconds' => $seconds]),
            Response::HTTP_TOO_MANY_REQUESTS
        );
    }

    /**
     * @return JsonResponse
     */
    public function user()
    {
        try {
            if (!isset(auth('api')->user()->user_profile->user_id)) {
                auth('api')->user()->profiles()->save(
                    new Profile([
                        'profile_type_id' => Profile::PROFILE_PERSONAL,
                    ])
                );
            }
            if (
                !isset(auth('api')->user()->code) &&
                !isset(auth('api')->user()->email_verified_at)
            ) {
                if (method_exists(auth('api')->user(), 'sendEmailVerificationNotification')) {
                    auth('api')->user()->sendEmailVerificationNotification();
                }
            }
            return $this->success_response(
                new UserResource(
                    auth('api')->user()->load('user_profile')
                )
            );
        } catch (\Exception $exception) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNAUTHORIZED,
                $exception->getMessage()
            );
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        $token = auth('api')->user()->token();
        $token->revoke();
        return $this->sendLogoutResponse($request);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logoutAllDevices(Request $request)
    {
        $tokens = auth('api')->user()->tokens;
        foreach ($tokens as $token) {
            $token->revoke();
        }
        return $this->sendLogoutResponse($request);
    }

    public function sendLogoutResponse(Request $request)
    {
        if (method_exists($this->guard(), 'logout')) {
            $this->guard()->logout();
        }
        if ($request->hasSession()) {
            $request->session()->invalidate();
        }

        return $this->success_message(__('validation.handler.logout'));
    }
}
