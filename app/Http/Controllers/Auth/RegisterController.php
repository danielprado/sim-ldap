<?php

namespace App\Http\Controllers\Auth;

use App\Entities\CitizenPortal\File;
use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\ProfileEntity;
use App\Entities\Security\PreRegister;
use App\Entities\Security\PreRegisterEntity;
use App\Entities\Security\User;
use App\Http\Controllers\Citizen\ProfileController;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Notifications\SendgridTemplates;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $minAge = now()->subYears(120)->format('Y-m-d');
        $maxAge = now()->subYears(18)->format('Y-m-d');
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:90'],
            's_name' => ['nullable', 'string', 'max:90'],
            'surname' => ['required', 'string', 'max:90'],
            's_surname' => ['nullable', 'string', 'max:90'],
            'document_type_id' => [
                'required',
                'numeric',
                "in:1,4,5,6,7,8,9,10,11,12,13,14,15",
                'exists:mysql_sim.tipo_documento,Id_TipoDocumento'
            ],
            'birthdate' => "required|date|date_format:Y-m-d|before:$maxAge|after:$minAge",
            // 'informed_consent'  => "required",
            'file'  => "required|file|mimes:pdf",
            'document' => ['required', 'numeric', 'unique:profiles', 'digits_between:3,20'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
            'code' => ['required', 'numeric', 'digits:6'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'sex' => ['required', 'numeric', 'exists:mysql_sim.genero,Id_Genero'],
            'blood_type'    => "required|numeric|exists:mysql_sim.grupo_sanguineo,Id_GrupoSanguineo",
            // 'terms'  => "required",
            'country_residence_id'  => "required|numeric|exists:mysql_users.countries,id",
            'state_residence_id'    => "required|numeric|exists:mysql_users.states,id",
            'city_residence_id' => "required|numeric|exists:mysql_users.cities,id",
            'locality_id'   => "required|numeric|exists:mysql_parks.localidad,Id_Localidad",
            'upz_id'    => "required|numeric|exists:mysql_parks.upz,cod_upz",
            'neighborhood_id'   => "required|numeric|exists:mysql_parks.Barrios,IdBarrio",
            'other_neighborhood_name'   => "nullable|required_if:neighborhood_id,-1|string|between:3,191",
            'address'   => "required|string|between:3,191",
            'stratum'   => "required|numeric|between:0,6",
            'mobile_phone'  => "required|numeric|digits_between:7,10",
            'country_birth_id'  => "required|numeric|exists:mysql_users.countries,id",
            'state_birth_id'    => "required|numeric|exists:mysql_users.states,id",
            'city_birth_id' => "required|numeric|exists:mysql_users.cities,id",
            'gender_id' => [
                "required",
                "numeric",
                "exists:mysql_sim.identidad_de_genero,id"
            ],
            'sexual_orientation_id' => [
                "required",
                "numeric",
                "exists:mysql_sim.lgbti,id"
            ],
            'population_group_id'   => "required|numeric|exists:mysql_sim.social_poblacional,id",
            'ethnic_group_id'   => "required|numeric|exists:mysql_sim.etnia,Id_Etnia",
            'eps_id'    => [
                "required",
                "numeric",
                Rule::exists("mysql_sim.eps", "Id_Eps")->where('estado', true),
            ],
            'has_disability'    => "required|string|in:SI,NO",
            'disability_id' => "nullable|required_if:has_disability,SI|numeric|exists:mysql_sim.discapacidad,id",
            'contact_name'  => "required|string|between:3,191",
            'contact_phone' => "required|numeric|digits_between:7,10",
            // 'contact_relationship'  => "required|numeric|in:3,7",
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request)));

        // $this->guard()->login($user);

        return $this->registered($request, $user);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerEntity(Request $request)
    {
        event(new Registered($user = $this->createEntity($request)));
        return $this->registeredEntity($request, $user);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $name = toTitle($request->get("name"));
        $surname = toTitle($request->get("surname"));
        $notify = new SendgridTemplates();
        $notify->sendWarningMessage(
            $request->get("email"),
            [
                "name"      => "$name $surname",
                "status"    =>  "Pendiente de Verificación",
                "observation" => "Los datos de identificación registrados serán validados frente al documento cargado.",
                "info"  =>  "Este proceso puede tardar entre 3 a 5 días hábiles."
            ],
            "Portal Ciudadano - Bienvenido"
        );
        return $this->success_message(
            'Usuario registrado satisfactoriamente',
            Response::HTTP_CREATED,
            Response::HTTP_CREATED,
            new UserResource($user->load('user_profile'))
        );
    }

        /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registeredEntity(Request $request, $user)
    {
        $name = toTitle($request->get("organization_name"));
        $surname = toTitle($request->get("organization_name"));
        $notify = new SendgridTemplates();
        $notify->sendWarningMessage(
            $request->get("email"),
            [
                "name"      => "$name $surname",
                "status"    =>  "Pendiente de Verificación",
                "observation" => "Los datos de identificación registrados serán validados frente al documento cargado.",
                "info"  =>  "Este proceso puede tardar entre 3 a 5 días hábiles."
            ],
            "Portal Ciudadano - Bienvenido"
        );
        return $this->success_message(
            'Usuario registrado satisfactoriamente',
            Response::HTTP_CREATED,
            Response::HTTP_CREATED,
            new UserResource($user->load('user_profile'))
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     * @throws \Exception
     */
    protected function create(Request $request)
    {
        try {
            DB::beginTransaction();
            $user = new User();
            $full_name = $request->get('name').' '.$request->get('s_name').' '.$request->get('surname').' '.$request->get('s_surname');
            $user->name = toUpper(preg_replace('!\s+!', ' ', $full_name));
            $user->email = $request->get('email');
            $user->password = Hash::make($request->get('password'));
            $user->save();
            PreRegister::query()
                ->where('email', $request->get('email'))
                ->where('code', $request->get('code'))
                ->delete();
            $profile = $user->profiles()->save(
                new Profile([
                    'profile_type_id' => Profile::PROFILE_PERSONAL,
                    'document_type_id' => $request->get('document_type_id'),
                    'document' => $request->get('document'),
                    'name' => toUpper($request->get('name')),
                    's_name' => toUpper($request->get('s_name')),
                    'surname' => toUpper($request->get('surname')),
                    's_surname' => toUpper($request->get('s_surname')),
                    'birthdate' => toUpper($request->get('birthdate')),
                    'sex' => toUpper($request->get('sex')),
                    'blood_type' => toUpper($request->get('blood_type')),
                    'country_residence_id' => $request->get('country_residence_id'),
                    'state_residence_id' => $request->get('state_residence_id'),
                    'city_residence_id' => $request->get('city_residence_id'),
                    'locality_id' => $request->get('locality_id'),
                    'upz_id' => $request->get('upz_id'),
                    'neighborhood_id' => $request->get('neighborhood_id'),
                    'other_neighborhood_name' => $request->get('other_neighborhood_name'),
                    'address' => toUpper($request->get('address')),
                    'stratum' => $request->get('stratum'),
                    'mobile_phone' => $request->get('mobile_phone'),
                    'country_birth_id' => $request->get('country_birth_id'),
                    'state_birth_id' => $request->get('state_birth_id'),
                    'city_birth_id' => $request->get('city_birth_id'),
                    'gender_id' => $request->get('gender_id'),
                    'sexual_orientation_id' => $request->get('sexual_orientation_id'),
                    'population_group_id' => $request->get('population_group_id'),
                    'ethnic_group_id' => $request->get('ethnic_group_id'),
                    'eps_id' => $request->get('eps_id'),
                    'has_disability' => $request->get('has_disability'),
                    'disability_id' => $request->get('disability_id'),
                    'contact_name' => toUpper($request->get('contact_name')),
                    'contact_phone' => $request->get('contact_phone'),
                    //'preferred_name' => $request->get('preferred_name')
                    // 'contact_relationship' => $request->get('contact_relationship'),
                ])
            );
            // $profile->verified_at = $request->hasFile('file') && $request->get("decoded") == true ? now() : null;
            //$profile->status_id = $request->hasFile('file') && $request->get("decoded") == true ? Profile::VERIFIED : Profile::PENDING;
            $profile->status_id = Profile::PENDING;
            $profile->save();
            if ($request->hasFile('file')) {
                $request->request->add([
                    "file_type_id" => File::DOCUMENT,
                ]);
                app(ProfileController::class)->saveFile($request, $profile);
            }
            $user->markEmailAsVerified();
            DB::commit();
            return $user;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

     /**
     * Create a new user instance after a valid registration entity.
     *
     * @param array $data
     * @return User
     * @throws \Exception
     */
    protected function createEntity(Request $request)
    {
        try {
            DB::beginTransaction();
            $user = new User();
            $full_name = $request->get('organization_name');
            $user->name = toUpper(preg_replace('!\s+!', ' ', $full_name));
            $user->email = $request->get('email');
            $user->password = Hash::make($request->get('password'));
            $user->save();

            PreRegisterEntity::query()
                ->where('email', $request->get('email'))
                ->where('code', $request->get('code'))
                ->delete();

            $legal_profile = Profile::query()
                ->where('document', $request->get('legal_rep_document'))
                ->first();

            $entityProfile = new ProfileEntity();
            $entityProfile->legal_rep_document = $request->get('legal_rep_document');
            $entityProfile->legal_rep_name = $request->get('legal_rep_name');
            $entityProfile->organization_type_id = $request->get('organization_type_id');
            $entityProfile->organization_subtype_id = $request->get('organization_subtype_id');
            $entityProfile->has_legal_person = $request->get('has_legal_person');
            $entityProfile->person_type_id = $request->get('person_type_id');
            $entityProfile->has_sporting_aval = $request->get('has_sporting_aval');
            $entityProfile->sporting_aval_number = $request->get('sporting_aval_number');
            $entityProfile->organization_name = $request->get('organization_name');
            $entityProfile->document_type_id = $request->get('document_type_id');
            $entityProfile->document = $request->get('document');
            $entityProfile->address = $request->get('address');
            $entityProfile->email = $request->get('email');
            $entityProfile->phone = $request->get('phone');
            //$entityProfile->legal_profile_id = $legal_profile->id;
            $entityProfile->legal_profile()->associate($legal_profile);
            $entityProfile->save();

            $profile = $user->profiles()->save(
                new Profile([
                    'profile_type_id' => Profile::PROFILE_ORGANIZATION,
                    'document_type_id' => $request->get('document_type_id'),
                    'document' => $request->get('document'),
                    'name' => toUpper($request->get('organization_name')),
                    'address' => toUpper($request->get('address')),
                    'mobile_phone' => $request->get('phone'),
                    'is_organization' => 1
                ])
            );
            $profile->status_id = Profile::PENDING;
            $profile->save();
            $entityProfile->profile()->associate($profile);
            $entityProfile->save();

            if ($request->hasFile('file_legal_representation')) {
                $request->request->add([
                    "file_type_id" => File::LEGAL_REPRESENTATION,
                ]);
                app(ProfileController::class)->saveFileComplete($request, $profile, 'file_legal_representation', File::LEGAL_REPRESENTATION);
            }
            if ($request->hasFile('file_sport_recognition')) {
                $request->request->add([
                    "file_type_id" => File::SPORT_RECOGNITION,
                ]);
                app(ProfileController::class)->saveFileComplete($request, $profile, 'file_sport_recognition', File::SPORT_RECOGNITION);
            }
            $user->markEmailAsVerified();
            DB::commit();
            return $user;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
