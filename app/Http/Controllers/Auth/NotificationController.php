<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class NotificationController extends Controller
{
    /**
     * NotificationController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $unread = auth('api')->user()
            ->loadCount("unreadNotifications")
            ->unread_notifications_count ?? 0;
        return $this->success_message(
            auth('api')->user()
                ->notifications()
                ->paginate($this->per_page),
            Response::HTTP_OK,
            Response::HTTP_OK,
            [
                'unread' => (int) $unread
            ]
        );
    }

    public function markAsRead($id)
    {
        $notification = auth('api')->user()->notifications()->where('id', $id)->first();
        if ($notification) {
            $notification->markAsRead();
        }
        return $this->success_message(
            'Notificación marcada como leida'
        );
    }

    public function markAllAsRead()
    {
        auth('api')->user()->unreadNotifications->markAsRead();
        return $this->success_message(
            'Notificación marcadas como leídas'
        );
    }

    public function destroy($id)
    {
        auth('api')->user()->notifications()->where('id', $id)->delete();
        return $this->success_message(
            'Notificación eliminada'
        );
    }
}
