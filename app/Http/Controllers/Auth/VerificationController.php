<?php

namespace App\Http\Controllers\Auth;

use App\Entities\Security\PendingUserEmail;
use App\Entities\Security\PreRegister;
use App\Entities\Security\PreRegisterEntity;
use App\Entities\Security\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\VerificationCodeRequest;
use App\Http\Requests\Security\PreRegisterRequest;
use App\Http\Requests\Security\PreRegisterEntityRequest;
use App\Http\Requests\Security\PreRegisterValidationRequest;
use App\Http\Requests\Security\PreRegisterEntityValidationRequest;
use App\Http\Resources\UserResource;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Exceptions\InvalidSignatureException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Entities\CitizenPortal\Profile;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth:api')->except([
            'manualVerificationCode',
            'preRegister',
            'preRegisterValidation',
            'preRegisterEntity',
            'preRegisterEntityValidation',
            'preRegisterInitValidation'
        ]);
        // $this->middleware('signed')->only('verify');
        $this->middleware('api.throttle:60,1')->only('verify', 'resend');
    }

    /**
     * Show the email verification notice.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function show(Request $request)
    {
        if ($request->user('api')->hasVerifiedEmail()) {
            return $this->success_message('El correo electrónico ha sido verificado.');
        }
        else {
            return $this->error_response(
                'El correo electrónico no ha sido verificado.'
            );
        }
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function verify(Request $request)
    {
        try {
            if ($request->route('id') != $request->user('api')->getValidationCode()) {
                return $this->error_response(
                    'El código de verificación es inválido'
                );
            }

            if ($request->user('api')->hasVerifiedEmail()) {
                return $this->error_response(
                    'Este correo electrónico ya ha sido verificado.',
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    new UserResource(
                        $request->user('api')
                    )
                );
            }

            if ($request->user('api')->markEmailAsVerified()) {
                event(new Verified($request->user('api')));
            }

            return $this->success_message(
                'El correo ha sido electrónico verificado satisfactoriamente.',
                Response::HTTP_OK,
                Response::HTTP_OK,
                new UserResource(
                    $request->user('api')
                )
            );
        } catch (\Exception $exception) {
            if ($exception instanceof InvalidSignatureException) {
                return $this->error_response(__('validation.handler.invalid_signature'), Response::HTTP_UNPROCESSABLE_ENTITY, $exception);
            }
        }
//        return redirect($this->redirectPath());
    }


    /**
     * Resend the email verification notification.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function resend(Request $request)
    {
        if ($request->user('api')->hasVerifiedEmail()) {
            return $this->error_response(
                'Este correo electrónico ya ha sido verificado.',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                new UserResource(
                    $request->user('api')
                )
            );
//            return redirect($this->redirectPath());
        }

        $request->user('api')->sendEmailVerificationNotification();

        return $this->success_message('El correo electrónico de verificación ha sido reenviado.');
//        return back()->with('resent', true);
    }

    /**
     * Resend the email verification notification.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function manualVerificationCode(User $user)
    {
        if ($user->hasVerifiedEmail()) {
            return $this->error_response(
                'Este correo electrónico ya ha sido verificado.',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $user->sendEmailVerificationNotification();

        return $this->success_message('El correo electrónico de verificación ha sido reenviado.');
    }

    public function changeEmail(Request $request)
    {
        $this->validator($request->all())->validate();
        if (!(Hash::check($request->get('password'), auth('api')->user()->password))) {
            return $this->error_response(
                "La contraseña no coincide con nuestros registros."
            );
        }
        $digits = 6;
        $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $form = PendingUserEmail::query()->updateOrCreate(
            ["user_id" => auth('api')->user()->id],
            [
                "email" =>  $request->get('email'),
                "code"  =>  $code,
            ]
        );
        $form->save();
        $form->sendNewEmailVerificationNotification();
        return $this->success_message('Hemos enviado un código de verificación a tu correo.');
    }

    public function confirmEmailChange(VerificationCodeRequest $request)
    {
        try {
            $form = PendingUserEmail::query()
                ->where('user_id', auth()->user()->id)
                ->where('code', $request->get('code'))
                ->firstOrFail();
            $form->activate();
            return $this->success_message('Tu correo ha sido cambiado satisfactoriamente.');
        } catch (\Exception $exception) {
            if ($exception instanceof NotFoundHttpException) {
                return $this->error_response(
                    'El código de verificación es inválido'
                );
            }
            return $this->error_response(
                'El código de verificación es inválido'
            );
        }
    }

    public function resendNewEmailConfirmation()
    {
        try {
            $form = PendingUserEmail::query()
                ->where('user_id', auth()->user()->id)
                ->firstOrFail();
            $digits = 6;
            $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
            $form->code = $code;
            $form->save();
            $form->sendNewEmailVerificationNotification();
            return $this->success_message('Hemos enviado un código de verificación a tu correo.');
        } catch (\Exception $exception) {
            return $this->error_response(
                'No se encontraron solicitudes de cambio de correo.'
            );
        }
    }

    public function preRegister(PreRegisterRequest $request)
    {
        $form = new PreRegister();
        $form->fill($request->validated());
        $form->save();
        $form->sendEmailVerificationNotification();
        return $this->success_message(
            "Para continuar con el proceso de registro, hemos enviado un código de verificación al correo electrónico registrado.",
            Response::HTTP_CREATED
        );
    }

    public function preRegisterValidation(PreRegisterValidationRequest $request)
    {
        try {
            $form = PreRegister::query()
                ->where('email', $request->get('email'))
                ->where('code', $request->get('code'))
                ->firstOrFail();
            return $this->success_message($form);
        } catch (\Exception $e) {
            return $this->error_response(
                'El código de verificación es inválido'
            );
        }
    }

    public function preRegisterEntity(PreRegisterEntityRequest $request)
    {
        $form = new PreRegisterEntity();
        $form->fill($request->validated());
        $form->save();
        $form->sendEmailVerificationNotification();
        return $this->success_message(
            "Para continuar con el proceso de registro, hemos enviado un código de verificación al correo electrónico registrado.",
            Response::HTTP_CREATED
        );
    }

    public function preRegisterEntityValidation(PreRegisterEntityValidationRequest $request)
    {
        try {
            $form = PreRegisterEntity::query()
                ->where('email', $request->get('email'))
                ->where('code', $request->get('code'))
                ->firstOrFail();
            return $this->success_message($form);
        } catch (\Exception $e) {
            return $this->error_response(
                'El código de verificación es inválido'
            );
        }
    }

    public function preRegisterInitValidation(Request $request)
    {
        try {
            $profile = Profile::query()
                            ->where('document', $request->get('legal_rep_document'))
                            ->where('document_type_id', '!=', 7)
                            ->firstOrFail();
            return $this->success_message($profile);
        } catch (\Exception $e) {
            return $this->error_response(
                'No se encontró un representante con ese número de documento.'
            );
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }
}
