<?php


namespace App\Http\Resources\Citizen;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class SchedulePublicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        =>  isset($this->id) ? (int) $this->id : null,
            'icon'        =>  $this->icon ?? null,
            'program_name'      =>  isset($this->program_name) ? (string) $this->program_name : null,
            'activity_name'      =>  isset($this->activity_name) ? (string) $this->activity_name : null,
            'stage_name'      =>  isset($this->stage_name) ? (string) $this->stage_name : null,
            'park_code'      =>  isset($this->park_code) ? (string) $this->park_code : null,
            'park_name'      =>  isset($this->park_name) ? (string) $this->park_name : null,
            'park_address'      =>  isset($this->park_address) ? (string) $this->park_address : null,
            'weekday_name'      =>  isset($this->weekday_name) ? (string) $this->weekday_name : null,
            'daily_name'      =>  isset($this->daily_name) ? (string) $this->daily_name : null,
            'min_age'      =>  isset($this->min_age) ? (int) $this->min_age : null,
            'max_age'      =>  isset($this->max_age) ? (int) $this->max_age : null,
            'quota'      =>  isset($this->quota) ? (int) $this->quota : null,
            'is_paid'      =>  isset($this->is_paid) ? (bool) $this->is_paid : null,
            'rate_id'      =>  isset($this->rate_id) ? (int) $this->rate_id : null,
            'rate_name'      =>  isset($this->rate_name) ? (string) $this->rate_name : null,
            'rate_value'      =>  isset($this->rate_value) ? (int) $this->rate_value : null,
            'is_initiate'      =>  isset($this->is_initiate) ? (bool) $this->is_initiate : null,
            'start_date'      =>  isset($this->start_date) ? $this->start_date->format('Y-m-d H:i:s') : null,
            'final_date'      =>  isset($this->final_date) ? $this->final_date->format('Y-m-d H:i:s') : null,
            'is_activated'      =>  isset($this->is_activated) ? (bool) $this->is_activated : null,
            'is_teams'      =>  isset($this->is_teams) ? (bool) $this->is_teams : null,
            'min_team_participants_quota'      =>  isset($this->min_team_participants_quota) ? (int) $this->min_team_participants_quota : 0,
            'max_team_participants_quota'      =>  isset($this->max_team_participants_quota) ? (int) $this->max_team_participants_quota : 0,
            'team_quota'      =>  isset($this->taken_teams) ? (int) $this->taken_teams : 0,
            'disability'      =>  isset($this->disability) ? (bool) $this->disability : null,
            'taken'   => isset($this->users_schedules_count) ? (int) $this->users_schedules_count : 0,
            'users_schedules_count'   => isset($this->users_schedules_count) ? (int) $this->users_schedules_count : 0,
            'teams_schedules_count'   => isset($this->teams_schedules_count) ? (int) $this->teams_schedules_count : 0,
            'consent' => $this->consents_data(),
            "regulations" => $this->regulations,
            'created_at'    =>  isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public static function headers()
    {
        return [
            'headers'   => [
                [
                    'align' => "right",
                    'text' => "#",
                    'value'  =>  "id",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.activity')),
                    'value'  =>  "activity_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.min_age')),
                    'value'  =>  "min_age",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.max_age')),
                    'value'  =>  "max_age",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.quota')),
                    'value'  =>  "quota",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.users_schedules_count')),
                    'value'  =>  "users_schedules_count",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.is_paid')),
                    'value'  =>  "is_paid",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.is_activated')),
                    'value'  =>  "is_activated",
                    'sortable' => true
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.actions')),
                    'value'  =>  "actions",
                    'sortable' => false
                ],
            ],
            'expanded'  => [
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.weekday')),
                    'value'  =>  "weekday_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.daily')),
                    'value'  =>  "daily_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.is_initiate')),
                    'value'  =>  "is_initiate",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.start_date')),
                    'value'  =>  "start_date",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.final_date')),
                    'value'  =>  "final_date",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.program')),
                    'value'  =>  "program_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.stage')),
                    'value'  =>  "stage_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.park_code')),
                    'value'  =>  "park_code",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.park')),
                    'value'  =>  "park_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.address')),
                    'value'  =>  "park_address",
                    'sortable' => true
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.created_at')),
                    'value'  =>  "created_at",
                    'sortable' => true
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.updated_at')),
                    'value'  =>  "updated_at",
                    'sortable' => true
                ],
            ],
        ];
    }

    public function consents_data()
    {
        if (auth('api')->check()) {
            $principal = auth('api')->user()->user_profile;
            $profiles = auth('api')->user()->profiles()->get()->map(function ($model) use ($principal) {
                $stage = $this->stage_name ?? null;
                $age = isset($model->birthdate->age) ? (int) $model->birthdate->age : 9999;
                $merge = [];
                if ($age < 18) {
                    $merge = [
                        "beneficiary_name" => toUpper($model->full_name),
                        "beneficiary_document" => $model->document,
                        "contact_name" => toUpper($model->contact_name),
                        "contact_phone" => $model->contact_phone,
                    ];
                }
                $profile_name = $principal->full_name ?? $model->full_name;
                $profile_document = $principal->document ?? $model->document;
                return array_merge([
                    "id" => $model->id ?? null,
                    "name" => $age > 18 ? $model->full_name : $profile_name,
                    "age" => $age,
                    "document" => $age > 18 ? $model->document : $profile_document,
                    "phone" => $model->mobile_phone,
                    "email" => $model->user->email ?? null,
                    "activity" => $this->activity_name ?? null,
                    "place" => toUpper("Bogotá $stage"),
                    "date" => now()->format("Y-m-d"),
                ], $merge);
            });
            $texts = [];
            foreach ($this->informed_consents as $consent) {
                foreach ($profiles as $profile) {
                    $phrase  = $consent->text ?? "";
                    $newPhrase = preg_replace_callback('~{(.+?)}~',
                        function($x) use ($profile) {
                        return $profile[$x[1]] ?? '';
                        },
                        $phrase
                    );
                    if ($profile["age"] > 17 && !!$consent->adult) {
                        $texts[] = [
                            "id" => $consent->id ?? null,
                            "title" => $consent->title ?? null,
                            "text"  => $newPhrase,
                            "profile_id" => $profile["id"] ?? null,
                            'created_at'    =>  isset($consent->created_at) ? $consent->created_at->format('Y-m-d H:i:s') : null,
                            'updated_at'    =>  isset($consent->updated_at) ? $consent->updated_at->format('Y-m-d H:i:s') : null,
                        ];
                    }
                    if ($profile["age"] < 18 && !$consent->adult) {
                        $texts[] = [
                            "id" => $consent->id ?? null,
                            "title" => $consent->title ?? null,
                            "text"  => $newPhrase,
                            "profile_id" => $profile["id"] ?? null,
                            'created_at'    =>  isset($consent->created_at) ? $consent->created_at->format('Y-m-d H:i:s') : null,
                            'updated_at'    =>  isset($consent->updated_at) ? $consent->updated_at->format('Y-m-d H:i:s') : null,
                        ];
                    }
                }
            }
            return $texts;
        }
        return $this->informed_consents;
    }
}
