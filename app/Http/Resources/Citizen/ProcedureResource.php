<?php

namespace App\Http\Resources\Citizen;

use App\Entities\CitizenPortal\ProcedureType;
use App\Entities\Clubes\Club;
use App\Entities\Schools\School;
use Illuminate\Http\Resources\Json\JsonResource;

class ProcedureResource extends JsonResource
{
    const CLUB = 1;
    const SCHOOL = 2;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (isset($this->club_id)) {
            $club = Club::query()->where('id', $this->club_id)->first();
        }

        if (isset($this->school_id)) {
            $school = School::query()->where('id', $this->school_id)->first();
        }

        return [
            'id'                    => isset($this->id) ? (int) $this->id : null,
            'procedure_type_id'     => isset($this->procedure_type_id) ? (int) $this->procedure_type_id : null,
            'procedure_type'        => isset($this->procedure_type_id) ? $this->procedureType() : null,
            'request_type'          => isset($club) ? $this->requestTypeClub($club) : (isset($school) ? $this->requestTypeSchool($school) : null),
            'organization_name'     => isset($club) ? $club->club : (isset($school) ? $school->school : null),
            'profile_id'            => isset($this->profile_id) ? (int) $this->profile_id : null,
            'entity_profile_id'     => isset($this->entity_profile_id) ? (int) $this->entity_profile_id : null,
            'club_id'               => isset($this->club_id) ? (int) $this->club_id : null,
            'school_id'             => isset($this->school_id) ? (int) $this->school_id : null,
            'state'                 => $this->status() ?? null,
            'state_color'           => $this->statusColor() ?? null,
            'created_at'            => isset($this->created_at) ? $this->created_at->format('Y-m-d') : null,
            'updated_at'            => isset($this->updated_at) ? $this->updated_at->format('Y-m-d') : null,
        ];
    }

    public function procedureType(): ?string
    {
        $type = ProcedureType::query()
            ->find($this->procedure_type_id);

        return isset($type) ? $type->name : null;
    }

    public function requestTypeClub($club): ?string
    {
        $types = collect([
            0 => [
                'id' => 1,
                'name' => 'REQUISITOS PARA EL OTORGAMIENTO DEL RECONOCIMIENTO DEPORTIVO',
            ],
            1 => [
                'id' => 2,
                'name' => 'REQUISITOS PARA EL OTORGAMIENTO DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
            ],
            2 => [
                'id' => 3,
                'name' => 'REQUISITOS PARA LA RENOVACIÒN DEL RECONOCIMIENTO DEPORTIVO',
            ],
            3 => [
                'id' => 4,
                'name' => 'REQUISITOS PARA LA RENOVACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
            ],
            4 => [
                'id' => 5,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO',
            ],
            5 => [
                'id' => 6,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO',
            ],
            6 => [
                'id' => 7,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO',
            ],
            7 => [
                'id' => 8,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
            ],
            8 => [
                'id' => 9,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
            ],
            9 => [
                'id' => 10,
                'name' => 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS',
            ],
            10 => [
                'id' => 11,
                'name' => 'REQUISITOS PARA ACTUALIZACIÓN EXPEDIENTE',
            ],
            11 => [
                'id' => 12,
                'name' => 'SUBSANE',
            ],
            12 => [
                'id' => 13,
                'name' => 'DERECHO DE PETICIÓN',
            ],
            13 => [
                'id' => 14,
                'name' => 'SOLICITUD DE INFORMACIÓN',
            ],
            14 => [
                'id' => 15,
                'name' => 'EXPEDICIÓN DE COPIAS O SCANNER',
            ],
            15 => [
                'id' => 16,
                'name' => 'CERTIFICACIONES',
            ],
            16 => [
                'id' => 17,
                'name' => 'DESISTIMIENTO',
            ],
            17 => [
                'id' => 18,
                'name' => 'RECURSO DE REPOSICIÓN',
            ],
        ]);

        $type = $types
            ->where('id', $club->request_type)
            ->first();

        return $type['name'] ?? null;
    }

    public function requestTypeSchool($school): ?string
    {
        $types = collect([
            0 => [
                'id' => 1,
                'name' => 'REQUISITOS PARA EL OTORGAMIENTO DE AVAL PARA LAS ESCUELAS DE FORMACIÓN DEPORTIVA',
            ],
            1 => [
                'id' => 2,
                'name' => 'REQUISITOS PARA LA RENOVACIÓN DE AVAL PARA LAS ESCUELAS DE FORMACIÓN DEPORTIVA',
            ],
            2 => [
                'id' => 3,
                'name' => 'REQUISITOS PARA LA INCLUSIÓN DE NUEVOS DEPORTES EN LAS ESCUELAS DE FORMACIÓN',
                'description' => 'CUANDO SE PRODUZCA EL CAMBIO DEL DEPORTE O LA ADICIÓN DE DISCIPLINAS DEPORTIVAS',
            ],
            3 => [
                'id' => 4,
                'name' => 'REQUISITOS PARA ACTUALIZACIÓN EXPEDIENTE',
                'description' => 'LA ESCUELA DE FORMACIÓN PODRÁ RADICAR CUALQUIERA DE LOS SIGUIENTES DOCUMENTOS CON EL FIN DE ACTUALIZAR EL EXPEDIENTE'
            ],
            4 => [
                'id' => 5,
                'name' => 'SUBSANE',
            ],
            5 => [
                'id' => 6,
                'name' => 'DERECHO DE PETICIÓN',
            ],
            6 => [
                'id' => 7,
                'name' => 'SOLICITUD DE INFORMACIÓN',
            ],
            7 => [
                'id' => 8,
                'name' => 'EXPEDICIÓN DE COPIAS O SCANNER',
            ],
            8 => [
                'id' => 9,
                'name' => 'DESISTIMIENTO',
            ],
            9 => [
                'id' => 10,
                'name' => 'RECURSO DE REPOSICIÓN',
            ],
        ]);

        $type = $types
            ->where('id', $school->request_type)
            ->first();

        return $type['name'] ?? null;
    }

    public function status(): string
    {
        switch ($this->state) {
            case 1:
                return 'FINALIZADO';
            default:
                return 'EN TRÁMITE';
        }
    }

    public function statusColor()
    {
        switch ($this->state) {
            case 0:
                return 'warning';
            case 1:
                return 'success';
        }
    }

    public static function headers()
    {
        return [
            'headers'   => [
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "#",
                    'value'  =>  "id",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Fecha de la solicitud",
                    'value'  =>  "created_at",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Tipo de trámite",
                    'value'  =>  "procedure_type",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Tipo de solicitud",
                    'value'  =>  "request_type",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Nombre",
                    'value'  =>  "organization_name",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Estado",
                    'value'  =>  "state",
                    'sortable' => false
                ],
            ],
        ];
    }
}
