<?php

namespace App\Http\Resources\Citizen;

use Illuminate\Http\Resources\Json\JsonResource;

class ModalityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => isset($this->id) ? (int) $this->id : null,
            'name'          => toUpper($this->name ?? null),
            'created_at'    => isset($this->created_at) ? (int) $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    => isset($this->updated_at) ? (int) $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}
