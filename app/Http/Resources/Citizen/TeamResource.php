<?php

namespace App\Http\Resources\Citizen;

use App\Entities\CitizenPortal\Status;
use App\Entities\CitizenPortal\TeanSchedule;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = TeanSchedule::query()
            ->where("team_id", $this->id)
            ->where("schedule_id", $this->activity_id)
            ->first();
        if (isset($status->status_id)) {
            $status = Status::query()->find($status->status_id);
        }
        return [
            'id'            => isset($this->id) ? (int) $this->id : null,
            'name'          => toUpper($this->name ?? null),
            'modality_id'   => isset($this->modality_id) ? (int) $this->modality_id : null,
            'modality'      => $this->modality->name  ?? null,
            'participants_count'  => isset($this->participants_count) ? (int) $this->participants_count : 0,
            'participants_info'  => isset($this->activity) ? $this->participants_count . ' de ' . $this->activity->max_team_participants_quota : null,
            'quota_info'  => isset($this->activity) ? 'Min ' . $this->activity->min_team_participants_quota . ' - Max ' . $this->activity->max_team_participants_quota : null,
            'participants'  => ParticipantResource::collection($this->participants),
            'activity_id'   => isset($this->activity_id) ? (int) $this->activity_id : null,
            'activity'   => isset($this->activity) ? (string) $this->activity->activity_name : null,
            "status_id"    => isset($status->id) ? (int)$status->id : null,
            "status"    => isset($status->name) ? toUpper($status->name) : null,
            'profile_id'     => isset($this->profile_id) ? (int) $this->profile_id : null,
            'confirmed'     => isset($this->confirmed) ? (bool) $this->confirmed : false,
            'created_at'    => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    => isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public static function headers()
    {
        return [
            'headers'   => [
                [
                    'align' => "left",
                    'text' => "#",
                    'value'  =>  "id",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => "Fecha Inscripción",
                    'value'  =>  "created_at",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => "Nombre",
                    'value'  =>  "name",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.activity')),
                    'value'  =>  "activity",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => "No.",
                    'value'  =>  "quota_info",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => "Participantes",
                    'value'  =>  "participants_info",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.status')),
                    'value'  =>  "status",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => "Confirmado",
                    'value'  =>  "confirmed",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.actions')),
                    'value'  =>  "actions",
                    'sortable' => false
                ]
            ],
            "expanded" => [
                [
                    'align' => "right",
                    'text' => 'Participantes',
                    'value'  =>  "participants",
                    'sortable' => false
                ],
            ]
        ];
    }
}
