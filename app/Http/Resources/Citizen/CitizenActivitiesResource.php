<?php


namespace App\Http\Resources\Citizen;



use App\Entities\CitizenPortal\File;
use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\ProfileView;
use App\Entities\CitizenPortal\Status;
use App\Entities\Payments\Payment;
use App\Entities\Payments\PsePark;
use App\Http\Resources\Payments\PaymentResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class CitizenActivitiesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = isset($this->status_id) ? (int) $this->status_id : Status::PENDING_SUBSCRIBE;
        $status = $status == Status::PENDING ? Status::PENDING_SUBSCRIBE : $status;
        $status_name = Status::find($status);
        return [
            'citizen_schedule_id'   => isset($this->id) ? (int) $this->id : null,
            $this->merge(new ScheduleResource($this->schedule_view)),
            'schedule_status_id'    => $status,
            'actions'               => $this->isAvailableToUnsubscribe(),
            'profile_id'             => isset($this->profile_id) ? (int) $this->profile_id : null,
            'profile_type_id'        => isset($this->profile->profile_type_id) ? (int) $this->profile->profile_type_id : null,
            'full_name'             => $this->profile->full_name ?? null,
            'schedule_status_color' => Status::getColor($status),
            'schedule_status_name'  => isset($status_name->name) ? (string) $status_name->name : null,
            'citizen_schedule_payment_files'  => FileResource::collection($this->files),
            'citizen_pse_reference' => $this->when(
              isset($this->reference_pse),
                new PaymentResource($this->payment),
              []
            ),
            'citizen_schedule_payment_at'   => isset($this->payment_at) ? $this->payment_at->format('Y-m-d H:i:s') : null,
            'citizen_schedule_created_at'   => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'citizen_schedule_updated_at'   => isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
            'payment' => $this->when(
                isset($this->schedule_view->is_paid) && $this->schedule_view->is_paid && auth('api')->check(),
                $this->payment_data(),
                []
            ),
        ];
    }

    public static function headers()
    {
        return [
            'headers'   => [
                [
                    'align' => "left",
                    'text' => "#",
                    'value'  =>  "citizen_schedule_id",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.full_name')),
                    'value'  =>  "full_name",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.activity')),
                    'value'  =>  "activity_name",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.weekday')),
                    'value'  =>  "weekday_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.daily')),
                    'value'  =>  "daily_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.min_age')),
                    'value'  =>  "min_age",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.max_age')),
                    'value'  =>  "max_age",
                    'sortable' => false
                ],
                [
                    'align' => "center",
                    'text' => 'Estado inscripción',
                    'value'  =>  "schedule_status_name",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.is_paid')),
                    'value'  =>  "is_paid",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.actions')),
                    'value'  =>  "actions",
                    'sortable' => false
                ],
            ],
            'expanded'   => [
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.program')),
                    'value'  =>  "program_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.stage')),
                    'value'  =>  "stage_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.park_code')),
                    'value'  =>  "park_code",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.park')),
                    'value'  =>  "park_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.address')),
                    'value'  =>  "park_address",
                    'sortable' => true
                ],
            ],
        ];
    }

    public function payment_data()
    {
        if (isset($this->payment_at) && now()->lessThanOrEqualTo($this->payment_at)) {
            $user = ProfileView::query()
                ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                ->where('user_id', auth('api')->user()->id)
                ->first();
            $name = $user->name ?? null;
            $s_name = $user->s_name ?? null;
            $surname = $user->surname ?? null;
            $s_surname = $user->s_surname ?? null;
            $id = $this->id ?? null;
            $profile_id = $this->profile->document ?? $this->profile_id ?? null;
            $concept = $this->schedule_view->activity_name ?? "ACTIVIDAD NATACIÓN/PISCINAS";
            $pse_park = PsePark::where('codigo_parque', park_code_to_pse_code($this->schedule_view->park_code ?? ""))->first()->id_parque ?? null;
            if (is_null($pse_park)) {
                return [];
            }
            $profile_name = $this->profile->name ?? null;
            $profile_s_name = $this->profile->s_name ?? null;
            $profile_surname = $this->profile->surname ?? null;
            $profile_s_surname = $this->profile->s_surname ?? null;
            return [
                'name'          => toUpper("{$profile_name} {$profile_s_name}"),
                'surname'       => toUpper("{$profile_surname} {$profile_s_surname}"),
                'document_type_id' => pse_document_type($this->profiles_view->document_type ?? null),
                'document'       => $this->profile->document ?? null,
                'email'         => $this->profiles_view->email ?? null,
                'phone'         => $this->profile->mobile_phone ?? null,
                'address'         => $this->profile->address ?? null,
                'booking_id'    => null,
                'amount'        => $this->schedule_view->rate_value ?? null,
                'park_id'       => $pse_park,
                'service_id'    => $this->schedule_view->rate_service_id ?? null,
                'person_type_id'=> "N",
                'bank_id'       => null,
                'ip_address'    => null,
                'namePayer'    => toUpper("{$name} {$s_name}"),
                'lastNamePayer'    => toUpper("{$surname} {$s_surname}"),
                'phonePayer'    => $user->mobile_phone ?? null,
                'emailPayer'    => $user->email ?? null,
                'addressPayer'    => $user->address ?? null,
                'documentTypeSelectedPayer'    => pse_document_type($user->document_type ?? null),
                'documentPayer'    => $user->document ?? null,
                // P = Id del perfil - A = Id de la actividad asociada.
                'concept'       => Str::substr(toUpper("P$profile_id-A$id - $concept"), 0, 100),
                'created_at'    => now()->format('Y-m-d H:i:s'),
            ];
        }

        return [];
    }
}
