<?php

namespace App\Http\Resources\Citizen;

use App\Entities\Security\DocumentType;
use App\Http\Resources\Clubes\ClubResource;
use App\Http\Resources\Schools\SchoolResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrganizationResource extends JsonResource
{
    const EMPRESAS = 1;
    const ORGANISMOS = 2;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                               => isset($this->id) ? (int) $this->id : null,
            'legal_document'                   => $this->legal_rep_document  ?? null,
            'legal_representative'             => toUpper($this->legal_rep_name ?? null),
            'organization_type_id'             => isset($this->organization_type_id) ? (int) $this->organization_type_id : null,
            'organization_type'                => isset($this->organization_type_id) ? $this->organizationType() : null,
            'organization_subtype_id'          => isset($this->organization_subtype_id) ? (int) $this->organization_subtype_id : null,
            'organization_subtype'             => isset($this->organization_subtype_id) ? $this->organizationSubType() : null,
            'document_type_id'                 => isset($this->document_type_id) ? (int) $this->document_type_id : null,
            'document_type'                    => isset($this->document_type_id) ? $this->documentType() : null,
            'document'                         => $this->document  ?? null,
            'dv'                               => $this->dv  ?? null,
            'organization_name'                => $this->organization_name  ?? null,
            'address'                          => $this->address  ?? null,
            'email'                            => $this->email  ?? null,
            'phone'                            => $this->phone  ?? null,
            'has_legal_person'                 => isset($this->has_legal_person) ? (int) $this->has_legal_person : null,
            'has_legal_person_text'            => isset($this->has_legal_person) ? $this->legalPersonText() : null,
            'legal_status_color'               => $this->legalStatusColor(),
            'has_sporting_aval'                => isset($this->has_sporting_aval) ? (int) $this->has_sporting_aval : null,
            'has_sporting_aval_text'           => isset($this->has_sporting_aval) ? $this->sportingAvalText() : null,
            'aval_status_color'                => $this->avalStatusColor(),
            'has_sporting_recognition'         => isset($this->has_sporting_recognition) ? (int) $this->has_sporting_recognition : null,
            'has_sporting_recognition_text'    => isset($this->has_sporting_recognition) ? $this->sportingRecognitionText() : null,
            'recognition_status_color'         => $this->recognitionStatusColor(),
            'sporting_aval_number'             => $this->sporting_aval_number  ?? null,
            'club'                             => new ClubResource($this->whenLoaded('club')),
            'school'                           => new SchoolResource($this->whenLoaded('school')),
            'created_at'                       => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'                       => isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function organizationType(): string
    {
        return $this->organization_type_id == self::EMPRESAS ? 'EMPRESA' : 'ORGANISMO DEPORTIVO';
    }

    public function legalStatusColor(): string
    {
        return $this->has_legal_person == 1 ? 'success' : 'error';
    }

    public function avalStatusColor(): string
    {
        return $this->has_sporting_aval == 1 ? 'success' : 'error';
    }

    public function recognitionStatusColor(): string
    {
        return $this->has_sporting_recognition == 1 ? 'success' : 'error';
    }

    public function sportingAvalText(): string
    {
        return $this->has_sporting_aval == 1 ? 'SI' : 'NO';
    }

    public function legalPersonText(): string
    {
        return $this->has_legal_person == 1 ? 'SI' : 'NO';
    }

    public function sportingRecognitionText(): string
    {
        return $this->has_sporting_recognition == 1 ? 'SI' : 'NO';
    }

    public function organizationSubType(): string
    {
        switch ($this->organization_subtype_id) {
            case 1:
                return 'CLUB';
            case 2:
                return 'ESCUELA';
            case 3:
                return 'LIGA';
            case 4:
                return 'FEDERACIÓN';
            default:
                return 'NO ES UN ORGANISMO DEPORTIVO';
        }
    }

    public function documentType(): ?string
    {
        $document_type = DocumentType::query()
            ->find($this->document_type_id);

        return isset($document_type) ? $document_type->Nombre_TipoDocumento : null;
    }

    public static function headers()
    {
        return [
            'headers'   => [
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "#",
                    'value'  =>  "id",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Representante Legal",
                    'value'  =>  "legal_representative",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Tipo de organización",
                    'value'  =>  "organization_type",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Tipo de organismo",
                    'value'  =>  "organization_subtype",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Nombre",
                    'value'  =>  "organization_name",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Tiene persona jurídica?",
                    'value'  =>  "has_legal_person",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Tiene reconocimineto deportivo?",
                    'value'  =>  "has_sporting_recognition",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "Tiene aval deportivo?",
                    'value'  =>  "has_sporting_aval",
                    'sortable' => false
                ],
            ],
            "expanded" => [
                [
                    'icon'  => 'mdi-pound',
                    'align' => "right",
                    'text' => "Tipo de documento",
                    'value'  =>  "document_type",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "right",
                    'text' => "No. de documento",
                    'value'  =>  "document",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "right",
                    'text' => "Dígito de verificación",
                    'value'  =>  "dv",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "right",
                    'text' => "Dirección",
                    'value'  =>  "address",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "right",
                    'text' => "Email",
                    'value'  =>  "email",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "right",
                    'text' => "Teléfono",
                    'value'  =>  "phone",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "right",
                    'text' => "No. Reconocimiento o aval deportivo",
                    'value'  =>  "sporting_aval_number",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "right",
                    'text' => "No. Documento Representante Legal",
                    'value'  =>  "legal_document",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "right",
                    'text' => "Nombre Representante Legal",
                    'value'  =>  "legal_representative",
                    'sortable' => false
                ],
            ]
        ];
    }
}
