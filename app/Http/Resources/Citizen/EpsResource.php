<?php

namespace App\Http\Resources\Citizen;

use Illuminate\Http\Resources\Json\JsonResource;

class EpsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => isset($this->id) ? (int) $this->id : null,
            'name'          => toUpper($this->name ?? null),
            'status'        => isset($this->status) ? (bool) $this->status : null,
        ];
    }
}
