<?php

namespace App\Http\Resources;

use App\Entities\CitizenPortal\Profile;
use App\Http\Resources\Citizen\ProfileResource;
use App\Http\Resources\Citizen\ProfileViewResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? (int) $this->id : null,
            'name' => toUpper($this->name ?? null),
            'email' => toLower($this->email ?? null),
            'is_verified' => isset($this->is_verified) ? (boolean) $this->is_verified :  null,
            'activated' => isset($this->activated) ? (boolean) $this->activated :  null,
            'has_profile'   => $this->profiles()->where('profile_type_id', Profile::PROFILE_PERSONAL)->count(),
            'profiles'      => ProfileViewResource::collection( $this->whenLoaded('profiles') ),
            'profile'   => new ProfileViewResource( $this->user_profile ),
            'email_verified_at' => isset($this->email_verified_at) ? $this->email_verified_at->format('Y-m-d H:i:s') : null,
            'verified_at' => isset($this->verified_at) ? $this->verified_at->format('Y-m-d H:i:s') : null,
            'created_at' => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at' => isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}
