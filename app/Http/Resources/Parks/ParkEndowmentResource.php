<?php

namespace App\Http\Resources\Parks;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Resources\Json\JsonResource;

class ParkEndowmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->merge( new ParkFinderResource( $this->park ) ),
            'image'    =>  isset( $this->Imagen ) ? $this->image_exist( $this->Imagen ) : null,
            'park_endowment_id' => isset( $this->Id ) ? (int) $this->Id : null,
            'endowment_id'      => isset( $this->Id_Dotacion ) ? (int) $this->Id_Dotacion : null,
            'endowment_description'      => isset( $this->Descripcion ) ? (string) $this->Descripcion : null,
            'details' => new EndowmentResource($this),
        ];
    }

    public function image_exist( $image = null )
    {
        $base = 'https://sim1.idrd.gov.co/SIM/Parques/Foto/';
        if ( $image ) {
            return $this->urlExists( "{$base}{$image}" ) ? "{$base}{$image}" : null;
        }
        return null;
    }
    function urlExists($url = null)
    {
        try {
            if ($url == null) {
                return false;
            }
            $client = new Client();
            $data = $client->head( $url );
            $status = $data->getStatusCode();
            return $status >= 200 && $status < 320;
        } catch (ClientException $e) {
            return false;
        }
    }
}
