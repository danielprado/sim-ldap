<?php

namespace App\Http\Resources\Parks;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Resources\Json\JsonResource;

class ParkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        =>  (int) isset( $this->Id ) ? (int) $this->Id : null,
            'code'      =>  isset( $this->Id_IDRD ) ? toUpper($this->Id_IDRD) : null,
            'name'      =>  isset( $this->Nombre ) ? toUpper($this->Nombre) : null,
            'phone'     =>  isset( $this->TelefonoParque ) ? (int)$this->TelefonoParque : null,
            'stratum'     =>  isset( $this->Estrato ) ? (int)$this->Estrato : 2,
            'pqrs'      =>  'atncliente@idrd.gov.co',
            'email'     =>  isset( $this->Email ) ? toLower($this->Email) : null,
            'schedule_service'  =>  'Lunes a Viernes: 6:00 AM - 6:00 PM / Sábados y Domingos: 5:00 AM - 6:00 PM',
            'schedule_admin'    =>  'Lunes a Viernes:  8:00 AM A  4:00 PM / Sábados y Domingos:  9:00 AM -2:00 PM',
            'scale_id'  =>  isset( $this->Id_Tipo ) ? (int) $this->Id_Tipo : null,
            'scale'     =>  isset( $this->scale->Tipo ) ? toUpper($this->scale->Tipo) : null,
            'locality'  =>  isset( $this->location->Localidad ) ? toUpper($this->location->Localidad) : null,
            'address'   =>  isset( $this->Direccion ) ? toUpper($this->Direccion) : null,
            'upz_code'  =>  isset( $this->Upz ) ? (int) $this->Upz : null,
            'upz'       =>  isset( $this->upz_name->Upz ) ? toUpper($this->upz_name->Upz) : null,
            'concept'   =>  isset( $this->certified->EstadoCertificado ) ? toUpper($this->certified->EstadoCertificado) : null,
            'alert'     => toLower($this->certified->EstadoCertificado ?? "") != 'certificado' ? "El predio se encuentra en proceso de incorporación al patrimonio inmobiliario de Bogotá" : null,
            'file'      =>  isset( $this->Id_IDRD ) ? $this->certified_exist($this->Id_IDRD) : null,
            'regulation'      => isset($this->CompeteIDRD) ? toUpper( $this->regulation($this->CompeteIDRD) ) : null,
            'regulation_file' => isset($this->CompeteIDRD) ? $this->regulation_file($this->CompeteIDRD) : null,
            'color'     =>  isset( $this->Id_Tipo ) ? $this->getColor((int) $this->Id_Tipo) : 'grey',
            'general_conditions' => [
                // "Se permite el ingreso de Bicicletas.",
                // "Se permite el ingreso de Mascotas.",
                "No se permite el consumo de bebidas alcohólicas y/o sustancias psicoactivas.",
                "Prohibido fumar.",
                "Respetar las condiciones de uso de cada escenario.",
            ],
            'loan_application' => [
                "Verificar con la administración del parque y/o el encargado, la disponibilidad del escenario requerido vía correo electrónico.",
                "1. Escenario Requerido para el evento.",
                "2. Fecha y Horario.",
                "3. Tipo de evento (¿Tiene aprovechamiento económico?)",
                "4. Aforo (Cantidad de asistentes)",
                "5. Datos del solicitante con ubicación (correo, teléfono, etc)",
                "De acuerdo al alcance y solicitud de escenario, se indicará el paso a seguir por parte del encargado del parque.",
                "El préstamo de escenarios está condicionado a la disponibilidad de los mismos.",
                "Se da prioridad a los proyectos y programas Institucionales.",
                "Todo préstamo para el uso del espacio público debe cumplir previamente con los requisitos de: Disponibilidad, Solicitud de permiso, entrega y revisión de soportes documentales (Documento de identidad, listado de participantes, descripción breve de la actividad), liquidación de costos y soporte de Pago (Consignación Davivienda / PSE), Solicitud y expedición de póliza por parte de aseguradora y aprobación IDRD de la misma, suscripción de permiso, y los demás que apliquen de acuerdo al alcance del evento y la normatividad vigente.",
            ],
        ];
    }

    public function getColor($id = null)
    {
        switch ($id) {
            case 1:
            case 2:
            case 3:
                return 'success';
                break;
            default;
                return 'grey';
                break;
        }
    }

    public function certified_exist( $code = null )
    {
        $base = 'https://sim1.idrd.gov.co/SIM/Parques/Certificado/';
        if ( $code ) {
            $path_tif = $this->urlExists( "{$base}{$code}.tif" ) ? "{$base}{$code}.tif" : null;
            $path_pdf = $this->urlExists( "{$base}{$code}.pdf" ) ? "{$base}{$code}.pdf" : null;
            return ($path_tif) ? $path_tif : $path_pdf;
        }
        return null;
    }

    function urlExists($url = null)
    {
        try {
            if ($url == null) {
                return false;
            }
            $client = new Client();
            $data = $client->head( $url );
            $status = $data->getStatusCode();
            return $status >= 200 && $status < 300;
        } catch (ClientException $e) {
            return false;
        }
    }

    public function regulation( $text = null )
    {
        $locality = isset( $this->location->Localidad ) ? toUpper($this->location->Localidad) : null ;
        switch ($text) {
            case 'Junta Administradora Local':
                return "Alcaldía Local / {$text} / {$locality}";
                break;
            case 'SI':
                return 'IDRD';
                break;
            default:
                return $text;
        }
    }

    public function regulation_file( $text = null )
    {
        switch ($text) {
            case 'Junta Administradora Local':
                return 'https://sim1.idrd.gov.co/SIM/Parques/Certificado/Resolucion2011.pdf';
                break;
            default:
                return null;
        }
    }
}
