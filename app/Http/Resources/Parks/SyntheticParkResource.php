<?php

namespace App\Http\Resources\Parks;

use App\Entities\Payments\Schedule;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\Paginator;

class SyntheticParkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $id = isset($this->park_endowment_id) ? (int) $this->park_endowment_id : null;
        return [
            'park_endowment_id' => $id,
            'endowment_id' => isset($this->endowment_id) ? (int) $this->endowment_id : null,
            'park_id' => isset($this->park_id) ? (int) $this->park_id : null,
            'park_name' =>  toUpper( $this->park_name ?? null ),
            'park_address' => toUpper($this->park_address ?? null),
            'park_code' =>  $this->park_code ?? null,
            'endowment_description' =>  $this->endowment_description ?? null,
            'image' =>  $this->image_exist( $this->image ?? null ),
            'locality_id' => isset($this->locality_id) ? (int) $this->locality_id : null,
            'locality' => toUpper($this->locality ?? null),
            'upz_id' => isset($this->upz_id) ? (int) $this->upz_id : null,
            'upz_code' => toUpper($this->upz_code ?? null),
            'upz' => toUpper($this->upz ?? null),
            "for_booking" => Schedule::query()->where("id_dotacion", $id)->count(),
        ];
    }

    public static function headers()
    {
        return [
            'headers'   => [
                [
                    'align' => "right",
                    'text' => "#",
                    "icon"  => 'mdi-pound',
                    'value'  =>  "park_endowment_id",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    "icon"  => 'mdi-pound',
                    'text' => "Código",
                    'value'  =>  "park_code",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    "icon"  => 'mdi-pine-tree',
                    'text' => "Parque",
                    'value'  =>  "park_name",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    "icon"  => 'mdi-pin',
                    'text' => "Dirección",
                    'value'  =>  "park_address",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    "icon"  => 'mdi-pin',
                    'text' => "Localidad",
                    'value'  =>  "locality",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    "icon"  => 'mdi-pin',
                    'text' => "UPZ",
                    'value'  =>  "upz",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    "icon"  => 'mdi-pin',
                    'text' => "Descripción",
                    'value'  =>  "endowment_description",
                    'sortable' => false
                ],
                [
                    'align' => "right",
                    "icon"  => 'mdi-photo',
                    'text' => "Imagen",
                    'value'  =>  "image",
                    'sortable' => false
                ],
            ]
        ];
    }

    public function image_exist( $image = null )
    {
        $base = 'https://sim1.idrd.gov.co/SIM/Parques/Foto/';
        if ( $image ) {
            return $this->urlExists( "{$base}{$image}" ) ? "{$base}{$image}" : null;
        }
        return null;
    }
    function urlExists($url = null)
    {
        try {
            if ($url == null) {
                return false;
            }
            $client = new Client();
            $data = $client->head( $url );
            $status = $data->getStatusCode();
            return $status >= 200 && $status < 320;
        } catch (ClientException $e) {
            return false;
        }
    }

    public static function paginate($resource)
    {
        return [
            "links" => [
                "first" => $resource->url(1),
                "last" => $resource->lastPage(),
                "next" => $resource->nextPageUrl(),
                "prev" => $resource->previousPageUrl(),
            ],
            "meta" => [
                "current_page" => $resource->currentPage(),
                "from" => $resource->firstItem(),
                "last_page" => $resource->lastPage(),
                "path" => Paginator::resolveCurrentPath(),
                "per_page" => $resource->perPage(),
                "to" => $resource->lastItem(),
                "total" => $resource->total(),
            ]
        ];
    }

    public static function transformMeili($hits)
    {
        return collect($hits ?? [])
                ->map(function ($item) {
                    return array_merge($item, [
                        "for_booking" => Schedule::query()->where("id_dotacion", $item['park_endowment_id'])->count(),
                    ]);
                });
    }
}
