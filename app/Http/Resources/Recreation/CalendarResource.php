<?php

namespace App\Http\Resources\Recreation;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CalendarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        =>  isset( $this->id ) ? (int) $this->id : null,
            'name'     =>  isset(  $this->stage ) ? "{$this->stage}" : null,
            'program'     =>  isset( $this->program ) ?  $this->program : null,
            'activity'     =>  isset( $this->activity ) ?  $this->activity : null,
            'address'     =>  isset( $this->address ) ?  $this->address : null,
            'meeting_point'     =>  isset( $this->meeting_point ) ?  $this->meeting_point : null,
            'responsable'       =>  isset( $this->responsable ) ?  $this->responsable : null,
            'stage_location'     =>  isset( $this->stage_location ) ?  $this->stage_location : null,
            'stage_upz'     =>  isset( $this->stage_upz ) ?  $this->stage_upz : null,
            'community_location'     =>  isset( $this->community_location ) ?  $this->community_location : null,
            'community_upz'     =>  isset( $this->community_upz ) ?  $this->community_upz : null,
            'position'  =>  [
                'lat'     =>  isset( $this->lat ) ?  $this->lat : null,
                'lng'     =>  isset( $this->lng ) ?  $this->lng : null,
            ],
            'lat'     =>  isset( $this->lat ) ?  $this->lat : null,
            'lng'     =>  isset( $this->lng ) ?  $this->lng : null,
            'quantity'     =>  isset( $this->quantity ) ?  (int) $this->quantity : null,
            'start'     =>  isset( $this->date, $this->start_hour ) ? $this->formatDate( "{$this->date} {$this->start_hour}" ) : null,
            'end'       =>  isset( $this->date, $this->end_hour ) ? $this->formatDate( "{$this->date} {$this->end_hour}" ) : null,
            'color' =>  isset( $this->date, $this->start_hour ) ?  $this->getClassName( "{$this->date} {$this->start_hour}" ): null,
            'created_at'   =>  isset( $this->created_at ) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'   =>  isset( $this->updated_at ) ? $this->updated_at->format('Y-m-d H:i:s') : null,

        ];
    }

    public function formatDate( $date = null )
    {
        if ( isset( $date ) ) {
            return Carbon::parse( $date )->format( 'Y-m-d H:i' );
        } else {
            return null;
        }
    }

    public function getClassName( $date = null )
    {
        if ( isset( $date ) ) {
            $date = Carbon::parse( $date );

            if ( now()->greaterThan( $date ) && now()->format('Y-m-d') != $date->format('Y-m-d') ) {
                return 'grey';
            }

            if ( now()->lessThan( $date ) && now()->format('Y-m-d') != $date->format('Y-m-d') ) {
                return 'primary';
            }

            if ( now()->format('Y-m-d') == $date->format('Y-m-d') ) {
                return 'warning';
            }
        }

        return 'red';
    }

}
