<?php

namespace App\Http\Resources\Clubes;

use App\Entities\Clubes\Sport;
use App\Entities\Clubes\SportClub;
use App\Entities\Security\DocumentType;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class ClubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => isset($this->id) ? (int) $this->id : null,
            'club'              => $this->club ?? null,
            'email'             => isset($this->email) ? toLower($this->email) : null,
            'president'         => isset($this->president) ? toUpper($this->president) : null,
            'phone'             => $this->phone ?? null,
            'club_type'         => $this->club_type ?? null,
            'request_type'      => isset($this->request_type) ? $this->requestType($this->request_type) : null,
            'accept_media'      => $this->accept_media ?? null,
            'status'            => $this->status ?? null,
            'response'          => $this->response ?? null,
            'sports'            => $this->sports(),
        ];
    }

    public function requestType($request): string
    {
        switch ($request) {
            case 1:
                return 'REQUISITOS PARA EL OTORGAMIENTO DEL RECONOCIMIENTO DEPORTIVO';
            case 2:
                return 'REQUISITOS PARA EL OTORGAMIENTO DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS';
            case 3:
                return 'REQUISITOS PARA LA RENOVACIÒN DEL RECONOCIMIENTO DEPORTIVO';
            case 4:
                return 'REQUISITOS PARA LA RENOVACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS';
            case 5:
            case 6:
            case 7:
                return 'REQUISITOS PARA LA ACTUALIZACIÓN DEL RECONOCIMIENTO DEPORTIVO';
            case 8:
            case 9:
            case 10:
                return 'REQUISITOS PARA LA ACTUALIZACIÒN DEL RECONOCIMIENTO DEPORTIVO A ENTIDADES NO DEPORTIVAS';
            case 11:
                return 'REQUISITOS PARA ACTUALIZACIÓN EXPEDIENTE';
            case 12:
                return 'SUBSANE';
            case 13:
                return 'DERECHO DE PETICIÓN';
            case 14:
                return 'SOLICITUD DE INFORMACIÓN';
            case 15:
                return 'EXPEDICIÓN DE COPIAS O SCANNER';
            case 16:
                return 'CERTIFICACIONES';
            case 17:
                return 'DESISTIMIENTO';
            case 18:
                return 'RECURSO DE REPOSICIÓN';
            default:
                return 'NINGUNO';
        }
    }

    public function sports(): array
    {
        $sports = [];
        if (isset($this->id)) {
            $sports = SportClub::query()
                ->where('club_id', $this->id)
                ->latest()
                ->get()
                ->pluck('sport_id')
                ->toArray();

            if (count($sports) > 0) {
                $sports = Sport::query()->whereKey($sports)->get()->map(function($sport) {
                    return [
                        'id'   => (int) $sport->PK_I_ID_DEPORTE,
                        'name'  => toUpper($sport->V_NOMBRE_DEPORTE)
                    ];
                })->toArray();
            }
        }

        return !empty($sports) ? $sports : [];
    }
}
