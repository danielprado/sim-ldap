<?php

namespace App\Http\Resources\Payments;

use App\Http\Resources\Parks\SyntheticParkResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $date = isset($this->fecha) ? $this->fecha->format('Y-m-d') : null;
        $start_hour = $this->hora_inicio ?? null;
        $final_hour = $this->hora_fin ?? null;

        return [
            'id'        => isset($this->id) ? (int) $this->id : null,
            'date'      => $date,
            'start_hour'=>  $start_hour,
            'final_hour' =>  $final_hour,
            'start'     => $start_hour ? toUpper("$date $start_hour") : $date,
            'end'       => $final_hour ? toUpper("$date $final_hour") : $date,
            'hour_range'=> "$start_hour - $final_hour",
            'endowment_id' => isset($this->id_dotacion) ? (int) $this->id_dotacion : null,
            'name'      => toUpper($this->endowment->Descripcion ?? null),
            'park'   => toUpper($this->endowment->park->Nombre ?? null),
            'address'   => toUpper($this->endowment->park->Direccion ?? null),
            'sent'      => $this->enviado ?? null,
            'total'     => isset($this->valor) ? (int) $this->valor : null,
            'total_format'  => cop_money_format($this->valor ?? null),
        ];
    }

    public static function headers() {
        return [
            'headers'   => [
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "#",
                    'value'  =>  "id",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-calendar',
                    'align' => "left",
                    'text' => "Fecha",
                    'value'  =>  "date",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-clock',
                    'align' => "left",
                    'text' => "Hora",
                    'value'  =>  "hour_range",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => "Reserva",
                    'value'  =>  "name",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => "Parque",
                    'value'  =>  "park",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => "Dirección",
                    'value'  =>  "address",
                    'sortable' => false
                ],
            ]
        ];
    }
}
