<?php

namespace App\Http\Resources\Payments;

use App\Http\Resources\Parks\SyntheticParkResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingViewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => isset($this->id) ? (int) $this->id : null,
            'date'      => isset($this->date) ? $this->date->format('Y-m-d') : null,
            'start_hour' => $this->start_hour ?? null,
            'final_hour' => $this->final_hour ?? null,
            'start'  => isset($this->start) ? $this->start->format('Y-m-d H:i:s') : null,
            'end'  => isset($this->end) ? $this->end->format('Y-m-d H:i:s') : null,
            'hour_range' => $this->hour_range ?? null,
            'name' => toUpper($this->name ?? null),
            'park' => toUpper($this->park ?? null),
            'address' => toUpper($this->address ?? null),
            'stratum' => isset($this->stratum) ? (int) $this->stratum : null,
            'longitude' => $this->longitude ?? null,
            'latitude' => $this->latitude ?? null,
            'endowment_id' => isset($this->endowment_id) ? (int) $this->endowment_id : null,
            'endowment_type' => toUpper($this->endowment_type ?? null),
            'equipment' => toUpper($this->equipment ?? null),
            'sent' => $this->sent ?? null,
            'total' => isset($this->total) ? (float) $this->total : null,
            'pse_park_id' => isset($this->pse_park_id) ? (int) $this->pse_park_id : null,
            'pse_id' => isset($this->pse_id) ? (int) $this->pse_id : null,
            'service_id' => isset($this->service_id) ? (int) $this->service_id : null,
            'service_name' => toUpper($this->service_name ?? null),
            'service_code' => $this->service_code ?? null,
            'document' => $this->document ?? null,
            'document_type' => $this->document_type ?? null,
            'payment_code' => $this->payment_code ?? null,
            'pse_transaction_id' => $this->pse_transaction_id ?? null,
            'payer_email' => $this->payer_email ?? null,
            'payer_name' => $this->payer_name ?? null,
            'payer_surname' => $this->payer_surname ?? null,
            'payer_phone' => $this->payer_phone ?? null,
            'status_id' => isset($this->status_id) ? (int) $this->status_id : null,
            'status_bank' => $this->status_bank ?? null,
            'status' => toUpper($this->status ?? null),
            'concept' => toUpper($this->concept ?? null),
            'currency' => $this->currency ?? null,
            'permission' => $this->permission ?? null,
            'permission_type' => $this->permission_type ?? null,
            'tax' => isset($this->tax) ? (float) $this->tax : null,
            'payer_total' => isset($this->payer_total) ? (float) $this->payer_total : null,
            'pse_user_id' => $this->pse_user_id ?? null,
            'created_at' => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'total_format'  => cop_money_format($this->payer_total ?? null),
        ];
    }

    public static function headers() {
        return [
            'headers'   => [
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => "#",
                    'value'  =>  "id",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-calendar',
                    'align' => "left",
                    'text' => "Fecha",
                    'value'  =>  "date",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-clock',
                    'align' => "left",
                    'text' => "Hora",
                    'value'  =>  "hour_range",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => "Reserva",
                    'value'  =>  "name",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => "Parque",
                    'value'  =>  "park",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => "Dirección",
                    'value'  =>  "address",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-currency-usd',
                    'align' => "left",
                    'text' => "Comprobante de pago",
                    'value'  =>  "payment_code",
                    'sortable' => false
                ],
            ]
        ];
    }
}
