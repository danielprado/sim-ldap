<?php

namespace App\Http\Resources\SwimmingPool;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SwimmingPool\LevelLaneResource;
use App\Http\Resources\SwimmingPool\DirectionLaneResource;

class LaneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'quota' => $this->cupo,
            'name' => $this->carril,
            'id' => $this->id_carril,
            'enable' => $this->habilitado,
            'pool_id' => $this->id_piscina,
            'level' => new LevelLaneResource($this->level),
            'direction' => new DirectionLaneResource($this->direction),
            'pool' => $this->swimPool,
        ];
    }
}
