<?php

namespace App\Http\Resources\SwimmingPool;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Resources\Json\JsonResource;

class DirectionLaneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id_sentido_carril,
            'name' => $this->sentido_carril,
            'description' => $this->descripcion
        ];
    }
}
