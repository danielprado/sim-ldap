<?php

namespace App\Http\Resources\SocialManagement;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Resources\Json\JsonResource;

class ProgramResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        =>  isset( $this->id ) ? (int) $this->id : null,
            'title'     =>  isset( $this->title ) ? $this->title : null,
            'date'      =>  isset( $this->execution_date ) ? $this->execution_date->format('Y-m-d') : null,
            'place'     =>  isset( $this->place ) ? $this->place : null,
            'professional'     =>  isset( $this->full_name ) ? $this->full_name : null,
            'process'     =>  isset( $this->process ) ? $this->process : null,
            'reunion_type'     =>  isset( $this->reunion_type ) ? $this->reunion_type : null,
            'who_summons'     =>  isset( $this->who_summons ) ? $this->who_summons : null,
            'objective'     =>  isset( $this->objective ) ? $this->objective : null,
            'activities'    =>  isset( $this->programming_activities ) ? $this->programming_activities->implode('activity', ', ') : null,
            'files'         =>  isset( $this->programming_files ) ? $this->setFiles( $this->programming_files ) : [],
            'images'         =>  isset( $this->programming_images ) ? $this->setImages( $this->programming_images ) : [],
        ];
    }

    public function setFiles($files = null)
    {
        $files_data = [];
        if ($files) {
            foreach ($files as $file) {
                $path = 'https://sim1.idrd.gov.co/SIM/gestion-social-v3/public/storage/files/';
                $url = isset( $file->pivot->path ) ? $path.$file->pivot->path : null;
                if ( $url && $this->urlExists($url) ) {
                    $files_data[] = [
                        'type'  =>  isset($file->file_type) ? $file->file_type : null,
                        'file'  =>  $url
                    ];
                }
            }
        }
        return $files_data;
    }

    public function setImages($files = null)
    {
        $files_data = [];
        if ($files) {
            foreach ($files as $file) {
                $path = 'https://sim1.idrd.gov.co/SIM/gestion-social-v3/public/storage/images/';
                $url = isset( $file->path ) ? $path.$file->path : null;
                if ( $url && $this->urlExists($url) ) {
                    $files_data[] = $url;
                }
            }
        }
        return $files_data;
    }

    function urlExists($url = null)
    {
        try {
            if ($url == null) {
                return false;
            }
            $client = new Client();
            $data = $client->head( $url );
            $status = $data->getStatusCode();
            return $status >= 200 && $status < 300;
        } catch (ClientException $e) {
            return false;
        }
    }
}
