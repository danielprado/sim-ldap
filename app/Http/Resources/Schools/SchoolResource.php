<?php

namespace App\Http\Resources\Schools;

use App\Entities\Clubes\Sport;
use App\Entities\Schools\SportSchool;
use App\Entities\Security\DocumentType;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class SchoolResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => isset($this->id) ? (int) $this->id : null,
            'school'            => $this->school ?? null,
            'email'             => isset($this->email) ? toLower($this->email) : null,
            'director'          => isset($this->director) ? toUpper($this->director) : null,
            'phone'             => $this->phone ?? null,
            'school_type'       => $this->school_type ?? null,
            'request_type'      => isset($this->request_type) ? $this->requestType($this->request_type) : null,
            'accept_media'      => $this->accept_media ?? null,
            'status'            => $this->status ?? null,
            'response'          => $this->response ?? null,
            'sports'            => $this->sports(),
        ];
    }

    public function requestType($request): string
    {
        switch ($request) {
            case 1:
                return 'REQUISITOS PARA EL OTORGAMIENTO DE AVAL PARA LAS ESCUELAS DE FORMACIÓN DEPORTIVA';
            case 2:
                return 'REQUISITOS PARA LA RENOVACIÓN DE AVAL PARA LAS ESCUELAS DE FORMACIÓN DEPORTIVA';
            case 3:
                return 'REQUISITOS PARA LA INCLUSIÓN DE NUEVOS DEPORTES EN LAS ESCUELAS DE FORMACIÓN';
            case 4:
                return 'REQUISITOS PARA ACTUALIZACIÓN EXPEDIENTE';
            case 5:
                return 'SUBSANE';
            case 6:
                return 'DERECHO DE PETICIÓN';
            case 7:
                return 'SOLICITUD DE INFORMACIÓN';
            case 8:
                return 'EXPEDICIÓN DE COPIAS O SCANNER';
            case 9:
                return 'DESISTIMIENTO';
            case 10:
                return 'RECURSO DE REPOSICIÓN';
            default:
                return 'NINGUNO';
        }
    }

    public function sports(): array
    {
        $sports = [];
        if (isset($this->id)) {
            $sports = SportSchool::query()
                ->where('school_id', $this->id)
                ->latest()
                ->get()
                ->pluck('sport_id')
                ->toArray();

            if (count($sports) > 0) {
                $sports = Sport::query()->whereKey($sports)->get()->map(function($sport) {
                    return [
                        'id'   => (int) $sport->PK_I_ID_DEPORTE,
                        'name'  => toUpper($sport->V_NOMBRE_DEPORTE)
                    ];
                })->toArray();
            }
        }

        return !empty($sports) ? $sports : [];
    }
}
