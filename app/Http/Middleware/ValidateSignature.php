<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Routing\Exceptions\InvalidSignatureException;

class ValidateSignature extends \Illuminate\Routing\Middleware\ValidateSignature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\JsonResponse|Response
     *
     * @throws \Illuminate\Routing\Exceptions\InvalidSignatureException
     */
    public function handle($request, Closure $next)
    {
        if ($request->hasValidSignature()) {
            return $next($request);
        }

        if ($request->expectsJson()) {
            return response()->json([
                'message' =>  __('validation.handler.invalid_signature'),
                'details' => $request,
                'code'  =>  Response::HTTP_UNPROCESSABLE_ENTITY,
                'requested_at'  =>  now()->toIso8601String()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        throw new InvalidSignatureException;
    }
}
