<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return $request->wantsJson()
             ? response()->json([
                'message' =>  __('validation.handler.unauthorized'),
                'details' => $request,
                'code'  =>  Response::HTTP_FORBIDDEN,
                'requested_at'  =>  now()->toIso8601String()
            ], Response::HTTP_FORBIDDEN)
            : redirect('/home');
        }

        return $next($request);
    }
}
