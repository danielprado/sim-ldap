<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Response;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        } else {
            return response()->json([
                'message' =>  __('validation.handler.unauthenticated'),
                'details' => $request,
                'code'  =>  Response::HTTP_UNAUTHORIZED,
                'requested_at'  =>  now()->toIso8601String()
            ], Response::HTTP_UNAUTHORIZED);
        }
    }
}
