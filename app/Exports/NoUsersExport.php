<?php

namespace App\Exports;


use App\Entities\CitizenPortal\ProfileView;
use App\Entities\Payments\Payment;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class NoUsersExport implements FromQuery, WithMapping, WithHeadings, WithColumnFormatting, WithTitle
{
    public function query()
    {
        return ProfileView::query()
            ->withCount(["user_schedules", "files"]);
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'B' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_NUMBER,
            'I' => NumberFormat::FORMAT_NUMBER,
            'J' => NumberFormat::FORMAT_NUMBER,
            'K' => NumberFormat::FORMAT_NUMBER,
            'L' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
        ];
    }

    public function headings(): array
    {
        return [
            "ID",
            "USUARIO RELACIONADO",
            "EMAIL REGISTRADO",
            "ESTADO",
            "TIPO PERFIL",
            "DOCUMENTO",
            "NOMBRE",
            "ACTIVIDADES INSCRITAS",
            "PAGOS REALIZADOS",
            "ARCHIVOS ALMACENADOS",
            "PORCENTAJE DE DATOS",
            "FECHA DE REGISTRO",
        ];
    }

    public function map($row): array
    {
        return [
            'id'    =>  isset($row->id) ? (int) $row->id : null,
            'user_id'    =>  isset($row->user_id) ? (int) $row->user_id : null,
            'email'    =>  isset($row->email) ? (string) $row->email : null,
            'status'      =>  isset($row->status) ? (string) $row->status : 'PENDIENTE',
            'profile_type'      =>  isset($row->profile_type) ? (string) $row->profile_type : null,
            'document'      =>  isset($row->document) ? (string) $row->document : null,
            'full_name'      =>  isset($row->full_name) ? (string) $row->full_name : null,
            'user_schedules_count'    =>  isset($row->user_schedules_count) ? (int) $row->user_schedules_count : 0,
            'files_count'    =>  isset($row->files_count) ? (int) $row->files_count : 0,
            'payments_count'    => isset($row->document) ? Payment::query()
                ->where("identificacion", $row->document)
                ->orWhere("identificacion_pagador", $row->document)->count() : 0,
            "profile_completion" => isset($row->profile_completion) ? (int) $row->profile_completion : 0,
            'created_at'    =>  isset($row->created_at) ? date_time_to_excel($row->created_at) : null,
        ];
    }

    public function title(): string
    {
        return "PERFILES SIN USO";
    }
}
