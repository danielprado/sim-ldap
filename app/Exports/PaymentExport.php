<?php

namespace App\Exports;

use App\Entities\Payments\Payment;
use App\Helpers\ExcelEvents;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PaymentExport implements FromQuery, WithMapping, WithHeadings, WithColumnFormatting, WithEvents, WithTitle
{

    /**
     * @var Request
     */
    private $request;

    /**
     * Excel constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    /**
     * @return Builder
     */
    public function query()
    {
        return Payment::query()
            ->when($this->request->has('query'), function ($query) {
                return $query->search($this->request->get('query'));
            })
            ->whereIn(
                'identificacion',
                auth('api')->user()->profiles->pluck('document')->toArray()
            )
            ->orderByDesc('fecha_pago');
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'J' => NumberFormat::FORMAT_NUMBER,
            'O' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'P' => NumberFormat::FORMAT_PERCENTAGE_00,
            'U' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                ExcelEvents::header($event, "MIS PAGOS", "U", 5);
            }
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            "PARQUE",
            "CÓDIGO PARQUE",
            "SERVICIO",
            "DOCUMENTO",
            "CÓDIGO PAGO",
            "TRANSACCIÓN PSE",
            "EMAIL",
            "NOMBRES",
            "APELLIDOS",
            "TELÉFONO",
            "ESTADO",
            "ESTADO DEL BANCO",
            "CONCEPTO",
            "MONEDA",
            "TOTAL",
            "IVA",
            "PERMISO",
            "TIPO DE PERMISO",
            "ID DE RESERVA",
            "MÉTODO",
            "FECHA DE PAGO",
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            'park'                  => $row->park->nombre_parque ?? null,
            'park_code'             => $row->park->codigo_parque ?? null,
            'service'               => toUpper($row->service->servicio_nombre ?? null),
            'document'              => $row->identificacion ?? null,
            'payment_code'          => $row->codigo_pago ?? null,
            'pse_transaction_id'    => $row->id_transaccion_pse ?? null,
            'email'                 => toLower($row->email ?? null),
            'name'                  => toUpper($row->nombre ?? null),
            'lastname'              => toUpper($row->apellido ?? null),
            'phone'                 => $row->telefono ?? null,
            'status'                => toUpper($row->status->descripcion ?? null),
            'status_bank'           => toUpper($row->estado_banco ?? null),
            'concept'               => toUpper($row->concepto ?? null),
            'money'                 => $row->moneda ?? null,
            'total'                 => $row->total ?? null,
            'tax'                   => $row->iva ?? null,
            'permission'            => $row->permiso ?? null,
            'permission_type'       => $row->tipo_permiso ?? null,
            'booking_id'            => $row->id_reserva ?? null,
            'method'                => toUpper($row->method->Nombre ?? null),
            'payment_at'            => date_time_to_excel($row->fecha_pago ?? null),
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return "MIS PAGOS";
    }
}
