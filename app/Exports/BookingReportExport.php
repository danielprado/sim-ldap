<?php

namespace App\Exports;

use App\Entities\Payments\BookingReportCEFESView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Events\AfterSheet;

class BookingReportExport implements FromCollection, WithHeadings, WithColumnFormatting, WithEvents
{
    /**
     * Export the collection of data.
     *
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return BookingReportCEFESView::query()
            ->select([
                'booking_date',
                'day_of_week',
                'week_of_year',
                'park',
                'service',
                'booking_total',
                'booking_amount_total',
                'booking_amount_free',
            ])
            ->get();
    }

    /**
     * Define the headings for the Excel file.
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'FECHA RESERVA',
            'DÍA DE LA SEMANA',
            'SEMANA DEL AÑO',
            'PARQUE',
            'SERVICIO',
            'TOTAL RESERVAS',
            'TOTAL RECAUDO',
            'RESERVAS GRATUITAS',
        ];
    }

    /**
     * Define column formatting.
     *
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER,              // SEMANA DEL AÑO como número
            'F' => NumberFormat::FORMAT_NUMBER,              // TOTAL RESERVAS como número
            'G' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE, // TOTAL RECAUDO como moneda
            'H' => NumberFormat::FORMAT_NUMBER,              // RESERVAS GRATUITAS como número
        ];
    }

    /**
     * Apply styles to the worksheet after it is created.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();

                // Estilo para los encabezados
                $sheet->getStyle('A1:H1')->applyFromArray([
                    'font' => ['bold' => true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => ['argb' => 'FFE5E5E5'], // Color de fondo gris claro
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);

                // Ajuste automático del ancho de las columnas
                foreach (range('A', 'H') as $column) {
                    $sheet->getColumnDimension($column)->setAutoSize(true);
                }
            },
        ];
    }
}
