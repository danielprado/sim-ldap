<?php

namespace App\Exports;

use App\Entities\CitizenPortal\Profile;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;

class ProfilesReportExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting
{
    protected $exportAll;
    protected $startDate;
    protected $endDate;

    /**
     * Constructor para recibir parámetros de filtro.
     */
    public function __construct($startDate = null, $endDate = null)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * Obtener los datos de los perfiles con filtros dinámicos.
     */
    public function collection()
    {
        $startDate = Carbon::parse($this->startDate)->startOfDay();
        $endDate = Carbon::parse($this->endDate)->endOfDay();

        $query = Profile::with(['user', 'profile_type', 'status', 'viewer', 'sex_name'])
            ->whereHas('user', function ($q) {
                $q->where('activated', 1);
            })
            ->whereNotNull('document')
            ->whereBetween('created_at', [$startDate, $endDate]); // Se usa created_at de profiles

        return $query->get();
    }

    /**
     * Definir los encabezados del archivo Excel.
     */
    public function headings(): array
    {
        return [
            'DOCUMENTO', 'NOMBRES', 'SEGUNDO_NOMBRE', 'APELLIDO', 'SEGUNDO_APELLIDO',
            'CORREO', 'FECHA_NACIMIENTO', 'DISCAPACIDAD', 'DIRECCION', 'ESTRATO',
            'TELEFONO', 'FECHA_CREACION_USUARIO', 'AÑO_CREACION_USUARIO', 'SEMANA_ANIO',
            'FECHA_CREACION', 'FECHA_VERIFICACION', 'PERFIL', 'ESTADO', 'GENERO', 'NOMBRE VERIFICADOR', 'APELLIDO VERIFICADOR'
        ];
    }

    /**
     * Mapear los datos de los perfiles a las columnas del reporte.
     */
    public function map($profile): array
    {
        return [
            $profile->document,
            $profile->name,
            $profile->s_name,
            $profile->surname,
            $profile->s_surname,
            $profile->user->email ?? '',
            $profile->birthdate,
            $profile->has_disability,
            $profile->address,
            $profile->stratum,
            $profile->mobile_phone,
            optional($profile->created_at)->format('Y-m-d H:i:s'),
            optional($profile->created_at)->format('Y'),
            optional($profile->created_at)->format('W'), // Semana del año
            optional($profile->created_at)->format('Y-m-d'), // Fecha sin hora
            optional($profile->verified_at)->format('Y-m-d H:i:s'), // Fecha de verificación
            $profile->profile_type->name ?? '',
            $profile->status->name ?? '',
            $profile->sex_name->Nombre_Genero ?? 'Desconocido',
            $profile->viewer->name ?? 'Sin Asignar',
            $profile->viewer->surname ?? 'Sin Asignar'
        ];
    }

    /**
     * Formato de columnas.
     */
    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT, // Documento como texto
            'G' => NumberFormat::FORMAT_DATE_YYYYMMDD, // Fecha de nacimiento
            'L' => NumberFormat::FORMAT_DATE_DATETIME, // Fecha de creación usuario
            'M' => NumberFormat::FORMAT_NUMBER, // Año de creación usuario
            'N' => NumberFormat::FORMAT_NUMBER, // Semana del año
            'O' => NumberFormat::FORMAT_DATE_DATETIME, // Fecha de verificación
        ];
    }
}
