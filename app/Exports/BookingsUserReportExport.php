<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Entities\Payments\Payment;
use Illuminate\Support\Facades\DB;
use App\Entities\CitizenPortal\Profile;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Events\AfterSheet;

class BookingsUserReportExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting, WithEvents
{
    protected $startDate;
    protected $endDate;
    protected $cefes;

    public function __construct($startDate, $endDate, $cefes)
    {
        $this->startDate = Carbon::parse($startDate)->startOfDay();
        $this->endDate = Carbon::parse($endDate)->endOfDay();
        $this->cefes = $cefes;
    }

    public function collection()
    {
        $startDate = $this->startDate->toDateTimeString();
        $endDate = $this->endDate->toDateTimeString();

        // Obtener los pagos con relaciones y aplicar condiciones de servicio_id y id_dotacion
        $payments = Payment::with([
            'park',
            'service',
            'booking'
        ])
            ->whereHas('booking', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('fecha', [$startDate, $endDate])
                    ->where(function ($query) {
                        if ($this->cefes) {
                            $query->where(function ($q) {
                                $q->where('servicio_id', 8)
                                    ->whereIn('id_dotacion', [16893, 17256, 17082, 18181]);
                            })->orWhere(function ($q) {
                                $q->where('servicio_id', 15)
                                    ->whereIn('id_dotacion', [17598, 15227, 17167, 18178]);
                            });
                        }
                    });
            })
            ->where('estado_id', 2) // Solo pagos confirmados
            ->get();

        // Obtener documentos únicos
        $documents = $payments->pluck('identificacion')->unique();

        // Obtener perfiles de usuarios indexados por documento
        $profiles = Profile::whereIn('document', $documents)
            ->with([
                'sex_name' => function ($query) {
                    $query->select('Id_Genero', 'Nombre_Genero');
                }
            ])
            ->select('document', 'name', 'surname', 'mobile_phone', 'birthdate', 'address', 'has_disability', 'profile_type_id', 'sex')
            ->get()
            ->keyBy('document');

        return $payments->map(function ($payment) use ($profiles) {
            // Manejo de reservas sin perfil
            $profile = $profiles->get($payment->identificacion) ?? (object) [
                'name' => 'NO REGISTRADO',
                'surname' => '',
                'mobile_phone' => 'NO REGISTRADO',
                'birthdate' => null,
                'has_disability' => 'NO',
                'profile_type_id' => 0,
                'sex' => 'Desconocido',
                'sex_name' => (object) ['Nombre_Genero' => 'Desconocido'],
                'address' => 'NO REGISTRADO',
            ];

            $birthdate = optional($profile)->birthdate;
            $edad = (!empty($birthdate) && strtotime($birthdate))
                ? Carbon::parse($birthdate)->age
                : 'NO REGISTRADO';
            $exonerado = $profile->has_disability === "SI" ? "DISCAPACIDAD" : ($edad !== 'NO REGISTRADO' && $edad >= 60 ? "ADULTO MAYOR" : "NO APLICA");
            $dia_semana = optional($payment->booking)->fecha
                ? Carbon::parse($payment->booking->fecha)->isoFormat('dddd')
                : 'No registrado';

            $servicio = strtoupper(optional($payment->service)->servicio_nombre ?? 'DESCONOCIDO');
            if ($servicio === 'MODULOS') {
                $servicio = 'GIMNASIO';
            }

            $parque = strtoupper(optional($payment->park)->nombre_parque ?? 'NO ASOCIADO');
            $genero = $profile->sex_name->Nombre_Genero ?? 'Desconocido';

            $hora_inicio = optional($payment->booking)->hora_inicio;
            $hora_fin = optional($payment->booking)->hora_fin;

            $tipo_usuario = 'NO REGISTRA';
            if ($profile->profile_type_id == Profile::PROFILE_PERSONAL) {
                $tipo_usuario = 'PRINCIPAL';
            } elseif ($profile->profile_type_id == Profile::PROFILE_BENEFICIARY) {
                $tipo_usuario = 'BENEFICIARIO';
            }


            return [
                'NOMBRE' => "{$payment->nombre} {$payment->apellido}",
                'CORREO' => $payment->email,
                'TELÉFONO' => $payment->telefono ?? "NO REGISTRADO",
                'CÉDULA' => $payment->identificacion ?? "NO REGISTRADO",
                'EDAD' => $edad,
                'FECHA DE NACIMIENTO' => $profile->birthdate ?? "NO REGISTRADO",
                'DIRECCIÓN' => $profile->address ?? "NO REGISTRADO",
                'EXONERADO' => $exonerado,
                'TIPO DE USUARIO' => $tipo_usuario,
                'GÉNERO' => $genero,
                'SERVICIO' => $servicio,
                'PARQUE' => $parque,
                'VALOR SERVICIO' => $payment->total ?? "0",
                'FECHA RESERVA' => optional($payment->booking)->fecha ?? 'NO REGISTRADO',
                'HORA INICIO' => $hora_inicio,
                'HORA FIN' => $hora_fin,
                'FECHA PAGO' => optional($payment->created_at)->format('Y-m-d H:i:s') ?? 'NO REGISTRADO',
                'DÍA SEMANA USO' => strtoupper($dia_semana),
                'COMPROBANTE DE PAGO' => $payment->codigo_pago ?? "NO DISPONIBLE",
                'ID_TRANSACCION_PSE' => $payment->id_transaccion_pse ?? "NO DISPONIBLE",
            ];
        })->filter();
    }

    public function headings(): array
    {
        return [
            'NOMBRE',
            'CORREO',
            'TELÉFONO',
            'CÉDULA',
            'EDAD',
            'FECHA DE NACIMIENTO',
            'DIRECCIÓN',
            'EXONERADO',
            'TIPO DE USUARIO',
            'GÉNERO',
            'SERVICIO',
            'PARQUE',
            'VALOR SERVICIO',
            'FECHA RESERVA',
            'HORA INICIO',
            'HORA FIN',
            'FECHA PAGO',
            'DÍA SEMANA USO',
            'COMPROBANTE DE PAGO',
            'ID_TRANSACCION_PSE'
        ];
    }

    public function map($row): array
    {
        return array_values($row);
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_TEXT, // Cédula como texto
            'F' => NumberFormat::FORMAT_DATE_YYYYMMDD, // Fecha de nacimiento
            'M' => NumberFormat::FORMAT_CURRENCY_USD_SIMPLE, // Valor servicio (puede ser en formato moneda)
            'N' => NumberFormat::FORMAT_DATE_YYYYMMDD, // Fecha de reserva
            'O' => NumberFormat::FORMAT_DATE_TIME4, // Hora inicio (hh:mm)
            'P' => NumberFormat::FORMAT_DATE_TIME4, // Hora fin (hh:mm)
            'Q' => NumberFormat::FORMAT_DATE_DATETIME, // Fecha de pago con hora
        ];
    }

     /**
     * Apply styles to the worksheet after it is created.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();

                // Estilo para los encabezados
                $sheet->getStyle('A1:U1')->applyFromArray([
                    'font' => ['bold' => true],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => ['argb' => 'FFE5E5E5'], // Color de fondo gris claro
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);

                // Ajuste automático del ancho de las columnas
                foreach (range('A', 'U') as $column) {
                    $sheet->getColumnDimension($column)->setAutoSize(true);
                }
            },
        ];
    }
}
