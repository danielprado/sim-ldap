<?php

namespace App\Notifications;

use App\Channels\SendGridChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class SendProfilesReportNotification extends Notification
{
    /**
     * The path to the report file.
     *
     * @var string
     */
    protected $filePath;

    /**
     * The file name of the report.
     *
     * @var string
     */
    protected $fileName;

    /**
     * Create a new notification instance.
     *
     * @param string $filePath
     * @param string $fileName
     */
    public function __construct($filePath, $fileName)
    {
        $this->filePath = $filePath;
        $this->fileName = $fileName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SendGridChannel::class];
    }

    /**
     * Get the file path.
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Get the file name.
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Reporte de Perfiles - ' . now()->format('d/m/Y'))
            ->greeting('Hola,')
            ->line('Adjunto encontrarás el reporte de perfiles de portal ciudadano.')
            ->line('Por favor revisa el archivo y contáctanos si tienes preguntas.')
            ->attach($this->filePath, [
                'as' => $this->fileName,
                'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ])
            ->line('Saludos,')
            ->salutation('Portal Ciudadano IDRD');
    }
}
