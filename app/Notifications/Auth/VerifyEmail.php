<?php

namespace App\Notifications\Auth;

use App\Channels\SendGridChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

class VerifyEmail extends Notification
{
    use Queueable;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SendGridChannel::class];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $verificationUrl = $this->verificationUrl($notifiable).'/usuarios/registrarme';
        $digits = 6;
        $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $notifiable->code = $code;
        $notifiable->save();

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
        }

        return (new MailMessage)
            ->subject(Lang::getFromJson('Verify Email Address'))
            ->greeting('¡Hola!')
            ->line("Recientemente agregaste $notifiable->email como la dirección de correo electrónico principal de comunicación para el Portal Ciudadano - IDRD.")
            ->line("Para confirmar que esta dirección de correo electrónico le pertenece, accede a la pestaña 'REGISTRARME' y seleccione el botón “Ya tengo un código, continuar con mi registro”. Luego, ingrese el siguiente número en la página de verificación de correo electrónico:")
            ->line("`$code`")
            ->line("# ¿Por qué recibiste este correo electrónico?")
            ->line("El IDRD requiere verificar cualquier dirección de correo electrónico que se agregue al Portal Ciudadano.")
            ->line("Tu cuenta no podrá ser utilizada hasta que la verifiques.")
            ->action('Ir al Portal Ciudadano', $verificationUrl)
            ->line("Si no realizaste ningún registro no se require ninguna acción")
            ->salutation(Lang::getFromJson('Citizen Portal - IDRD'));
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        return Config::get('app.frontend_url');
        /*
        return URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            ['id' => $notifiable->getKey()],
            false
        );
        */
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
