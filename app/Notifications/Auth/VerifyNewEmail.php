<?php

namespace App\Notifications\Auth;

use App\Channels\SendGridChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

class VerifyNewEmail extends Notification
{
    use Queueable;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SendGridChannel::class];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $verificationUrl = $this->verificationUrl($notifiable);
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
        }

        return (new MailMessage)
            ->subject(Lang::getFromJson('Verify Email Address'))
            ->greeting('¡Hola!')
            ->line("Recientemente agregaste $notifiable->email como la dirección de correo electrónico principal de comunicación para el Portal Ciudadano - IDRD.")
            ->line("Para verificar que esta dirección te pertenece, inicia sesión en el Portal Ciudadano e ingresa el siguiente código en la página de verificación de correo electrónico:")
            ->line("`$notifiable->code`")
            ->line("# ¿Por qué recibiste este correo electrónico?")
            ->line("El IDRD requiere verificar cualquier dirección de correo electrónico que se agregue al Portal Ciudadano.")
            ->line("El cambio de correo no se verá reflejado hasta que verifiques tu correo.")
            ->action('Ir al Portal Ciudadano', $verificationUrl)
            ->line("Si no realizaste ningún cambio no se require ninguna acción")
            ->salutation(Lang::getFromJson('Citizen Portal - IDRD'));
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        return Config::get('app.frontend_url');
        /*
        return URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            ['id' => $notifiable->getKey()],
            false
        );
        */
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
