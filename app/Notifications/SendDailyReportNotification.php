<?php

namespace App\Notifications;

use App\Channels\SendGridChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class SendDailyReportNotification extends Notification
{
    /**
     * The path to the report file.
     *
     * @var string
     */
    protected $filePath;

    /**
     * The file name of the report.
     *
     * @var string
     */
    protected $fileName;

    /**
     * Create a new notification instance.
     *
     * @param string $filePath
     * @param string $fileName
     */
    public function __construct($filePath, $fileName)
    {
        $this->filePath = $filePath;
        $this->fileName = $fileName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SendGridChannel::class];
    }

    /**
     * Get the file path.
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Get the file name.
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Reporte Diario Reservas - PISCINAS Y GIMNASIOS')
            ->greeting('¡Hola!')
            ->line('Adjunto encontrarás el reporte diario de reservas; del último mes a la fecha, para Piscinas y Gimnasios para los CEFES.')
            ->line('Este informe contiene los datos más recientes y está generado automáticamente por el sistema.')
            ->line('Por favor, revisa el archivo adjunto para más detalles.')
            ->attach($this->filePath, [
                'as' => $this->fileName,
                'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ])
            ->line('Gracias por utilizar el sistema del Portal Ciudadano - IDRD.')
            ->salutation('Portal Ciudadano - IDRD');
    }
}
