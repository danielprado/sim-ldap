<?php

namespace App\Entities\Parks;

use App\Entities\Payments\Payment;
use App\Entities\Payments\Schedule;
use App\Helpers\SyntheticFieldActive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class ParkEndowment extends Model
{
    use Searchable;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_parks';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'parquedotacion';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Id_Parque',
        'Id_Dotacion',
        'Num_Dotacion',
        'Estado',
        'Material',
        'Iluminacion',
        'AprovechamientoEconomico',
        'Area',
        'MaterialPiso',
        'Cerramiento',
        'Camerino',
        'Luz',
        'Agua',
        'Gas',
        'Capacidad',
        'Carril',
        'Bano',
        'BateriaSanitaria',
        'Descripcion',
        'Diag_Mantenimiento',
        'Diag_Construcciones',
        'Posicionamiento',
        'Destinacion',
        'Imagen',
        'Fecha',
        'TipoCerramiento',
        'AlturaCerramiento',
        'Largo',
        'Ancho',
        'Cubierto',
        'Dunt',
        'B_Masculino',
        'B_Femenino',
        'B_Discapacitado',
        'C_Vehicular',
        'C_BiciParqueadero',
        'Publico',
        'i_fk_id_sector',
        'mapeo'
    ];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'endowment_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $base = 'https://sim1.idrd.gov.co/SIM/Parques/Foto/';
        $image = $this->Imagen ?? null;
        return [
            'id'    =>  isset( $this->Id ) ? (int) $this->Id : null,
            'endowment'         => isset($this->endowment) ? toUpper($this->endowment->Dotacion) : null,
            'image' =>  $image && verify_url( "{$base}{$image}" ) ? "{$base}{$image}" : null,
            'material'    =>  isset( $this->Material ) ? toUpper($this->Material) : null,
            'floor_material'    =>  isset($this->material) ? toUpper($this->material->Material) : null,
            'equipment'    =>  isset( $this->endowment->equipment ) ? toUpper($this->endowment->equipment->Equipamento) : null,
            'park_id'    =>  isset( $this->Id_Parque ) ? (int) $this->Id_Parque : null,
            'park_code'      =>  isset( $this->park->Id_IDRD ) ? toUpper($this->park->Id_IDRD) : null,
            'park_name'      =>  isset( $this->park->Nombre ) ? toUpper($this->park->Nombre) : null,
            'locality'  =>  isset( $this->park->location->Localidad ) ? toUpper($this->park->location->Localidad) : null,
            'park_address'   =>  isset( $this->park->Direccion ) ? toUpper($this->park->Direccion) : null,
            'upz_code'  =>  isset( $this->park->Upz ) ? (int) $this->park->Upz : null,
            'upz'       =>  isset( $this->park->upz_name->Upz ) ? toUpper($this->park->upz_name->Upz) : null,
            'scale'     =>  isset( $this->park->scale->Tipo ) ? toUpper($this->park->scale->Tipo) : null,
        ];
    }

    public function getAllDayAttribute()
    {
        return $this->Id_Dotacion == Endowment::GRILL && $this->Id_Parque == Payment::LA_FLORIDA_ID;
    }

    public function park()
    {
        return $this->belongsTo( Park::class, 'Id_Parque', 'Id' );
    }

    public function material()
    {
        return $this->belongsTo( Material::class, 'MaterialPiso', 'IdMaterial');
    }

    public function endowment()
    {
        return $this->belongsTo(Endowment::class, 'Id_Dotacion');
    }

    public function enclosure()
    {
        return $this->belongsTo(Enclosure::class, 'Cerramiento');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'Estado');
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'Id','id_dotacion');
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeEnabled($query)
    {
        return $query->whereIn('Id', SyntheticFieldActive::fields());
    }
}
