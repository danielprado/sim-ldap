<?php

namespace App\Entities\Parks;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_parks';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sectores';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'i_pk_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'i_fk_id_parque',
        'Sector',
        'coordenada'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function endowments()
    {
        return $this->hasMany(ParkEndowment::class, 'i_fk_id_sector');
    }
}
