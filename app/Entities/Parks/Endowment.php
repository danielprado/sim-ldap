<?php

namespace App\Entities\Parks;

use Illuminate\Database\Eloquent\Model;

class Endowment extends Model
{
    // ID de tipo dotación asignado para asadores
    const GRILL = 116;

    // ID de tipo dotación asignado para canchas de grama natural
    const FIELD_NATURAL = 1;

    // ID de tipo dotación asignado para canchas de grama sintetica
    const FIELD_SYNTHETIC = 1;

    // IDs de tipo dotación asignado para canchas de tenis
    const FIELD_TENNIS = 5;
    const FIELD_TENNIS_MINI = 87;
    // ID PARA GUMNASIOS
    const GYM = 126;
    // ID PARA PISCINAS
    const SWIMMING_POOL = 67;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_parks';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dotacion';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Dotacion';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function parks()
    {
        return $this->belongsToMany(Park::class, 'parquedotacion','Id_Dotacion','Id_Parque')
            ->withPivot('Num_Dotacion', 'Estado', 'Material', 'iluminacion', 'Aprovechamientoeconomico', 'Area', 'MaterialPiso', 'Cerramiento', 'Camerino', 'Luz', 'Agua', 'Gas', 'Capacidad', 'Carril', 'Bano', 'BateriaSanitaria', 'Descripcion', 'Diag_Mantenimiento', 'Diag_Construcciones', 'Posicionamiento', 'Destinacion', 'Imagen', 'Fecha', 'TipoCerramiento', 'AlturaCerramiento', 'Largo', 'Ancho', 'Cubierto', 'Dunt', 'B_Masculino', 'B_Femenino', 'B_Discapacitado', 'C_Vehicular', 'C_BiciParqueadero', 'Publico');
    }

    public function material()
    {
        return $this->hasOne( Material::class, 'MaterialPiso', 'IdMaterial');
    }


    public function equipment()
    {
        return $this->hasOne(Equipment::class, 'Id_Equipamento', 'Id_Equipamento');
    }

}
