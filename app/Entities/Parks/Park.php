<?php

namespace App\Entities\Parks;

use App\Entities\Parks\Location;
use App\Traits\FullTextSearch;
use ArrayAccess;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Laravel\Scout\Searchable;
use OwenIt\Auditing\Contracts\Auditable;

class Park extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, FullTextSearch, SoftDeletes, Searchable;
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_parks';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'parque';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Nombre',
        'Direccion',
        'Id_Localidad',
        'Id_Tipo',
        'CompeteIDRD',
        'Area',
        'Estrato',
        'Mapa',
        'Id_IDRD',
        'Imagen',
        'TelefonoParque',
        'Upz',
        'EstadoCertificado',
        'AreaZDura',
        'AreaZVerde',
        'UbicacionCertificado',
        'FechaVisita',
        'EstadoGeneral',
        'Cerramiento',
        'Viviendas',
        'TipoZona',
        'Administracion',
        'CantidadSenderos',
        'EstadoSendero',
        'ViasAcceso',
        'EstadoVias',
        'PoblacionInfantil',
        'PoblacionJuvenil',
        'PoblacionMayor',
        'NomAdministrador',
        'Estado',
        'Longitud',
        'Latitud',
        'Urbanizacion',
        'Areageo_enHa',
        'Email',
        'Id_Barrio',
        'Vigilancia',
        'Aforo',
        'RecibidoIdrd',
        'Id_Tipo_Escenario',
        'Id_Vocacion',
    ];

    /**
     * The columns of the full text index
     *
     * @var array
     */
    protected $searchable = [
        'Id_IDRD',
        'Nombre',
        'Direccion',
    ];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'parks_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id'        =>  (int) isset( $this->Id ) ? (int) $this->Id : null,
            'code'      =>  isset( $this->Id_IDRD ) ? toUpper($this->Id_IDRD) : null,
            'name'      =>  isset( $this->Nombre ) ? toUpper($this->Nombre) : null,
            'address'   =>  isset( $this->Direccion ) ? toUpper($this->Direccion) : null,
            'locality_id'  =>  isset( $this->Id_Localidad ) ? (int) $this->Id_Localidad : null,
            'locality'  =>  isset( $this->location->Localidad ) ? toUpper($this->location->Localidad) : null,
            'upz_code'  =>  isset( $this->Upz ) ? (int) $this->Upz : null,
            'upz'       =>  isset( $this->upz_name->Upz ) ? toUpper($this->upz_name->Upz) : null,
            'scale_id'  =>  isset( $this->Id_Tipo ) ? (int) $this->Id_Tipo : null,
            'color'     =>  $this->color,
            'scale'     =>  isset( $this->scale->Tipo ) ? toUpper($this->scale->Tipo) : null,
        ];
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['FechaVisita'];

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'Nombre',
        'Direccion',
        'Id_Localidad',
        'Id_Tipo',
        'CompeteIDRD',
        'Area',
        'Estrato',
        'Mapa',
        'Id_IDRD',
        'Imagen',
        'TelefonoParque',
        'Upz',
        'EstadoCertificado',
        'AreaZDura',
        'AreaZVerde',
        'UbicacionCertificado',
        'FechaVisita',
        'EstadoGeneral',
        'Cerramiento',
        'Viviendas',
        'TipoZona',
        'Administracion',
        'CantidadSenderos',
        'EstadoSendero',
        'ViasAcceso',
        'EstadoVias',
        'PoblacionInfantil',
        'PoblacionJuvenil',
        'PoblacionMayor',
        'NomAdministrador',
        'Estado',
        'Longitud',
        'Latitud',
        'Urbanizacion',
        'Areageo_enHa',
        'Email',
        'Id_Barrio',
        'Vigilancia',
        'Aforo',
        'RecibidoIdrd',
        'Id_Tipo_Escenario',
        'Id_Vocacion',
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['park'];
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @return Model|void|null
     */
    public function resolveRouteBinding($value)
    {

        $park = $this->where('Id_IDRD', $value)->first();
        if (!$park) {
            $park = $this->where('Id', $value)->first();
        }
        if (!$park) {
            abort(404, __('validation.handler.park_does_not_exist', ['code' => $value]));
        }

        return $park;
    }

    public function getColorAttribute()
    {
        switch ($this->Id_Tipo) {
            case 1:
            case 2:
            case 3:
                return 'success';
                break;
            default;
                return 'grey';
                break;
        }
    }

    /**
     * @param $column
     * @return string
     */
    public function getSortableColumn($column)
    {
        switch ($column) {
            case $this->primaryKey:
                return $this->primaryKey;
            case 'created_at':
            case 'updated_at':
            case 'deleted_at':
                return $column;
            default:
                return in_array($this->getFillableValues($column), $this->fillable)
                    ? $this->getFillableValues($column)
                    : $this->primaryKey;
        }
    }

    /**
     * @param $value
     * @return array|ArrayAccess|mixed
     */
    public function getFillableValues($value)
    {
        $attrs = [
            'id' => 'Id',
            'code' => 'Id_IDRD',
            'name' => 'Nombre',
            'address'  => 'Direccion',
            'stratum'  => 'Estrato',
            'locality_id'  => 'Id_Localidad',
            'upz_code' => 'Upz',
            'neighborhood_id'  => 'Id_Barrio',
            'urbanization' => 'Urbanizacion',
            'latitude' => 'Latitud',
            'longitude'    => 'Longitud',
            'area_hectare' => 'Areageo_enHa',
            'area' => 'Area',
            'grey_area'    => 'AreaZDura',
            'green_area'   => 'AreaZVerde',
            'capacity' => 'Aforo',
            'children_population'  => 'PoblacionInfantil',
            'youth_population' => 'PoblacionJuvenil',
            'older_population' => 'PoblacionMayor',
            'enclosure'    => 'Cerramiento',
            'households'   => 'Viviendas',
            'walking_trails'   => 'CantidadSenderos',
            'walking_trails_status'    => 'EstadoSendero',
            'access_roads' => 'ViasAcceso',
            'access_roads_status'  => 'EstadoVias',
            'zone_type'    => 'TipoZona',
            'scale_id' => 'Id_Tipo',
            'concern'  => 'CompeteIDRD',
            'visited_at'   => 'FechaVisita',
            'general_status'   => 'EstadoGeneral',
            'stage_type_id'    => 'Id_Tipo_Escenario',
            'status_id'    => 'Estado',
            'admin'    => 'Administracion',
            'phone'    => 'TelefonoParque',
            'email'    => 'Email',
            'admin_name'   => 'NomAdministrador',
            'vigilance'    => 'Vigilancia',
            'received' => 'RecibidoIdrd',
            'vocation_id'  => 'Id_Vocacion',
        ];
        return Arr::get($attrs, $value, $value);
    }

    /*
    * ---------------------------------------------------------
    * Eloquent Relations
    * ---------------------------------------------------------
    */

    /**
     * @return HasOne
     */
    public function location()
    {
        return $this->hasOne(Location::class, 'Id_Localidad', 'Id_Localidad');
    }

    /**
     * @return HasOne
     */
    public function scale()
    {
        return $this->hasOne(Scale::class, 'Id_Tipo', 'Id_Tipo');
    }

    /**
     * @return HasOne
     */
    public function upz_name()
    {
        return $this->hasOne(Upz::class, 'cod_upz', 'Upz');
    }

    /**
     * @return HasOne
     */
    public function certified()
    {
        return $this->hasOne( Certified::class, 'id_EstadoCertificado', 'EstadoCertificado' );
    }

    /**
     * @return HasMany
     */
    public function park_endowment()
    {
        return $this->hasMany( ParkEndowment::class, 'Id_Parque', 'Id' );
    }

    /**
     * @return BelongsToMany
     */
    public function endowments()
    {
        return $this->belongsToMany(Endowment::class, 'parquedotacion', 'Id_Parque', 'Id_Dotacion')
            ->withPivot('Num_Dotacion', 'Estado', 'Material', 'iluminacion', 'Aprovechamientoeconomico', 'Area', 'MaterialPiso', 'Cerramiento', 'Camerino', 'Luz', 'Agua', 'Gas', 'Capacidad', 'Carril', 'Bano', 'BateriaSanitaria', 'Descripcion', 'Diag_Mantenimiento', 'Diag_Construcciones', 'Posicionamiento', 'Destinacion', 'Imagen', 'Fecha', 'TipoCerramiento', 'AlturaCerramiento', 'Largo', 'Ancho', 'Cubierto', 'Dunt', 'B_Masculino', 'B_Femenino', 'B_Discapacitado', 'C_Vehicular', 'C_BiciParqueadero', 'Publico');
    }

    /**
     * @return HasMany
     */
    public function sectors()
    {
        return $this->hasMany(Sector::class, 'i_fk_id_parque');
    }
}
