<?php

namespace App\Entities\Parks;

use App\Entities\Payments\Schedule;
use App\Helpers\SyntheticFieldActive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Scout\Searchable;

class Grill extends Model
{
    use Searchable;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_parks';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'grills_park';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'park_endowment_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'endowment_id',
        'park_id',
        'park_name',
        'park_address',
        'park_code',
        'endowment_description',
        'image',
        'locality_id',
        'locality',
        'upz_id',
        'upz_code',
        'upz',
    ];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'booking_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $base = 'https://sim1.idrd.gov.co/SIM/Parques/Foto/';
        $image = $this->image ?? null;
        return [
            'id' => isset($this->park_endowment_id) ? (int) $this->park_endowment_id : null,
            'endowment_id' => isset($this->endowment_id) ? (int) $this->endowment_id : null,
            'park_id' => isset($this->park_id) ? (int) $this->park_id : null,
            'park_name' =>  toUpper( $this->park_name ?? null ),
            'park_address' => toUpper($this->park_address ?? null),
            'park_code' =>  $this->park_code ?? null,
            'endowment_description' =>  $this->endowment_description ?? null,
            'image' =>  $image && verify_url( "{$base}{$image}" ) ? "{$base}{$image}" : null,
            'locality_id' => isset($this->locality_id) ? (int) $this->locality_id : null,
            'locality' => toUpper($this->locality ?? null),
            'upz_id' => isset($this->upz_id) ? (int) $this->upz_id : null,
            'upz_code' => toUpper($this->upz_code ?? null),
            'upz' => toUpper($this->upz ?? null),
            "tag"   => "grills"
        ];
    }

    /**
     * @return BelongsTo
     */
    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'park_endowment_id','id_dotacion');
    }
}
