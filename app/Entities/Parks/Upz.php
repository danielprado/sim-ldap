<?php

namespace App\Entities\Parks;

use Illuminate\Database\Eloquent\Model;

class Upz extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_parks';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'upz';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Upz';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cod_upz', 'upz'];
}
