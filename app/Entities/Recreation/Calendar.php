<?php

namespace App\Entities\Recreation;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_recreation';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'calendar_view';
}
