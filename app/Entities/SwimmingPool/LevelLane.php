<?php

namespace App\Entities\SwimmingPool;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LevelLane extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $connection = 'mysql_swim_pool';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "nivel_carril";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id_nivel_carril";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nivel_carril',
        'is_active'
    ];

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    // public function sports()
    // {
    //     return $this->belongsToMany(SportSchool::class, 'clubes_sports', 'sport_id', 'club_id');
    // }
}
