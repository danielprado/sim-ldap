<?php

namespace App\Entities\SwimmingPool;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SwimPoolType extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $connection = 'mysql_swim_pool';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "tipo_piscina";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id_tipo_piscina";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_piscina',
        'is_active'
    ];

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    // public function sports()
    // {
    //     return $this->belongsToMany(SportSchool::class, 'clubes_sports', 'sport_id', 'club_id');
    // }
}
