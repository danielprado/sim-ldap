<?php

namespace App\Entities\SwimmingPool;

use App\Entities\Payments\Schedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\SwimmingPool\LevelLane;
use App\Entities\SwimmingPool\DirectionLane;
use  App\Entities\SwimmingPool\SwimPool;

class Lane extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $connection = 'mysql_swim_pool';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "carril";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id_carril";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_piscina',
        'id_nivel_carril',
        'id_sentido_carril',
        'carril',
        'habilitado',
        'cupo',
        'is_active'
    ];

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    public function level()
    {
        return $this->hasOne(LevelLane::class, 'id_nivel_carril', 'id_nivel_carril');
    }

    public function direction()
    {
        return $this->hasOne(DirectionLane::class, 'id_sentido_carril', 'id_sentido_carril');
    }

    public function swimPool()
    {
        return $this->belongsTo(SwimPool::class, 'id_piscina', 'id_piscina')->with('swimPoolClass');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'id_carril', 'id_carril');
    }

}
