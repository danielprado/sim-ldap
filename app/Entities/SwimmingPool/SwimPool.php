<?php

namespace App\Entities\SwimmingPool;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use  App\Entities\SwimmingPool\SwimPoolClass;

class SwimPool extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $connection = 'mysql_swim_pool';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "piscina";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id_piscina";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_tipo_piscina',
        'id_clase_piscina',
        'id_dotacion',
        'is_active'
    ];



    public function swimPoolClass()
    {
        return $this->belongsTo(SwimPoolClass::class, 'id_clase_piscina', 'id_clase_piscina');
    }
    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    // public function sports()
    // {
    //     return $this->belongsToMany(SportSchool::class, 'clubes_sports', 'sport_id', 'club_id');
    // }
}
