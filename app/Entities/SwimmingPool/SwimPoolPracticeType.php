<?php

namespace App\Entities\SwimmingPool;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SwimPoolPracticeType extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $connection = 'mysql_swim_pool';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "piscina_tipo_practica";

    protected $primaryKey = null;
    public $incrementing = false; 
    protected $keyType = 'string';

    protected function getKeyForSaveQuery()
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::getKeyForSaveQuery();
        }

        $query = [];
        foreach ($keys as $key) {
            $query[$key] = $this->getAttribute($key);
        }

        return $query;
    }

    public function getKeyName()
    {
        return ['id_piscina', 'id_tipo_practica'];
    }
}
