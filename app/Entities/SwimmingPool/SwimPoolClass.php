<?php

namespace App\Entities\SwimmingPool;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use  App\Entities\SwimmingPool\SwimPool;

class SwimPoolClass extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $connection = 'mysql_swim_pool';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "clase_piscina";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id_clase_piscina";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clase_piscina',
        'is_active'
    ];


    public function swimPools()
    {
        return $this->hasMany(SwimPool::class, 'id_clase_piscina', 'id_clase_piscina');
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    // public function sports()
    // {
    //     return $this->belongsToMany(SportSchool::class, 'clubes_sports', 'sport_id', 'club_id');
    // }
}
