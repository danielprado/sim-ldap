<?php

namespace App\Entities\Clubes;

use Illuminate\Database\Eloquent\Model;

class SportClub extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "clubes_sports";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'club_id',
        'sport_id',
    ];
}
