<?php

namespace App\Entities\Clubes;

use App\Entities\Schools\SportSchool;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "clubes";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'club',
        'email',
        'president',
        'phone',
        'request_type',
        'club_type',
        'accept_media',
        "status",
        "response",
        "file_1_1",
        "file_1_2",
        "file_1_3",
        "file_1_4",
        "file_1_5",
        "file_1_6",
        "file_1_7",
        "file_1_8",
        "file_1_9",
        "file_2_1",
        "file_2_2",
        "file_2_3",
        "file_2_4",
        "file_2_5",
        "file_2_6",
        "file_2_7",
        "file_2_8",
        "file_2_9",
        "file_2_10",
        "file_2_11",
        "file_3_1",
        "file_3_2",
        "file_3_3",
        "file_3_4",
        "file_3_5",
        "file_3_6",
        "file_3_7",
        "file_3_8",
        "file_3_9",
        "file_4_1",
        "file_4_2",
        "file_4_3",
        "file_4_4",
        "file_4_5",
        "file_4_6",
        "file_4_7",
        "file_5_1_1",
        "file_5_1_2",
        "file_5_1_3",
        "file_5_1_4",
        "file_5_1_5",
        "file_5_1_6",
        "file_5_1_7",
        "file_5_1_8",
        "file_5_2_1",
        "file_5_2_2",
        "file_5_2_3",
        "file_5_2_4",
        "file_5_2_5",
        "file_5_3_1",
        "file_5_3_2",
        "file_5_3_3",
        "file_5_3_4",
        "file_5_3_5",
        "file_5_3_6",
        "file_6_1_1",
        "file_6_1_2",
        "file_6_1_3",
        "file_6_1_4",
        "file_6_1_5",
        "file_6_2_1",
        "file_6_2_2",
        "file_6_2_3",
        "file_6_3_1",
        "file_6_3_2",
        "file_6_3_3",
        "file_6_3_4",
        "file_7_1",
        "file_7_2",
        "file_7_3",
        "file_7_4",
        "file_7_5",
        "file_7_6",
        "file_8_1",
        "file_8_2",
        "file_9_1",
        "file_9_2",
        "file_10_1",
        "file_10_2",
        "file_11_1",
        "file_11_2",
        "file_12_1",
        "file_12_2",
        "file_13_1",
        "file_13_2",
        "file_14_1",
        "file_14_2",
    ];

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    public function sports()
    {
        return $this->belongsToMany(SportSchool::class, 'clubes_sports', 'sport_id', 'club_id');
    }
}
