<?php

namespace App\Entities\Clubes;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_sports';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "TB_SRD_DEPORTE";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "PK_I_ID_DEPORTE";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'V_NOMBRE_DEPORTE',
    ];
}
