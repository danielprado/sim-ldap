<?php

namespace App\Entities\Security;

use App\Notifications\Auth\VerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PreRegisterEntity extends Model
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pre_register_entity';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_type_id',
        'organization_subtype_id',
        'organization_name',
        'has_legal_person',
        'person_type_id',
        'document_type_id',
        'document',
        'phone',
        'address',
        'email',
        'code',
        'legal_rep_document',
        'legal_rep_name'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'organization_type_id' => 'int',
        'organization_subtype_id' => 'int',
        'person_type_id' => 'int',
        'document_type_id' => 'int',
        'code' => 'int'
    ];


    protected $guarded = ['has_legal_person'];

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail()); // my notification
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }
}
