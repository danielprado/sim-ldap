<?php

namespace App\Entities\Security;


use App\Traits\FullTextSearch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use OwenIt\Auditing\Contracts\Auditable;

class Admin extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, FullTextSearch;

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = "mysql_users";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guid',
        'name',
        'surname',
        'document',
        'email',
        'username',
        'description',
        'dependency',
        'company',
        'phone',
        'ext',
        'password',
        'password_expired',
        'is_locked',
        'vacation_start_date',
        'vacation_final_date',
        'expires_at',
        'sim_id',
    ];

    /**
     * The columns of the full text index
     *
     * @var array
     */
    protected $searchable = [
        'name',
        'surname',
        'document',
        'email',
        'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'expires_at', 'vacation_start_date', 'vacation_final_date' ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'password_expired' => 'boolean',
        'is_locked' => 'boolean',
        'expires_at' => 'datetime',
        'vacation_start_date' => 'datetime',
        'vacation_final_date' => 'datetime',
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return toUpper( "{$this->name} {$this->surname}" );
    }

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'guid',
        'name',
        'surname',
        'document',
        'email',
        'username',
        'description',
        'dependency',
        'company',
        'phone',
        'ext',
        'vacation_start_date',
        'vacation_final_date',
        'password_expired',
        'is_locked',
        'expires_at',
        'sim_id',
    ];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        'password',
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags() : array
    {
        return ['user'];
    }

    /*
    * ---------------------------------------------------------
    * Query Scopes
    * ---------------------------------------------------------
    */

    /**
     * Check if user is active
     *
     * @param $query
     * @return Builder
     */
    public function scopeActive($query)
    {
        return $query->where('expires_at', '>', now()->format('Y-m-d H:i:s'));
    }

    /**
     * Check if user is not locked
     *
     * @param $query
     * @return Builder
     */
    public function scopeUnlocked($query)
    {
        return $query->where('is_locked', '!=', true);
    }

    /**
     * Check if user is not locked
     *
     * @param $query
     * @return Builder
     */
    public function scopePasswordNotExpired($query)
    {
        return $query->where('password_expired', '!=', true);
    }
}
