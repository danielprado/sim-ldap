<?php

namespace App\Entities\Security;


use App\Entities\CitizenPortal\Profile;
use App\Entities\CitizenPortal\ProfileView;
use App\Notifications\Auth\ResetPassword;
use App\Notifications\Auth\VerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Arr;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable, MustVerifyEmail
{
    use Notifiable, HasApiTokens, \OwenIt\Auditing\Auditable, CanResetPassword;
    // MustVerifyEmail

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $guarded = ['is_verified', 'activated', 'verified_at', 'email_verified_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['email_verified_at', "verified_at"];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'name',
        'email',
        'is_verified',
        'activated',
        'verified_at',
        'code',
        'email_verified_at'
    ];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        'password',
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags() : array
    {
        return ['user'];
    }

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return \App\Entities\Security\User
     */
    public function findForPassport($username)
    {
        return $this->where('email', $username)->first();
    }

    /**
     * Receive an array or string of permissions and models
     * Ex: create-user$App\Model\User,update-role$App\Model\Role
     *
     * @param $permission
     * @return bool
     */
    public function hasAnyPermission($permission)
    {
        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);

        foreach ($permissions as $permission) {
            $perm = explode('$', $permission);
            if (count($perm) == 1) {
                if ($this->can(Arr::first($perm))) {
                    return true;
                }
            } else {
                if ($this->can(Arr::first($perm), Arr::last($perm))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification( $token )
    {
        $this->notify( new ResetPassword( $token ) );
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail()); // my notification
    }

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markEmailAsVerified()
    {
        return $this->forceFill([
            'is_verified'  => true,
            'activated'  => true,
            'code'  => null,
            'verified_at' => $this->freshTimestamp(),
            'email_verified_at' => $this->freshTimestamp(),
        ])->save();
    }

    public function getValidationCode()
    {
        return $this->code;
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    public function user_profile()
    {
        return $this->hasOne(Profile::class, 'user_id', 'id');
                //->where('profile_type_id', Profile::PROFILE_PERSONAL)
                //->orWhere('profile_type_id', Profile::PROFILE_ORGANIZATION);
    }

    /**
     * @return HasMany
     */
    public function profiles_view()
    {
        return $this->hasMany(ProfileView::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function profiles()
    {
        return $this->hasMany(Profile::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class);
    }
}
