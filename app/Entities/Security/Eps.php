<?php

namespace App\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class Eps extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = "mysql_sim";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'eps';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Eps';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre_Eps', 'estado'];

    /*
    * ---------------------------------------------------------
    * Scopes
    * ---------------------------------------------------------
    */
    public function scopeActive($query)
    {
        return $query->where('estado', true);
    }

    /*
    * ---------------------------------------------------------
    * Accessors and Mutator
    * ---------------------------------------------------------
    */

    public function getIdAttribute()
    {
        return (int) $this->Id_Eps;
    }

    public function getNameAttribute()
    {
        return toUpper($this->Nombre_Eps);
    }

    public function getStatusAttribute()
    {
        return (bool) $this->estado;
    }
}
