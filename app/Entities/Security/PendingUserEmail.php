<?php

namespace App\Entities\Security;

use App\Notifications\Auth\VerifyNewEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Auth\Events\Verified;
use Illuminate\Notifications\Notifiable;

class PendingUserEmail extends Model
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pending_user_emails';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * User relationship
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Updates the associated user and removes all pending models with this email.
     *
     * @return void
     */
    public function activate()
    {
        $user = $this->user;

        $dispatchEvent = !$user->hasVerifiedEmail() || $user->email !== $this->email;

        $user->email = $this->email;
        $user->save();
        $user->markEmailAsVerified();
        $this->delete();
        $dispatchEvent ? event(new Verified($user)) : null;
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendNewEmailVerificationNotification()
    {
        $this->notify(new VerifyNewEmail());
    }
}
