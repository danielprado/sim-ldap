<?php

namespace App\Entities\Schools;

use App\Entities\Clubes\Club;
use Illuminate\Database\Eloquent\Model;

class SportSchool extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "schools_sports";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id',
        'sport_id',
    ];

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    public function clubes()
    {
        return $this->belongsToMany(Club::class, 'clubes_sports', 'sport_id', 'club_id');
    }
}
