<?php

namespace App\Entities\Schools;


use Illuminate\Database\Eloquent\Model;

class School extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'schools';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school',
        'email',
        'director',
        'phone',
        'request_type',
        'school_type',
        'accept_media',
        'status',
        'response',
        'profile_id',
        'entity_profile_id',
        'sports',
        'file_1_1',
        'file_1_2',
        'file_1_3',
        'file_1_4',
        'file_1_5',
        'file_1_5_1',
        'file_1_5_2',
        'file_1_5_3',
        'file_1_5_4',
        'file_1_5_5',
        'file_1_5_6',
        'file_1_5_7',
        'file_1_5_8',
        'file_1_6',
        'file_1_6_1',
        'file_1_6_2',
        'file_1_6_3',
        'file_1_6_4',
        'file_1_6_5',
        'file_1_6_6',
        'file_1_6_7',
        'file_1_6_8',
        'file_1_6_9',
        'file_1_7',
        'file_1_8',
        'file_1_9',
        'file_1_10',
        'file_1_11',
        'file_1_12',
        'file_2_1',
        'file_2_2',
        'file_2_3',
        'file_2_4',
        'file_2_5',
        'file_2_5_1',
        'file_2_5_2',
        'file_2_5_3',
        'file_2_5_4',
        'file_2_5_5',
        'file_2_5_6',
        'file_2_5_7',
        'file_2_5_8',
        'file_2_6',
        'file_2_6_1',
        'file_2_6_2',
        'file_2_6_3',
        'file_2_6_4',
        'file_2_6_5',
        'file_2_6_6',
        'file_2_6_7',
        'file_2_6_8',
        'file_2_6_9',
        'file_2_7',
        'file_3_1',
        'file_3_2',
        'file_3_3',
        'file_3_4',
        'file_3_4_1',
        'file_3_4_2',
        'file_3_4_3',
        'file_3_4_4',
        'file_3_4_5',
        'file_3_4_6',
        'file_3_4_7',
        'file_3_4_8',
        'file_3_4_9',
        'file_3_5',
        'file_3_6',
        'file_4_1',
        'file_4_2',
        'file_5_1',
        'file_5_2',
        'file_6_1',
        'file_6_2',
        'file_7_1',
        'file_7_2',
        'file_8_1',
        'file_8_2',
        'file_9_1',
        'file_9_2',
        'file_10_1',
        'file_10_2',
    ];
}
