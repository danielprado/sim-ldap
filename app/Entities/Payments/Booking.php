<?php

namespace App\Entities\Payments;

use App\Entities\Parks\Endowment;
use App\Entities\Parks\ParkEndowment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

      /**
       * The connection name for the model.
       *
       * @var string
       */
      protected $connection = 'mysql_pse';

      /**
       * The table associated with the model.
       *
       * @var string
       */
      protected $table = 'reserva_sintetica';

      /**
       * The primary key for the model.
       *
       * @var string
       */
      protected $primaryKey = 'id';

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
          'fecha',
          'hora_inicio',
          'hora_fin',
          'id_dotacion',
          'enviado',
          'valor',
          'id_carril',
          'is_successful',
          'documento'
      ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['fecha'];

    public function pay()
    {
        return $this->belongsTo(Pay::class,'id','id_reserva');
    }
    public function payment()
    {
        return $this->belongsTo(Payment::class,'id','id_reserva');
    }

    public function endowment()
    {
        return $this->hasOne(ParkEndowment::class,'Id','id_dotacion');
    }
}
