<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use SoftDeletes;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_pse';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'horario_dotacion';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_dotacion',
        'dia',
        'horai',
        'horaf',
        'id_carril'
    ];

    /**
     * @return string|null
     */
    public function getDayAttribute()
    {
        $days =[
            1 => 'LUNES',
            2 => 'MARTES',
            3 => 'MIÉRCOLES',
            4 => 'JUEVES',
            5 => 'VIERNES',
            6 => 'SÁBADO',
            7 => 'DOMINGO'
        ];
        return $this->dia ? $days[$this->dia] : null;
    }
}
