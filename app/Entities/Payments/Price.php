<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{

    /****** Calcular tarifa según categoía parque, diurno/nocturno  ****/
    /* PARQUES CATEGORÍA A */
    // 2397	Canchas de FÚTBOL 5 en parques vecinales o de proximidad CATEGORÍA A	Diurno
    // 2398	Canchas de FÚTBOL 5 en parques vecinales o de proximidad CATEGORÍA A	Nocturno
    // 2399	Canchas de FÚTBOL 8 en parques vecinales o de proximidad CATEGORÍA A	Diurno
    // 2400	Canchas de FÚTBOL 8 en parques vecinales o de proximidad CATEGORÍA A	Nocturno
    // 2401	Canchas de FÚTBOL 11 en parques vecinales o de proximidad CATEGORÍA A	Diurno
    // 2402	Canchas de FÚTBOL 11 en parques vecinales o de proximidad CATEGORÍA A	Nocturno

    /* PARQUES CATEGORÍA B */
    // 2403	Canchas de FÚTBOL 5 en parques vecinales o de proximidad CATEGORÍA B	Diurno
    // 2404	Canchas de FÚTBOL 5 en parques vecinales o de proximidad CATEGORÍA B	Nocturno
    // 2405	Canchas de FÚTBOL 8 en parques vecinales o de proximidad CATEGORÍA B	Diurno
    // 2406	Canchas de FÚTBOL 8 en parques vecinales o de proximidad CATEGORÍA B	Nocturno
    // 2407	Canchas de FÚTBOL 11 en parques vecinales o de proximidad CATEGORÍA B	Diurno
    // 2408	Canchas de FÚTBOL 11 en parques vecinales o de proximidad CATEGORÍA B	Nocturno

    /* PARQUES CATEGORÍA C */
    // 2409	Canchas de FÚTBOL 5 en parques vecinales o de proximidad CATEGORÍA C	Diurno
    // 2410	Canchas de FÚTBOL 5 en parques vecinales o de proximidad CATEGORÍA C	Nocturno
    // 2411	Canchas de FÚTBOL 8 en parques vecinales o de proximidad CATEGORÍA C	Diurno
    // 2412	Canchas de FÚTBOL 8 en parques vecinales o de proximidad CATEGORÍA C	Nocturno
    // 2413	Canchas de FÚTBOL 11 en parques vecinales o de proximidad CATEGORÍA C	Diurno
    // 2414	Canchas de FÚTBOL 11 en parques vecinales o de proximidad CATEGORÍA C	Nocturno

    const PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_5_DAY = 2397;
    const PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_5_NIGHT = 2398;
    const PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_8_DAY = 2399;
    const PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_8_NIGHT = 2400;
    const PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_11_DAY = 2401;
    const PARK_ENDOWMENT_CATEGORY_A_SOCCER_FIELD_11_NIGHT = 2402;

    const PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_5_DAY = 2403;
    const PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_5_NIGHT = 2404;
    const PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_8_DAY = 2405;
    const PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_8_NIGHT = 2406;
    const PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_11_DAY = 2407;
    const PARK_ENDOWMENT_CATEGORY_B_SOCCER_FIELD_11_NIGHT = 2408;

    const PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_5_DAY = 2409;
    const PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_5_NIGHT = 2410;
    const PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_8_DAY = 2411;
    const PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_8_NIGHT = 2412;
    const PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_11_DAY = 2413;
    const PARK_ENDOWMENT_CATEGORY_C_SOCCER_FIELD_11_NIGHT = 2414;

    /****** INICIO Calcular tarifa asadores, según estrato y diurno/nocturno  ****/
    const GRILL_1_2_3_DAY_NIGHT = 1526;
    const GRILL_4_5_6_DAY_NIGHT = 1535;

    /****** INICIO Calcular tarifa canchas de tenis, según estrato y diurno/nocturno  ****/
    const TENNIS_COURT_1_2_3_DAY_NIGHT  = 1498;
    const TENNIS_COURT_4_5_6_DAY_NIGHT  = 1507;

    /****** INICIO Tarifa de piscinas CEFES  ****/
    /****** INICIO Tarifa de gimnasios para hombres, mujeres  ****/
    const GYM_FEMALE_PRICE  = 2416;
    const GYM_MALE_PRICE  = 2417;

    const SWIMMING_POOL_CEFE  = 2415;



    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_liquidador';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TB_SECTOR_TARIFA';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'PK_I_ID_SECTOR_TARIFA';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'D_TOTAL_REDONDEADO',
        'I_SMLMV'
    ];
}
