<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;

class BookingView extends Model
{
      /**
       * The connection name for the model.
       *
       * @var string
       */
      protected $connection = 'mysql_pse';

      /**
       * The table associated with the model.
       *
       * @var string
       */
      protected $table = 'booking';

      /**
       * The primary key for the model.
       *
       * @var string
       */
      protected $primaryKey = 'id';

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
          'date',
          'start_hour',
          'final_hour',
          'start',
          'end',
           // CARRILES           
          'lane_sense_id',
          'lane_sense',
          'lane_level_id',
          'lane_level',
          'lane_id',
          'lane',
          'hour_range',
          'endowment_id',
          'name',
          'park',
          'address',
          'stratum',
          'longitude',
          'latitude',
          'endowment_type',
          'equipment',
          'sent',
          'total',
          'pse_park_id',
          'pse_id',
          'service_id',
          'service_name',
          'service_code',
          'document',
          'document_type',
          'payment_code',
          'pse_transaction_id',
          'payer_email',
          'payer_name',
          'payer_surname',
          'payer_phone',
          'status_id',
          'status_bank',
          'status',
          'concept',
          'currency',
          'permission',
          'permission_type',
          'tax',
          'payer_total',
          'pse_user_id',
          'is_successful',
          'booking_user_document',
          'created_at',
      ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'date', 'start', 'end'];
}
