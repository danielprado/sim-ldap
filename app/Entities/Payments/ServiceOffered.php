<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceOffered extends Model
{
      use SoftDeletes;

      /**
       * The connection name for the model.
       *
       * @var string
       */
      protected $connection = 'mysql_pse';

      /**
       * The table associated with the model.
       *
       * @var string
       */
      protected $table = 'servicio';

      /**
       * The primary key for the model.
       *
       * @var string
       */
      protected $primaryKey = 'id_servicio';

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
            'servicio_nombre',
            'codigo_servicio',
      ];

      public function parks()
      {
            return $this->belongsToMany(PsePark::class, 'parque_servicio', 'id_servicio', 'id_parque')
                  ->withPivot('id_parque_servicio');
      }
}
