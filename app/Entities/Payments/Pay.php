<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;

class Pay extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_pse';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pago';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_pago';

    public function park()
    {
        return $this->belongsTo(PsePark::class,'id_parque');
    }

    public function service()
    {
        return $this->belongsTo(ServiceOffered::class,'id_servicio');
    }

    public function  status_pay(){
        return $this->belongsTo(StatusPay::class,'estado','id_estado');
    }

    public function booking() {
        return $this->belongsTo(Booking::class,'id_reserva','id');
    }
}
