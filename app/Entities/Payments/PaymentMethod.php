<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
      /**
       * The connection name for the model.
       *
       * @var string
       */
      protected $connection = 'mysql_pse';

      /**
       * The table associated with the model.
       *
       * @var string
       */
      protected $table = 'medio_pago';

      /**
       * The primary key for the model.
       *
       * @var string
       */
      protected $primaryKey = 'id';

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
          'Nombre'
      ];

      public function payments() {
          return $this->hasMany(Payment::class, 'medio_id', 'id');
      }
}
