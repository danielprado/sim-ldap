<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;

class BookingReportCEFESView extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_pse';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'booking_report_cefes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'park_id',
        'park',
        'service_id',
        'booking_date',
        'day_of_week',      // Nuevo campo para el día de la semana
        'week_of_year',     // Nuevo campo para la semana del año
        'service',
        'booking_total',
        'booking_amount_total',
        'booking_amount_free',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['booking_date'];
}
