<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    // Código del parque La Florida para aplicar reserv del día completo.
    const LA_FLORIDA = "10-311";
    const LA_FLORIDA_ID = 10778;

    /*
        *Fecha Actualizació: 28/03/2024
        *Soporte: 127306
        *De: JONATHAN ALEXANDER YEPES ARIAS
        *Resolución No 379 de 2023 “Por medio de la cual se adopta el Protocolo de Aprovechamiento Económico
        *de los espacios administrados por el Instituto Distrital de Recreación y Deportes y se dictan otras disposiciones”,
        *específicamente el numeral 2.2.2.13 Kioscos con asadores,
        *... Se deben cobrar 4 horas y la taifa que se aplica por hora es 0.018 es decir 23.400...... en resumen se debe cobrar  $93.600 (23400 * 4)
     */
    const LA_FLORIDA_BOOKING_PAYMENT = 93600;
    // Id's tomados de la tabla servicios de PSE
    // Cancha sintética
    const SYNTHETIC_SERVICE = 13;
    // Cancha grama natural
    const NATURAL_SERVICE = 2;
    // Asador
    const GRILLS = 18;
    // Piscinas
    const POOLS = 8;
    // Cancha tenis
    const TENNIS_COURT_SERVICE = 4;
    // Gimnasios 
    const GYM_SERVICE = 15;
    /*
     * De: Pedro Leon Vargas Enciso <pedro.vargas@idrd.gov.co>
     * Date: El mié, 15 jun 2022 a las 16:08
     * Subject: Solicitud información actividades pagos PSE
     */
    const POOLS_PAYMENT = 55100;

    const PAYMENT_OK = 2;
    const MEDIO_PSE = 1;
    const PAYMENT_BANK_FILE_TYPE = 5;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_pse';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pago_pse';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parque_id',
        'servicio_id',
        'identificacion',
        'tipo_identificacion',
        'codigo_pago',
        'id_transaccion_pse',
        'email',
        'nombre',
        'apellido',
        'telefono',
        'estado_id',
        'estado_banco',
        'concepto',
        'moneda',
        'total',
        'iva',
        'permiso',
        'tipo_permiso',
        'id_reserva',
        'fecha_pago',
        'user_id_pse',
        'medio_id',
        'email_pagador',
        'nombre_pagador',
        'apellido_pagador',
        'telefono_pagador',
        'identificacion_pagador',
        'tipo_identificacion_pagador',
    ];

    /**
     * The columns of the full text index
     *
     * @var array
     */
    protected $searchable = [
        'codigo_pago',
        'email',
        'nombre',
        'apellido',
        'telefono',
        'estado_banco',
        'concepto',
        'total',
        'permiso',
        'tipo_permiso',
        'fecha_pago',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'fecha_pago' ];

    public function scopeSearch($query, $value)
    {
        $q = $query;
        foreach ($this->searchable as $item) {
            $q = $q->orWhere($item, "LIKE", "%{$value}%");
        }
        return $q;
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'estado_id', 'id')->select(['id', 'estado_paymentez', 'descripcion']);
    }

    public function method()
    {
        return $this->belongsTo(PaymentMethod::class, 'medio_id', 'id')->select(['id', 'Nombre']);
    }
    public function service()
    {
        return $this->belongsTo(ServiceOffered::class, 'servicio_id', 'id_servicio')->select(['id_servicio', 'servicio_nombre', 'codigo_servicio']);
    }

    public function park()
    {
        return $this->belongsTo(PsePark::class, 'parque_id', 'id_parque')->select(['id_parque', 'nombre_parque', 'codigo_parque']);
    }

    public function booking()
    {
        return $this->hasOne(Booking::class, 'id', 'id_reserva');
    }

    public function getTypeDocument($type)
    {
        switch ($type) {
            case 'CC':
                return 1;
                break;
            case 'TI':
                return 2;
                break;
            case 'NIT':
                return 7;
                break;
            case 'CE':
                return 4;
                break;
            case 'PP':
                return 6;
                break;
            case 'RC':
                return 3;
                break;
            default:
                return 14;
                break;
        }
    }
}
