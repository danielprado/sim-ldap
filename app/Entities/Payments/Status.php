<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
     * Estado aprobado tomado del ID de la base de datos
     */
    const OK = 2;

      /**
       * The connection name for the model.
       *
       * @var string
       */
      protected $connection = 'mysql_pse';

      /**
       * The table associated with the model.
       *
       * @var string
       */
      protected $table = 'estado_pse';

      /**
       * The primary key for the model.
       *
       * @var string
       */
      protected $primaryKey = 'id';

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
          'estado_paymentez',
          'descripcion',
      ];


      public function payments()
      {
            return $this->hasMany(Payment::class, 'estado_id', 'id');
      }
}
