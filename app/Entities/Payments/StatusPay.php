<?php

namespace App\Entities\Payments;

use Illuminate\Database\Eloquent\Model;

class StatusPay extends Model
{
      /**
       * The connection name for the model.
       *
       * @var string
       */
      protected $connection = 'mysql_pse';

      /**
       * The table associated with the model.
       *
       * @var string
       */
      protected $table = 'pago';

      /**
       * The primary key for the model.
       *
       * @var string
       */
      protected $primaryKey = 'id_pago';

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
          'estado',
      ];

}
