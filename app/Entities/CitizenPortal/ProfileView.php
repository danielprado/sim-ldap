<?php

namespace App\Entities\CitizenPortal;

use App\Entities\Payments\Payment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class ProfileView extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'profiles_view_upd';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'profile_type_id',
        'profile_type',
        'document_type_id',
        'document_type',
        'document',
        'name',
        'surname',
        'email',
        'sex_id',
        'sex',
        'blood_type_id',
        'blood_type',
        'birthdate',
        'country_birth_id',
        'country_birth',
        'state_birth_id',
        'state_birth',
        'city_birth_id',
        'city_birth',
        'country_residence_id',
        'country_residence',
        'state_residence_id',
        'state_residence',
        'city_residence_id',
        'city_residence',
        'locality_id',
        'locality',
        'upz_id',
        'upz',
        'neighborhood_id',
        'neighborhood',
        'other_neighborhood_name',
        'address',
        'stratum',
        'ethnic_group_id',
        'ethnic_group',
        'population_group_id',
        'population_group',
        'gender_id',
        'gender',
        'sexual_orientation_id',
        'sexual_orientation',
        'eps_id',
        'eps',
        'has_disability',
        'disability_id',
        'disability',
        'contact_name',
        'contact_phone',
        'contact_relationship',
        'verified_at',
        'assigned_by_id',
        'assignor_name',
        'assignor_surname',
        'assignor_document',
        'assigned_at',
        'checker_id',
        'checker_name',
        'checker_surname',
        'checker_document',
        'status_id',
        'status',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'verified_at', 'assigned_at', 'birthdate' ];

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @return Model|void|null
     */
    public function resolveRouteBinding($value)
    {
        return $this
                ->whereKey($value)
                ->where('user_id', auth('api')->user()->id)
                ->first() ?? abort(Response::HTTP_NOT_FOUND, __('validation.handler.resource_not_found'));
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * Calculate how much a profile is completed
     *
     * @return float|int
     */
    function getProfileCompletionAttribute()
    {
        $only = [
            "id",
            "document_type_id",
            "document",
            "name",
            "surname",
            "sex",
            "blood_type",
            "birthdate",
            "country_birth_id",
            "state_birth_id",
            "city_birth_id",
            "country_residence_id",
            "state_residence_id",
            "city_residence_id",
            "locality_id",
            "upz_id",
            "neighborhood_id",
            "address",
            "stratum",
            "mobile_phone",
            "ethnic_group_id",
            "population_group_id",
            "gender_id",
            "sexual_orientation_id",
            "eps_id",
            "has_disability",
            "contact_name",
            "contact_phone",
            "contact_relationship"
        ];
        return percent_filled($this->toArray(), $only, true);
    }

    /**
     * @return mixed|string|null
     */
    public function getFullNameAttribute()
    {
        return toUpper( "{$this->name} {$this->s_name} {$this->surname} {$this->s_surname}" );
    }


    /**
     * @return mixed|string|null
     */
    public function getFullNameAssignorAttribute()
    {
        return toUpper( "{$this->assignor_name} {$this->assignor_surname}" );
    }

    /**
     * @return mixed|string|null
     */
    public function getFullNameVerifierAttribute()
    {
        return toUpper( "{$this->checker_name} {$this->checker_surname}" );
    }

    /**
     * @return string|null
     */
    public function getContactRelationshipNameAttribute()
    {
        $data = collect( Profile::RELATIONSHIP )->firstWhere('id', '=', $this->contact_relationship);
        return isset($data->name) ? (string) $data->name : null;
    }

    /**
     * @param $column
     * @return string
     */
    public function getSortableColumn($column)
    {
        switch ($column) {
            case 'id':
            case 'created_at':
            case 'updated_at':
                return $column;
            default:
                return in_array($column, $this->fillable)
                    ? $column
                    : $this->primaryKey;
        }
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return HasMany
     */
    public function observations()
    {
        return $this->hasMany(Observation::class, 'profile_id', 'id')->latest();
    }

    /**
     * @return HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class, 'profile_id', 'id')->latest();
    }

    /**
     * @return HasMany
     */
    public function user_schedules()
    {
        return $this->hasMany(CitizenSchedule::class, 'profile_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function payments()
    {
        return $this->hasMany(Payment::class, 'identificacion', 'document')
                    ->orderByDesc('created_at');
    }
}
