<?php

namespace App\Entities\CitizenPortal;


use App\Entities\Clubes\Club;
use App\Entities\Schools\School;
use App\Traits\FullTextSearch;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Procedure extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, FullTextSearch;

    const SOLICITUD_DE_PERMISO_DE_USO_DE_ESPACIOS = 1; // Id de la tabla procedure_types
    const RECONOCIMIENTO_DEPORTIVO_A_CLUBES = 2; // Id de la tabla procedure_types
    const AVAL_DEPORTIVO_DE_ESCUELAS = 3; // Id de la tabla procedure_types

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'procedures';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'procedure_type_id',
        'profile_id',
        'entity_profile_id',
        'club_id',
        'school_id',
        'state',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'procedure_type_id',
        'profile_id',
        'entity_profile_id',
        'club_id',
        'school_id',
        'state',
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['citizen_portal_procedures'];
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profile_id', 'id')->latest();
    }

    public function entityProfile()
    {
        return $this->belongsTo(ProfileEntity::class, 'entity_profile_id', 'id')->latest();
    }

    public function club()
    {
        return $this->belongsTo(Club::class, 'club_id', 'id')->latest();
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id', 'id')->latest();
    }

}
