<?php

namespace App\Entities\CitizenPortal;

use Illuminate\Database\Eloquent\Model;

class InformedConsent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "informed_consents";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'text',
    ];
}
