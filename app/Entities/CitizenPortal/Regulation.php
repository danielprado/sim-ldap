<?php

namespace App\Entities\CitizenPortal;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Regulation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "regulations";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file',
        'name',
    ];

    public function getFileAttribute()
    {
        if (Storage::disk("public")->exists("regulations/{$this->attributes['file']}")) {
            return Storage::disk("public")->url("regulations/{$this->attributes['file']}");
        }
        return null;
    }
}
