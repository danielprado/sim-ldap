<?php

namespace App\Entities\CitizenPortal;

use App\Entities\Payments\Payment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use OwenIt\Auditing\Contracts\Auditable;
use function Complex\theta;

class CitizenSchedule extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    const PENDING_SUBSCRIBE = 5;
    const SUBSCRIBED = 6;
    const UNSUBSCRIBED = 7;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "user_schedule";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'schedule_id',
        'profile_id',
        'status_id',
        'payment_at',
        'reference_pse',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ["payment_at"];

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'schedule_id',
        'profile_id',
        'status_id',
        'payment_at',
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['citizen_portal_citizen_schedule'];
    }

    /*
    * ---------------------------------------------------------
    * Validation Methods
    * ---------------------------------------------------------
    */

    /**
     * @return bool
     */
    public function hasPaymentRegistered()
    {
        if (isset($this->reference_pse)) {
            $payment = $this->payment;
            if (isset($payment->estado_id) && ((int) $payment->estado_id == Payment::PAYMENT_OK)) {
                return true;
            }
        } else if ($this->files()->whereNotNull("pse")->count() > 0) {
            return true;
        }
        return false;
    }

    public function hasNotPaymentRegistered()
    {
        return !$this->hasPaymentRegistered();
    }

    public function isAvailableToUnsubscribe()
    {
        $unsubscribe_status = [
            Status::PENDING,
            Status::PENDING_SUBSCRIBE,
            Status::FILE_PENDING,
            Status::AVAILABLE_TO_PAY,
        ];

        $status = (int) $this->status_id;
        $is_paid = $this->schedule->is_paid ?? false;
        $close_date = $this->schedule->fecha_cierre ?? null;

        if ($status == Status::SUBSCRIBED) {
            if ($is_paid && $this->hasNotPaymentRegistered() && ($close_date && now()->lessThan($close_date))) {
                return true;
            } else if (!$is_paid && ($close_date && now()->lessThan($close_date))) {
                return true;
            } else {
                return false;
            }
        } else if (in_array((int) $this->status_id, $unsubscribe_status)) {
            if ($is_paid && $this->hasNotPaymentRegistered()) {
                return true;
            } else if ($is_paid && $this->hasPaymentRegistered()) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }


    /*
    * ---------------------------------------------------------
    * Query Scopes
    * ---------------------------------------------------------
    */

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status_id', '=', Profile::SUBSCRIBED);
    }

    /*
    * ---------------------------------------------------------
    * Eloquent Relationships
    * ---------------------------------------------------------
    */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany( File::class, 'citizen_schedule_id', 'id' );
    }

    /**
     * @return HasOne
     */
    public function status()
    {
        return $this->hasOne( Status::class, 'id', 'status_id' );
    }

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profile_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function profiles_view()
    {
        return $this->belongsTo(ProfileView::class, 'profile_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'schedule_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function schedule_view()
    {
        return $this->belongsTo(ScheduleView::class, 'schedule_id', 'id');
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'reference_pse', 'codigo_pago');
    }
}
