<?php

namespace App\Entities\CitizenPortal;


use App\Entities\Parks\Location;
use App\Entities\Parks\Neighborhood;
use App\Entities\Parks\Upz;
use App\Entities\Payments\Payment;
use App\Entities\Security\Admin;
use App\Entities\Security\BloodType;
use App\Entities\Security\CityLDAP;
use App\Entities\Security\CountryLDAP;
use App\Entities\Security\Disability;
use App\Entities\Security\DocumentType;
use App\Entities\Security\Eps;
use App\Entities\Security\EthnicGroup;
use App\Entities\Security\GenderIdentity;
use App\Entities\Security\PopulationGroup;
use App\Entities\Security\Sex;
use App\Entities\Security\SexualOrientation;
use App\Entities\Security\StateLDAP;
use App\Traits\FullTextSearch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use OwenIt\Auditing\Contracts\Auditable;

class Profile extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, FullTextSearch;

    // Relationship
    const RELATIONSHIP = [
        [
            'id'    =>  3,
            'name'  =>  'PADRE - MADRE',
        ],
        [
            'id'    =>  7,
            'name'  =>  'TUTOR LEGAL',
        ],
    ];

    const PROFILE_PERSONAL = 1; // Id de la tabla profile_types
    const PROFILE_BENEFICIARY = 2; // Id de la tabla profile_types
    const PROFILE_ORGANIZATION = 3; // Id de la tabla profile_types

    // Ids de la tabla status "estados"

    // Validate Profile
    const PENDING = 1;
    const VALIDATING = 2;
    const VERIFIED = 3;
    const RETURNED = 4;

    // Validate Subscription
    const PENDING_SUBSCRIBE = 5;
    const SUBSCRIBED = 6;
    const UNSUBSCRIBED = 7;

    // Document
    const FILE_PENDING = 8;
    const FILE_VERIFIED = 9;
    const FILE_RETURNED = 10;

    // PERSON_TYPE
    const LEGAL_PERSON = 2;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'relationship',
        'user_id',
        'profile_type_id',
        'birthdate',
        'document_type_id',
        'document',
        'name',
        's_name',
        'surname',
        's_surname',
        'country_residence_id',
        'state_residence_id',
        'city_residence_id',
        'locality_id',
        'upz_id',
        'neighborhood_id',
        'other_neighborhood_name',
        'address',
        'stratum',
        'mobile_phone',
        'country_birth_id',
        'state_birth_id',
        'city_birth_id',
        'sex',
        'gender_id',
        'sexual_orientation_id',
        'population_group_id',
        'blood_type',
        'ethnic_group_id',
        'eps_id',
        'has_disability',
        'disability_id',
        'contact_name',
        'contact_phone',
        'contact_relationship',
        'preferred_name',
        'is_organization'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [ 'verified_at', 'assigned_by_id', 'assigned_at', 'checker_id', 'status_id' ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'verified_at', 'assigned_at', 'birthdate' ];

    /**
     * The columns of the full text index
     *
     * @var array
     */
    protected $searchable = [
        'document',
        'name',
        'surname',
    ];

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'user_id',
        'relationship',
        'profile_type_id',
        'document_type_id',
        'document',
        'name',
        's_name',
        'surname',
        's_surname',
        'sex',
        'blood_type',
        'birthdate',
        'country_birth_id',
        'state_birth_id',
        'city_birth_id',
        'country_residence_id',
        'state_residence_id',
        'city_residence_id',
        'locality_id',
        'upz_id',
        'neighborhood_id',
        'other_neighborhood_name',
        'address',
        'stratum',
        'ethnic_group_id',
        'population_group_id',
        'gender_id',
        'sexual_orientation_id',
        'has_disability',
        'disability_id',
        'contact_name',
        'contact_phone',
        'contact_relationship',
        'verified_at',
        'assigned_by_id',
        'assigned_at',
        'checker_id',
        'status_id',
        'preferred_name',
        'is_organization'
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['citizen_portal_profiles'];
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @return Model|void|null
     */
    public function resolveRouteBinding($value)
    {
        return $this
                ->whereKey($value)
                ->where('user_id', auth('api')->user()->id)
                ->latest()
                ->first() ?? abort(Response::HTTP_NOT_FOUND, __('validation.handler.resource_not_found'));
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * @return mixed|string|null
     */
    public function getFullNameAttribute()
    {
        return toUpper( "{$this->name} {$this->s_name} {$this->surname} {$this->s_surname}" );
    }

    /**
     * @return mixed|string|null
     */
    public function getEmailFullNameAttribute()
    {
        return toTitle( $this->name )." ".toTitle($this->surname);
    }

    public function setSNameAttribute($value)
    {
        $this->attributes['s_name'] = toUpper($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = toUpper($value);
    }

    public function setSurnameAttribute($value)
    {
        $this->attributes['surname'] = toUpper($value);
    }

    public function setSSurnameAttribute($value)
    {
        $this->attributes['s_surname'] = toUpper($value);
    }

    /**
     * Calculate how much a profile is completed
     *
     * @return float|int
     */
    function getProfileCompletionAttribute()
    {
        $except = [
            's_name',
            's_surname',
            'other_neighborhood_name',
            'user_id',
            'profile_type_id',
            'disability_id',
            'verified_at',
            'assigned_by_id',
            'assigned_at',
            'checker_id',
            'status_id',
            'created_at',
            'updated_at',
        ];

        return percent_filled($this->toArray(), $except);
    }

    /**
     * @return string|null
     */
    public function getContactRelationshipNameAttribute()
    {
        $data = collect( Profile::RELATIONSHIP )->firstWhere('id', '=', $this->contact_relationship);
        return isset($data->name) ? (string) $data->name : null;
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    public function observations()
    {
        return $this->hasMany(Observation::class, 'profile_id', 'id')->latest();
    }

    public function files()
    {
        return $this->hasMany(File::class, 'profile_id', 'id')->latest();
    }

    public function user_schedules()
    {
        return $this->hasMany(CitizenSchedule::class, 'profile_id', 'id');
    }

    public function assigned()
    {
        return $this->hasOne( Admin::class, 'id', 'assigned_by_id' );
    }

    public function viewer()
    {
        return $this->hasOne( Admin::class, 'id', 'checker_id' );
    }

    public function status()
    {
        return $this->hasOne( Status::class, 'id', 'status_id' );
    }

    public function user()
    {
        return $this->belongsTo(Citizen::class, 'user_id', 'id');
    }

    public function profile_type()
    {
        return $this->belongsTo(ProfileType::class, 'profile_type_id', 'id');
    }

    public function document_type()
    {
        return $this->hasOne(DocumentType::class, 'Id_TipoDocumento', 'document_type_id');
    }

    public function sex_name()
    {
        return $this->hasOne(Sex::class, 'Id_Genero', 'sex');
    }

    public function blood_type_name()
    {
        return $this->hasOne(BloodType::class, 'Id_GrupoSanguineo', 'blood_type');
    }

    public function country_birth()
    {
        return $this->hasOne(CountryLDAP::class, 'id', 'country_birth_id');
    }

    public function state_birth()
    {
        return $this->hasOne(StateLDAP::class, 'id', 'state_birth_id');
    }

    public function city_birth()
    {
        return $this->hasOne(CityLDAP::class, 'id', 'city_birth_id');
    }

    public function country_residence()
    {
        return $this->hasOne(CountryLDAP::class, 'id', 'country_residence_id');
    }

    public function state_residence()
    {
        return $this->hasOne(StateLDAP::class, 'id', 'state_residence_id');
    }

    public function city_residence()
    {
        return $this->hasOne(CityLDAP::class, 'id', 'city_residence_id');
    }

    public function locality()
    {
        return $this->hasOne(Location::class, 'Id_Localidad', 'locality_id');
    }

    public function upz()
    {
        return $this->hasOne(Upz::class, 'cod_upz', 'upz_id');
    }

    public function neighborhood()
    {
        return $this->hasOne(Neighborhood::class, 'IdBarrio', 'neighborhood_id');
    }

    public function ethnic_group()
    {
        return $this->hasOne(EthnicGroup::class, 'Id_Etnia', 'ethnic_group_id');
    }

    public function population_group()
    {
        return $this->hasOne(PopulationGroup::class, 'id', 'population_group_id');
    }

    public function gender()
    {
        return $this->hasOne(GenderIdentity::class, 'id', 'gender_id');
    }

    public function sexual_orientation()
    {
        return $this->hasOne(SexualOrientation::class, 'id', 'sexual_orientation_id');
    }

    public function eps()
    {
        return $this->hasOne(Eps::class, 'Id_Eps', 'eps_id');
    }

    public function disability()
    {
        return $this->hasOne(Disability::class, 'id', 'disability_id');
    }

    /**
     * @return HasMany
     */
    public function payments()
    {
        return $this->hasMany(Payment::class, 'identificacion', 'document')
            ->orderByDesc('created_at');
    }
}
