<?php

namespace App\Entities\CitizenPortal;


use App\Entities\Clubes\Club;
use App\Entities\Schools\School;
use App\Traits\FullTextSearch;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProfileEntity extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, FullTextSearch;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'entity_profiles';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_type_id',
        'organization_subtype_id',
        'has_legal_person',
        'person_type_id',
        'has_sporting_aval',
        'sporting_aval_number',
        'has_sporting_recognition',
        'legal_rep_document',
        'legal_rep_name',
        'document_type_id',
        'document',
        'dv',
        'organization_name',
        'address',
        'email',
        'phone',
        'legal_profile_id',
        'profile_id',
        'club_id',
        'school_id',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [ 'has_legal_person', 'has_sporting_aval', 'has_sporting_recognition' ];

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'organization_type_id',
        'organization_subtype_id',
        'has_legal_person',
        'person_type_id',
        'has_sporting_aval',
        'has_sporting_recognition',
        'sporting_aval_number',
        'legal_rep_document',
        'legal_rep_name',
        'document_type_id',
        'document',
        'dv',
        'organization_name',
        'address',
        'email',
        'phone',
        'legal_profile_id',
        'profile_id'
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['citizen_portal_profile_entity'];
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    public function setLegalRepNameAttribute($value)
    {
        $this->attributes['legal_rep_name'] = toUpper($value);
    }

    public function setOrganizationNameAttribute($value)
    {
        $this->attributes['organization_name'] = toUpper($value);
    }

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = toUpper($value);
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profile_id', 'id')->latest();
    }

    public function legal_profile()
    {
        return $this->belongsTo(Profile::class, 'legal_profile_id', 'id')->latest();
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id', 'id')->latest();
    }

    public function club()
    {
        return $this->belongsTo(Club::class, 'club_id', 'id')->latest();
    }

}
