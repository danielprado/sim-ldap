<?php

namespace App\Entities\CitizenPortal;

use App\Entities\Payments\Payment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use OwenIt\Auditing\Contracts\Auditable;
use function Complex\theta;

class TeanSchedule extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    const PENDING_SUBSCRIBE = 5;
    const SUBSCRIBED = 6;
    const UNSUBSCRIBED = 7;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "team_schedule";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'schedule_id',
        'team_id',
        'status_id',
    ];

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'schedule_id',
        'profile_id',
        'status_id',
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['citizen_portal_team_schedule'];
    }

    /*
    * ---------------------------------------------------------
    * Query Scopes
    * ---------------------------------------------------------
    */

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status_id', '=', Profile::SUBSCRIBED);
    }

    /*
    * ---------------------------------------------------------
    * Eloquent Relationships
    * ---------------------------------------------------------
    */

    /**
     * @return HasOne
     */
    public function status()
    {
        return $this->hasOne( Status::class, 'id', 'status_id' );
    }

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profile_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function profiles_view()
    {
        return $this->belongsTo(ProfileView::class, 'profile_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'schedule_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function schedule_view()
    {
        return $this->belongsTo(ScheduleView::class, 'schedule_id', 'id');
    }
}
