<?php

namespace App\Entities\CitizenPortal;

use Illuminate\Database\Eloquent\Model;

class ScheduleInformedConsent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "schedules_informed_consents";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'schedule_id',
        'informed_id',
    ];
}
