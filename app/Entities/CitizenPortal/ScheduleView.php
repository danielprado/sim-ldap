<?php

namespace App\Entities\CitizenPortal;

use App\Entities\Parks\Park;
use App\Traits\PaginationWithHavings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class ScheduleView extends Model
{
    use SoftDeletes, PaginationWithHavings, Searchable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "schedule_view";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'program_id',
        'program_name',
        'activity_id',
        'activity_name',
        'stage_id',
        'stage_name',
        'park_id',
        'weekday_id',
        'weekday_name',
        'daily_id',
        'daily_name',
        'min_age',
        'max_age',
        'quota',
        'taken',
        'is_paid',
        'rate_id',
        'rate_name',
        'rate_value',
        'rate_service_id',
        'is_initiate',
        'start_date',
        'final_date',
        'is_activated',
        'project_id' => 'int',
        'activity_type_id' => 'int',
        'set_population_params' => 'boolean',
        'sex_id' => 'string',
        'orientation_id' => 'string',
        'gender_id' => 'string',
        'ethnic_group_id' => 'string',
        'population_group_id' => 'string',
        'disability_id' => 'string',
        'group_or_individual' => 'boolean',
        'men_participants_num' => 'int',
        'women_participants_num' => 'int',
        'staff_participants_num' => 'int',
        'activity_type_name' => 'string',
        'project_name' => 'string'
    ];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'schedules_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id'        =>  isset($this->id) ? (int) $this->id : null,
            'program_name'      =>  isset($this->program_name) ? (string) $this->program_name : null,
            'activity_name'      =>  isset($this->activity_name) ? (string) $this->activity_name : null,
            'stage_name'      =>  isset($this->stage_name) ? (string) $this->stage_name : null,
            'park_id'      =>  isset($this->park_id) ? (int) $this->park_id : null,
            'park_code'      =>  isset($this->park_code) ? (string) $this->park_code : null,
            'park_name'      =>  isset($this->park_name) ? (string) $this->park_name : null,
            'park_address'      =>  isset($this->park_address) ? (string) $this->park_address : null,
            'weekday_name'      =>  isset($this->weekday_name) ? (string) $this->weekday_name : null,
            'daily_name'      =>  isset($this->daily_name) ? (string) $this->daily_name : null,
            'min_age'      =>  isset($this->min_age) ? (int) $this->min_age : null,
            'max_age'      =>  isset($this->max_age) ? (int) $this->max_age : null,
            'quota'      =>  isset($this->quota) ? (int) $this->quota : null,
            'is_paid'      =>  isset($this->is_paid) ? (bool) $this->is_paid : null,
            'is_activated'      =>  isset($this->is_activated) ? (bool) $this->is_activated : null,
            'is_initiate'      =>  isset($this->is_initiate) ? (bool) $this->is_initiate : null,
            'start_date'      =>  isset($this->start_date) ? $this->start_date->unix() : null,
            'final_date'      =>  isset($this->final_date) ? $this->final_date->unix() : null,
            'created_at'    =>  isset($this->created_at) ? $this->created_at->unix() : null,
            'project_id' => isset($this->project_id) ? (int) $this->project_id : null,
            'activity_type_id' => isset($this->activity_type_id) ? (int) $this->activity_type_id : null,
            'set_population_params' => isset($this->set_population_params) ? (bool) $this->set_population_params : null,
            'sex_id' => isset($this->sex_id) ? (string) $this->sex_id : null,
            'orientation_id' => isset($this->orientation_id) ? (string) $this->orientation_id : null,
            'gender_id' => isset($this->gender_id) ? (string) $this->gender_id : null,
            'ethnic_group_id' => isset($this->ethnic_group_id) ? (string) $this->ethnic_group_id : null,
            'population_group_id' => isset($this->population_group_id) ? (string) $this->population_group_id : null,
            'disability_id' => isset($this->disability_id) ? (string) $this->disability_id : null,
            'group_or_individual' => isset($this->group_or_individual) ? (bool) $this->group_or_individual : null,
            'men_participants_num' => isset($this->men_participants_num) ? (int) $this->men_participants_num : null,
            'women_participants_num' => isset($this->women_participants_num) ? (int) $this->women_participants_num : null,
            'staff_participants_num' => isset($this->staff_participants_num) ? (int) $this->staff_participants_num : null,
            'activity_type_name' => isset($this->activity_type_name) ? (string) $this->activity_type_name : null,
            'project_name' => isset($this->project_name) ? (string) $this->project_name : null
        ];
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'final_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'program_id'    => 'int',
        'activity_id'   => 'int',
        'stage_id'      => 'int',
        'park_id'       => 'int',
        'weekday_id'    => 'int',
        'daily_id'      => 'int',
        'min_age'       => 'int',
        'max_age'       => 'int',
        'quota'         => 'int',
        'rate_id'       => 'int',
        'rate_value'    => 'int',
        'rate_service_id'    => 'int',
        'is_paid'       => 'boolean',
        'is_initiate'   => 'boolean',
        'is_activated'  => 'boolean',
        'project_id' => 'int',
        'activity_type_id' => 'int',
        'set_population_params' => 'boolean',
        'sex_id' => 'string',
        'orientation_id' => 'string',
        'gender_id' => 'string',
        'ethnic_group_id' => 'string',
        'population_group_id' => 'string',
        'disability_id' => 'string',
        'group_or_individual' => 'boolean',
        'men_participants_num' => 'int',
        'women_participants_num' => 'int',
        'staff_participants_num' => 'int',
        'activity_type_name' => 'string',
        'project_name' => 'string'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'name',
        'park_code',
        'park_name',
        'park_address',
    ];

    /*
     * ---------------------------------------------------------
     * Query Scopes
     * ---------------------------------------------------------
     */

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
            $parks = Park::query()->where('Id_IDRD', 'like', "%$value%")
                ->orWhere('Nombre', 'like', "%$value%")
                ->orWhere('Direccion', 'like', "%$value%")
                ->get(["Id"])->pluck("Id")->toArray();
            return $query
                ->where('program_name', 'like', "%$value%")
                ->orWhere('activity_name', 'like', "%$value%")
                ->orWhere('stage_name', 'like', "%$value%")
                ->orWhereIn("park_id", $parks)
                ->orWhere('weekday_name', 'like', "%$value%")
                ->orWhere('daily_name', 'like', "%$value%")
                ->orWhere('min_age', 'like', "%$value%")
                ->orWhere('max_age', 'like', "%$value%")
                ->orWhere('quota', 'like', "%$value%");
        });
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * @return mixed|string|null
     */
    public function getParkNameAttribute()
    {
        return isset($this->park->Nombre) ? toUpper( $this->park->Nombre ) : null;
    }

    /**
     * @return mixed|string|null
     */
    public function getParkCodeAttribute()
    {
        return isset($this->park->Id_IDRD) ? toUpper( $this->park->Id_IDRD ) : null;
    }

    /**
     * @return mixed|string|null
     */
    public function getParkAddressAttribute()
    {
        return isset($this->park->Direccion) ? toUpper( $this->park->Direccion ) : null;
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return "{$this->weekday_name} {$this->daily_name} - ({$this->min_age}-{$this->max_age}) AÑOS";
    }

    /**
     * @return string
     */
    public function getIconAttribute()
    {
        return self::icons($this->activity_name ?? "other");
    }

    /**
     * @param $column
     * @return string
     */
    public function getSortableColumn($column)
    {
        switch ($column) {
            case 'created_at':
            case 'updated_at':
            case 'deleted_at':
                return $column;
            default:
                return in_array($column, $this->fillable)
                    ? $column
                    : $this->primaryKey;
        }
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    /**
     * @return HasMany
     */
    public function users_schedules()
    {
        return $this->hasMany(CitizenSchedule::class, 'schedule_id', 'id');
    }

    /**
     * @return mixed
     */
    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function park()
    {
        return $this->belongsTo(Park::class, 'park_id', 'Id');
    }

    /**
     * @return HasMany
     */
    public function teams_schedules()
    {
        return $this->hasMany(TeanSchedule::class, 'schedule_id', 'id');
    }

    public function regulations()
    {
        return $this->belongsToMany(
            Regulation::class,
            "schedules_regulations",
            'schedule_id',
            'regulation_id'
        );
    }

    public function informed_consents()
    {
        return $this->belongsToMany(
            InformedConsent::class,
            "schedules_informed_consents",
            'schedule_id',
            'informed_id'
        );
    }

    static function icons($text)
    {
        if (search_in_string($text, "ajedrez")) {
            return "mdi-chess-pawn";
        } else if (search_in_string($text, "multilateral")) {
            return "mdi-hexagon-multiple";
        } else if (search_in_string($text, "arquería")) {
            return "mdi-bullseye-arrow";
        } else if (search_in_string($text, "arqueria")) {
            return "mdi-bullseye-arrow";
        } else if (search_in_string($text, "atletismo")) {
            return "mdi-run";
        } else if (search_in_string($text, "baloncesto")) {
            return "mdi-basketball";
        } else if (search_in_string($text, "beisbol")) {
            return "mdi-baseball-bat";
        } else if (search_in_string($text, "bolos")) {
            return "mdi-bowling";
        } else if (search_in_string($text, "boxeo")) {
            return "mdi-boxing-glove";
        } else if (search_in_string($text, "ciclismo")) {
            return "mdi-bike";
        } else if (search_in_string($text, "ciclomontañismo")) {
            return "mdi-bike";
        } else if (search_in_string($text, "escalada")) {
            return "mdi-image-filter-hdr";
        } else if (search_in_string($text, "esgrima")) {
            return "mdi-fencing";
        } else if (search_in_string($text, "fútbol")) {
            return "mdi-soccer";
        } else if (search_in_string($text, "futbol")) {
            return "mdi-soccer";
        } else if (search_in_string($text, "balonmano")) {
            return "mdi-soccer";
        } else if (search_in_string($text, "gimnasia")) {
            return "mdi-gymnastics";
        } else if (search_in_string($text, "judo")) {
            return "mdi-gymnastics";
        } else if (search_in_string($text, "karate")) {
            return "mdi-gymnastics";
        } else if (search_in_string($text, "pesas")) {
            return "mdi-dumbbell";
        } else if (search_in_string($text, "lucha")) {
            return "mdi-boxing-glove";
        } else if (search_in_string($text, "patinaje")) {
            return "mdi-roller-skate";
        } else if (search_in_string($text, "rugby")) {
            return "mdi-rugby";
        } else if (search_in_string($text, "skate")) {
            return "mdi-skateboarding";
        } else if (search_in_string($text, "squash")) {
            return "mdi-racquetball";
        } else if (search_in_string($text, "taekwondo")) {
            return "mdi-gymnastics";
        } else if (search_in_string($text, "talleres")) {
            return "mdi-book-open-variant";
        } else if (search_in_string($text, "curso")) {
            return "mdi-book-open-variant";
        } else if (search_in_string($text, "administración")) {
            return "mdi-book-open-variant";
        } else if (search_in_string($text, "administracion")) {
            return "mdi-book-open-variant";
        } else if (search_in_string($text, "tenis de mesa")) {
            return "mdi-table-tennis";
        } else if (search_in_string($text, "pinpon")) {
            return "mdi-table-tennis";
        } else if (search_in_string($text, "tenis")) {
            return "mdi-tennis";
        } else if (search_in_string($text, "voleibol")) {
            return "mdi-volleyball";
        } else if (search_in_string($text, "natación")) {
            return "mdi-swim";
        } else if (search_in_string($text, "natacion")) {
            return "mdi-swim";
        } else if (search_in_string($text, "subacuaticas")) {
            return "mdi-swim";
        } else if (search_in_string($text, "subacuáticas")) {
            return "mdi-swim";
        } else if (search_in_string($text, "lucha")) {
            return "mdi-boxing-glove";
        } else if (search_in_string($text, "pesas")) {
            return "mdi-dumbbell";
        } else if (search_in_string($text, "caminata")) {
            return "mdi-walk";
        } else {
            return "mdi-image-text";
        }
    }
}
