<?php

namespace App\Entities\SocialManagement;

use Illuminate\Database\Eloquent\Model;

class ProgramImage extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_social';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "programming_images";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'programming_id',
        'file_name',
        'confirmation',
        'description',
        'path',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'programming_id'    =>  'int',
        'confirmation'      =>  'boolean',
    ];
}
