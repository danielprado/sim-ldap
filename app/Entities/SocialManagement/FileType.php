<?php

namespace App\Entities\SocialManagement;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileType extends Model
{
    use SoftDeletes;

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_social';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'file_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'file_type' ];
}
