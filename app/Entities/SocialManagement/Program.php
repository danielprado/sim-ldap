<?php

namespace App\Entities\SocialManagement;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_social';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "programming_view";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'execution_date',
        'initial_hour',
        'final_hour',
        'upz',
        'upz_id',
        'location_id',
        'place',
        'process_id',
        'process',
        'park_id',
        'park_code',
        'park',
        'who_summons_id',
        'who_summons',
        'which',
        'user_id',
        'full_name',
        'name',
        'middle_name',
        'last_name',
        'second_last_name',
        'document',
        'request_type_id',
        'request',
        'objective',
        'who_cancel',
        'reason_for_cancellation',
        'include_attendance',
        'include_files',
        'include_images',
        'include_commitments',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'execution_date'    =>  'date',
        'deleted_at'        =>  'date',
        'id'                =>  'int',
        'upz_id'            =>  'int',
        'process_id'        =>  'int',
        'park_id'           =>  'int',
        'who_summons_id'    =>  'int',
        'user_id'           =>  'int',
        'request_type_id'   =>  'int',
        'include_attendance'=>  'bool',
        'include_files'     =>  'bool',
        'include_images'    =>  'bool',
        'include_commitments'=>  'bool',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function programming_activities()
    {
        return $this->belongsToMany( Activity::class, 'programmings_activities', 'programming_id')
            ->withPivot('activity_id', 'description', 'id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function programming_files()
    {
        return $this->belongsToMany( FileType::class, 'programming_files', 'programming_id')
            ->withPivot('file_type_id', 'file_name', 'path', 'id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function programming_images()
    {
        return $this->hasMany( ProgramImage::class,'programming_id');
    }
}
