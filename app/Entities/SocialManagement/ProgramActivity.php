<?php

namespace App\Entities\SocialManagement;

use Illuminate\Database\Eloquent\Model;

class ProgramActivity extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_social';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'programmings_activities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activity_id',
        'programming_id',
        'description'
    ];
}
