<?php

namespace App\Entities\SocialManagement;

use Illuminate\Database\Eloquent\Model;

class ProgramFile extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_social';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'programming_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_type_id',
        'programming_id',
        'file_name',
        'path',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'file_type_id'      =>  'int',
        'programming_id'    =>  'int',
    ];
}
