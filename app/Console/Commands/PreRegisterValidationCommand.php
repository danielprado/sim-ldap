<?php

namespace App\Console\Commands;

use App\Entities\Security\PreRegister;
use Illuminate\Console\Command;

class PreRegisterValidationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pre-register:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear pre register users after 3 days created';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        PreRegister::query()->where("created_at", "<=", now()->subDays(3))->delete();
        $this->info("Deleting older users");
    }
}
