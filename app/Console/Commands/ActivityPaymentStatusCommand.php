<?php

namespace App\Console\Commands;

use App\Entities\CitizenPortal\CitizenSchedule;
use App\Entities\CitizenPortal\Status;
use App\Entities\Payments\Payment;
use Illuminate\Console\Command;
use Illuminate\Http\Response;

class ActivityPaymentStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payments:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validate payment status and subscribe or delete people';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $i = 0;
        CitizenSchedule::query()
            ->where("status_id", "!=", Status::SUBSCRIBED)
            ->where(function ($query) {
              return $query->whereNotNull("reference_pse")
                            ->orWhere("reference_pse", "!=", "");
            })
            ->chunk(1000, function ($schedules) use(&$i) {
                foreach ($schedules as $schedule) {
                    $i++;
                    $medio = isset($schedule->payment->medio_id) ? (int) $schedule->payment->medio_id : 0;
                    $value_paid = isset($schedule->payment->total) ? (int) $schedule->payment->total : 0;
                    $value_to_pay = isset($schedule->schedule->rate_value) ? (int) $schedule->schedule->rate_value : 0;
                    $status = isset($schedule->payment->estado_id) ? (int) $schedule->payment->estado_id : 0;
                    if (($value_paid >= $value_to_pay) && ($status == Payment::PAYMENT_OK) && $medio == Payment::MEDIO_PSE) {
                        $this->info("$i - Updating Status {$schedule->reference_pse}");
                        $schedule->status_id = Status::SUBSCRIBED;
                        $schedule->save();
                    } else {
                        $this->warn("$i - Not paid Status {$schedule->reference_pse}");
                    }
                }
            });
    }
}
