<?php

namespace App\Console\Commands;

use App\Entities\CitizenPortal\Citizen;
use App\Entities\CitizenPortal\CitizenSchedule;
use App\Entities\CitizenPortal\File;
use App\Entities\CitizenPortal\Profile;
use App\Entities\Payments\Payment;
use App\Entities\Security\SocialAccount;
use App\Notifications\SendgridTemplates;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use SendGrid\Mail\Mail;

class DeleteInactiveUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:deactivate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $profilesCount = 0;
        foreach (Citizen::query()->whereDate("created_at", "<", "2022-06-01")->cursor() as $citizen) {
            $total = 0;
            $payments = 0;
            $activities = 0;
            $email = $citizen->email ?? null;
            foreach ($citizen->profiles as $profile) {
                $doc = $profile->document ?? null;
                $payments = $doc ? Payment::query()->where("identificacion", $doc)->count() : 0;
                $activities = CitizenSchedule::query()->where("profile_id", $profile->id)->count();
                $files = File::query()->where("profile_id", $profile->id)->where("file_type_id", File::DOCUMENT)->count();
                $total += $payments + $activities;
                $percent = $profile->profile_completion;
                if ($percent < 70 && $activities == 0 && $payments == 0 && $profile->profile_type_id != Profile::PROFILE_PERSONAL && $files == 0 && is_null($doc)) {
                    $this->destroyProfile($profile);
                    $this->warn("{$citizen->id} - {$profile->id} - {$email} - Pagos ($payments) - Actividades ($activities) - Incompleto $percent % - DELETED");
                }
            }
            if ($total == 0) {
                foreach ($citizen->profiles as $profile) {
                    $this->destroyProfile($profile);
                }
                SocialAccount::query()->where('user_id', $citizen->id)->delete();
                $citizen->delete();
                $answer = $this->notify($email);
                $msg = $answer ? "SENT" : "ERROR";
                $this->warn("{$citizen->id} - {$email} - Pagos ($payments) - Actividades ($activities) - DELETED - $msg");
                Log::info("{$citizen->id} - {$email} - Pagos ($payments) - Actividades ($activities) - DELETED - $msg");
            } else {
                $this->info("{$citizen->id} - {$email} - Pagos ($payments) - Actividades ($activities) - SAVED");
            }
        }
    }

    public function destroyProfile(Profile $profile)
    {
        try {
            foreach ($profile->observations as $observation) {
                $observation->delete();
            }
            foreach ($profile->files as $file) {
                if (Storage::disk('local')->exists("portal/$file->file") ) {
                    if (config("app.env") == 'production') {
                        Storage::disk('local')->delete("portal/$file->file");
                    }
                }
                if (config("app.env") == 'production') {
                    $file->delete();
                }
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function notify($email = null)
    {
        try {
            if ($email) {
                $sendgrid = new SendgridTemplates();
                $response = $sendgrid->sendDeleteAccountMessage($email);
                $this->info("$response");
                return str_contains($response, "MESSAGE_SENT");
            }
            return false;
        } catch (\Exception $exception) {
            $this->error("{$exception->getMessage()}");
            return false;
        }
    }
}
