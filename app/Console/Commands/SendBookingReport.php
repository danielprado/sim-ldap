<?php

namespace App\Console\Commands;

use App\Exports\BookingsUserReportExport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use App\Notifications\SendWeeklyUserReportNotification;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;
use Storage;

class SendBookingReport extends Command
{
    protected $signature = 'report:send-bookings {--start= : Fecha de inicio (YYYY-MM-DD)} {--end= : Fecha de fin (YYYY-MM-DD)} {--cefes=false}';
    protected $description = 'Generar y enviar un reporte de pagos realizados por reserva basado en la fecha de reserva (booking.fecha)';

    public function handle()
    {
        try {
            $startDate = $this->option('start') ?: $this->ask('Ingrese la fecha de inicio (YYYY-MM-DD)', '2025-01-03');
            $endDate = $this->option('end') ?: $this->ask('Ingrese la fecha de fin (YYYY-MM-DD)', now()->toDateString());
            $cefes = filter_var($this->option('cefes'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) ?? false;

            $fileName = 'Reporte_Bookings_' . now()->format('d_m_Y') . '.xlsx';
            $filePath = storage_path("app/public/{$fileName}");

            $export = new BookingsUserReportExport($startDate, $endDate, $cefes);
            $data = $export->collection();
            \Log::info("Records found for export:", ['count' => $data->count()]);

            if ($data->isEmpty()) {
                $this->error("No records found for the given date range.");
                return;
            }

            // Generate Excel and store in public storage
            Excel::store($export, "public/{$fileName}");

            if (!Storage::exists("public/{$fileName}")) {
                throw new \Exception("Failed to store the Excel file: {$fileName}");
            }

            $this->info("Excel file successfully stored at: {$filePath}");

            // Send email with the report

            if ($cefes){
                $recipients = [
                    'andres.cabeza@idrd.gov.co',
                    'daniel.forero@idrd.gov.co',
                ];
            } else {
                $recipients = [
                    'andres.cabeza@idrd.gov.co',
                    'carmen.vergara@idrd.gov.co',
                    'rosanna.mancilla@idrd.gov.co',
                    'daniel.forero@idrd.gov.co',
                    'jonathan.yepes@idrd.gov.co'
                ];
            }

            foreach ($recipients as $email) {
                $notifiable = new class($email) {
                    use Notifiable;
                    protected $email;
                    public function __construct($email) { $this->email = $email; }
                    public function routeNotificationForMail() { return $this->email; }
                };

                // Send email using SendWeeklyUserReportNotification
                $notifiable->notify(new SendWeeklyUserReportNotification($filePath, $fileName));
            }

            $this->info("Reporte generado y enviado exitosamente: {$fileName}");

        } catch (\Exception $e) {
            $this->error("Error generando o enviando el reporte: " . $e->getMessage());
            \Log::error('Export Error:', ['exception' => $e]);
        }
    }
}
