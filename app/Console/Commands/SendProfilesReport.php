<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Exports\ProfilesReportExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Notifications\SendProfilesReportNotification;
use Illuminate\Notifications\Notifiable;
use Storage;

class SendProfilesReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:send-profiles {--all : Exportar todos los registros} {--start= : Fecha de inicio (YYYY-MM-DD)} {--end= : Fecha de fin (YYYY-MM-DD)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generar y enviar el reporte de perfiles verificados con opciones de filtrado';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            // Obtener opciones del usuario
            $startDate = $this->option('start');
            $endDate = $this->option('end');

            // Si no se pasa --all, pedir fechas al usuario si no las ingresó
            if (!$startDate) {
                $startDate = $this->ask('Ingrese la fecha de inicio (YYYY-MM-DD)', '2025-01-01');
            }
            if (!$endDate) {
                $endDate = $this->ask('Ingrese la fecha de fin (YYYY-MM-DD)', '2025-12-31');
            }

            $fileName = 'Reporte_Perfiles_' . now()->format('d_m_Y') . '.xlsx';
            $filePath = storage_path('app/' . $fileName);

            // Generar el archivo Excel
            Excel::store(new ProfilesReportExport($startDate, $endDate), $fileName);

            // Lista de destinatarios
            $recipients = [
                'andres.cabeza@idrd.gov.co',
                'carmen.vergara@idrd.gov.co',
                'rosanna.mancilla@idrd.gov.co',
                'daniel.forero@idrd.gov.co'
            ];

            // Enviar la notificación a cada destinatario
            foreach ($recipients as $email) {
                $notifiable = new class($email) {
                    use Notifiable;

                    protected $email;

                    public function __construct($email)
                    {
                        $this->email = $email;
                    }

                    public function routeNotificationForMail()
                    {
                        return $this->email;
                    }
                };

                $notifiable->notify(new SendProfilesReportNotification($filePath, $fileName));
            }

            $this->info("Reporte generado y enviado exitosamente: {$fileName}");
        } catch (\Exception $e) {
            $this->error("Error generando o enviando el reporte: " . $e->getMessage());
        }

        return 0;
    }
}
