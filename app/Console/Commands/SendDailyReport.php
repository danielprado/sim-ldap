<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Exports\BookingReportExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Notifications\SendDailyReportNotification;
use Illuminate\Notifications\Notifiable;

class SendDailyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:send-daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate and send daily booking report';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $fileName = 'Reporte_Reservas_' . now()->format('d_m_Y') . '.xlsx';
            $filePath = storage_path('app/' . $fileName);

            // Generar el archivo Excel
            Excel::store(new BookingReportExport, $fileName);

            // Lista de destinatarios
            $recipients = [
                'andres.cabeza@idrd.gov.co',
                'carmen.vergara@idrd.gov.co',
                'rosanna.mancilla@idrd.gov.co',
                'daniel.forero@idrd.gov.co'
            ];

            // Enviar la notificación a cada destinatario
            foreach ($recipients as $email) {
                $notifiable = new class($email) {
                    use Notifiable;

                    protected $email;

                    public function __construct($email)
                    {
                        $this->email = $email;
                    }

                    public function routeNotificationForMail()
                    {
                        return $this->email;
                    }
                };

                $notifiable->notify(new SendDailyReportNotification($filePath, $fileName));
            }

            $this->info("Report generated and sent successfully to all recipients: {$fileName}");
        } catch (\Exception $e) {
            $this->error("Error generating or sending report: " . $e->getMessage());
        }

        return 0;
    }
}
