<?php

namespace App\Helpers;

use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ExcelEvents
{
    /**
     * @param AfterSheet $event
     * @param string $title
     * @param string $cell Cell to merge header E,g, T
     * @param int $rows Cells to insert before E,g 5
     * @return void
     * @throws Exception
     */
    static function header(AfterSheet $event, string $title, string $cell, int $rows) {
        $event->sheet->getDelegate()->insertNewRowBefore(1, $rows);
        $event->sheet->getDelegate()->mergeCells("A1:{$cell}1");
        $event->sheet->getDelegate()->getCell("A1")
            ->setValue($title)
            ->getStyle()
            ->getFont()
            ->setSize(24)
            ->setBold(true);
        $event->sheet->getDelegate()->getCell("A1")
            ->getStyle()
            ->getAlignment()
            ->setVertical(Alignment::HORIZONTAL_CENTER)
            ->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(30);
        $event->sheet->getDelegate()->getCell("A3")
            ->setValue("GENERADO POR:")
            ->getStyle()
            ->getFont()
            ->setSize(14)
            ->setBold(true);

        $event->sheet->getDelegate()->getCell("B3")
            ->setValue("SISTEMA PORTAL CIUDADANO")
            ->getStyle()
            ->getFont()
            ->setSize(14)
            ->setBold(true);

        $event->sheet->getDelegate()->getCell("A4")
            ->setValue("FECHA:")
            ->getStyle()
            ->getFont()
            ->setSize(14)
            ->setBold(true);
        $event->sheet->getDelegate()->getCell("B4")
            ->setValue( date_time_to_excel(now()) )
            ->getStyle()
            ->getFont()
            ->setSize(14)
            ->setBold(true);
        $event->sheet->getDelegate()->getCell("B4")
            ->getStyle()
            ->getAlignment()
            ->setVertical(Alignment::HORIZONTAL_CENTER)
            ->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $event->sheet->getDelegate()->getCell("B4")
            ->getStyle()
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4);
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ],
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
        ];
        foreach (range('A', $cell) as $col) {
            $event->sheet->getDelegate()->getColumnDimension($col)->setAutoSize(true);
        }
        $row = $event->sheet->getDelegate()->getHighestDataRow($cell);
        $target = $rows + 1;
        $event->sheet->getDelegate()->getStyle("A{$target}:{$cell}{$row}")
            ->applyFromArray($styleArray);
        $cells = "A{$target}:{$cell}{$target}";
        $event->sheet->getDelegate()
            ->getStyle($cells)
            ->getFont()
            ->setSize(14)
            ->setBold(true)
            ->getColor()
            ->setRGB('FFFFFF');
        $event->sheet->getDelegate()->getRowDimension(6)->setRowHeight(50);
        $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(100);
        $event->sheet->getDelegate()->getStyle($cells)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setRGB('594d95');
    }
}
