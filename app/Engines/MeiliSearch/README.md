# Sincronizar MeiliSearch
Este comando realiza la sincronización o adición de datos al motor de búsqueda.

```sh
$ php artisan scout:import "App\Entities\Parks\Park"
$ php artisan scout:import "App\Entities\Parks\Grill"
$ php artisan scout:import "App\Entities\Parks\Synthetic"
$ php artisan scout:import "App\Entities\Parks\ParkEndowment"
$ php artisan scout:import "App\Entities\CitizenPortal\ScheduleView"
```

En cada modelo que se listó a anteriormente encontramos un trait implementado y una función que nos
indica los datos que se van a indexar y su estructura.
```php
use Laravel\Scout\Searchable;

class ModelName extends Model {

    use Searchable;
    //...
    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id'  =>  $this->code,  
            'name'  =>  $this->name,  
        ];
    }
    //...
}
```
# Añadir Atributos para filtros
Una vez realizada la importación de los datos, es necesario indicar qué atributos
serán utilizados para realizar los respectivos filtro de búsqueda.
```
// ScheduleView
// https://docker.idrd.gov.co:7701/indexes/schedules_index/settings
{
    "filterableAttributes": [
        "park_id",
        "park_code",
        "quota",
        "final_date",
        "is_initiate",
        "is_activated",
        "min_age",
        "max_age",
        "is_paid"
    ]
}
// Synthetic & Grill
// https://docker.idrd.gov.co:7701/indexes/booking_index/settings
{
    "filterableAttributes": [
        "park_id",
        "park_code",
        "upz_id",
        "upz_code",
        "locality_id",
        "tag"
    ]
}
// Park
// https://docker.idrd.gov.co:7701/indexes/parks_index/settings
{
    "filterableAttributes": [
        "id",  
        "code",
        "locality_id",
        "upz_code",
        "scale_id"
    ]
}
// ParkEndowment
// https://docker.idrd.gov.co:7701/indexes/endowment_index/settings
{
    "filterableAttributes": [
        "park_id",
        "park_code",
        "upz_id",
        "upz_code"
    ]
}
```
