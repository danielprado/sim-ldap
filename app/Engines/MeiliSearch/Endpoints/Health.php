<?php

declare(strict_types=1);

namespace App\Engines\MeiliSearch\Endpoints;

use App\Engines\MeiliSearch\Contracts\Endpoint;

class Health extends Endpoint
{
    protected const PATH = '/health';
}
