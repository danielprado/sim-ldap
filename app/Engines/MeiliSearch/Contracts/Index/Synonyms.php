<?php

declare(strict_types=1);

namespace App\Engines\MeiliSearch\Contracts\Index;

use App\Engines\MeiliSearch\Contracts\Data;

class Synonyms extends Data implements \JsonSerializable
{
    public function jsonSerialize()
    {
        return (object) $this->getIterator()->getArrayCopy();
    }
}
