<?php

declare(strict_types=1);

namespace App\Engines\MeiliSearch\Contracts\Index;

use App\Engines\MeiliSearch\Contracts\Data;

class Settings extends Data implements \JsonSerializable
{
    public function __construct(array $data = [])
    {
        $data['synonyms'] = new Synonyms($data['synonyms'] ?? []);
        parent::__construct($data);
    }

    public function jsonSerialize()
    {
        return $this->getIterator()->getArrayCopy();
    }
}
