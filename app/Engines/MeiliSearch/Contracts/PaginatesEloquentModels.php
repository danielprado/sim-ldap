<?php

namespace App\Engines\MeiliSearch\Contracts;

use App\Engines\MeiliSearch\Builder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Paginator;

interface PaginatesEloquentModels
{
    /**
     * Paginate the given search on the engine.
     *
     * @param Builder $builder
     * @param int $perPage
     * @param int $page
     * @return LengthAwarePaginator
     */
    public function paginate(Builder $builder, $perPage, $page);

    /**
     * Paginate the given search on the engine using simple pagination.
     *
     * @param Builder $builder
     * @param int $perPage
     * @param int $page
     * @return Paginator
     */
    public function simplePaginate(Builder $builder, $perPage, $page);
}
