<?php

declare(strict_types=1);

namespace App\Engines\MeiliSearch;

use App\Engines\MeiliSearch\Delegates\HandlesIndex;
use App\Engines\MeiliSearch\Delegates\HandlesSystem;
use App\Engines\MeiliSearch\Endpoints\Delegates\HandlesDumps;
use App\Engines\MeiliSearch\Endpoints\Delegates\HandlesKeys;
use App\Engines\MeiliSearch\Endpoints\Delegates\HandlesTasks;
use App\Engines\MeiliSearch\Endpoints\Dumps;
use App\Engines\MeiliSearch\Endpoints\Health;
use App\Engines\MeiliSearch\Endpoints\Indexes;
use App\Engines\MeiliSearch\Endpoints\Keys;
use App\Engines\MeiliSearch\Endpoints\Stats;
use App\Engines\MeiliSearch\Endpoints\Tasks;
use App\Engines\MeiliSearch\Endpoints\TenantToken;
use App\Engines\MeiliSearch\Endpoints\Version;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;

class Client
{
    use HandlesDumps;
    use HandlesIndex;
    use HandlesTasks;
    use HandlesKeys;
    use HandlesSystem;

    private $http;
    /**
     * @var Indexes
     */
    private $index;
    /**
     * @var Health
     */
    private $health;
    /**
     * @var Version
     */
    private $version;
    /**
     * @var Keys
     */
    private $keys;
    /**
     * @var Stats
     */
    private $stats;
    /**
     * @var Tasks
     */
    private $tasks;
    /**
     * @var Dumps
     */
    private $dumps;
    /**
     * @var TenantToken
     */
    private $tenantToken;

    public function __construct(
        string $url,
        string $apiKey = null,
        ClientInterface $httpClient = null,
        RequestFactoryInterface $requestFactory = null
    ) {
        $this->http = new Http\Client($url, $apiKey, $httpClient, $requestFactory);
        $this->index = new Indexes($this->http);
        $this->health = new Health($this->http);
        $this->version = new Version($this->http);
        $this->stats = new Stats($this->http);
        $this->tasks = new Tasks($this->http);
        $this->keys = new Keys($this->http);
        $this->dumps = new Dumps($this->http);
        $this->tenantToken = new TenantToken($this->http, $apiKey);
    }
}
