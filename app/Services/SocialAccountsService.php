<?php

namespace App\Services;

use App\Entities\CitizenPortal\Profile;
use App\Entities\Security\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Two\User as ProviderUser;
use League\OAuth2\Server\Exception\OAuthServerException;

class SocialAccountsService
{
    /**
     * Find or create user instance by provider user instance and provider name.
     *
     * @param ProviderUser $providerUser
     * @param string $provider
     *
     * @return User
     * @throws \Exception
     */
    public function findOrCreate(ProviderUser $providerUser, string $provider)
    {
        try {
            DB::beginTransaction();
            $socialAccount = User::whereHas('socialAccounts', function ($query) use ($providerUser, $provider) {
                                    return $query->where('provider_id', $providerUser->getId())
                                            ->where('provider_name', $provider);
                                })->first();
            if ($socialAccount) {
                DB::commit();
                return $socialAccount;
            }

            /**
             * We require user email from social provider only during sign-up.
             * We can allow user to login even if provider didn't send any email.
             */
            $this->ensureHasEmailAddress($provider, $providerUser);

            /**
             * Let's try to find the user by email address send by provider.
             *
             * @var User $user
             */
            $user = User::where('email', $providerUser->getEmail())->first();

            /**
             * User not found so let's persist it.
             */
            if (!$user) {
                $user = new User();
                $user->fill([
                    'name' => toUpper($providerUser->getName()),
                    'password' => Hash::make(Str::random(30)),
                    'email' => $providerUser->getEmail(),
                ]);
                $user->save();
                $user->markEmailAsVerified();
                $names = explode(' ', toUpper(preg_replace('!\s+!', ' ', $providerUser->getName())) );
                $data = [
                    'profile_type_id' => Profile::PROFILE_PERSONAL,
                    'name' => $names[0] ?? null,
                    's_name' => null,
                    'surname' => null,
                    's_surname' => null,
                ];
                if (count($names) > 2) {
                    $data['s_name'] = $names[1] ?? null;
                    $data['surname'] = $names[2] ?? null;
                    $data['s_surname'] = $names[3] ?? null;
                } else {
                    $data['surname'] = $names[1] ?? null;
                }
                $user->profiles()->save(new Profile($data));
                event(new \Illuminate\Auth\Events\Registered($user));
            }
            $user->socialAccounts()->updateOrCreate(
                [
                    'provider_id' => $providerUser->getId(),
                    'provider_name' => $provider,
                ],
                [
                    'provider_id' => $providerUser->getId(),
                    'provider_name' => $provider,
                ]
            );
            DB::commit();
            return $user;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new OAuthServerException(
                $exception->getMessage(),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                "social_service_exception",
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Make sure provider sent an email address in response.
     *
     * @param  string  $providerName
     * @param  ProviderUser  $providerUser
     *
     * @return void
     * @throws OAuthServerException
     */
    protected function ensureHasEmailAddress(string $providerName, ProviderUser $providerUser)
    {
        if (empty($providerUser->getEmail())) {
            throw new OAuthServerException(
                sprintf("Could not retrieve email address from %s.", ucfirst($providerName)),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                "inaccessible_email_address",
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
