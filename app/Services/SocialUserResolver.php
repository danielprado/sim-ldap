<?php
namespace App\Services;
use Exception;
use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Response;
use Laravel\Socialite\Facades\Socialite;
use League\OAuth2\Server\Exception\OAuthServerException;

class SocialUserResolver implements SocialUserResolverInterface
{
    /**
     * Resolve user by provider credentials.
     *
     * @param  string  $provider
     * @param  string  $accessToken
     *
     * @return Authenticatable|null
     * @throws \Throwable
     *
     */
    public function resolveUserByProviderCredentials(string $provider, string $accessToken): ?Authenticatable
    {
        $this->validateProviderName($provider);

        try {
            $providerUser = Socialite::driver($provider)->userFromToken($accessToken);
        } catch (\Throwable $exception) {
            // Send actual error message in development
            if (config('app.debug')) {
                throw  new OAuthServerException(
                    $exception->getMessage(),
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    "unsupported_social_provider",
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
            return null;
        }

        if ($providerUser) {
            return (new SocialAccountsService())->findOrCreate($providerUser, $provider);
        }
        return null;
    }

    /**
     * Make sure client has provided a valid provider name.
     *
     * @param  string  $provider
     *
     * @throws OAuthServerException
     */
    protected function validateProviderName($provider)
    {
        if (!in_array(strtolower($provider), $this->getValidProviders(), true)) {
            throw new OAuthServerException(
                sprintf("%s provider is not supported.", ucfirst($provider)),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                "unsupported_social_provider",
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    protected function getValidProviders()
    {
        return ['google', 'facebook'];
    }
}
