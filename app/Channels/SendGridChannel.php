<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use SendGrid\Mail\Mail;

class SendGridChannel
{
    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $template = $notification->toMail($notifiable);
        $mailer =  new Mail();
        $mailer->setFrom(
            config("mail.sendgrid.from"),
            config("mail.sendgrid.name")
        );
        $mailer->setSubject($template->subject);
        if (method_exists($notifiable, 'getEmailForPasswordReset')) {
            $mailer->addTo($notifiable->getEmailForPasswordReset());
        } else {
            // Si no existe, usar routeNotificationForMail como alternativa
            $mailer->addTo($notifiable->routeNotificationForMail());
        }
        $mailer->addContent("text/html", (string) $template->render());
        // Verificar si la notificación tiene un archivo adjunto
        if (method_exists($notification, 'getFilePath') && method_exists($notification, 'getFileName')) {
            $filePath = $notification->getFilePath();
            $fileName = $notification->getFileName();

            if ($filePath && $fileName) {
                $fileContent = base64_encode(file_get_contents($filePath));
                $mailer->addAttachment(
                    $fileContent,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    $fileName
                );
            }
        }
        $sendgrid = new \SendGrid(config("mail.sendgrid.api"));
        $sendgrid->send($mailer);
        /*
        Log::debug(
            "SENDGRID - {$response->statusCode()} - {$response->body()}"
        );
        */
    }
}
