<?php

namespace App\Providers;

use App\Engines\MeiliSearch\Client;
use App\Engines\MeiliSearch\MeiliSearchEngine;
use App\Entities\Security\User;
use App\Entities\World\City;
use App\Entities\World\Country;
use App\Entities\World\State;
use App\Services\SocialUserResolver;
use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;
use Laravel\Scout\EngineManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        SocialUserResolverInterface::class => SocialUserResolver::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Passport::ignoreMigrations();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Prevent MySQL key length error
         */
        Schema::defaultStringLength(191);
        $_SERVER["SERVER_NAME"] = "sim.idrd.gov.co";
        /**
         * Custom Polymorphic Types
         */
        Relation::morphMap([
            //Security
            'users'         =>      User::class,
            //World Data
            'countries'     =>      Country::class,
            'states'        =>      State::class,
            'city'          =>      City::class,
        ]);

        resolve(EngineManager::class)->extend('meilisearch', function () {
            return new MeiliSearchEngine(
                new Client(
                    config('scout.meilisearch.host'),
                    config('scout.meilisearch.key')
                ),
                config('scout.soft_delete', false)
            );
        });
    }
}
