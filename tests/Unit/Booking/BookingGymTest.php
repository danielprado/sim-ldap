<?php

namespace Tests\Unit\Booking;


use Tests\TestCase;
use App\Entities\Security\User;
use App\Entities\Payments\Booking;
use App\Entities\Payments\Payment;
use Illuminate\Support\Facades\DB;
use App\Entities\Payments\Schedule;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\UserResource;
use App\Entities\Parks\ParkEndowment;
use App\Entities\CitizenPortal\Profile;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Requests\Payments\PaymentRequest;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\Payments\PaymentController;

class BookingGymTest extends TestCase
{
    // const ID_DOTACION = 17598; de gimnasio
    const ID_DOTACION = 17598;
    const CUPO_MAXIMO = 15;

    const HORARIOS = [
        ['horai' => '08:00', 'horaf' => '9:00'],
            ['horai' => '10:00', 'horaf' => '11:00'],
            ['horai' => '14:00', 'horaf' => '15:00'],
            ['horai' => '16:00', 'horaf' => '17:00'],
            ['horai' => '18:00', 'horaf' => '19:00'],
    ]; 
    /**
     * Set Up Method
     * setUp method is called before a test is executed.
     */
    public function setUp(): void
    {
        parent::setUp();

        // Crear Horarios Para la Dotación 17598 que pertenece a un gimnasio    
        self::createSchedulesForDotacion(self::ID_DOTACION);

        // Iniciar sesión (en algunas funciones de reserva se guarda el id, por tanto se debe iniciar sesión)
        $user = User::where('email', 'felipecabezas98@gmail.com')->first(); // Encontrar el usuario
        $this->actingAs($user, 'api'); // Simulate authentication as the user

    }

    /**
     * Tear Down Method
     * tearDown method is called after a test is executed.
     */
    public function tearDown(): void
    {

        // Eliminar Horarios de la Dotación -- Force Delete para evitar el soft delete del último minuto
        self::limpiarReservasYPagos(self::ID_DOTACION);

        parent::tearDown();
    }


    public static function limpiarReservasYPagos($idDotacion): void
{

    $idDotacion = is_array($idDotacion) ? $idDotacion : [$idDotacion];

    // Lógica de limpieza
    Schedule::whereIn('id_dotacion', $idDotacion)
        ->where('created_at', '>', now()->subMinutes(1))
        ->forceDelete();

    Booking::where('created_at', '>', now()->subMinutes(5))
        ->forceDelete();

    Payment::where('created_at', '>', now()->subMinutes(5))
        ->forceDelete();
}

    /**
     * Create schedules for a specific dotacion.
     *
     * @param int $idDotacion
     * @return void
     */
    public static function createSchedulesForDotacion($idDotacion)
    {
        

        $dias = [1, 2, 3, 4, 5, 6, 7];

        foreach ($dias as $dia) {
            foreach (self::HORARIOS as $horario) {
                $exists = Schedule::where('id_dotacion', $idDotacion)
                    ->where('dia', $dia)
                    ->where('horai', $horario['horai'])
                    ->where('horaf', $horario['horaf'])
                    ->exists();

                if (!$exists) {
                    $schedule = new Schedule([
                        'id_dotacion' => $idDotacion,
                        'dia' => $dia,
                        'horai' => $horario['horai'],
                        'horaf' => $horario['horaf'],
                    ]);
                    $schedule->save();
                }
            }
        }
    }

    public static function crearPago($responseJSON)
    {
        try {
            // Convertir el JSON a un arreglo asociativo
            $responseData = json_decode($responseJSON, true);

            // Extraer los valores principales
            $data = $responseData['data'];

            $name = $data['name'];
            $surname = $data['surname'];
            $documentType = $data['document_type_id'];
            $document = $data['document'];
            $email = $data['email'];
            $phone = $data['phone'];
            $bookingId = $data['booking_id'];
            $amount = $data['amount'];
            $parkId = $data['park_id'];
            $serviceId = $data['service_id'];
            $details = $data['details'];
            $createdAt = $data['created_at'];

            // Extraer valores dentro de "details"
            if ($details) {
                $hours = $details['hours'];
                $unit = $details['unit'];
                $type = $details['type'];
            } else {
                $hours = $unit = $type = null;
            }

            // Guardar el pago en la base de datos
// create md5 unique code
            $md5 = md5($name . $surname . $document . $email . $phone . $amount . $createdAt);
            $payment = new Payment([
                'parque_id' => $parkId,
                'servicio_id' => $serviceId,
                'identificacion' => $document,
                'tipo_identificacion' => $documentType,
                'codigo_pago' => $md5,
                'id_transaccion_pse' => 'TEST-123',
                'email' => $email,
                'nombre' => $name,
                'apellido' => $surname,
                'telefono' => $phone,
                'estado_id' => 2,
                'estado_banco' => 'OK',
                'concepto' => 'Testing',
                'moneda' => 'COP',
                'total' => $amount,
                'iva' => 0,
                'permiso' => '999',
                'tipo_permiso' => 'PO',
                'id_reserva' => $bookingId,
                'fecha_pago' => $createdAt,
                'user_id_pse' => 'TEST-USER-123',
                'medio_id' => 4,
                'email_pagador' => $email,
                'nombre_pagador' => $name,
                'apellido_pagador' => $surname,
                'telefono_pagador' => $phone,
                'identificacion_pagador' => $document,
                'tipo_identificacion_pagador' => $documentType,
            ]);
            $payment->save();


        } catch (\Throwable $th) {
            Log::error($th->getMessage());
        }

    }

    public static function crearReserva($idDotacion, $fecha, $horaInicio, $horaFin)
    { // Crear una reserva para la dotación 17598
        $payload = [
            'date' => $fecha,
            'start_hour' => $horaInicio,
            'final_hour' => $horaFin,
            'id_dotacion' => $idDotacion,
        ];

        // fecha recibida
        Log::info('Fecha recibida: ' . $fecha);
        // Encontrar la dotación de parque
        $dotacionParque = ParkEndowment::find($idDotacion);
        if (!$dotacionParque) {
            $this->fail("No se encontró la dotación con ID " . $idDotacion);
        }

        // Crear una instancia de PaymentRequest
        $request = PaymentRequest::create(
            '/api/parks/schedules/' . $idDotacion . '/payment',
            'POST',
            $payload
        );

        // Instanciar el controlador y llamar al método payment
        $paymentController = new PaymentController();
        $response = $paymentController->payment($dotacionParque, $request);

        // Crear el pago
        // if status code is 200
        if ($response->getStatusCode() == 200) {
            self::crearPago($response->getContent());
        } else {
            Log::info('No se pudo crear el pago');
            Log::info($response);
        }

        return $response;
    }

    /**
     * Crear reserva desde el modelo Booking
     * @return void
     */
    public function testAssertCrearReserva()
    {
        // Crear una reserva para la dotación 17598
        $booking = new Booking([
            'fecha' => now()->toDateString(),
            'hora_inicio' => '10:00',
            'hora_fin' => '11:00',
            'id_dotacion' => self::ID_DOTACION,
            'enviado' => 1,
            'valor' => 0,
            'is_successful' => 1,
            'id' => '1121952226',
        ]);
        $booking->save();

        $this->assertTrue(
            Booking::where('id_dotacion', self::ID_DOTACION)->exists(),
            "La reserva con id_dotacion = " . self::ID_DOTACION . " no existe."
        );
    }



    public function testAssertCrearReservaControlador()
    {

        $response = self::crearReserva(self::ID_DOTACION, now()->toDateString(), '14:00', '15:00');

        $this->assertEquals(200, $response->getStatusCode());

    }

    /**
     * 
     * Falla en otra reserva a la misma hora;
     * realiza el mismo proceso de testAssertCrearReservaControlador
     */
    public function testAssertFallaReservaMismaHora()
    {

        $response = self::crearReserva(self::ID_DOTACION, now()->toDateString(), '16:00', '17:00');
        $this->assertEquals(200, $response->getStatusCode());

        // Falla la creación de la reserva
        $response = self::crearReserva(self::ID_DOTACION, now()->toDateString(), '16:00', '17:00');
        $this->assertEquals(422, $response->getStatusCode());

    }

    /**
     * Test creating multiple reservations at the same time for different users.
     *
     * @return void
     */
    public function testCrearVariasReservasALaMismaHora()
    {
        $cantidadDeReservas = self::CUPO_MAXIMO;

        // Get 5 existing users dynamically
        $users = User::take($cantidadDeReservas)
            ->where('activated', '1')
            ->get();

        // Ensure at least 5 users exist in the database
        if ($users->count() < $cantidadDeReservas) {
            $this->markTestIncomplete('Not enough users in the database for this test.');
        }

        $reserva = 1;
        foreach ($users as $user) {
            // Simulate authentication with each user
            $this->actingAs($user, 'api');


            $us = Profile::query()
                ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                ->where('user_id', auth('api')->user()->id)
                ->first();

            // imprimir el id del usuario
            $fecha = now()->toDateString();

            // Create reservation for each user
            $response = self::crearReserva(self::ID_DOTACION, $fecha, '18:00', '19:00');

            // SI no hay documento en el usuario, se espera un 422
            if (is_null($us->document)) {
                $this->assertEquals(422, $response->getStatusCode());
            } else {
                $this->assertEquals(200, $response->getStatusCode());
                $reserva++;
            }
            // Assert reservation is successfully created
            // print_r($response);
        }
    }



    /**
     * Test creating multiple reservations at the same time for different users.
     *
     * @return void
     */
    public function testSobrepasarCupo()
    {
        $cantidadDeReservas = self::CUPO_MAXIMO + 15;

        // Get 5 existing users dynamically
        $users = User::take($cantidadDeReservas)
            ->where('activated', '1')
            ->get();

        // Ensure at least 5 users exist in the database
        if ($users->count() < $cantidadDeReservas) {
            $this->markTestIncomplete('Not enough users in the database for this test.');
        }
        $reservaActual = 1;

        for ($i = 0; $i < $cantidadDeReservas; $i++) {

            $user = $users[$i];
            // Simulate authentication with each user
            $this->actingAs($user, 'api');


            $us = Profile::query()
                ->where('profile_type_id', Profile::PROFILE_PERSONAL)
                ->where('user_id', auth('api')->user()->id)
                ->first();

            // if document is null, assert 422
            if (!$us->document) {
                $this->assertEquals(422, 422);
            }


            // imprimir el id del usuario
            Log::info('######## Intentando crear Reserva ' . $reservaActual . ' para : documento del usuario: ' . $us->document .
                ' y document del usuario: ' . $us->document .

                ' ########');

            // Create reservation for each user
            $response = self::crearReserva(self::ID_DOTACION, now()->toDateString(), '18:00', '19:00');

            // Assert reservation is successfully created
            Log::info('Creada la reserva numero: ' . $reservaActual);
            Log::info($response);



            if ($reservaActual <= self::CUPO_MAXIMO) {
                if (is_null($us->document)) {
                    $this->assertEquals(422, $response->getStatusCode());
                } else {
                    $this->assertEquals(200, $response->getStatusCode());
                    $reservaActual++;

                }
            } else {
                $this->assertEquals(422, $response->getStatusCode());
            }
        }

    }

    public function testCrearReservaConFechaPasada()
    {
        Log::info('Fecha Pasada: ' . now()->subDay()->toDateString());
        $fechaPasada = now()->subDay()->toDateString();
        $response = self::crearReserva(self::ID_DOTACION, $fechaPasada, '18:00', '19:00');
        Log::info($response);
        $this->assertEquals(422, $response->getStatusCode());

    }



}