<?php

namespace Tests\Unit\Booking;

use Tests\TestCase;
use Tests\Unit\Booking\BookingGymTest;
use App\Entities\Security\User;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Log;

class PaymentRequestTest extends TestCase
{
    // ID DOTACIONES: 2 GIMNASIOS, 2 CANCHAS Sintéticas , 1 de tenis
    const ID_DOTACIONES = [17598, 17167, 11294, 11311, 4572, 11404];
    /**
     * Set Up Method
     * setUp method is called before a test is executed.
     */
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();

        // Crear Horarios Para la Dotación 17598 que pertenece a un gimnasio

        // for each dotaciones
        foreach (self::ID_DOTACIONES as $id) {
            BookingGymTest::createSchedulesForDotacion($id);
        }

        // Iniciar sesión (en algunas funciones de reserva se guarda el id, por tanto se debe iniciar sesión)
        $user = User::where('email', 'felipecabezas98@gmail.com')->first(); // Encontrar el usuario
        $this->actingAs($user, 'api'); // Simulate authentication as the user

    }

    /**
     * Tear Down Method
     * tearDown method is called after a test is executed.
     */
    public function tearDown(): void
    {

        foreach (self::ID_DOTACIONES as $id) {
            BookingGymTest::limpiarReservasYPagos($id);
        }
        parent::tearDown();
    }


    public function testAssertCrearDosReservasALaMismaHora()
    {

        // Crear una reserva para la dotación 17598
        $response = BookingGymTest::crearReserva(
            self::ID_DOTACIONES[0],
            now()->toDateString(),
            '10:00',
            '11:00'
        );


        $this->assertEquals(200, $response->getStatusCode());




        $response = BookingGymTest::crearReserva(
            self::ID_DOTACIONES[1],
            now()->toDateString(),
            '10:00',
            '11:00'
        );

        /** MISMA HORA; DEBERIA FALLAR */

        $this->assertEquals(422, $response->getStatusCode());


    }

    // Crear reservas con tipos de servicios diferentes; aqui se hace la petición por POST para probar el PaymentRequest

    public function testAssertCrearLimiteReservasDiferentesServicios()
    {
        // Start index = 0
        $startIndex = 0;

        for ($i = $startIndex; $i < count(self::ID_DOTACIONES); $i++) {
            // Construcción de la URL con el ID de dotación dinámico
            $url = "/api/parks/schedules/" . self::ID_DOTACIONES[$i] . "/payment";

            // Datos del cuerpo de la solicitud
            $body = [
                'date'       => now()->addDays(2)->toDateString(),
                'start_hour' => '10:00 AM',
                'final_hour' => '11:00 AM',
            ];

            // Realizar la solicitud POST como si fuera desde un cliente HTTP
            $response = $this->postJson($url, $body);

            // Log de la respuesta
            Log::info("Response for dotation ID: " . self::ID_DOTACIONES[$i]);
            Log::info($response->json());

            // Validación de la respuesta
            if ($startIndex == $i) {
                Log::info('Reserva Exitosa');
                $response->assertStatus(200);

                // Crear Pago si la reserva es exitosa
                $responseData = $response->json();  // Obtener el contenido de la respuesta
                if ($responseData && isset($responseData['data'])) {
                    BookingGymTest::crearPago(json_encode($responseData));
                    Log::info('Pago creado exitosamente');
                } else {
                    Log::error('No se pudo crear el pago, respuesta incompleta.');
                }
            } else {
                Log::info('Reserva Fallida');
                $response->assertStatus(422);
            }
        }
    }


    /**
     * Pasar el límite de reservas por servicio
     */
    public function testAssertSuperarLimiteReservasMismoServicioGimnasio()
    {

        BookingGymTest::limpiarReservasYPagos(self::ID_DOTACIONES[0]);
        // Gimnasios
        // 15227
        // 17167
        // 17598
        $gimnasios = [15227, 17167, 17598];

        // Crear 1 reserva para cada gimnasio, en diferentes horarios
        foreach ($gimnasios as $index => $value) {
            $response = BookingGymTest::crearReserva(
                $value,
                now()->addDays($index+1)->toDateString(),
                BookingGymTest::HORARIOS[$index]['horai'],
                BookingGymTest::HORARIOS[$index]['horaf']
            );
            // si indice es == 2, fallar
            if ($index == 2) {
                $this->assertEquals(422, $response->getStatusCode());
            } else {
                $this->assertEquals(200, $response->getStatusCode());
            }
        }

    }



}
