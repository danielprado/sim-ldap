<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Park Modules Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during park module access for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'excel' => [
        'matches' => 'Se encontró una coincidencia|Se encontraron :matches coincidencias de un total de :rows datos procesados del archivo cargado.',
        'row' => ':attribute en la fila :row',
    ],

    "observation" => [
        "title" => 'Tienes :pending observación por leer de :total. | Tienes :pending observaciones por leer de :total.',
    ],

    'validations' => [
        'relationship' => 'parentesco',
        'file' => 'archivo',
        'profile_type' => 'tipo de perfil',
        'document_type' => 'tipo de documento',
        'document' => 'documento',
        'sex' => 'sexo',
        'birthdate' => 'fecha de nacimiento',
        'blood_type' => 'grupo sanguíneo',
        'country_birth' => 'país de nacimiento',
        'state_birth' => 'departamento de nacimiento',
        'city_birth' => 'ciudad de nacimiento',
        'country_residence' => 'país de residencia',
        'state_residence' => 'departamento de residencia',
        'city_residence' => 'ciudad de residencia',
        'locality' => 'localidad',
        'upz' => 'upz',
        'neighborhood' => 'barrio',
        'other_neighborhood_name' => 'otro nombre del barrio',
        'stratum' => 'estrato',
        'ethnic_group' => 'grupo étnico',
        'population_group' => 'grupo poblacional',
        'gender' => 'género',
        'sexual_orientation' => 'orientació sexual',
        'eps' => 'eps',
        'has_disability' => 'tiene discapacidad',
        'disability' => 'discapacidad',
        'contact_name' => 'nombre del contácto',
        'contact_phone' => 'teléfono del contácto',
        'contact_relationship' => 'parentesco del contácto',
        'assignor_name' => 'nombre asignador',
        'assignor_document' => 'documento asignador',
        'assigned_at' => 'fecha de asignación',
        'checker_name' => 'nombre verificador',
        'checker_document' => 'documento verificador',
        'status' => 'estado',
        'status_citizen_schedule' => 'estado de inscripción',
        'full_name' => 'nombre',
        'name' => 'primer nombre',
        's_name' => 'segundo nombre',
        'surname' => 'primer apellido',
        's_surname' => 'segundo apellido',
        'program' => 'programa',
        'activity' => 'actividad',
        'stage' => 'escenario',
        'park_code' => 'codigo del parque',
        'park' => 'parque',
        'address' => 'dirección',
        'weekday' => 'día(s) de la semana',
        'daily' => 'horas',
        'min_age' => 'edad mínima',
        'max_age' => 'edad máxima',
        'quota' => 'cupos',
        'is_paid' => 'es pago',
        'is_initiate' => 'es iniciación',
        'start_date' => 'fecha de apertura de inscripción',
        'final_date' => 'fecha de cierre de inscripción',
        'start_date_filter' => 'fecha inicial de registro',
        'final_date_filter' => 'fecha final de registro',
        'is_activated' => 'está activo',
        'users_schedules_count' => 'usuarios inscritos',
        'created_at' => 'fecha de creación',
        'updated_at' => 'fecha de actualización',
        'actions' => 'acciones',
        'event' => 'evento',
        'type_trans' => 'tipo',
        'user' => 'usuario',
        'tags' => 'etiquetas',
        'url' => 'url',
        'age' => 'edad',
        'validator' => 'validador',
        'user_agent' => 'agente de usuario',
        'new_values' => 'valores nuevos',
        'old_values' => 'valores anteriores',
        'email' => 'email',
        'phone' => 'teléfono',
        'observations_count' => 'cantidad de observaciones',
        'files_count' => 'cantidad de archivos',
        'pending_files_count' => 'cantidad de archivos pendientes por validación',
        'verified_at' => 'verificado',
        'citizen_portal' => 'portal ciudadano',
        'generated_by' => 'generado por',
        'period' => 'periodo',
        'period_dates' => 'del :start_date al :final_date',
    ],

    "privacy" => [
        "consent" => [
            "adult" => [
                "title" => "## Consentimiento Informado Mayores de Edad",
                "text" =>
                    "Yo, **:user**, identificado con el documento de identidad Nº **:document** entiendo y acepto que
                    con el diligenciamiento de este formulario debo acatar
                    los requisitos y recomendaciones establecidas para la actividad a realizar, cumpliendo lo dispuesto
                    en el 2º párrafo del artículo 2º de la Resolución 1840 de 2020 “Por medio de la cual se adopta el
                    protocolo de bioseguridad para el manejo del riesgo del coronavirus COVID – 19 para la práctica de
                    actividad física, recreativa y deportiva”, emitida por el Ministerio de Salud y Protección Social,
                    exigible a cada usuario, deportista, entrenador, padre de familia o cuidador y personal de apoyo,
                    por medio del presente otorgo el consentimiento informado para el reinicio de la práctica, manifiesto
                    encontrarme sano, sin signos o síntomas sospechosos de COVID – 19 y me comprometo a respetar de
                    manera estricta las normas de bioseguridad y distanciamiento social para la actividad a realizar. <br>
                    Así mismo, asumo la responsabilidad del riesgo de contagio que conllevan los desplazamientos hacia
                    los escenarios del IDRD para entrenamiento, servicios de alimentos y Unidad de Ciencias Aplicadas al
                    Deporte, para mí, mi acompañante y mi familia. <br>
                    Me han sido explicados todos los riesgos adicionales a que me expongo al realizar esta práctica
                    deportiva y, por lo tanto, me comprometo a comunicar inmediatamente si tengo algún síntoma de alarma
                    concordante con el cuadro propio del COVID 19 e iniciar el aislamiento preventivo de rigor. <br>
                    El hecho de haber tenido una exposición previa al virus, no me libera de una reinfección, por lo que
                    me comprometo a cumplir con todos los protocolos de seguridad y toma de las pruebas seriadas. <br>
                    Soy consciente de que, pese al cumplimiento de todos los protocolos de seguridad, existe una
                    probabilidad de contagio del COVID 19. <br>
                    Cabe señalar que a pesar de las pruebas que me hayan realizado previamente y que no tenga síntomas,
                    tengo la posibilidad de ser portador asintomático de la enfermedad y que los riesgos asociados a la
                    exposición de COVID 19 son: muerte, síndrome de dificultad respiratoria, coagulopatias, eventos
                    tromboticos severos, otras manifestaciones como eventos respiratorios, gastrointestinales,
                    neurológicos relacionado con el COVID 19 y en general aumento de probabilidad de las complicaciones
                    de enfermedades preexistentes. <br>
                    Realizo reconocimiento de firma, contenido, expreso conocer, aceptar y acatar los lineamientos para
                    la reactivación de la actividad a realizar y protocolos de bioseguridad de estas actividades.
                    "
            ]
        ],
        "terms" => [
            "title" => "#### TÉRMINOS Y CONDICIONES DE SERVICIOS\n #### INSTITUTO DISTRITAL DE RECREACIÓN Y DEPORTE",
            "text" => file_get_contents(resource_path("lang/es/terms_and_cond.md"))
        ],
    ],

    "booking" => [
        "requirements_field" => implode("\n", [
            "## Normas de uso en los parques con campo de grama sintética o natural:",
            "",
            "* Ingresar siempre a la cancha por las puertas de acceso habilitadas por la administración.",
            "* El ingreso a la cancha de los jugadores debe realizarse de acuerdo con las dimensiones de la misma Futbol 5, Futbol 8 o Futbol 11, esto para no subutilizar la grama y gastar el terreno.",
            "* Al escenario deberán ingresar únicamente las personas que harán parte de la actividad deportiva y/o recreativa, no está autorizado el ingreso de acompañantes para evitar accidentes o situaciones que afecten la percepción de seguridad.",
            "* No se debe jugar mientras este lloviendo o con tormentas eléctricas o cuando la cancha se encuentre demasiado húmeda, esto para evitar accidentes.",
            "* No es permitido ingreso de bicicletas.",
            "* No consumir alimentos dentro del escenario.",
            "* Los usuarios deben hacer uso adecuado del espacio, Según las normas de convivencia básicas como: no consumir licor o cigarrillos, no ingresar mascotas y no ingresar ningún tipo de envases o recipientes de vidrio que causen accidentes, de la misma manera no arrojar basura ni dentro ni fuera de la cancha. No es permitido el ingreso de animales de compañía.",
            "* No se permite el uso de guayos con taches de aluminio, se recomienda el uso de tenis tipo torretin.",
            "* No mover los arcos para evitar daños en la grama y tampoco subirse en ellos para evitar accidentes, los mismos deben permanecer fijos con una guaya.",
            "* El espacio debe ser entregado en los tiempos acordados, es pilar fundamental el respeto para fortalecer las normas sociales establecidas en cada uno de los campos sintéticos.",
            "* No se deben dañar, saltar, empujar o cargar el cerramiento perimetral.",
            "* Los préstamos de canchas no se pueden realizar en fracciones de hora. Por este motivo es necesario ingresar la hora inicio y final sin minutos.",
            "* No se debe permitir que se instalen ningún tipo de elementos que generen daño en las mismas, equipos pesados, tarimas, elementos de anclajes, carpas o asadores.",
            "* Abecé de las canchas sintéticas [ver aquí](https://www.idrd.gov.co/sites/default/files/documentos/parques/abc-de-las-canchas-sinteticas.pdf).",
            "",
            "## Recomendaciones de uso en los campos de grama sintética o natural", // obligaciones
            "",
            "* El espacio público debe ser utilizado únicamente para el desarrollo de la actividad autorizada en las fechas y horarios o período asignado",
            "* No se realizará devolución de dinero por inasistencia a la reserva solicitada. Debe tener en cuenta que las reprogramaciones se darán únicamente si el no uso se da por alguna de las siguientes situaciones: lluvias con tormenta eléctrica, programaciones institucionales, mantenimientos y/o novedades de orden público.",
            "* El beneficiario de la reserva deberá estar presente durante el desarrolló de la actividad y será el responsable del escenario.",
            "* El campo sintético puede ser usado para cualquier actividad recreativa y deportiva, siempre y cuando la misma no afecte la grama, ni la infraestructura del escenario",
            "* Realizar el pago de la liquidación por anticipado y confirmar con la Administración del Parque o Escenario, el recibido de la consignación o comprobante que acredita el pago de la obligación económica definida por el IDRD en la reserva.",
            "* Recibir el escenario y/o espacio asignado, durante cada fecha (según corresponda), bajo inventario y entregar en las mismas condiciones en que lo recibió, como en orden y aseo.",
            "* Permitir al funcionario del Instituto, realizar las inspecciones pertinentes y relacionadas con el cumplimiento de las condiciones del permiso.",
            "* Responder por los daños causados a las instalaciones, elementos, implementos y/o estructuras que integran el Parque o Escenario. En tal caso, la reparación, reposición y/o valor equivalente deberá ser asumido por el beneficiario del permiso - sin perjuicio de su facultad de exigir a los responsables, la reparación o indemnización de los daños causados, cuando a ello haya lugar, en un término máximo 15 días hábiles contados a partir de la fecha del acta de recibo.",
            "* Respetar la autonomía del IDRD, como entidad administradora del espacio público objeto de los Parques o Escenarios, sobre las distintas instalaciones y espacios a utilizar.",
            "* El usuario podrá reservar máximo 2 horas continuas por día y un total de máximo 4 horas a la semana en el sistema distrital de parques.",
            "* Implementar las medidas de seguridad que sean necesarias y que garanticen la preservación del espacio asignado y la integridad física de las personas que sean usuarias del servicio y/o que participen o asistan bajo cualquier calidad a la actividad objeto de esta habilitación, respondiendo por los perjuicios materiales y morales que se ocasionen en desarrollo de las diferentes actividades objeto de la presente autorización del uso del espacio público.",
            "* Acatar las medidas que sugiera y disponga la administración del parque o escenario, según sea el caso en materia de uso, distribución del espacio, carga – tráfico o número de participantes de las prácticas deportivas o actividades objeto del permiso, factores climáticos que afecten el desarrollo de estas o generen riesgo.",
            "* El cuidado de los elementos personales es competencia de cada ciudadano, el IDRD no se hace responsable por la pérdida o daño de los mismos.",
            "* Contar con la afiliación a los servicios de salud. Adicionalmente, para el caso de actividades deportivas, recreativas, culturales o comerciales el beneficiario del permiso deberá velar porque cada uno de los integrantes de la actividad cuente con el servicio mencionado",
            "* Los préstamos de estos escenarios se podrán realizar con mínimo 24 horas de anticipación.",
        ]),
        "requirements_field_natural" => implode("\n", [
            "## Normas de uso en los parques con campo de grama sintética o natural:",
            "",
            "* El uso de las canchas de fútbol debe ser de máximo tres (3) días entre semana. Los sábados y domingos los préstamos deben ser máximo para cuatro (4) partidos, la cancha se debe dejar descansar por doce (12) días cada cuarenta y cinco (45) días para garantizar en el tiempo el buen estado y la recuperación en las mismas.",
            "* Cuando llueva el campo de grama natural no se debe prestar hasta tanto el campo esté seco.",
            "* Ingresar siempre a la cancha por las puertas de acceso habilitadas por la administración.",
            "* El ingreso a la cancha de los jugadores debe realizarse de acuerdo con las dimensiones de la misma Futbol 5, Futbol 8 o Futbol 11, esto para no subutilizar la grama y gastar el terreno.",
            "* Al escenario deberán ingresar únicamente las personas que harán parte de la actividad deportiva y/o recreativa, no está autorizado el ingreso de acompañantes para evitar accidentes o situaciones que afecten la percepción de seguridad.",
            "* No se debe jugar mientras este lloviendo o con tormentas eléctricas o cuando la cancha se encuentre demasiado húmeda, esto para evitar accidentes.",
            "* No es permitido ingreso de bicicletas.",
            "* No consumir alimentos dentro del escenario.",
            "* Los usuarios deben hacer uso adecuado del espacio, Según las normas de convivencia básicas como: no consumir licor o cigarrillos, no ingresar mascotas y no ingresar ningún tipo de envases o recipientes de vidrio que causen accidentes, de la misma manera no arrojar basura ni dentro ni fuera de la cancha. No es permitido el ingreso de animales de compañía.",
            "* Para utilizar este espacio es obligatorio el uso de guayos, canilleras o espinilleras y ropa deportiva adecuada.",
            "* No mover los arcos para evitar daños en la grama y tampoco subirse en ellos para evitar accidentes, los mismos deben permanecer fijos con una guaya.",
            "* El espacio debe ser entregado en los tiempos acordados, es pilar fundamental el respeto para fortalecer las normas sociales establecidas en cada uno de los campos sintéticos.",
            "* No se deben dañar, saltar, empujar o cargar el cerramiento perimetral.",
            "* Los préstamos de canchas no se pueden realizar en fracciones de hora. Por este motivo es necesario ingresar la hora inicio y final sin minutos.",
            "* No se debe permitir que se instalen ningún tipo de elementos que generen daño en las mismas, equipos pesados, tarimas, elementos de anclajes, carpas o asadores.",
            "* Abecé de las canchas sintéticas [ver aquí](https://www.idrd.gov.co/sites/default/files/documentos/parques/abc-de-las-canchas-sinteticas.pdf).",
            "",
            "## Recomendaciones de uso en los campos de grama sintética o natural: ", //obligaciones
            "",
            "* El espacio público debe ser utilizado únicamente para el desarrollo de la actividad autorizada en las fechas y horarios o período asignado.",
            "* No se realizará devolución de dinero por inasistencia a la reserva solicitada. Debe tener en cuenta que las reprogramaciones se darán únicamente si el no uso se da por alguna de las siguientes situaciones: lluvias con tormenta eléctrica, programaciones institucionales, mantenimientos y/o novedades de orden público.",
            "* El beneficiario de la reserva deberá estar presente durante el desarrollo de la actividad y será el responsable del escenario.",
            "* El campo sintético puede ser usado para cualquier actividad recreativa y deportiva, siempre y cuando la misma no afecte la grama, ni la infraestructura del escenario",
            "* Realizar el pago de la liquidación por anticipado y confirmar con la Administración del Parque o Escenario, el recibido de la consignación o comprobante que acredita el pago de la obligación económica definida por el IDRD en la reserva.",
            "* Recibir el escenario y/o espacio asignado, durante cada fecha (según corresponda), bajo inventario y entregar en las mismas condiciones en que lo recibió, como en orden y aseo.",
            "* Permitir al funcionario del Instituto, realizar las inspecciones pertinentes y relacionadas con el cumplimiento de las condiciones del permiso.",
            "* Responder por los daños causados a las instalaciones, elementos, implementos y/o estructuras que integran el Parque o Escenario. En tal caso, la reparación, reposición y/o valor equivalente deberá ser asumido por el beneficiario del permiso - sin perjuicio de su facultad de exigir a los responsables, la reparación o indemnización de los daños causados, cuando a ello haya lugar, en un término máximo 15 días hábiles contados a partir de la fecha del acta de recibo.",
            "* Respetar la autonomía del IDRD, como entidad administradora del espacio público objeto de los Parques o Escenarios, sobre las distintas instalaciones y espacios a utilizar.",
            "* El usuario podrá reservar máximo 2 horas continuas por día y un total de máximo 4 horas a la semana en el sistema distrital de parques.",
            "* Implementar las medidas de seguridad que sean necesarias y que garanticen la preservación del espacio asignado y la integridad física de las personas que sean usuarias del servicio y/o que participen o asistan bajo cualquier calidad a la actividad objeto de esta habilitación, respondiendo por los perjuicios materiales y morales que se ocasionen en desarrollo de las diferentes actividades objeto de la presente autorización del uso del espacio público.",
            "* Acatar las medidas que sugiera y disponga la administración del parque o escenario, según sea el caso en materia de uso, distribución del espacio, carga – tráfico o número de participantes de las prácticas deportivas o actividades objeto del permiso, factores climáticos que afecten el desarrollo de estas o generen riesgo.",
            "* El cuidado de los elementos personales es competencia de cada ciudadano, el IDRD no se hace responsable por la pérdida o daño de los mismos.",
            "* Contar con la afiliación a los servicios de salud. Adicionalmente, para el caso de actividades deportivas, recreativas, culturales o comerciales el beneficiario del permiso deberá velar porque cada uno de los integrantes de la actividad cuente con el servicio mencionado",
            "* Los préstamos de estos escenarios se podrán realizar con mínimo 24 horas de anticipación.",
        ]),
        "requirements_field_tennis" => implode("\n", [
            "## Obligaciones de uso de las canchas de tenis:",
            "",
            "* Recibir el escenario y/o espacio asignado, durante cada fecha (según corresponda), bajo inventario y entregar en las mismas condiciones en que lo recibió, como en orden y aseo.",
            "* Permitir al funcionario del Instituto, realizar las inspecciones pertinentes y relacionadas con el cumplimiento de las condiciones del permiso.",
            "* El préstamo se autoriza para práctica libre, por lo que el uso del espacio debe ser acorde con su destinación, sin implicar la obtención de beneficios económicos para el usuario practicante, por lo que no está autorizado el uso de implementos para formación deportiva, tales como canastas para pelotas de tenis, platillos, y/o conos.",
            "* El espacio público debe ser utilizado únicamente para el desarrollo de la actividad autorizada en las fechas y horarios o período asignado. El IDRD, sólo repondrá aquellas fechas que sean suspendidas o aplazadas por razones o eventos institucionales, diferentes a los programas de mantenimiento propios del campo deportivo y/o espacio asignado. En ningún caso se modificará o adicionará la reserva, a manera de compensación, por fechas que sean suspendidas por motivos ajenos a la voluntad del IDRD o del Distrito.",
            "* Dar buen uso a las diferentes instalaciones del Parque o Escenario disponiendo de una persona exclusivamente para el control y el buen trato de los mismos.",
            "* Acatar las medidas que sugiera y disponga la Administración del Parque o Escenario, según sea el caso en materia de uso, distribución del espacio, carga – tráfico o número de participantes de las prácticas deportivas o actividades objeto del permiso, factores climáticos que afecten el desarrollo de las mismas o generen riesgo.",
            "* Implementar las medidas de seguridad que sean necesarias y que garanticen la preservación del espacio objeto del permiso y de las personas que sean usuarias del servicio y/o que participen o asistan bajo cualquier calidad a la actividad objeto del permiso, respondiendo por los perjuicios materiales y morales que se ocasionen en desarrollo de las diferentes actividades objeto del presente permiso.",
            "* Responder por los daños causados a las instalaciones, elementos, implementos y/o estructuras que integran el Parque o Escenario. En tal caso, la reparación, reposición y/o valor equivalente deberá ser asumido por el beneficiario del permiso - sin perjuicio de su facultad de exigir a los responsables, la reparación o indemnización de los daños causados, cuando a ello haya lugar, en un término máximo 15 días hábiles contados a partir de la fecha del acta de recibo.",
            "* Responder frente a terceros por las posibles novedades que se presenten.",
            "* Velar por el cumplimiento de la normatividad relativa al manejo de los Parques o Escenarios del Sistema Distrital de Parques, en especial el Decreto 825 del  27 de diciembre de 2019 o aquellos que lo modifiquen, los cuales limitan el expendio y consumo de bebidas alcohólicas en los Parques o Escenarios recreativos y deportivos del Distrito Capital.",
            "* Respetar la autonomía del IDRD, como entidad administradora del espacio público objeto de los Parques o Escenarios, sobre las distintas instalaciones y espacios a utilizar.",
            "* Se prohíbe el uso de altoparlantes o amplificadores de sonido que no se ajusten a la normatividad establecida por la Secretaría del Medio Ambiente. Respetar y acatar la normatividad vigente en materia de los niveles de sonido y ruido permitidos para actividades y/o eventos que se desarrollan en el espacio público. En todo caso se debe velar por la convivencia y bienestar de los habitantes de una zona.",
            "* Utilizar implementos y ropa acordes a las condiciones técnicas de cada disciplina deportiva, que garanticen la protección personal.",
            "* El espacio debe ser entregado en los tiempos acordados, es pilar fundamental el respeto para fortalecer las normas sociales establecidas en cada uno de los campos sintéticos.",
            "* No se realizará devolución de dinero por inasistencia a la reserva solicitada. Debe tener en cuenta que las reprogramaciones se darán únicamente si el no uso se da por alguna de las siguientes situaciones: lluvias con tormenta eléctrica, programaciones institucionales, mantenimientos y/o novedades de orden público.",
            "* El beneficiario de la reserva deberá estar presente durante el desarrollo de la actividad y será el responsable de la entrega del escenario.",
            "* El usuario podrá reservar máximo 2 horas continuas por día y un total de máximo 4 horas a la semana en el sistema distrital de parques.",
            "* Los préstamos de estos escenarios se podrán realizar con mínimo 24 horas de anticipación.",
            "",
            "## Recomendaciones para el buen uso de las canchas de tenis:",
            "",
            "* Para utilizar este espacio de forma adecuada es obligatorio el uso de zapatos de suela blanda, o tenis especiales para este tipo de deporte.",
            "* No se debe dañar, saltar, empujar o cargar el cerramiento perimetral o la baranda protectora de las graderías, utilice los pasillos del escenario.",
            "* Ingresar siempre a la cancha por las puertas de acceso habilitadas por la administración, cuando aplique.",
            "* Se debe respetar los horarios y los espacios que cada usuario haya reservado o tomado en préstamo.",
            "* Cada jugador deberá traer su raqueta de tenis y sus pelotas.",
            "* No se permitirá el ingreso a la cancha con ropa mojada.",
            "* No se debe jugar mientras esté lloviendo o con tormentas eléctricas o cuando la cancha se encuentre demasiado húmeda, esto para evitar accidentes.",
            "* Al escenario deberán ingresar únicamente las personas que harán parte de la actividad deportiva y/o recreativa, no está autorizado el ingreso de acompañantes para evitar accidentes o situaciones que afecten la percepción de seguridad.",
            "* No está permitido el ingreso de bicicletas.",
            "* No consumir alimentos dentro del escenario.",
            "* Los usuarios deben hacer uso adecuado del espacio, Según las normas de convivencia básicas como: no consumir licor o cigarrillos, no ingresar mascotas y no ingresar ningún tipo de envases o recipientes de vidrio que causen accidentes, de la misma manera no arrojar basura ni dentro ni fuera de la cancha. No es permitido el ingreso de animales de compañía.",
        ]),


        "requirements_grill" => implode("\n", [
            "## Aviso Informativo Asadores",
            "",
            "* No se realizará devolución de dinero por inasistencia a la reserva solicitada. Debe tener en cuenta que las reprogramaciones se darán únicamente si el no uso se da por alguna de las siguientes situaciones: lluvias con tormenta eléctrica, programaciones institucionales, mantenimientos y/o novedades de orden público.",
            "* No es posible pagar horas adicionales en el parque y menos en efectivo.",
            "* Los espacios son solo para 25 personas, si sobrepasa el aforo la reserva se tiene que hacer en la administración.",
            "* No se puede ingresar barriles para hacer asados ni mobiliarios que puedan afectar el parque o sin autorización de la administración.",
            "* Se puede reservar a partir de 3 horas en adelante.",
            "* Se prohíbe el consumo de bebidas alcohólicas.",
            "* La persona que reservó debe presentarse puntualmente a la hora de dicha reserva.",
            "* Se prohíbe el ingreso de inflables, saltarines, plantas eléctricas, pipetas de gas.",
            "* Al terminar debe dejar limpio y organizado como encontró el espacio.",
            "* Un ciudadano no podrá reservar el mismo espacio dos veces en la misma semana.",
        ]),

        "requirements_swimming_pool" => implode("\n", [
            "## REGLAMENTO PARA USO Y DISFRUTE DE LAS PISCINAS",
            "",
            "### 1. Condiciones Generales de Uso",
            "",
            "* El acceso a la piscina solo está permitido cuando haya personal salvavidas presente.",
            "* Todo usuario debe acatar las instrucciones y recomendaciones del personal salvavidas e instructores en todo momento.",
            "* El uso de las piscinas está dirigido a usuarios de todos los niveles, desde principiantes hasta avanzados.",
            "* Para acceder a la práctica libre en los CEFE, se debe realizar el registro a través del PORTAL CIUDADANO (https://portalciudadano.idrd.gov.co). Este acceso está disponible solo para mayores de 18 años.",
            "* Para los parques estructurantes, el acceso a la práctica libre se realiza directamente en los parques. Más información en: https://www.idrd.gov.co/parques/escenarios-idrd/piscinas-ubicadas-en-parquesdistritales.",
            "",
            "### 2. Normas de Higiene y Seguridad",
            "",
            "* Es obligatorio ingresar con traje de baño especializado para natación y gorro de natación. Además, se debe tomar una ducha con agua y jabón antes de ingresar a la piscina.",
            "* Utilizar calzado exclusivo para la piscina y áreas húmedas, diferente al calzado de calle, como chanclas diseñadas específicamente para este propósito.",
            "* Se debe retirar maquillaje, joyas y cualquier objeto que pueda representar un riesgo para la seguridad o funcionamiento del escenario.",
            "* No se permite el ingreso con alimentos, envases de vidrio u objetos peligrosos.",
            "* Se recomienda consumir alimentos como mínimo dos (2) horas antes del ingreso.",
            "* En caso de estar en periodo menstrual, se deben tomar medidas de higiene adecuadas y está prohibido el uso de protectores sanitarios o materiales contaminantes dentro de la piscina.",
            "* Se prohíbe el ingreso de personas en estado de embriaguez o bajo efectos de sustancias psicoactivas.",
            "* No se permitirá tomar fotografías ni realizar videos en las instalaciones en cumplimiento de la Ley 1098 de 2006 sobre la protección a menores y datos personales.",
            "* En caso de enfermedades infectocontagiosas, heridas abiertas, sangrado o condiciones de salud que puedan comprometer la seguridad, se recomienda abstenerse de hacer uso de la piscina.",
            "",
            "### 3. Normas de Seguridad y Comportamiento",
            "",
            "* No se permite correr en áreas húmedas ni realizar juegos bruscos, clavados o maniobras peligrosas.",
            "* Está prohibido sentarse, colgarse o recostarse sobre los separadores de carril.",
            "* No se permite el uso de aletas y remos y elementos que afecten la práctica en piscinas del IDRD.",
            "* El uso de carriles será compartido, con un máximo de doce (12) personas por carril.",
            "* En caso de emergencia los usuarios deben seguir las indicaciones del personal del IDRD para su respectiva evacuación.",
            "",
            "### 4. Restricciones y Consideraciones Especiales",
            "",
            "* No se repondrán sesiones por inasistencia, salvo en casos de incapacidad médica, calamidad debidamente certificada o periodo menstrual.",
            "* Se recomienda no llevar objetos de valor, el IDRD ni la administración del escenario se hacen responsables por pérdidas en casilleros, vestuarios o áreas comunes.",
            "* Se debe tener un comportamiento tolerante, respetuoso y acorde entre usuarios y con el personal adscrito al CEFE. El IDRD podrá tomar medidas en caso de incumplimiento.",
            "",
        ]),

      "requirements_gym" => implode("\n", [
    "## REQUISITOS GIMNASIOS",
    "PARA LA PRÁCTICA LIBRE EN GIMNASIOS El presente documento establece los Términos y Condiciones que rigen el uso y disfrute de los escenarios deportivos, gimnasios, multifuerzas, entre otros administrados por el Instituto Distrital de Recreación y Deporte (IDRD) - Centros de Felicidad- CEFE, en el marco de las disposiciones normativas vigentes en la ciudad de Bogotá D.C. Estas reglas están diseñadas para garantizar el adecuado funcionamiento, la seguridad y el bienestar de los usuarios, alineándose con las políticas públicas locales y las disposiciones legales establecidas para la administración de espacios recreativos y deportivos. Es responsabilidad de cada usuario leer, comprender y cumplir estos términos antes de hacer uso de las instalaciones.",
    "",
    "1. PRESENTACIÓN PERSONAL Y USO ADECUADO DE LOS GIMNASIOS",
    "Los ejercicios se deben practicar con ropa deportiva, zapatos deportivos o el atuendo adecuado a la actividad que se esté realizando. Cada USUARIO deberá traer para su comodidad una toalla y elementos de autocuidado, cuyo uso es obligatorio durante y después de cada ejercicio. Está prohibido quitarse la ropa y/o circular por las instalaciones sin camiseta. El único sitio dispuesto para que los usuarios se cambien de ropa es el vestier y el baño. Se debe tener un comportamiento tolerante, respetuoso y acorde entre usuarios y con el personal adscrito al CEFE. El IDRD tomará medidas temporales o definitivas en caso de incumplimiento. Una vez concluida la práctica de cualquier ejercicio o rutina, se deberán dejar los correspondientes implementos (mancuernas, discos, pesas, barras, colchonetas, etc.) en su lugar. Se deberá limpiar las máquinas e implementos utilizados con los paños de papel y líquidos desinfectantes dispuestos para este fin. Está prohibido el consumo de cigarrillos, bebidas alcohólicas, esteroides y/o drogas nocivas para la salud. El incumplimiento de estas normas podrá generar la suspensión temporal o definitiva del acceso a las instalaciones.",
    "Así mismo, certifico que me encuentro afiliado y activo a una entidad prestadora de salud del Sistema de Seguridad Integral en Salud en el régimen contributivo o subsidiado y me encuentro en condiciones médicas, psicológicas, sociales y destrezas para adelantar la práctica libre. De esta manera, exonero de toda responsabilidad al personal del IDRD ante cualquier eventualidad que pueda presentar en la práctica.",
    "",
    "2. LOCKERS",
    "Se hace claridad que el IDRD, NO presta el servicio de custodia de bienes por tener a disposición de los usuarios unos LOCKERS. Estos espacios son simplemente una prerrogativa que se otorga y, por ende, esta prerrogativa NO hace parte de la Prestación de Servicios. El usuario deberá colocar un candado de su propiedad que sea fuerte y seguro. NO se recomienda guardar en los lockers artículos de valor. El IDRD no asume ninguna responsabilidad por los bienes dejados en los lockers con o sin candado.",
    "",
    "3. UTILIZACIÓN DE INSTALACIONES",
    "Los USUARIOS no están autorizados para vender bienes o servicios dentro de las instalaciones, prestar servicios de entrenamiento personalizado, mover o modificar los equipos. Si un equipo presenta fallas o daños, el USUARIO debe reportarlo de inmediato al personal encargado. El USUARIO asume total responsabilidad por las lesiones físicas o accidentes que ocurran durante el uso de las instalaciones.",
    "",
    "4. PROHIBICIONES",
    "- ❌ Vender o comercializar productos o servicios.",
    "- ❌ Utilizar lenguaje ofensivo o agresivo contra otros usuarios o personal del IDRD.",
    "- ❌ Manipular equipos de manera inapropiada o sin autorización.",
    "- ❌ Incumplir las normas puede resultar en la terminación unilateral de la práctica.",
    "- ❌ Reportar cualquier situación sospechosa o irregular al personal autorizado mediante los canales oficiales dispuestos para este fin.",
    "",
    "5. PARQUEADEROS",
    "El IDRD no incluye el servicio de parqueadero en los servicios contratados. Cualquier convenio con terceros para el uso de parqueaderos será responsabilidad exclusiva de los administradores de dichos parqueaderos. El usuario debe conocer y aceptar las políticas establecidas por el administrador del parqueadero.",
    "",
    "6. COMIDAS Y BEBIDAS",
    "No se permite el ingreso con alimentos, envases de vidrio u objetos peligrosos.",
    "",
    "7. GRUPO DE EDADES",
    " Mayores de 18 años: Podrán acceder y hacer uso de las instalaciones bajo su entera responsabilidad para la práctica libre.",
    "",
    "8. El IDRD no se hace responsable por los acuerdos realizados entre usuarios y terceros independientes dentro de las instalaciones.",
    "",
    "",
    "9. Exonero expresamente al Instituto Distrital de Recreación y Deporte- IDRD de las circunstancias que pudiesen ser constituidas con ocasión de las actividades que conlleven el desarrollo de la práctica libre en gimnasios, manifiesto que cuento con las habilidades y destrezas que ello implica, estado físico y mental en general de salud es apto para práctica libre en gimnasios. Por lo expuesto, asumo la responsabilidad exclusiva y personal respecto a los riesgos que la práctica de esta actividad implica, y por lo tanto renuncio expresamente a efectuar reclamaciones al IDRD o Bogotá DC.",
    "",
    "10. NORMAS ADICIONALES",
    "El IDRD se reserva el derecho de modificar estas normas en cualquier momento, las cuales se actualizarán en los diferentes canales oficiales. El incumplimiento de estas normas podrá dar lugar a la cancelación de acceso a los diferentes escenarios.",
    "",
    "He sido informado acerca de los protocolos que debe seguir para entrenar en las sedes INSTITUTO DISTRITAL DE RECREACIÓN Y DEPORTE – IDRD. CEFES CENTROS DE FELICIDAD.",
    "",
    "Reconozco y acepto que INSTITUTO DISTRITAL DE RECREACIÓN Y DEPORTE – IDRD ha tomado todas las medidas necesarias y exigidas para la prevención del accidente.",
    "",
    "Soy consciente de que durante la práctica libre en las sedes del INSTITUTO DISTRITAL DE RECREACIÓN Y DEPORTE – IDRD. CEFES CENTROS DE FELICIDAD, pueden surgir circunstancias inesperadas, imprevistos, inconvenientes propios o asociados a las actividades, incluyendo, pero no limitando a lesiones, caídas, cortadas y otros accidentes e incidentes, enfermedades generales, específicas y de tipo contagioso. Renuncio expresamente a efectuar cualquier tipo de reclamación de toda naturaleza al INSTITUTO DISTRITAL DE RECREACIÓN Y DEPORTE – IDRD. CEFES CENTROS DE FELICIDAD.",
    "",
    "Asumo todos los riesgos y contingencias asociados a estas circunstancias o a eventos de fuerza mayor o caso fortuito, tales como desastres naturales, alteraciones del orden público, asonadas, actos de terrorismo, vandalismo, sedición o revueltas, acciones delincuenciales, y hurto. Desisto de presentar reclamaciones judiciales o extrajudiciales por estos motivos, conforme a lo establecido en las normas vigentes aplicables."
])





    ]
];
