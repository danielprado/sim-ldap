Sea usted bienvenido al portal ciudadano del Instituto Distrital de Recreación y Deporte, en adelante el IDRD, el mismo se encuentra dispuesto con el fin de brindar acceso a los servicios de la entidad de forma concentrada y amigable para la ciudadanía. El presente documento de términos y condiciones de servicios regula el uso del portal Web https://portalciudadano.idrd.gov.co/ que el IDRD da a conocer a todos sus usuarios registrados y ciudadanía en general.

El registro y acceso a los servicios aquí dispuestos implica la aceptación plena de lo dispuesto en el presente documento de términos y condiciones de servicio, por lo que, si el interesado no está de acuerdo, no deberá realizar el registro en el portal ciudadano.

El USUARIO autoriza al Instituto Distrital de Recreación y Deporte – IDRD, así como cualquier dependencia aliada o filial del Instituto, a realizar el tratamiento sobre los datos personales, conforme a la ley 1581 de 2012, con la finalidad de ser utilizados para realizar registros de participación en los diferentes eventos, convocatorias y estadísticas. El IDRD se reserva el derecho de verificación de los datos e inhabilitación del usuario en cualquier momento.

El USUARIO autoriza al IDRD para hacer modificaciones en el formulario de inscripción en el evento que se detecten errores de digitación en el número de identificación. La verificación de datos se realizará dentro de los cinco días hábiles siguientes al registro, una vez realizada podrá consultar los servicios ofrecidos por el IDRD y hacer uso de ellos.

En caso de realizar el registro para menores de edad, solo podrá realizarse por su padre, madre o tutor legal, adjuntando el registro civil de nacimiento o el documento administrativo o judicial que lo acredite como tutor legal. En el caso de personas declaradas en interdicción, se deberá aportar el documento que acredite dicha condición.

El IDRD se reserva el derecho de modificar en cualquier momento los términos y condiciones, así como, lineamientos, reglamentos de uso, instrucciones o avisos. Las modificaciones serán publicadas por el IDRD, siendo obligación del USUARIO revisar las actualizaciones que se realicen y acatar las instrucciones respectivas, so pena de cancelación de su registro.

El Usuario se obliga a hacer un uso correcto de los servicios del IDRD de conformidad con las leyes, la buena fe, el orden público, lineamientos, reglamentos y términos y condiciones.

El Usuario responderá frente al IDRD y frente a terceros, por los daños y perjuicios que pudieran causarse como consecuencia del incumplimiento de dicha obligación.

Los servicios del portal ciudadano son de acceso restringido conforme a la capacidad establecida para los escenarios y pueden ser gratuitos u onerosos conforme a la modalidad. Este portal tiene como finalidad consolidar los servicios del IDRD para brindar al USUARIO todo tipo de información relacionada con los mismos.

El contenido del Portal Ciudadano es de carácter general y tiene una finalidad meramente informativa. El IDRD, no será responsable por los daños y perjuicios que pudieran ocasionarse mediante el uso indebido de la información.

<br />

### DEBERES DEL USUARIO:

<br />

<ol type="a">
    <li>Realizar el registro en el portal ciudadano de forma completa, con información veraz, dando lectura adecuada a las orientaciones brindadas por el Instituto para su uso.</li>	
    <li>Cargar en el portal ciudadano los documentos de forma legible solicitados en la calidad y tamaño requeridos para cada caso que soportan su registro.</li>	
    <li>Hacer uso adecuado de los contenidos y servicios ofrecidos.</li>	
    <li>No suplantar la identidad de otro Usuario, o de un tercero;</li>	
    <li>Diligenciar en forma adecuada, oportuna y veraz los formularios dispuestos por el IDRD para la inscripción en los servicios.</li>	
    <li>En los servicios que se requiera acreditar afiliación activa al sistema de seguridad social en salud en el régimen contributivo o subsidiado, con una antelación mínima a 30 días, deberá hacerlo con el documento de certificación expedido por la entidad correspondiente.</li>	
    <li>Conocer y respetar los TÉRMINOS Y CONDICIONES DE SERVICIOS expuestos en este documento, los reglamentos establecidos para las actividades y servicios específicos y seguir las indicaciones de los funcionarios y colaboradores del IDRD.</li>	
    <li>Realizar el pago a través del portal ciudadano o en el botón de pagos PSE en la forma y valor establecido por el IDRD para cada servicio, identificando plenamente el escenario, horario, modalidad toda vez que cualquier error en la escogencia de dichos factores por parte del USUARIO, no generará devolución del dinero.</li>	
    <li>EL USUARIO se obliga a preservar todos los elementos puestos a su disposición por el IDRD para realizar la actividad o uso del servicio, so pena de pagar el valor equivalente a su reposición y/o arreglo.</li>	
    <li>EL USUARIO deberá presentar las Peticiones, Quejas y Reclamos según los parámetros establecidos en la ley, mediante el link para PQRS dispuesto en la página web www.idrd.gov.co.</li>	
</ol>

<br />

### DERECHOS Y DEBERES DEL IDRD:

<br />

<ol type="a">
    <li>Permitir el acceso del USUARIO a los escenarios donde se preste el servicio seleccionado y pagado (si es el caso) por el mismo.</li>  
    <li>Mantener en forma adecuada las instalaciones.</li>
    <li>Establecer los escenarios y horarios para cada uno de los servicios en el portal ciudadano para ser de conocimiento del USUARIO antes de efectuar el registro y la inscripción a los mismos.</li>
    <li>Ampliar, reducir, remodelar o cerrar, temporal o definitivamente los escenarios de manera preventiva o correctiva informando a los USUARIOS para definir su reubicación o reprogramación.</li>
    <li>Dar por terminada la prestación del servicio en cualquier tiempo de manera unilateral y sin la devolución de sumas de dinero, por el incumplimiento del USUARIO de los términos y condiciones aquí dispuestos o de los lineamientos y/o reglamentos de cada servicio. La terminación no generará indemnización alguna a favor del USUARIO.</li>
</ol>

<br />

### DEVOLUCIONES:

<br />

El usuario no podrá reclamar la devolución del pago. Sólo se devolverá el dinero en caso de cancelación de la actividad por parte del organizador de la misma. Por tanto, es responsabilidad del usuario la elección del servicio requerido y lectura de las condiciones del mismo previo al pago.

Al aceptar los términos y condiciones El USUARIO acepta las normas y criterios que descritos para cada uno de los servicios.

El USUARIO manifiesta hacer uso de los servicios del IDRD de manera voluntaria y por tanto acepta las condiciones descritas, las cuales hacen parte integral de estos términos y condiciones específicas de este tipo de servicios.

El USUARIO desiste expresamente de reclamar indemnización por los daños o lesiones ocasionadas en el marco de la prestación de estos servicios, eximiendo al IDRD de toda responsabilidad por hechos de esta índole.

El USUARIO autoriza al Instituto Distrital de Recreación y Deporte – IDRD a iniciar las acciones que consideren pertinentes, en caso de que por su participación genere un hecho que pueda afectar los intereses de la entidad. Igualmente, autoriza al IDRD para que haga uso de las fotografías, películas, videos, grabaciones y cualquier otro medio de registro de este programa para su uso legítimo y legal sin reclamación o compensación económica, material, inmaterial o financiera alguna en el desarrollo de las actividades recreativas, tanto en formato papel (impreso) como digital, teniendo en cuenta la normatividad vigente colombiana aplicable para tales fines, en especial aquella que proteja los derechos a la intimidad, buen nombre y protección de datos de carácter personal.
