@extends('errors.illustrated-layout')

@section('title', trans('validation.handler.service_unavailable'))
@section('code', '503')
@section('message', trans('validation.handler.service_unavailable'))
@section('image')
    <div style="background-image: url({{ asset('svg/503.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center"></div>
@endsection
