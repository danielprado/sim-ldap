@extends('errors.illustrated-layout')

@section('title', trans('validation.handler.unauthenticated'))
@section('code', '401')
@section('message', trans('validation.handler.unauthenticated'))
@section('image')
    <div style="background-image: url({{ asset('svg/404.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center"></div>
@endsection
