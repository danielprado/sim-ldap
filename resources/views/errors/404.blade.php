@extends('errors.illustrated-layout')

@section('title', trans('validation.handler.resource_not_found_url'))
@section('code', '404')
@section('message', trans('validation.handler.resource_not_found_url'))
@section('image')
    <div style="background-image: url({{ asset('svg/404.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center"></div>
@endsection
