@extends('errors.illustrated-layout')

@section('title', trans('validation.handler.unauthorized'))
@section('code', '403')
@section('message', trans('validation.handler.unauthorized'))
@section('image')
    <div style="background-image: url({{ asset('svg/403.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center"></div>
@endsection
