<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    @foreach ($links as $link)
        <url>
            <loc>{{ $link['loc'] }}</loc>
            <lastmod>{{ $link['lastmod'] }}</lastmod>
            <changefreq>{{ $link['changefreq'] }}</changefreq>
            <priority>{{ $link['priority'] }}</priority>
        </url>
    @endforeach
</urlset>
