<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if IE]><link rel="icon" href="{{ asset('favicon.ico') }}"><![endif]-->
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('icons/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('icons/favicon-16x16.png') }}">
        <meta name="theme-color" content="#ffffff">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="{{ config('app.name') }}">
        <link rel="apple-touch-icon" href="{{ asset('icons/apple-touch-icon-152x152.png') }}">
        <link rel="mask-icon" href="{{ asset('icons/safari-pinned-tab.svg') }}" color="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('icons/msapplication-icon-144x144.png') }}">
        <meta name="msapplication-TileColor" content="#594d95">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 60px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Verificación de Correo
                </div>
                <div class="flex-center">
                    <lottie-player
                        src="https://assets5.lottiefiles.com/packages/lf20_zd2j6msi.json"
                        class="flex-center"
                        background="transparent"
                        speed="1"
                        style="width: 200px; height: 200px;"
                        loop
                        autoplay
                    >
                    </lottie-player>
                </div>
                <div class="links">
                    <a href="http://localhost:8080/buscador-de-parques/iniciar-sesion">Iniciar Sesión</a>
                </div>
            </div>
        </div>
    </body>
</html>
